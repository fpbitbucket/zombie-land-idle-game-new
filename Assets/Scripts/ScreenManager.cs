﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using com.ootii.Messages;
using DG.Tweening;
public class ScreenManager : MonoBehaviour {
	public ScrollRect genSroll;
	public GameObject FillBg;
	public GameObject[] GameScreen;
	public GameObject[] buttons;
//	public Sprite[] switchImage;
//	private Sprite[] currentImage;
	public int currentActiveScreen = 99;
	public GameObject[] menucirclePort;
	public GameObject[] btnIcon;

	public GameObject testPopup;

	void Start () {
//		currentImage = new Sprite[buttons.Length];
		OnCloseScreen ();
		testPopup.SetActive (false);
//		for (int i = 0; i < GameScreen.Length; i++) {
//			currentImage[i] = buttons[i].GetComponent<Image>().sprite;
//		}

		for (int i = 0; i < menucirclePort.Length; i++) {
			menucirclePort [i].SetActive (false);
		}

		currentActiveScreen = 0;
		OnActiveStopScreen (0);
		currentActiveScreen = 99;
	}

	void OnEnable(){

		MessageDispatcher.AddListener ("OnRestMenuTriggers", OnRestMenuTriggers, true);
		MessageDispatcher.AddListener ("OnTriggerMenuNotifyOn", OnTriggerMenuNotifyOn, true);
		MessageDispatcher.AddListener ("OnTriggerMenuNotifyOff", OnTriggerMenuNotifyOff, true);
	}

	void OnDisable(){
		MessageDispatcher.RemoveListener ("OnRestMenuTriggers", OnRestMenuTriggers, true);
		MessageDispatcher.RemoveListener ("OnTriggerMenuNotifyOn", OnTriggerMenuNotifyOn, true);
		MessageDispatcher.RemoveListener ("OnTriggerMenuNotifyOff", OnTriggerMenuNotifyOff, true);
	}


	int numUpG = 0;

	void OnTriggerMenuNotifyOn (IMessage rMessage){
		int tempData = (int)rMessage.Data;
//		menuNotify = true;

		if (11 == tempData || 12 == tempData || 13 == tempData) {
			numUpG = tempData;
			tempData = 0;
		}

		//		menuMainCircle.SetActive (true);
		menucirclePort [tempData].SetActive (true);
		btnIcon[tempData].transform.DORotate (new Vector3 (0, 0, -10), 0);
		btnIcon[tempData].transform.DORotate (new Vector3 (0, 0, 10), 0.2f).SetEase (Ease.Linear).SetLoops (9999, LoopType.Yoyo);
	}

	bool bolUpg = false;

	void OnTriggerMenuNotifyOff(IMessage rMessage){
		int tempData = (int)rMessage.Data;
		if (11 == tempData || 12 == tempData || 13 == tempData) {
			int tempInt = tempData;
			if (numUpG == tempData) {
				bolUpg = true;
				tempData = 0;
			}else {
				bolUpg = false;
			}
			if (bolUpg == true) {
				menucirclePort [0].SetActive (false);
				btnIcon [0].transform.DOKill ();
				btnIcon[0].transform.DORotate (new Vector3 (0, 0, 0), 0);
			} 
		} else {
			menucirclePort [tempData].SetActive (false);
			btnIcon [tempData].transform.DOKill ();
			btnIcon[tempData].transform.DORotate (new Vector3 (0, 0, 0), 0);
		}

	}

	void OnRestMenuTriggers(IMessage rMessage){
		for (int i = 0; i < menucirclePort.Length; i++) {
			menucirclePort [i].SetActive (false);
		}
	}

	public void OnShowTestPopup(){
		testPopup.SetActive (true);
	}

	public void OnCloseTestPopup(){
		testPopup.SetActive (false);
	}

	public void OnShowScreen (int NumScreen) {
		MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
		OnCloseScreen ();
		FillBg.SetActive (true);
		GameScreen [NumScreen].SetActive (true);
		string ActionString = "";

		if (currentActiveScreen == NumScreen) {
			currentActiveScreen = 99;
			OnCloseScreen ();

			ActionString = "Close";
//			buttons[NumScreen].GetComponent<Image>().sprite = currentImage[NumScreen]; 
			buttons [NumScreen].GetComponent<Image> ().color = Color.white;

			if (NumScreen == 0) { //Cash Upgrade
			
				if (GameObject.Find ("Managers/TutorialManager").GetComponent<TutorialManager> ().tutorialStats [12]) {
					MessageDispatcher.SendMessage (this, "OnCloseTutorial", 12, 0);
					MessageDispatcher.SendMessage (this, "OnCloseTutorial", 13, 0);	
				} 

			}

		} else {
			ActionString = "Open";
			OnDefaultAllButton ();
			currentActiveScreen = NumScreen;
//			buttons[NumScreen].GetComponent<Image>().sprite = switchImage[NumScreen];
			buttons [NumScreen].GetComponent<Image> ().color = Color.gray;
			btnIcon [NumScreen].transform.DOKill ();
			btnIcon[NumScreen].transform.DORotate (new Vector3 (0, 0, 0), 0);

			if (NumScreen == 0) { //Cash Upgrade
				MessageDispatcher.SendMessage (this, "OnTriggerMenuNotifyOff", 0, 0);
//				OnActiveStopScreen (0);
				if (GameObject.Find ("Upgrades/CashUpgradeManager").GetComponent<UpGradeScript> ().hasAvailableToBuy) {
					MessageDispatcher.SendMessage (this, "OnCloseTutorial", 12, 0);
				}
				if (GameObject.Find ("Managers/TutorialManager").GetComponent<TutorialManager> ().tutorialStats.Length > 12) {
					if (GameObject.Find ("Managers/TutorialManager").GetComponent<TutorialManager> ().tutorialStats [12] && GameObject.Find ("Upgrades/CashUpgradeManager").GetComponent<UpGradeScript> ().hasAvailableToBuy) {
						MessageDispatcher.SendMessage (this, "OnShowTutorial", 13, 0);		
						OnActiveStopScreen (0);
					} else {
						MessageDispatcher.SendMessage (this, "OnCloseTutorial", 12, 0);
						MessageDispatcher.SendMessage (this, "OnCloseTutorial", 13, 0);
					}
				}
			}


			if (NumScreen == 1) { //manager
				Debug.Log("Off");
				MessageDispatcher.SendMessage (this, "OnTriggerMenuNotifyOff", 1, 0);
				MessageDispatcher.SendMessage(this, "OnCloseTutorial",3, 0);
//				btnIcon [NumScreen].transform.DOKill ();
//				btnIcon[NumScreen].transform.DORotate (new Vector3 (0, 0, 0), 0);
			}

			if (NumScreen == 2) { //Investor
				MessageDispatcher.SendMessage (this, "OnTriggerMenuNotifyOff", 2, 0);
				MessageDispatcher.SendMessage(this, "OnShowTutorial",5, 0);
//				btnIcon [NumScreen].transform.DOKill ();
//				btnIcon[NumScreen].transform.DORotate (new Vector3 (0, 0, 0), 0);
			}

			if (NumScreen == 3) { //gold upgrade
				MessageDispatcher.SendMessage (this, "OnTriggerMenuNotifyOff", 3, 0);
//				btnIcon [NumScreen].transform.DOKill ();
//				btnIcon[NumScreen].transform.DORotate (new Vector3 (0, 0, 0), 0);
			}

			if (NumScreen == 4) { //Shop
				MessageDispatcher.SendMessage (this, "OnTriggerMenuNotifyOff", 4, 0);

//				btnIcon [NumScreen].transform.DOKill ();
//				btnIcon[NumScreen].transform.DORotate (new Vector3 (0, 0, 0), 0);

			}

			if (NumScreen == 5) { //Stats
				MessageDispatcher.SendMessage (this, "OnTriggerMenuNotifyOff", 5, 0);
//				btnIcon [NumScreen].transform.DOKill ();
//				btnIcon[NumScreen].transform.DORotate (new Vector3 (0, 0, 0), 0);
			}

		}


		// if (GameObject.Find ("Exploration")) {
		// 	GameObject.Find ("Exploration").GetComponent<ExplorationTutorial> ().ToturialCheck ();
		// }

		if (NumScreen == 0) {
			MessageDispatcher.SendMessage(this, "OnTapGA_Report","CashUpgrade"+ActionString, 0);
		}
		if (NumScreen == 1) { //manager
			MessageDispatcher.SendMessage(this, "OnTapGA_Report","Manager"+ActionString, 0);
		}

		if (NumScreen == 2) { //Investor
			MessageDispatcher.SendMessage(this, "OnTapGA_Report","ZombieIdol"+ActionString, 0);

		}

		if (NumScreen == 3) { //Gold Upgrade
			MessageDispatcher.SendMessage(this, "OnTapGA_Report","GoldUpgrade"+ActionString, 0);

		}

		if (NumScreen == 4) { //Shop
			MessageDispatcher.SendMessage(this, "OnTapGA_Report","Shop"+ActionString, 0);

		}

		if (NumScreen == 5) { //Stats
			MessageDispatcher.SendMessage(this, "OnTapGA_Report","Stats"+ActionString, 0);
		}

	}


	public void OnActiveStopScreen(int NumScreen){
		MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
		if (NumScreen == 0) {
			GameScreen [currentActiveScreen].transform.GetChild (1).gameObject.SetActive (true);
			GameScreen [currentActiveScreen].transform.GetChild (2).gameObject.SetActive (false);

			GameScreen [currentActiveScreen].transform.GetChild (3).gameObject.GetComponent<Image>().color = Color.gray;
			GameScreen [currentActiveScreen].transform.GetChild (4).gameObject.GetComponent<Image>().color = Color.white;
			MessageDispatcher.SendMessage(this, "OnTapGA_Report","PillUpgradeShopOpen", 0);
//			GameScreen [currentActiveScreen].transform.GetChild (5).gameObject.SetActive (true);
//			GameScreen [currentActiveScreen].transform.GetChild (6).gameObject.SetActive (false);
		}else if (NumScreen == 1) {
			MessageDispatcher.SendMessage(this, "OnTapGA_Report","IdolsUpgradeShopOpen", 0);
			GameScreen [currentActiveScreen].transform.GetChild (1).gameObject.SetActive (false);
			GameScreen [currentActiveScreen].transform.GetChild (2).gameObject.SetActive (true);

			GameScreen [currentActiveScreen].transform.GetChild (3).gameObject.GetComponent<Image>().color = Color.white;
			GameScreen [currentActiveScreen].transform.GetChild (4).gameObject.GetComponent<Image>().color = Color.gray;

//			GameScreen [currentActiveScreen].transform.GetChild (5).gameObject.SetActive (false);
//			GameScreen [currentActiveScreen].transform.GetChild (6).gameObject.SetActive (true);
		}
	}

	public void OnNormalizeScrollGenerator(){
		genSroll.verticalNormalizedPosition = 1;
	}

	public void OnCloseScreen (){
		FillBg.SetActive (false);
		for (int i = 0; i < GameScreen.Length; i++) {
			GameScreen [i].SetActive (false);
		}
		OnDefaultAllButton ();
	}

	void OnDefaultAllButton(){
		for (int i = 0; i < buttons.Length; i++) {
			buttons [i].GetComponent<Image> ().color = Color.white;//sprite = currentImage [i];
		}
	}

}
