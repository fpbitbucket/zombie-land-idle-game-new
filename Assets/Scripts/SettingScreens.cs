﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingScreens : MonoBehaviour {
	public GameObject StatsOptionScreen;
	// Use this for initialization
	void Start () {
		

		SwitchScreen (0);
	}
	

	public void SwitchScreen(int num){
		if (num == 0) {
			StatsOptionScreen.transform.GetChild (3).gameObject.SetActive (false);
			StatsOptionScreen.transform.GetChild (2).gameObject.SetActive (true);

			StatsOptionScreen.transform.GetChild (4).gameObject.GetComponent<Button> ().enabled = false;
			StatsOptionScreen.transform.GetChild (5).gameObject.GetComponent<Button> ().enabled = true;

			StatsOptionScreen.transform.GetChild (4).gameObject.GetComponent<Image> ().color = Color.gray;
			StatsOptionScreen.transform.GetChild (5).gameObject.GetComponent<Image> ().color = Color.white;

		} else {
			StatsOptionScreen.transform.GetChild (3).gameObject.SetActive (true);
			StatsOptionScreen.transform.GetChild (2).gameObject.SetActive (false);

			StatsOptionScreen.transform.GetChild (4).gameObject.GetComponent<Button> ().enabled = true;
			StatsOptionScreen.transform.GetChild (5).gameObject.GetComponent<Button> ().enabled = false;

			StatsOptionScreen.transform.GetChild (4).gameObject.GetComponent<Image> ().color = Color.white;
			StatsOptionScreen.transform.GetChild (5).gameObject.GetComponent<Image> ().color = Color.gray;

		}
	}
}
