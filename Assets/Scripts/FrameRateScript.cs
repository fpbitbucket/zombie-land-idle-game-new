﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrameRateScript : MonoBehaviour {
	public int numFrameRate = 30;
	// Use this for initialization
	void Start () {
		Application.targetFrameRate = numFrameRate;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
