﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonScript : MonoBehaviour {
	public bool ObjStatus = false;
	private Sprite currentImage;
	public Sprite switchImage;
//	public GameObject ObjToSwitch;
	void Start () {
		currentImage = gameObject.GetComponent<Image> ().sprite;
		OnSwitchObj(ObjStatus);
	}
	
	// Update is called once per frame
	public void SwitchObj () {
		if (ObjStatus == false) {
			ObjStatus = true;
			gameObject.GetComponent<Image> ().sprite = switchImage;
		} else {
			ObjStatus = false;
			gameObject.GetComponent<Image> ().sprite = currentImage;
		}
		OnSwitchObj(ObjStatus);
	}

	void OnSwitchObj(bool statusObj){
//		ObjToSwitch.SetActive (statusObj);
	}
}
