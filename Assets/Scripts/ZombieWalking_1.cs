﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class ZombieWalking_1 : MonoBehaviour {
	Animator zombieAnim;
	public GameObject charObj;
	public int ZombieNum = 0;
	float origXpos;
	float scalex ;

	public string walk_name = "walk";
	public string[] Anim_Ids_Names;

	void Start () {
		scalex = charObj.transform.localScale.x;
		origXpos = charObj.transform.localPosition.x;
		zombieAnim = charObj.GetComponent<Animator> ();
		starwalking ();
	}
	
	// Update is called once per frame
	void starwalking () {
		charObj.transform.DOScaleX (scalex, 0);
		zombieAnim.Play (walk_name);
		float walkDis = Random.Range (origXpos+7, 21f);
		Vector3 pos_ = new Vector3 (origXpos, charObj.transform.localPosition.y, charObj.transform.localPosition.z);
		Vector3 ids_pos = new Vector3 (walkDis, charObj.transform.localPosition.y, charObj.transform.localPosition.z);

		float distance = Vector3.Distance (pos_, ids_pos);
		if (distance <= 10) {
			zombieAnim.speed = 0.6f;
		} else {
			zombieAnim.speed = 1f;
		}

//		Debug.Log (ZombieNum + ". " + distance);
		charObj.transform.DOLocalMoveX (walkDis, 5).SetEase (Ease.Linear).OnComplete (OnRotateForback );
	}

	void OnRotateForback (){
		zombieAnim.speed = 1f;
		charObj.transform.DOScaleX (-scalex, 0);
		int randomAnim = Random.Range (0, Anim_Ids_Names.Length);
		if (charObj.activeInHierarchy) {
			zombieAnim.Play (Anim_Ids_Names [randomAnim]);
		} else {
		
		}
		int numDelay = Random.Range (2, 5);
		StartCoroutine (startWalkingAgain(numDelay));
		charObj.transform.DOLocalMoveX (origXpos, 5).SetEase (Ease.Linear).OnComplete (starwalking).SetDelay(numDelay);
	}

	IEnumerator startWalkingAgain(int numDelay){
		yield return new WaitForSeconds (numDelay);
		zombieAnim.Play (walk_name);
	}
}
