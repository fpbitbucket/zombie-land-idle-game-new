#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("3O4A6Mx9symU87A0WJGtGs1tOwXzQcLh887FyulFi0U0zsLCwsbDwEHCzMPzQcLJwUHCwsNq8vi7Cu4ssCzHzeQeYLOx31FSuyluyPx+ibBz+lrtBLqiY5CZRgJGoUvSIrgSnlunB19GlHSYtK1P8GTOlM6TxNtCjIMzacAAR4fNb2ohL3JAMXX7N2VlB02XfgcvJYDVY6gRl05iobC6LlCc6nwg+kH2o2UX4aQzSn4waXkD9A8hWkx02+V2YxmGSXW6p1zbF/2GLhG7npWUyvSzFNjnFRexWmxI6dlkS/seuQg3IPg/EILzLPF0H3Hga8WLlwNQs8l/gwwJG9WesHDSqzB9FGZdZnyiY9R5gazakbzwj+JWr5vsY4S5C2mkEsHAwsPC");
        private static int[] order = new int[] { 12,12,12,4,7,10,11,13,10,11,11,12,13,13,14 };
        private static int key = 195;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
