#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleTangle
    {
        private static byte[] data = System.Convert.FromBase64String("iNGQgoKEnJSC0ZCSkpSBhZCfkpT38qTs//Xn9eXaIZi2ZYf4DwWafDGSwoYGy/bdpxor/tD/K0uC6L5ETwWCah8jlf46iL7FKVPPCIkOmjn3wf738qTs4vDwDvX0wfLw8A7B7Fktj9PEO9QkKP4nmiVT1dLgBlBdc/Dx9/jbd7l3BpKV9PDBcAPB2/fXwdX38qT1+uLssIGBnZTRspSDhY6wWWkIIDuXbdWa4CFSShXq2zLu+a/Bc/Dg9/Kk7NH1c/D5wXPw9cFAwakdq/XDfZlCfuwvlIIOlq+UTd+xVwa2vI75r8Hu9/Kk7NL16cHn7nRydOpozLbGA1hqsX/dJUBh4yn52vfw9PT28/Dn75mFhYGCy97ehsLHq8GTwPrB+PfypPX34vOkosDiceXaIZi2ZYf4DwWafN+xVwa2vI5Ey1wF/v/xY/pA0OffhSTN/CqT59UTGiBGgS7+tBDWOwCciRwWRObmKMeOMHakKFZoSMOzCikkgG+PUKN66HgvCLqdBPZa08HzGenPCaH4IoGdlNGylIOFmJeYkpCFmJ6f0bCE0bKwwXPw08H89/jbd7l3Bvzw8PCWfvlF0QY6Xd3RnoFHzvDBfUayPovBc/CHwf/38qTs/vDwDvX18vPwhobfkIGBnZTfkp6c3pCBgZ2UkpA46IMErP8kjq5qA9TyS6R+vKz8APX34vOkosDiweD38qT1++L7sIGBn5XRkp6flZiFmJ6fgtGel9GEgpSVxNLkuuSo7EJlBgdtbz6hSzCpoZiXmJKQhZien9GwhIWZnoOYhYjA/mzMAtq42es5Dz9ESP8or+0nOsyFmZ6DmIWIwOfB5ffypPXy4vywgbSP7r2aoWeweDWFk/rhcrB2wntwgZ2U0aOenoXRsrDB7+b8wcfBxcNaUoBjtqKkMF7esEIJChKBPBdSve5gKu+2oRr0HK+Iddwax1OmvaQd0ZCfldGSlIOFmJeYkpCFmJ6f0YG4KYduwuWUUIZlONzz8vDx8FJz8N3RkpSDhZiXmJKQhZTRgZ6dmJKI9PHyc/D+8cFz8Pvzc/Dw8RVgWPijlJ2YkJ+SlNGen9GFmZiC0ZKUg36CcJE36qr43mNDCbW5AZHJb+QEwXP1SsFz8lJR8vPw8/Pw88H89/iFmJeYkpCFlNGTiNGQn4jRgZCDhdt3uXcG/PDw9PTxwZPA+sH49/KkzNeW0XvCmwb8cz4vGlLeCKKbqpVG6kxis9Xj2zb+7Ee8ba+SObpx5qhW9PiN5rGn4O+FIkZ60sq2UiSe0Z6X0YWZlNGFmZSf0ZCBgZ2YkpDHaL3ciUYcfWotAoZqA4cjhsG+MMTDwMXBwser5vzCxMHDwcjDwMXB3sFwMvf52vfw9PT28/PBcEfrcEL2HYzIcnqi0SLJNUBOa777mg7aDZ2U0bifkt/A18HV9/Kk9fri7LCBZG+L/VW2eqol58bCOjX+vD/lmCCDkJKFmJKU0YKFkIWUnJSfhYLfwZOdlNGChZCflZCDldGFlIOcgtGQ58Hl9/Kk9fLi/LCBgZ2U0aOenoX89/jbd7l3Bvzw8PT08fJz8PDxrcHg9/Kk9fvi+7CBgZ2U0bifkt/AoVt7JCsVDSH49sZBhITQ");
        private static int[] order = new int[] { 40,30,57,50,29,54,34,10,17,33,26,31,49,35,17,32,23,53,58,23,29,35,35,28,36,28,57,43,29,31,35,54,54,34,36,54,45,43,44,53,44,55,44,53,54,56,59,52,55,55,57,53,53,57,58,59,58,59,59,59,60 };
        private static int key = 241;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
