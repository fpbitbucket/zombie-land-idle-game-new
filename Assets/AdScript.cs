﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using AppAdvisory.Ads;
using UnityEngine.Advertisements;
public class AdScript : MonoBehaviour {

	public string Vungle_iOSAppID = "587c7257205150140400048b";
	public string Vungle_androidAppID = "587c72983fd4073438000536";

	public string Vungle_iOSPlacement = "REWARDA94710";
	public string Vungle_androidPlacement = "REWARDA22922";
	Dictionary<string, bool> placements;
	List<string> placementIdList;
	string appID = "";
	string appPlacementID = "";

	public string AppLovinSDKKey = "4Ecd3GndLvdhEo_Z2r5RgE9R_8VFs0zxFZZIrKxADTsSLs0Ng6qMKzoK6IayJXuT6Rsyno6_LzRgqS3VZttvrf";
	void Start () {
		
		#if UNITY_IPHONE
		appID = Vungle_iOSAppID;
		appPlacementID = Vungle_iOSPlacement;
		#elif UNITY_ANDROID
		appID = Vungle_androidAppID;
		appPlacementID = Vungle_androidPlacement;
		#endif



		placements = new Dictionary<string, bool>
		{
			{appPlacementID, false },
		};

		placementIdList = new List<string>(placements.Keys);

		string[] array = new string[placements.Keys.Count];
		placements.Keys.CopyTo(array, 0);
		Vungle.init(appID, array);


		AppLovin.SetSdkKey(AppLovinSDKKey);
		AppLovin.InitializeSdk();
		AppLovin.LoadRewardedInterstitial();
		AppLovin.SetUnityAdListener(gameObject.name);

		initializeEventHandlers ();
	}


	// Update is called once per frame
	void Update () {
//		if (AdsManager.instance.IsReadyVideoAds()) {
//			videoStatus.text = "Video Ad Ready";
//		}else {
//			videoStatus.text = "Video Ad Not Ready";
//		}
//
//		if (AdsManager.instance.IsReadyRewardedVideo()) {
//			RewardStatus.text = "Reward Ad Ready";
//		}else {
//			RewardStatus.text = "Reward Ad Not Ready";
//		}
	}

	void AdCallbackhanler(ShowResult result){
		switch (result){
		case ShowResult.Finished:
			Debug.Log ("Ad Finished. Rewarding player...");
			break;
		case ShowResult.Skipped:
			Debug.Log ("Ad Skipped");
			break;
		case ShowResult.Failed:
			Debug.Log("Ad failed");
			break;
		}
	}

	public void OnShowVideoAds(){
		if (AdsManager.instance.IsReadyVideoAds()) {
			Debug.Log ("Showing Video Ads");
			AdsManager.instance.ShowVideoAds ();
		}else {
			Debug.Log ("No Video Ads");
//			Vungle.loadAd(appPlacementID);
		}
	}


	public void OnShowRewardVideo(){

		if (AdsManager.instance.IsReadyRewardedVideo()) {
			AdsManager.instance.ShowRewardedVideo(Method);
		} else {
			if (placements [appPlacementID]) {
				Vungle.playAd(appPlacementID);
			}else {
				Vungle.loadAd (appPlacementID);
				if(AppLovin.IsIncentInterstitialReady()){
					AppLovin.ShowRewardedInterstitial();
				}
				else{
					AppLovin.LoadRewardedInterstitial();
					Debug.Log("No rewarded ad is available.  Perform failover logic...");
				}
			}
		}
	}

	void Method(bool success) { 
		if (success) { 
			Debug.Log ("give a reward to the player ");
		} else { 
			Debug.Log ("the player do not complete the video");
		}
	} 



	// Setup EventHandlers for all available Vungle events 
	void initializeEventHandlers() {

		//Event triggered during when an ad is about to be played
		Vungle.onAdStartedEvent += (placementID) => {
			Debug.Log ("Ad " + placementID + " is starting!  Pause your game  animation or sound here.");
		};

		//Event is triggered when a Vungle ad finished and provides the entire information about this event
		//These can be used to determine how much of the video the user viewed, if they skipped the ad early, etc.
		Vungle.onAdFinishedEvent += (placementID, args) => {
			Debug.Log ("Ad finished - placementID " + placementID + ", was call to action clicked:" + args.WasCallToActionClicked +  ", is completed view:" 
				+ args.IsCompletedView);

		};

		//Event is triggered when the ad's playable state has been changed
		//It can be used to enable certain functionality only accessible when ad plays are available
		Vungle.adPlayableEvent += (placementID, adPlayable) => {
			Debug.Log ("Ad's playable state has been changed! placementID " + placementID + ". Now: " + adPlayable);
			placements[placementID] = adPlayable;
		};

		//Fired log event from sdk
		Vungle.onLogEvent += (log) => {
			Debug.Log ("Log: " + log);
		};

		//Fired initialize event from sdk
		Vungle.onInitializeEvent += () => {
			Debug.Log ("SDK initialized");
		};
	}

	//AppLovin Event Handler 
	void onAppLovinEventReceived(string ev){
		if(ev.Contains("REWARDAPPROVEDINFO")){
			Debug.Log("Lovin REWARDAPPROVEDINFO");

		}
		else if(ev.Contains("LOADEDREWARDED")) {
			Debug.Log("Lovin A rewarded video was successfully loaded.");
		}
		else if(ev.Contains("LOADREWARDEDFAILED")) {
			Debug.Log("Lovin A rewarded video failed to load.");
		}
		else if(ev.Contains("HIDDENREWARDED")) {
			Debug.Log("Lovin A rewarded video was closed.  Preload the next rewarded video.");
			AppLovin.LoadRewardedInterstitial();
		}
	}
}