﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using com.ootii.Messages;
using TMPro;

public class SpinnerDG : MonoBehaviour
{
	public List<float> weightList;
	public List<int> prizeList;

	public float defaultAngle = 360f * 5;

	protected bool isRotating;
	protected float rotateSpeed;
	protected float rotateAcceleration;
	protected List<float> angleList;

	private float angleUnit;
	public GameObject wheelObj;

	public bool canSpin = false;

	public Transform LandTransform;
	public float shakeDuration = 0f;
	public float shakeAmount = 0.7f;
	public float decreaseFactor = 1.0f;

	Vector3 originalPos;
	int adLimit = 5;
	void OnEnable()
	{
		MessageDispatcher.AddListener("OnWatchAndSpin", OnWatchAndSpin, true);
		MessageDispatcher.AddListener("OnWatchAndSpinAddTime", OnWatchAndSpinAddTime, true);
		MessageDispatcher.AddListener("OnAdsUpdate", OnAdsUpdate, true);
		originalPos = LandTransform.localPosition;
		OnActiveCloseButton();


		isRotating = false;
		angleUnit = 360f / weightList.Count;
		angleList = new List<float>();
		for (int i = 0; i < weightList.Count; i++)
		{
			float angle = defaultAngle + i * angleUnit;
			angleList.Add(angle);
			prizeList.Add(i);
		}

		wheelObj.transform.parent.Find("DefaultPanel").GetChild(2).GetComponentInChildren<TextMeshProUGUI>().text = "+4HRS TO TIME";
		wheelObj.transform.parent.Find("DefaultPanel").GetChild(0).gameObject.GetComponent<Button>().onClick.AddListener(Spin);
		wheelObj.transform.parent.Find("DefaultPanel").GetChild(1).gameObject.GetComponent<Button>().onClick.AddListener(Spin);
		wheelObj.transform.parent.Find("DefaultPanel").GetChild(2).gameObject.GetComponent<Button>().onClick.AddListener(AddTime);
		wheelObj.transform.parent.Find("DefaultPanel").GetChild(1).gameObject.SetActive(false);
		wheelObj.transform.parent.Find("DefaultPanel").GetChild(2).gameObject.SetActive(false);
		wheelObj.transform.parent.Find("DefaultPanel").GetChild(0).gameObject.SetActive(false);

		StartCoroutine(delaySetup());
	}

	void AddTime()
	{
		OnSpinAgain ();
		GameObject.Find("Managers/AdManager").GetComponent<AdManagerScript>().OnWatchAd("WatchSpinAddtime");
		
	}

	public void OnActiveCloseButton()
	{
		wheelObj.transform.parent.GetChild(4).gameObject.SetActive(true);
	}

	void Update()
	{

	}

	public void OnDoneTime()
	{
		if (canSpin == false)
		{
			StartCoroutine(delaySetup());
		}
	}

	public void OnCheckForAds()
	{
		if (canSpin == false)
		{
			StartCoroutine(delaySetup());
		}
	}

	IEnumerator delaySetup()
	{
		yield return new WaitForSeconds(0.25f);
		if (ES2.Exists("SpinAdLimit_" + Application.loadedLevelName))
		{
			adLimit = ES2.Load<int>("SpinAdLimit_" + Application.loadedLevelName);
		}
		else
		{
			ES2.Save(adLimit, "SpinAdLimit_" + Application.loadedLevelName);
		}
		if (gameObject.GetComponent<SpinHandler>().startSpinner)
		{
			wheelObj.transform.parent.Find("DefaultPanel").GetChild(1).GetComponent<Button>().interactable = true;
			wheelObj.transform.parent.Find("DefaultPanel").GetChild(1).gameObject.SetActive(true);
			wheelObj.transform.parent.Find("DefaultPanel").GetChild(2).gameObject.SetActive(true);
			wheelObj.transform.parent.Find("DefaultPanel").GetChild(0).gameObject.SetActive(false);
			wheelObj.transform.parent.Find("DefaultPanel").GetChild(1).GetComponentInChildren<TextMeshProUGUI>().text = "SPIN AGAIN";

			if (GameObject.Find("Managers/AdManager").GetComponent<AdManagerScript>().ifHasAdsAvailable == false)
			{
				wheelObj.transform.parent.Find("DefaultPanel").GetChild(1).GetComponent<Button>().interactable = false;

				wheelObj.transform.parent.Find("DefaultPanel").GetChild(1).GetComponentInChildren<TextMeshProUGUI>().text = "ADS NOT AVAILBLE";
			}

			bool ifCanSpin = false;
			if (adLimit <= 0)
			{
				wheelObj.transform.parent.Find("DefaultPanel").GetChild(2).GetComponent<Button>().interactable = false;
				wheelObj.transform.parent.Find("DefaultPanel").GetChild(4).GetComponentInChildren<TextMeshProUGUI>().text = "";
			}
			else
			{
				ifCanSpin = true;
				wheelObj.transform.parent.Find("DefaultPanel").GetChild(2).GetComponent<Button>().interactable = true;
				wheelObj.transform.parent.Find("DefaultPanel").GetChild(4).GetComponent<TextMeshProUGUI>().text = "You have " + adLimit + " more ads bonuses that you can use today!";
			}

			if (GameObject.Find("Managers/AdManager").GetComponent<AdManagerScript>().ifHasAdsAvailable && ifCanSpin == true)
			{
				wheelObj.transform.parent.Find("DefaultPanel").GetChild(2).GetComponent<Button>().interactable = true;
				wheelObj.transform.parent.Find("DefaultPanel").GetChild(2).GetComponentInChildren<TextMeshProUGUI>().text = "+4HRS TO TIME";
			}
			else
			{
				if (GameObject.Find("Managers/AdManager").GetComponent<AdManagerScript>().ifHasAdsAvailable == false && ifCanSpin == true)
				{
					wheelObj.transform.parent.Find("DefaultPanel").GetChild(2).GetComponent<Button>().interactable = false;
					wheelObj.transform.parent.Find("DefaultPanel").GetChild(2).GetComponentInChildren<TextMeshProUGUI>().text = "+4HRS TO TIME";
				}
			}
		}
		else
		{
			wheelObj.transform.parent.Find("DefaultPanel").GetChild(4).GetComponentInChildren<TextMeshProUGUI>().text = "";

			wheelObj.transform.parent.Find("DefaultPanel").GetChild(1).gameObject.SetActive(false);
			wheelObj.transform.parent.Find("DefaultPanel").GetChild(2).gameObject.SetActive(false);
			wheelObj.transform.parent.Find("DefaultPanel").GetChild(0).gameObject.SetActive(true);

			if (GameObject.Find("Managers/AdManager").GetComponent<AdManagerScript>().ifHasAdsAvailable)
			{

				wheelObj.transform.parent.Find("DefaultPanel").GetChild(0).GetComponent<Button>().interactable = true;
				wheelObj.transform.parent.Find("DefaultPanel").GetChild(0).GetComponentInChildren<TextMeshProUGUI>().text = "WATCH A VIDEO TO SPIN";
			}
			else
			{

				wheelObj.transform.parent.Find("DefaultPanel").GetChild(0).GetComponent<Button>().interactable = false;
				wheelObj.transform.parent.Find("DefaultPanel").GetChild(0).GetComponentInChildren<TextMeshProUGUI>().text = "ADS NOT AVAILBLE";
			}
		}
	}

	void OnDisable()
	{
		MessageDispatcher.RemoveListener("OnWatchAndSpin", OnWatchAndSpin, true);
		MessageDispatcher.RemoveListener("OnWatchAndSpinAddTime", OnWatchAndSpinAddTime, true);
		MessageDispatcher.RemoveListener("OnAdUpdate", OnAdsUpdate, true);

	}

	void OnAdsUpdate(IMessage rMessage)
	{
		if (canSpin == false)
		{
			OnCheckForAds();
		}
	}


	void OnWatchAndSpinAddTime(IMessage rMessage)
	{
		adLimit--;
		if (adLimit <= 0)
		{
			adLimit = 0;
		}
		ES2.Save(adLimit, "SpinAdLimit_" + Application.loadedLevelName);
		StartCoroutine(delaySetup());
	}

	void OnWatchAndSpin(IMessage rMessage)
	{
		if (ES2.Exists("SpinTutorialAgain") == false)
		{
			MessageDispatcher.SendMessage(this, "OnShowTutorial", 11, 0f);
		}
		else
		{

		}
		Debug.Log("Done Watch ADS");
		wheelObj.transform.parent.Find("DefaultPanel").GetChild(1).gameObject.SetActive(false);
		wheelObj.transform.parent.Find("DefaultPanel").GetChild(2).gameObject.SetActive(false);
		wheelObj.transform.parent.Find("DefaultPanel").GetChild(0).gameObject.SetActive(true);
		wheelObj.transform.parent.Find("DefaultPanel").GetChild(0).GetComponent<Button>().interactable = true;
		wheelObj.transform.parent.Find("DefaultPanel").GetChild(0).GetComponentInChildren<TextMeshProUGUI>().text = "START TO SPIN NOW!";
		// wheelObj.transform.parent.Find ("DefaultPanel/SpinBtn").GetComponentInChildren<TextMeshProUGUI> ().text = "START TO SPIN NOW!";
		// wheelObjPort.transform.parent.Find ("DefaultPanel/SpinBtn").GetComponentInChildren<TextMeshProUGUI> ().text = "START TO SPIN NOW!";
		canSpin = true;
	}

	public void Spin()
	{
		if (!isRotating && canSpin)
		{
			shakeDuration = 0f;
			isRotating = true;
			MessageDispatcher.SendMessage(this, "OnCloseTutorialTemp", 11, 0);
			wheelObj.transform.parent.GetChild(4).gameObject.SetActive(true);
			gameObject.GetComponent<AudioSource>().Play();
			int randomAngleIndex = BiasRandom(weightList);
			float rotateAngle = angleList[randomAngleIndex];
			Vector3 rotateEnd = wheelObj.transform.eulerAngles - Vector3.forward * rotateAngle;
			Shift<int>(randomAngleIndex, ref prizeList);
			Shift<float>(randomAngleIndex, ref weightList);
			wheelObj.transform.DORotate(rotateEnd, 5.0f, RotateMode.FastBeyond360).SetEase(Ease.InOutQuad).OnComplete(RotateEnd);
			MessageDispatcher.SendMessage(this, "OnGA_Report", "Spinner:Press To Spin Wheel", 0);
		}
		else
		{
			if (canSpin == false)
			{
				gameObject.GetComponent<SpinHandler>().OnClosePopupSpinAgain();
				MessageDispatcher.SendMessage(this, "OnGA_Report", "Spinner:Watch Ads To Spin", 0);
				GameObject.Find("Managers/AdManager").GetComponent<AdManagerScript>().OnWatchAd("WatchSpin");

				OnSpinAgain ();

			}
		}
	}

	void OnSpinAgain(){
		if (ES2.Exists("SpinTutorialAgain") == true)
		{
			Debug.Log("SpinTutorialAgain " + ES2.Load<int>("SpinTutorialAgain"));
			if (ES2.Load<int>("SpinTutorialAgain") == 0)
			{
				MessageDispatcher.SendMessage(this, "OnUpdateTextTutorial", 2, 0f);
				MessageDispatcher.SendMessage(this, "OnCloseTutorial", 11, 0f);
			}
		}
	}

	void RotateEnd()
	{
		/*
		 * You may need to add something like reward here
		 */

		if (ES2.Exists("SpinTutorialAgain"))
		{
			if (ES2.Load<int>("SpinTutorialAgain") == 0)
			{
				MessageDispatcher.SendMessage(this, "OnUpdateTextTutorial", 1, 0f);
				MessageDispatcher.SendMessage(this, "OnShowTutorial", 11, 0f);
			}
			else if (ES2.Load<int>("SpinTutorialAgain") == 1)
			{

			}
		}
		else
		{
			MessageDispatcher.SendMessage(this, "OnShowTutorial", 11, 0f);
			MessageDispatcher.SendMessage(this, "OnUpdateTextTutorial", 0, 0f);
		}

		gameObject.GetComponent<AudioSource>().Stop();
		Debug.Log("CONGRATULATIONS! GET PRIZE AT INDEX " + prizeList[0]);
		MessageDispatcher.SendMessage(this, "OnSpinnerGiveReward", prizeList[0], 0f);
		MessageDispatcher.SendMessage(this, "OnSoundFx", 4, 0f);
		wheelObj.transform.parent.GetChild(4).gameObject.SetActive(true);
		isRotating = false;
		canSpin = false;
		StartCoroutine(delaySetup());

		// wheelObj.transform.parent.Find ("DefaultPanel/SpinBtn").GetComponentInChildren<TextMeshProUGUI> ().text = "WATCH A VIDEO TO SPIN AGAIN";
		// wheelObjPort.transform.parent.Find ("DefaultPanel/SpinBtn").GetComponentInChildren<TextMeshProUGUI> ().text = "WATCH A VIDEO TO SPIN AGAIN";
	}

	void Shift<T>(int shift, ref List<T> list)
	{
		List<T> newList = new List<T>();
		for (int i = 0; i < weightList.Count; i++)
		{
			newList.Add(list[(i + shift) % list.Count]);
		}
		list = newList;
	}

	int BiasRandom(List<float> weightList)
	{
		float total = weightList.Sum();
		if (total == 0)
		{
			return 0;
		}
		float rand = Random.Range(0, total);
		for (int i = 0; i < weightList.Count; i++)
		{
			rand -= weightList[i];
			if (rand <= 0)
			{
				return i;
			}
		}
		return 0;
	}
}