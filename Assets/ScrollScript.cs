﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using DG.Tweening;
using com.ootii.Messages;
public class ScrollScript : MonoBehaviour {

	static ScrollRect scrollRect;
	public Camera maincamera;
	public RectTransform[] generators;
	public GameObject[] cameraObj,rooms,rawImage;
	public float[] Yplace;
	public float multiplierMove = 1.1f;
	public MainGeneratorScript mainGen;

	void OnEnable()
	{
		scrollRect = GetComponent<ScrollRect> ();
//		scrollRect.onValueChanged.AddListener(ListenerMethod);
		OnCheckIfVisible ();

		for (int i = 0; i < generators.Length; i++) {
			string getName = mainGen.genNames_Code [i] ;
			if (ES2.Exists ("btnName_" + getName + "?tag=Status")) {
				bool isActiveGen = ES2.Load<bool> ("btnName_" + getName + "?tag=Status");

				if (isActiveGen == true) {
					if (i <= 4) {
						rooms [i].SetActive (true);
						rawImage [i].SetActive (true);
						cameraObj [i].GetComponent<Camera> ().enabled = true;
					} else {
						OnDisableGen (i);
					}
				} else {
					OnDisableGen (i);
				}
			}
		}
	
		MessageDispatcher.AddListener ("OnUpdateScrollData", OnUpdateScrollData, true);
	}

	void OnDisable () {
		MessageDispatcher.RemoveListener ("OnUpdateScrollData", OnUpdateScrollData, true);
	}

	public void ListenerMethod(Vector2 value)
	{

		OnCheckIfVisible ();
//		Debug.Log (isFullyVisible);
		if (value.y >= 0 && value.y <= 1) {
//			Debug.Log ("ListenerMethod: " + value.y + " " +value.y*10);
			float tempNum = value.y * 10;
			for (int i = 0; i < cameraObj.Length; i++) {
				cameraObj[i].transform.DOLocalMoveY (Yplace[i] + (tempNum * multiplierMove), 0);
			}
		}
	
	}

	void OnCheckIfVisible(){
		Plane[] planes = GeometryUtility.CalculateFrustumPlanes(maincamera);

		float fUiUnitToWorldUnit = Screen.height / (maincamera.orthographicSize * 2);
		for (int i = 0; i < generators.Length; i++) {
			Bounds buttonBounds = new Bounds (new Vector3 (generators[i].position.x, generators[i].position.y, generators[i].position.z),
				new Vector3 (generators[i].sizeDelta.x / fUiUnitToWorldUnit, generators[i].sizeDelta.y / fUiUnitToWorldUnit, 1));

			if (GeometryUtility.TestPlanesAABB (planes, buttonBounds)) {
//				Debug.Log ("Visible rooms " + i);
				string getName = mainGen.genNames_Code [i] ;
				if (ES2.Exists ("btnName_" + getName + "?tag=Status")) {
					bool isActiveGen = ES2.Load<bool> ("btnName_" + getName + "?tag=Status");
				
					if (isActiveGen == true) {
						rooms [i].SetActive (true);
						rawImage [i].SetActive (true);
						cameraObj [i].GetComponent<Camera> ().enabled = true;
					} else {
						OnDisableGen(i);
					}
				} else {
					OnDisableGen(i);
				}
			} else {
//				Debug.Log ("Not visible rooms " + i);
				OnDisableGen(i);
			}
		}
	}

	void OnDisableGen(int i){
		rawImage [i].SetActive (false);
		rooms[i].SetActive (false);
		cameraObj [i].GetComponent<Camera> ().enabled = false;
	}

	public void OnUpdateScrollData(IMessage rMessage){
		OnCheckIfVisible ();
	}

}
