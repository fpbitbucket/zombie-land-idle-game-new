﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandAnimScript : MonoBehaviour {
	
	// Use this for initialization
	void Start () {
		StartCoroutine (ranTime ());
	}
	
	// Update is called once per frame
	IEnumerator ranTime () {
		yield return new WaitForSeconds (Random.Range(3, 10));
		gameObject.GetComponent<Animator> ().Play ("Anim");
		yield return new WaitForSeconds (1);
		StartCoroutine (ranTime ());
	}
}
