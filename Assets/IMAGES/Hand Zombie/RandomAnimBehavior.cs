﻿using UnityEngine;
using System.Collections;
using com.ootii.Messages;
public class RandomAnimBehavior : StateMachineBehaviour {

	public string m_parameterName = "idleAnimID";
	public int[] m_stateIDArray = {0,1,2,3};
	private bool canExit = false;
	void OnEnable(){
	}

	void OnDisable(){
		
	}


	override public void OnStateMachineEnter(Animator animator,int stateMatchinePathHash){
		if (m_stateIDArray.Length <= 0) {
			animator.SetInteger (m_parameterName, 0);
		} else {
			int index = Random.Range (0, m_stateIDArray.Length);
			animator.SetInteger (m_parameterName, m_stateIDArray[index]);
		}

	}

	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
//		Debug.Log ("exit");
	}
}
