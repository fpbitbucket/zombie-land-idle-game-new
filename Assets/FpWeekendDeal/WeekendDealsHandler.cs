﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using LitJson;
using TMPro;
using com.ootii.Messages;

public class WeekendDealsHandler : MonoBehaviour
{

	private static WeekendDealsHandler _instance;

	public static WeekendDealsHandler instance {
		get {
			if (_instance == null) {
				_instance = GameObject.FindObjectOfType<WeekendDealsHandler> ();

				//Tell unity not to destroy this object when loading a new scene!
				DontDestroyOnLoad (_instance.gameObject);
			}

			return _instance;
		}
	}


	public string bundleId;
	public string JsonVersion = "";
	public float timerWeekend;
	public string EventInput = "";
	private string JsonString;
	public string url;
	public bool networkState = false;
	private JsonData checkjsondata;
	public bool hasData = false;
	public bool StartTime = false;
	public int ScriptversionNum = 0;
	public Sprite[] IconScript;

	//	public double goldAmount = 0;
	//	public double coinsAmount = 0;
	//	public double multiplierAmount = 0;
	//	public double TimeWarpAmount = 0;

	public double[] DealsAmounts;
	public string[] DealsNames;

	string dayToday = "";

	void Awake ()
	{


		if (_instance == null) {
			//If I am the first instance, make me the Singleton
			_instance = this;
			DontDestroyOnLoad (this);
		} else {
			//If a Singleton already exists and you find
			//another reference in scene, destroy it!
			if (this != _instance)
				Destroy (this.gameObject);
		}
		hasData = false;
		StartTime = false;

		IconScript = new Sprite[4];
		DealsAmounts = new double[4];
		DealsNames = new string[4];
		ScriptversionNum = UnityEngine.Random.Range (1, 10000);
		url += "?rand=" + ScriptversionNum.ToString ();

//		OnStartWeekendDeal ();
		MessageDispatcher.AddListener ("OnSuccessBuyDeals", OnSuccessBuyDeals, true); 
	}

	void OnDisable(){
		MessageDispatcher.RemoveListener ("OnSuccessBuyDeals", OnSuccessBuyDeals, true); 
	}

	public void OnStartWeekendDeal ()
	{
		DateTime date = DateTime.Now;
		string dateToday = date.ToString ("d");
		DayOfWeek day = DateTime.Now.DayOfWeek;
		dayToday = day.ToString ();
		networkState = false;
		StartCoroutine (checkInternetConnection ());
	}


	IEnumerator checkInternetConnection ()
	{
		while (networkState == false) {
			WWW www = new WWW (url);
			yield return www;
			if (www.error != null) {
				Debug.Log ("Netword Error: " + www.error);
				GameObject.Find("ShopManager").GetComponent<ShopManager>().ShopSpaceObj.SetActive (true);
			} else {
				networkState = true;
				StartCoroutine (OnCheckUpdate ());
				Debug.Log ("WeekendDeal OnCheckUpdate");
			}
			yield return new WaitForSeconds (3);
		}
	}

	int numGoods = 1;

	IEnumerator OnCheckUpdate ()
	{
		bool canLoadDeals = false;
		EventInput = dayToday;
		WWW www1 = new WWW (url);
		yield return www1;
		Debug.Log ("WeekendDeal Start Updates " + EventInput);


		if (www1.progress == 1) {
			checkjsondata = JsonMapper.ToObject (www1.text);
			JsonVersion = checkjsondata ["Version_"].ToString ();
			Debug.Log (JsonVersion + " Version");
			if (File.Exists (Application.persistentDataPath + "/CCWDealsJson.json")) {
				JsonString = File.ReadAllText (Application.persistentDataPath + "/CCWDealsJson.json");
				if (ES2.Exists ("jsonversion")) {
					if (ES2.Load<string> ("jsonversion") != JsonVersion) {

						for (int i = 1; i <= 4; i++) {
							string nameImage = "digitalGood" + i + "_Icon" + JsonVersion.ToString () + ".png";
							string LinkString = checkjsondata [EventInput] ["digitalGood" + i + "_Icon"].ToString ();
							if (File.Exists (Application.persistentDataPath + "/" + nameImage)) {
								File.Delete (Application.persistentDataPath + "/" + nameImage);
							}
						}

						WWW www = new WWW (url + "?rand=" + ScriptversionNum);
						yield return www;
						JsonString = www.text;
						checkjsondata = JsonMapper.ToObject (JsonString);
						File.WriteAllText (Application.persistentDataPath + "/CCWDealsJson.json", JsonString);
						JsonVersion = checkjsondata ["Version_"].ToString ();
						ES2.Save (JsonVersion, "jsonversion");
					} else {
						checkjsondata = JsonMapper.ToObject (JsonString);
						JsonVersion = checkjsondata ["Version_"].ToString ();
					} 
				} else {
					checkjsondata = JsonMapper.ToObject (JsonString);
					JsonVersion = checkjsondata ["Version_"].ToString ();
				}
			} else {
				Debug.Log ("Downloading Json File");
				WWW www = new WWW (url);
				yield return www;
				JsonString = www.text;
				checkjsondata = JsonMapper.ToObject (JsonString);
				File.WriteAllText (Application.persistentDataPath + "/CCWDealsJson.json", JsonString);
				JsonVersion = checkjsondata ["Version_"].ToString ();
				ES2.Save (JsonVersion, "jsonversion");
			}

			if (ES2.Exists ("ActiveDealDate")) {
				Debug.Log ("ActiveDealDate");
				EventInput = ES2.Load<string> ("ActiveDealDate");
				canLoadDeals = true;
			} else {
				Debug.Log ("Day " + checkjsondata ["DayToShow"] [EventInput].ToString ().ToLower ());
				if (checkjsondata ["DayToShow"] [EventInput].ToString ().ToLower () == "true") {
					if (ES2.Exists ("SetTime") && ES2.Exists ("DealDate_")) {
						if (ES2.Load<int> ("SetTime") == 0 && ES2.Load<int> ("DealDate_") == DateTime.Now.Day) {
							canLoadDeals = false;
						} else {
							canLoadDeals = true;
							ES2.Save (EventInput, "ActiveDealDate"); 
						}
					} else {
						canLoadDeals = true;
						ES2.Save (EventInput, "ActiveDealDate"); 
					}
				} else {
					canLoadDeals = false;
				}
			}
			if (canLoadDeals) {
				StartCoroutine (getSprite ());
			} else {
				GameObject.Find("ShopManager").GetComponent<ShopManager>().ShopSpaceObj.SetActive (true);
			}
			Debug.Log ("can deal " + canLoadDeals);
		} else {
			Debug.Log ("Error Link : " + www1.error);
			GameObject.Find("ShopManager").GetComponent<ShopManager>().ShopSpaceObj.SetActive (true);
		}
	}



	IEnumerator getSprite ()
	{
		for (int i = 1; i <= 4; i++) {
			string nameImage = "digitalGood" + i + "_Icon" + JsonVersion.ToString () + ".png";
			int applySprite = i;
			string LinkString = checkjsondata [EventInput] ["digitalGood" + i + "_Icon"].ToString ();
			if (File.Exists (Application.persistentDataPath + "/" + nameImage)) {
				byte[] bytes = File.ReadAllBytes (Application.persistentDataPath + "/" + nameImage);
				Texture2D texture1 = new Texture2D (4, 4, TextureFormat.RGBA32, false);
				texture1.LoadImage (bytes);
				addSprites (i, texture1);
			} else {
				WWW www = new WWW (LinkString);
				Debug.Log ("Downloading " + LinkString);
				yield return www;
				if (www.progress == 1) {
					if (!string.IsNullOrEmpty (www.error)) {
						Debug.Log ("Error Download " + www.error);
					} else {
						Texture2D UIObj = www.texture;
						byte[] bytes = www.texture.EncodeToPNG ();
						File.WriteAllBytes (Application.persistentDataPath + "/" + nameImage, bytes);
						addSprites (applySprite, UIObj);
					}
				}
			}
		}
	}



	void addSprites (int NumIcon, Texture2D spriteTexture)
	{
		Sprite createSprite = Sprite.Create (spriteTexture, new Rect (0, 0, spriteTexture.width, spriteTexture.height), new Vector2 (0.5f, 0.5f));
		if (NumIcon == 1) {
			IconScript [0] = createSprite;
		} else if (NumIcon == 2) {
			IconScript [1] = createSprite;
		} else if (NumIcon == 3) {
			IconScript [2] = createSprite;
		} else if (NumIcon == 4) {
			IconScript [3] = createSprite;
			OnLoadedData ();
		}
	}

	void OnLoadedData ()
	{
		
		for (int i = 0; i < DealsAmounts.Length; i++) {
			DealsAmounts [i] = double.Parse (checkjsondata [EventInput] ["digitalGoodsAmount" + (i + 1)].ToString ());
			DealsNames [i] = checkjsondata [EventInput] ["digitalGood" + (i + 1)].ToString ();
		}
		float timerWeekendHolder;
		timerWeekend = float.Parse (checkjsondata [EventInput] ["Time"].ToString ());
		timerWeekendHolder = float.Parse (checkjsondata [EventInput] ["Time"].ToString ());

		StartTime = true;
		if (ES2.Exists ("WeekendDealsTimeLeft")) {
			timerWeekend = ES2.Load<float> ("WeekendDealsTimeLeft");
			OnCalculateTime ();
		} 
		ES2.Save (timerWeekend, "SetTime"); 

//		if(GameObject.Find ("GameTimer")){
//			for (int i = 0; i < GameObject.Find ("GameTimer").GetComponent<IAP_Script> ().WeekendDealIDs.Length; i++) {
//				if (GameObject.Find ("GameTimer").GetComponent<IAP_Script> ().WeekendDealIDs [i] == checkjsondata [EventInput] ["Iap_id"].ToString ()) {
//					gameObject.GetComponent<WeekendDeals> ().
//				}
//			}
//		}

		bundleId = checkjsondata [EventInput] ["Iap_id"].ToString();

//		gameObject.GetComponent<WeekendDeals_IapScript> ().kProductIDConsumable = checkjsondata [EventInput] ["Iap_id"].ToString ();
//		gameObject.GetComponent<WeekendDeals_IapScript> ().OnStart_InitializePurchasing ();

		StartCoroutine (OnDelayLoad ());

		hasData = true;
	}


	IEnumerator OnDelayLoad(){
		yield return new WaitForSeconds (1);
		GameObject.Find ("WeekEndDealPrefab").GetComponent<WeekendDeals> ().OnUpdatePriceAmount ();
		GameObject.Find ("WeekEndDealPrefab").GetComponent<WeekendDeals> ().OnUpdateData ();
	}

	void Update ()
	{
		if (StartTime) {
			float tempDeltaTime = Time.deltaTime;
			timerWeekend -= tempDeltaTime;
			if (timerWeekend < 0) {
				timerWeekend = 0;
				StartTime = false;
				if (ES2.Exists ("WeekendDealsTimeLeft")) {
					ES2.Delete ("WeekendDealsTimeLeft");
				}
				if (ES2.Exists ("ActiveDealDate")) {
					ES2.Delete ("ActiveDealDate");
				}
				ES2.Save ((int)0, "SetTime"); 
				ES2.Save (DateTime.Now.Day, "DealDate_"); 
				if (GameObject.Find ("WeekEndDealPrefab")) {
					GameObject.Find ("WeekEndDealPrefab").GetComponent<WeekendDeals> ().OnCloseWeekendDeal ();
					GameObject.Find ("WeekEndDealPrefab").GetComponent<WeekendDeals> ().OnHideButtons ();
				}
			}
		}
	}

	DateTime currentDate;

	void OnCalculateTime ()
	{
		if (ES2.Exists ("WeekendDealsTimeLeft")) {
			currentDate = System.DateTime.Now;
			long temp = 0;
			if (ES2.Exists ("WeekendDealsTimeLeftOffline")) {
				temp = Convert.ToInt64 (ES2.Load<string> ("WeekendDealsTimeLeftOffline")); //load the time starts offline
			} 
			DateTime oldDate = DateTime.FromBinary (temp);
			TimeSpan difference = currentDate.Subtract (oldDate);
			int secondsInt = Convert.ToInt32 (difference.TotalSeconds); //seconds during offline
			float tempGenTimeLeft = ES2.Load<float> ("WeekendDealsTimeLeft");
			float numReturnValue = 0;

			if (tempGenTimeLeft < difference.TotalSeconds) { // time offline is bigger than time remaining
				numReturnValue = 0;
			} else { // time remaining is bigger than time offline
				numReturnValue = tempGenTimeLeft - (float)difference.TotalSeconds;
			}
			StartTime = true;
			timerWeekend = numReturnValue;
		}
	}

	string[] prefix = new string[] {
		"Hundred",
		"Thousand",
		"Million",
		"Billion",
		"Trillion",
		"Quadrillion",
		"Quintillion",
		"Sextillion",
		"Septillion",
		"Octillion",
		"Nonillion",
		"Decillion",
		"Undecillion",
		"Duodecillion",
		"Tredecillion",
		"Quattuordecillion",
		"Quindecillion",
		"Sexdecillion",
		"Septendecillion",
		"Octodecillion",
		"Novemdecillion",
		"Vigintillion",
		"Unvigintillion",
		"Duovigintillion",
		"Tresvigintillion",
		"Quattuorvigintillion",
		"Quinvigintillion",
		"Sexvigintillion",
		"Septenvigintillion",
		"Octovigintillion",
		"Novemvigintillion",
		"Trigintillion",
		"Untrigintillion",
		"Duotrigintillion",
		"Tretrigintillion",
		"Quattuortrigintillion",
		"Quintrigintillion",
		"Sextrigintillion",
		"Septentrigintillion",
		"Octotrigintillion",
		"Novemtrigintillion",
		"Quadragintillion",
		"Unquadragintillion",
		"Duoquadragintillion",
		"Trequadragintillion",
		"Quattuorquadragintillion",
		"Quinquadragintillion",
		"Sexquadragintillion",
		"Septquadragintillion",
		"Octoquadragintillion",
		"Novemquadragintillion",
		"Quinquagintillion",
		"Unquinquagintillion",
		"Duoquinquagintillion",
		"Trequinquagintillion",
		"Quattuorquinquagintillion",
		"Quinquinquagintillion",
		"Sexquinquagintillion",
		"Septquinquagintillion",
		"Octoquinquagintillion",
		"Novemquinquagintillion",
		"Sexagintillion",
		"Unsexagintillion",
		"Duosexagintillion",
		"Tresexagintillion",
		"Quattuorsexagintillion",
		"Quinsexagintillion",
		"Sexsexagintillion",
		"Septsexagintillion",
		"Octosexagintillion",
		"Novemsexagintillion",
		"Septuagintillion",
		"Unseptuagintillion",
		"Duoseptuagintillion",
		"Treseptuagintillion",
		"Quattuorseptuagintillion",
		"Quinseptuagintillion",
		"Sexseptuagintillion",
		"Septseptuagintillion",
		"Octoseptuagintillion",
		"Novemseptuagintillion",
		"Octogintillion",
		"Unoctogintillion",
		"Duooctogintillion",
		"Treoctogintillion",
		"Quattuoroctogintillion",
		"Quinoctogintillion",
		"Sexoctogintillion",
		"Septoctogintillion",
		"Octooctogintillion",
		"Novemoctogintillion",
		"Nonagintillion",
		"Unnonagintillion",
		"Duononagintillion",
		"Trenonagintillion",
		"Quattuornonagintillion",
		"Quinnonagintillion",
		"Sexnonagintillion",
		"Septnonagintillion",
		"Onctononagintillion",
		"Novemnonagintillion",
		"Centillion",
		"Uncentillion"
	};

	string GEtNumberNoDecimal (double value)
	{
		string stringformat = "";
		double newMoneyAmount = 0;
		string moneySuffix = ""; 
		double numPow = 0;
		if (value > 999999) {
			while (value >= Math.Pow (1000, numPow)) {
				moneySuffix = " " + prefix [(int)numPow];
				newMoneyAmount = (double)value / Math.Pow (1000, numPow);
				if ((Math.Round (newMoneyAmount) - newMoneyAmount) != 0) {
					stringformat = ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));
				} else {
					stringformat = ((string.Format ("{0:n0}", newMoneyAmount) + moneySuffix));
				}

				numPow++;

			}
		} else {
			if ((Math.Round (newMoneyAmount) - newMoneyAmount) != 0) {
				stringformat = ((string.Format ("{0:n2}", value)));
			} else {
				stringformat = ((string.Format ("{0:n0}", value)));
			}

		}
		return stringformat;
	}

	void OnSuccessBuyDeals (IMessage rMessage)
	{
		MessageDispatcher.SendMessage (this, "OnGA_Report", "Purchase:" + EventInput + "_Deal", 0);
//		if (EventInput == "set1") {
//			
//		}else if (EventInput == "set2") {
//			MessageDispatcher.SendMessage (this, "OnGA_Report", "Purchase:ccwd2", 0);
//		}else if (EventInput == "set3") {
//			MessageDispatcher.SendMessage (this, "OnGA_Report", "Purchase:ccwd3", 0);
//		}

		MessageDispatcher.SendMessage (this, "OnIncrementMoney", DealsAmounts [0], 0);	
		MessageDispatcher.SendMessage (this, "OnIncrementGold", DealsAmounts [1], 0);
		MessageDispatcher.SendMessage (this, "OnPermanentMultiplier", DealsAmounts [2], 0);	
		MessageDispatcher.SendMessage (this, "OnDealsTimeWarp", (int)DealsAmounts [3], 0);
		StartCoroutine (StopingTime ());
	}

	IEnumerator StopingTime(){
		yield return new WaitForSeconds (0.5f);
		timerWeekend = 0;
		ES2.Save (timerWeekend, "WeekendDealsTimeLeft"); 
	}

	public void OnSetupData ()
	{
		
	}

	void OnApplicationQuit ()
	{
		if (StartTime) {
			ES2.Save (System.DateTime.Now.ToBinary ().ToString (), "WeekendDealsTimeLeftOffline");
			ES2.Save (timerWeekend, "WeekendDealsTimeLeft"); 
		
		}

	}

	void OnApplicationPause (bool pauseStatus)
	{

		if (pauseStatus == true) {
			if (StartTime) {
				ES2.Save (System.DateTime.Now.ToBinary ().ToString (), "WeekendDealsTimeLeftOffline");
				ES2.Save (timerWeekend, "WeekendDealsTimeLeft"); 
			}
		} else if (pauseStatus == false) {
			if (StartTime) {
				StartTime = false;
				OnCalculateTime ();
			}


		}

	}
}
