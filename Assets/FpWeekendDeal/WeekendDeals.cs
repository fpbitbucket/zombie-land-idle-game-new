﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;

using System.IO;
using TMPro;
using com.ootii.Messages;
public class WeekendDeals : MonoBehaviour {

	WeekendDealsHandler WeekendDealsHandlerObj;

	//	public GameObject LandscapeCanvas, PortraitCanvas;
	public GameObject[] WeekendPanels;
	public GameObject[] WeekendBtns;
	public GameObject WeekendBtnsShop;
//	public GameObject weekendIdleBtn;
//	bool IdleState = false;
	public float timeIdle = 600f;//,timeIdleTemp= 0;


	public TextMeshProUGUI[] IapPricesAmount;//CoinAmount,GoldAmount,MultiplierAmount,

	public TextMeshProUGUI timerText;
	bool portraitcheck = false,landscapecheck = false;

	public float timerWeekend = 172800;
	public float TimeCounter = 0;
	public bool OnOpenWeekendDeal =false;
	public GameObject[] itemDealsPortrait;

	public GameObject[]SetToUse;

	void Start () {
//		timeIdleTemp = timeIdle;
//		weekendIdleBtn.SetActive (false);
		OnOpenWeekendDeal = false;
		WeekendBtnsShop.SetActive (false);


		for (int i = 0; i < SetToUse.Length; i++) {
			SetToUse[i].SetActive (false);
		}

		WeekendDealsHandlerObj = GameObject.Find ("WeekendDealsHandler").GetComponent<WeekendDealsHandler> ();
		for (int i = 0; i < WeekendPanels.Length; i++) {
			WeekendPanels[i].SetActive (false);
			WeekendBtns[i].SetActive (false);
		}
		OnHideButtons ();
		if (WeekendDealsHandlerObj.hasData) {
			OnUpdateData ();
			CalculateTimeLeft ();
		}
	}


	public void OnUpdateData(){


		for (int i = 0; i < itemDealsPortrait.Length; i++) {
			string goodsString = "";
			itemDealsPortrait [i].transform.GetChild (1).GetComponentInChildren<TextMeshProUGUI> ().text = "";
			if (i == 0) {
				goodsString = GEtNumberNoDecimal (WeekendDealsHandlerObj.DealsAmounts[i]) ;

			} else if (i == 2) {
				goodsString = "x" +WeekendDealsHandlerObj.DealsAmounts[i];
			}else if (i == 3) {
				goodsString = WeekendDealsHandlerObj.DealsAmounts[i] +" Days ";
			} else {
				goodsString = WeekendDealsHandlerObj.DealsAmounts[i].ToString ();
			}
			goodsString += " "+WeekendDealsHandlerObj.DealsNames [i];

			itemDealsPortrait [i].transform.GetChild (1).GetComponent<Image> ().sprite = WeekendDealsHandlerObj.IconScript [i];
			itemDealsPortrait [i].transform.GetChild (2).GetComponent<TextMeshProUGUI> ().text =goodsString;
			if (WeekendDealsHandlerObj.DealsAmounts[i] == 0) {
				itemDealsPortrait [i].SetActive (false);
			} else {

			}
		}

		int numItemAvailble = 0;
		List <string> amountString = new List<string>();
		List <Sprite> useSpriteIcon = new List<Sprite>();

		for (int i = 0; i < WeekendDealsHandlerObj.DealsAmounts.Length; i++) {
			if (WeekendDealsHandlerObj.DealsAmounts [i] != 0) {
				numItemAvailble++;
				string goodsString = "";
				if (i == 0) {
					if (WeekendDealsHandlerObj.EventInput == "Tuesday" || WeekendDealsHandlerObj.EventInput == "Friday") {
						goodsString = "10B" ;
					} else {
						goodsString = "1B" ;
					}
				} else if (i == 2) {
					goodsString = "x" +WeekendDealsHandlerObj.DealsAmounts[i];
				}else if (i == 3) {
					goodsString = WeekendDealsHandlerObj.DealsAmounts[i] +"Days";
				} else {
					goodsString = WeekendDealsHandlerObj.DealsAmounts[i].ToString ();
				}
				amountString.Add (goodsString);
				useSpriteIcon.Add (WeekendDealsHandlerObj.IconScript [i]);
//				Debug.Log (WeekendDealsHandlerObj.DealsAmounts [i].ToString ());
			} else {
				
			}
		}
//		Debug.Log (numItemAvailble + " items");

		for (int i = 0; i < SetToUse.Length; i++) {
			SetToUse[i].SetActive (false);
		}

		int numSet = 0;
		if (numItemAvailble == 2) {
			numSet = 0;
		}else if (numItemAvailble == 3) {
			numSet = 1;
		}else if (numItemAvailble == 4) {
			numSet = 2;
		}
		SetToUse [numSet].SetActive (true);

		for (int i = 0; i < SetToUse[numSet].transform.childCount; i++) {
//			Debug.Log(SetToUse[numSet].transform.GetChild (i).name);
			SetToUse[numSet].transform.GetChild (i).transform.Find("Icon").GetComponent<Image> ().sprite = useSpriteIcon [i];
			SetToUse[numSet].transform.GetChild (i).transform.Find("Amount").GetComponent<TextMeshProUGUI> ().text = amountString [i];
		}


//		Debug.Log (numItemAvailble + "Number items");


	}

	public void OnUpdatePriceAmount(){
		string priceAmount = "";
		if (GameObject.Find ("GameTimer").GetComponent<IAP_Script> ().OnLoadedAMount){
			for (int i = 0; i < GameObject.Find ("GameTimer").GetComponent<IAP_Script> ().WeekendDealIDs.Length; i++) {
				if (WeekendDealsHandlerObj.bundleId == GameObject.Find ("GameTimer").GetComponent<IAP_Script> ().WeekendDealIDs [i]) {
					priceAmount = GameObject.Find ("GameTimer").GetComponent<IAP_Script> ().WeekendDealCurrency [i];
					break;
				}
			}
		}

		for (int i = 0; i < IapPricesAmount.Length; i++) {
			IapPricesAmount[i].text = "For Only "+priceAmount;
		}
		OnShowButtons ();
//		IdleState = true;
		CalculateTimeLeft ();
	}

	void OnShowButtons(){

		WeekendBtnsShop.SetActive (true);
		for (int i = 0; i < WeekendBtns.Length; i++) {
			WeekendBtns[i].SetActive (true);
		}
		GameObject.Find("ShopManager").GetComponent<ShopManager>().ShopSpaceObj.SetActive (false);
	}

	public void OnHideButtons(){
		WeekendBtnsShop.SetActive (false);
		for (int i = 0; i < WeekendBtns.Length; i++) {
			WeekendBtns[i].SetActive (false);
		}
	}

	public void OnBuy(){
		MessageDispatcher.SendMessage(this, "OnTapGA_Report","WeekendDealBuyBtn", 0);
		string activeBundleID = WeekendDealsHandlerObj.bundleId;
		MessageDispatcher.SendMessage(this, "OnBuyWeekendDeal",activeBundleID, 0);

//		MessageDispatcher.AddListener ("OnHiremanagerFromAdvisor", OnHiremanagerFromAdvisor, true); 
//		WeekendDealsHandlerObj.GetComponent<WeekendDeals_IapScript> ().BuyConsumable ();
	}

	string[] prefix = new string[]{"Hundred","Thousand","Million","Billion","Trillion","Quadrillion","Quintillion","Sextillion","Septillion","Octillion","Nonillion","Decillion","Undecillion","Duodecillion","Tredecillion","Quattuordecillion","Quindecillion","Sexdecillion","Septendecillion","Octodecillion","Novemdecillion","Vigintillion","Unvigintillion","Duovigintillion","Tresvigintillion","Quattuorvigintillion","Quinvigintillion","Sexvigintillion","Septenvigintillion","Octovigintillion","Novemvigintillion","Trigintillion","Untrigintillion","Duotrigintillion","Tretrigintillion","Quattuortrigintillion","Quintrigintillion","Sextrigintillion","Septentrigintillion","Octotrigintillion","Novemtrigintillion","Quadragintillion","Unquadragintillion","Duoquadragintillion","Trequadragintillion","Quattuorquadragintillion","Quinquadragintillion","Sexquadragintillion","Septquadragintillion","Octoquadragintillion","Novemquadragintillion","Quinquagintillion","Unquinquagintillion","Duoquinquagintillion","Trequinquagintillion","Quattuorquinquagintillion","Quinquinquagintillion","Sexquinquagintillion","Septquinquagintillion","Octoquinquagintillion","Novemquinquagintillion","Sexagintillion","Unsexagintillion","Duosexagintillion","Tresexagintillion","Quattuorsexagintillion","Quinsexagintillion","Sexsexagintillion","Septsexagintillion","Octosexagintillion","Novemsexagintillion","Septuagintillion","Unseptuagintillion","Duoseptuagintillion","Treseptuagintillion","Quattuorseptuagintillion","Quinseptuagintillion","Sexseptuagintillion","Septseptuagintillion","Octoseptuagintillion","Novemseptuagintillion","Octogintillion","Unoctogintillion","Duooctogintillion","Treoctogintillion","Quattuoroctogintillion","Quinoctogintillion","Sexoctogintillion","Septoctogintillion","Octooctogintillion","Novemoctogintillion","Nonagintillion","Unnonagintillion","Duononagintillion","Trenonagintillion","Quattuornonagintillion","Quinnonagintillion","Sexnonagintillion","Septnonagintillion","Onctononagintillion","Novemnonagintillion","Centillion","Uncentillion"};

	string GEtNumberNoDecimal (double value)
	{
		string stringformat = "";
		double newMoneyAmount = 0;
		string moneySuffix = ""; 
		double numPow = 0;
		if (value > 999999) {
			while (value >= Math.Pow (1000, numPow)) {
				moneySuffix = " " + prefix [(int)numPow];
				newMoneyAmount = (double)value / Math.Pow (1000, numPow);
				if ((Math.Round (newMoneyAmount) - newMoneyAmount) != 0) {
					stringformat = ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));
				} else {
					stringformat = ((string.Format ("{0:n0}", newMoneyAmount) + moneySuffix));
				}

				numPow++;

			}
		} else {
			if ((Math.Round (newMoneyAmount) - newMoneyAmount) != 0) {
				stringformat = ((string.Format ("{0:n2}", value)));
			} else {
				stringformat = ((string.Format ("{0:n0}", value)));
			}

		}
		return stringformat;
	}

	public void OnShowWeekendDeal(string fromTap){
		MessageDispatcher.SendMessage(this, "OnTapGA_Report","WeekendDealPopupOpen_"+fromTap, 0);
		for (int i = 0; i < WeekendPanels.Length; i++) {
			WeekendPanels[i].SetActive (true);
		}

		OnHideButtons ();
		CalculateTimeLeft ();
		OnOpenWeekendDeal = WeekendDealsHandlerObj.StartTime;

	}
	public void OnCloseWeekendDeal(){
		MessageDispatcher.SendMessage(this, "OnTapGA_Report","WeekendDealPopupClose", 0);
		for (int i = 0; i < WeekendPanels.Length; i++) {
			WeekendPanels[i].SetActive (false);
		}

		OnShowButtons ();
//		IdleState = true;
//		timeIdleTemp = timeIdle;
		OnOpenWeekendDeal = false;
	}

	public void OnWeekendIdleBtn(){
//		IdleState = true;
//		timeIdleTemp = timeIdle;
//		weekendIdleBtn.SetActive (false);
		OnShowButtons ();
	}

	void Update ()
	{
		if (WeekendDealsHandlerObj.StartTime) {
			
				TimeCounter = WeekendDealsHandlerObj.timerWeekend;
				System.TimeSpan t = System.TimeSpan.FromSeconds (TimeCounter);
				

				if (TimeCounter < 0) {
					TimeCounter = 0;
					GameObject.Find("ShopManager").GetComponent<ShopManager>().ShopSpaceObj.SetActive (true);
					OnHideButtons ();
					for (int i = 0; i < WeekendPanels.Length; i++) {
						WeekendPanels[i].SetActive (false);
					}
//					weekendIdleBtn.SetActive (false);
//					IdleState = false;
//					timeIdleTemp = timeIdle;
				}

//				if (IdleState) {
//					timeIdleTemp -= Time.deltaTime;;
//					if (timeIdleTemp < 0) {
//						IdleState = false;
//						timeIdleTemp = timeIdle;
//						
//						weekendIdleBtn.SetActive (true);
//
//					}
//				}

			if (OnOpenWeekendDeal) {
				timerText.text = ((int)t.TotalHours).ToString ("d2") + ":" + t.Minutes.ToString ("d2") + ":" + t.Seconds.ToString ("d2");//string.Format ("{0:D2}:{1:D2}:{2:D2}", 0, 0, 0);
			}
		}

//		if (WeekendDealsHandlerObj.StartTime) {
//			TimeCounter = WeekendDealsHandlerObj.timerWeekend;
//			System.TimeSpan t = System.TimeSpan.FromSeconds (TimeCounter);
//			for (int i = 0; i < WeekendBtns.Length; i++) {
//				WeekendBtns [i].transform.GetChild (1).gameObject.GetComponent<TextMeshProUGUI> ().text = ((int)t.TotalHours).ToString ("d2") + ":" + t.Minutes.ToString ("d2") + ":" + t.Seconds.ToString ("d2");
//			}
//		}
	}

	void OnApplicationQuit ()
	{


	}

	void OnApplicationPause (bool pauseStatus)
	{

		if (pauseStatus == true) {


		} else if (pauseStatus == false) {
			CalculateTimeLeft ();
		}

	}

	void CalculateTimeLeft(){

		DateTime currentDate = System.DateTime.Now;
		//		DayOfWeek day = DateTime.Now.DayOfWeek;
		//		if (day.ToString () == DayOfWeek.Saturday.ToString ()) {
		//			timerWeekend = 172800;
		//		}else if (day.ToString () == DayOfWeek.Sunday.ToString ()) {
		//			timerWeekend = 86400;
		//
		//		}
		//		TimeSpan difference = currentDate.Subtract (System.DateTime.Today);
		//		float secondsInt = Convert.ToInt32 (difference.TotalSeconds);
		//		TimeCounter = timerWeekend - secondsInt;

	}

}
