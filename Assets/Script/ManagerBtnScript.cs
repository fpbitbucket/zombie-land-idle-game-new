﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;
using TMPro;
using com.ootii.Messages;
public class ManagerBtnScript : MonoBehaviour {
	public MainGeneratorScript mainGen;
	public MoneyManager MoneyManagerObj;
	public InvestorManager investorManagerObj;
	public Sprite[] mangerIcon;
	public string[] nameString;
	public string[] nameManagerString;
	public string[] nameManagerStringDiscript = new string[]{"Cave Johnson","Perry White","Walter 'Heisenberg' White","Superman","Papa John's","Tim Hortons","Jim Treliving","Forrest Gump","Donald Trump",",Don Cherry","Steven Spielberg"};
	public double[] AmountManager;

	public GameObject[] ManagerObjPort,ManagerObjPortAngel;


	public string[] nameStringAngel = new string[] {"Space","Savings","Debit","Insurance","Credit","Personal","Housing","Cars","Bonds","Stocks"};
	public string[] nameManagerStringAngel = new string[] {"Ebenezer Rockerfeller","Darky McGrumpface","Jenn Catsburger","Dwayne Greatsky","Cpt Janeway Krunch","Homer Sprinkles","P.J. Fries","Gus Pollos","Milly Bayes","Gladys Caroline"};
	public double[] AmountManagerAngel3 = new double[]{10 ,100 ,1000 ,9999 ,100000 ,1000000 ,10000000 ,100000000 ,1000000000 ,10000000000 };
	public string[] nameManagerStringDiscriptAngel = new string[]{"Ebenezer Scrooge (A Christmas Carol); Rockefeller family","Grumpy Cat","Jeffrey Katzenberg (CEO of Dreamworks), Shrek (Shrek)","Wayne Gretzky","Cap'n Crunch cereal, Captain Kathryn Janeway (Star Trek: Voyager)","Homer Simpson, Homer the Greek Poet (Author of the Iliad and the Odyssey)","Phillip J. Fry (Futurama)","Gustavo Fring (Breaking Bad), Pollos in spanish means chicken and there is a fast-food restaurant in Breaking Bad called Los Pollos Hermanos with chicken in logo","Billy Mays (TV commercials)","GLaDOS/Caroline (Portal 2)"};
	private int numManager,numManagerAngel;
	private int notifynum;

	public Color availColor;
	public Color notavailColor;

	void OnEnable(){
		nameString = mainGen.genNames_Code;
		if (ES2.Exists ("TotalManagers_" + MoneyManagerObj.worldName)) {
			numManager = ES2.Load<int> ("TotalManagers_" + MoneyManagerObj.worldName);
		} else {
			numManager = 0;
			ES2.Save(numManager, "TotalManagers_" + MoneyManagerObj.worldName);
		}

		if (ES2.Exists ("TotalManagersAngel_" + MoneyManagerObj.worldName)) {
			numManagerAngel = ES2.Load<int> ("TotalManagersAngel_" + MoneyManagerObj.worldName);
		} else {
			numManagerAngel = 0;
			ES2.Save(numManagerAngel, "TotalManagersAngel_" + MoneyManagerObj.worldName);
		}

		for (int i = 0; i < ManagerObjPort.Length; i++) {
			int tempInt = i;
			ManagerObjPort [i].transform.Find ("ItemBtn").GetComponent<Button> ().onClick.AddListener(() => { OnBuyManager(tempInt); });
			nameManagerStringDiscript [i] = "Manage " + mainGen.genNamesString [i];
		}

		int num = mainGen.genNamesString.Length-1;
		for (int i = 0; i < ManagerObjPortAngel.Length; i++) {
			int tempInt = i;
			ManagerObjPortAngel [i].transform.Find ("ItemBtn").GetComponent<Button> ().onClick.AddListener(() => { OnBuyManageraAngel(tempInt); });
			if (MoneyManagerObj.worldName == "Bitcoin"){
				nameStringAngel [i] = mainGen.genNames_Code [num - i];
			}
		}
			
		OnSetupManager ();
		StartCoroutine (CheckBuyManager ());
		MessageDispatcher.AddListener ("OnResetTotalMoney", OnResetMoney, true); 
		MessageDispatcher.AddListener ("OnHiremanagerFromAdvisor", OnHiremanagerFromAdvisor, true); 
	}

	void OnDisable(){
		MessageDispatcher.RemoveListener ("OnResetTotalMoney", OnResetMoney, true); 
		MessageDispatcher.RemoveListener ("OnHiremanagerFromAdvisor", OnHiremanagerFromAdvisor, true); 
		
	}


	private void OnSetupManager(){
		for (int i = 0; i < ManagerObjPort.Length; i++) {
			if (ES2.Exists ("ManagerHire_Manager_upNum_" + MoneyManagerObj.worldName+ "_" + i.ToString())) {
				ManagerObjPort [i].SetActive (false);
			} else {
				ManagerObjPort [i].SetActive (true);
			}

			ManagerObjPort [i].transform.Find ("ItemBgGrp").transform.Find ("Icon-Manager").GetComponent<Image> ().sprite = mangerIcon [i];
			ManagerObjPort [i].transform.Find ("ItemBgGrp").transform.Find ("Text_Title").GetComponent<TextMeshProUGUI> ().text = nameManagerString[i]; //Name of Upgrade
			ManagerObjPort [i].transform.Find ("ItemBgGrp").transform.Find ("Text_Discript").GetComponent<TextMeshProUGUI> ().text = nameManagerStringDiscript[i];
			ManagerObjPort [i].transform.Find ("ItemBgGrp").transform.Find ("Text_Amount").GetComponent<TextMeshProUGUI> ().text = MoneyManagerObj.GetStringForValue(AmountManager[i]) + " Pills"; //Amount of Upgrade
		}


		for (int i = 0; i < ManagerObjPortAngel.Length; i++) {
			
			if (ES2.Exists ("ManagerHire_Angle_upNum_" + MoneyManagerObj.worldName+ "_" + i.ToString())) {
				ManagerObjPortAngel [i].SetActive (false);
			} else {
				ManagerObjPortAngel [i].SetActive (true);
			}

//			if (Application.loadedLevelName == "Banker") {
//				ManagerObjPortAngel [i].SetActive (false);
//			}
//
//			if (nameStringAngel [i] == "Investor" || nameStringAngel [i] == "All") {
//				ManagerObjPortAngel [i].transform.Find ("Icon").transform.Find ("Icon").GetComponent<Image> ().sprite = mangerIcon [10];
//			} else if (nameStringAngel [i] == "QuickBuy") {
//				ManagerObjPortAngel [i].transform.Find ("Icon").transform.Find ("Icon").GetComponent<Image> ().sprite = mangerIcon [12];
//			}else {
//				ManagerObjPortAngel [i].transform.Find ("Icon").transform.Find ("Icon").GetComponent<Image> ().sprite  = mangerIcon[10+i];
//			}


			ManagerObjPortAngel [i].transform.Find ("ItemBgGrp").transform.Find ("Icon-Manager").GetComponent<Image> ().sprite  = mangerIcon[10+i];
			ManagerObjPortAngel [i].transform.Find ("ItemBgGrp").transform.Find ("Text_Title").GetComponent<TextMeshProUGUI> ().text = nameManagerStringAngel[i]; //Name of Upgrade
			ManagerObjPortAngel [i].transform.Find ("ItemBgGrp").transform.Find ("Text_Amount").GetComponent<TextMeshProUGUI> ().text = MoneyManagerObj.GetStringForValueNumbers(AmountManagerAngel3[i]) + " Brains"; //Amount of Upgrade
			ManagerObjPortAngel [i].transform.Find ("ItemBgGrp").transform.Find ("Text_Discript").GetComponent<TextMeshProUGUI> ().text = nameManagerStringDiscriptAngel[i];
		}
	}

	void OnResetMoney (IMessage rMessage){ 
		numManager = 0;
		ES2.Save(numManager, "TotalManagers_" + MoneyManagerObj.worldName);

		numManagerAngel = 0;
		ES2.Save(numManagerAngel, "TotalManagersAngel_" + MoneyManagerObj.worldName);

		for (int i = 0; i < ManagerObjPort.Length; i++) {
			if (ES2.Exists ("ManagerHire_Manager_upNum_" + MoneyManagerObj.worldName+ "_" + i.ToString())) {
				ES2.Delete ("ManagerHire_Manager_upNum_" + MoneyManagerObj.worldName+ "_" + i.ToString());
			} 
		}


		for (int i = 0; i < ManagerObjPortAngel.Length; i++) {

			if (ES2.Exists ("ManagerHire_Angle_upNum_" + MoneyManagerObj.worldName+ "_" + i.ToString())) {
				ES2.Delete ("ManagerHire_Angle_upNum_" + MoneyManagerObj.worldName+ "_" + i.ToString());
			} 
		}
		OnSetupManager ();
	}

	bool tutorialbool = false;

	IEnumerator CheckBuyManager(){
		while (true) {
			yield return new WaitForSeconds (0.5f);
			double tempMoney = MoneyManagerObj.totalMoney;
			int numTotalManager = ManagerObjPort.Length - 1;
			bool checker = false;
			for (int i = 0; i < ManagerObjPort.Length; i++) {
				if (tempMoney >= AmountManager[i] && ES2.Exists ("ManagerHire_Manager_upNum_" + MoneyManagerObj.worldName+ "_" + i.ToString()) == false) {
					checker = true;
					if (ManagerObjPort [i].transform.Find ("ItemBtn").GetComponent<Image> ().color == notavailColor && ManagerObjPort [i].activeSelf == true) {
						MessageDispatcher.SendMessage(this, "OnTriggerMenuNotifyOn",1, 0);
					}

					ManagerObjPort[i].transform.Find ("ItemBtn").GetComponent<Button> ().enabled = true;
					ManagerObjPort [i].transform.Find ("ItemBtn").GetComponent<Image> ().color = availColor;

					if (tutorialbool == false) {
						OnShowTutorial ();
					}
				} else {
					ManagerObjPort[i].transform.Find ("ItemBtn").GetComponent<Button> ().enabled = false;
					ManagerObjPort [i].transform.Find ("ItemBtn").GetComponent<Image> ().color = notavailColor;
//					break;
				}
			}

			double tempAngel = investorManagerObj.TotalAngel;
			if (Application.loadedLevelName != "Banker") {
				for (int i = 0; i < ManagerObjPortAngel.Length; i++) {
					if (tempAngel >= AmountManagerAngel3 [i] && ES2.Exists ("ManagerHire_Angle_upNum_" + MoneyManagerObj.worldName + "_" + i.ToString ()) == false) {
						checker = true;
						if (ManagerObjPortAngel [i].transform.Find ("ItemBtn").GetComponent<Image> ().color == notavailColor && ManagerObjPortAngel [i].activeSelf == true) {
							MessageDispatcher.SendMessage (this, "OnTriggerMenuNotifyOn", 1, 0);
						}

						ManagerObjPortAngel[i].transform.Find ("ItemBtn").GetComponent<Button> ().enabled = true;
						ManagerObjPortAngel [i].transform.Find ("ItemBtn").GetComponent<Image> ().color = availColor;
					} else {
						ManagerObjPortAngel[i].transform.Find ("ItemBtn").GetComponent<Button> ().enabled = false;
						ManagerObjPortAngel [i].transform.Find ("ItemBtn").GetComponent<Image> ().color = notavailColor;
					}
				}
			}
			if (checker == false) {
				MessageDispatcher.SendMessage(this, "OnTriggerMenuNotifyOff",1, 0);
			}
			yield return true;
		}
	}



	private void OnShowTutorial(){
		MessageDispatcher.SendMessage(this, "OnShowTutorial",3, 0);
	}


	public void OnBuyManager(int numbtn){
		if (MoneyManagerObj.totalMoney >= AmountManager [numbtn]) {
			MessageDispatcher.SendMessage(this, "OnTapGA_Report","BuyManagerHasCash", 0);
			MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);

			ES2.Save (1, "ManagerHire_Manager_upNum_" + MoneyManagerObj.worldName + "_" + numbtn.ToString ());
			ManagerObjPort [numbtn].SetActive (false);

			if (numbtn == 0) {
				MessageDispatcher.SendMessage (this, "OnShowTutorial", 8, 0);
			}

			numManager++;
			ES2.Save (numManager, "TotalManagers_" + MoneyManagerObj.worldName);
			MessageDispatcher.SendMessage (this, "OnDicrementMoney", AmountManager [numbtn], 0);
			MessageDispatcher.SendMessage (this, "OnHireManager" + nameString [numbtn], "HireManager", 0);
		} else {
			MessageDispatcher.SendMessage(this, "OnTapGA_Report","BuyManagerNoCash", 0);
			MessageDispatcher.SendMessage (this, "PopButton", 2, 0f);
		}
	}

	public void OnBuyManageraAngel(int numbtn){
		if (investorManagerObj.TotalAngel >= AmountManagerAngel3 [numbtn]) {
			MessageDispatcher.SendMessage(this, "OnTapGA_Report","BuyAccountantHasCash", 0);
			MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
			ES2.Save (1, "ManagerHire_Angle_upNum_" + MoneyManagerObj.worldName + "_" + numbtn.ToString ());
			ManagerObjPortAngel [numbtn].SetActive (false);
			numManagerAngel++;
			ES2.Save (numManagerAngel, "TotalManagersAngel");
			MessageDispatcher.SendMessage (this, "OnDicrementAngel", AmountManagerAngel3 [numbtn], 0);
//			Debug.Log (nameStringAngel [numbtn] + " aaa");
			if (nameStringAngel [numbtn] == "QuickBuy") {
				MessageDispatcher.SendMessage (this, "OnShowQuickBuyUpgrades", 0, 0);
			} else if (nameStringAngel [numbtn] == "EzBuy") {
				MessageDispatcher.SendMessage (this, "OnShowEzBuyUpgrades", 0, 0);
			} else {
				MessageDispatcher.SendMessage (this, "OnHireAccountant" + nameStringAngel [numbtn], "HireManager", 0);
			}
		} else {
			MessageDispatcher.SendMessage(this, "OnTapGA_Report","BuyAccountantNoCash", 0);
			MessageDispatcher.SendMessage (this, "PopButton", 2, 0f);
		}

	}


	void OnHiremanagerFromAdvisor (IMessage rMessage){
		int numManager = (int)rMessage.Data;
		OnBuyManager (numManager);

	}

}
