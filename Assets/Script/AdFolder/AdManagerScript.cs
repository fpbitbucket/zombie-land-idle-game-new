﻿using UnityEngine;
using System.Collections;
using com.ootii.Messages;
//using SA.UltimateAds;
using TMPro;
public class AdManagerScript : MonoBehaviour {
	public GameObject NoAdsAvailable_PorTrait;
	public TextMeshProUGUI[] Message_text, Tittle_text;
//	public int AdmobWeight;
//	public int UnityWeight;

	private string adNameUsed;
	public bool ifHasAdsAvailable = false;
	void OnEnable () {
		NoAdsAvailable_PorTrait.SetActive (false);
	

	}
	
	void Start(){
		StartCoroutine(OnCheckAdsStatus());
	}

	public bool isAvailableAds(){
		ifHasAdsAvailable = false;
		bool checkNetwork = false;
		bool canCheckADs = GameObject.Find ("GameTimer").GetComponent<AdController> ().playingAds;
		if (canCheckADs == false) {
			if (GameObject.Find ("GameTimer")) {
				checkNetwork = GameObject.Find ("GameTimer").GetComponent<NetWorkCheck> ().networkState;
			}
			if (checkNetwork) {
				if (GameObject.Find ("GameTimer")) {
					ifHasAdsAvailable = GameObject.Find ("GameTimer").GetComponent<AdController> ().OnCheckAds ();
				}
			}
		}
		return ifHasAdsAvailable;
	}

	IEnumerator OnCheckAdsStatus(){
		while (true) {
			Debug.Log("Checking Ads");
			isAvailableAds();
			MessageDispatcher.SendMessage(this, "OnAdUpdate", 0, 0);
			yield return new WaitForSeconds (3);
		}
	}



	public void OnCloseNoAdsAvail(){
		MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
		NoAdsAvailable_PorTrait.SetActive (false);
	}


	public void OnWatchAd(string adName){
		//		adNameUsed = adName;
		MessageDispatcher.SendMessage (this, "OnCloseTutorial", 10, 0);

	
		if (adName == "offline") {
			MessageDispatcher.SendMessage(this, "OnTapGA_Report","OfflineDoubleWatchBtn", 0);
		} else if (adName == "freeReward") {
			MessageDispatcher.SendMessage(this, "OnTapGA_Report","FreeRewardWatchBtn", 0);
		} else if (adName == "proTip") {
			MessageDispatcher.SendMessage(this, "OnTapGA_Report","ProfessorTipWatchAds", 0);
		} else if (adName == "LauchAds") {
			MessageDispatcher.SendMessage(this, "OnTapGA_Report","LauchAdsAdsBtn", 0);
		} else if (adName == "WatchSpin") {
			MessageDispatcher.SendMessage(this, "OnTapGA_Report","WatchSpinAdsBtn", 0);
		} else if (adName == "admultiplier") {
			MessageDispatcher.SendMessage(this, "OnTapGA_Report","WatchAdsBoostBtn", 0);
		}

		#if UNITY_EDITOR
//		//		MessageDispatcher.SendMessage(this, "OnPickAdToShow",adName, 0);
		GameObject.Find ("GameTimer").GetComponent<AdController> ().adNameUsed = adName;
		GameObject.Find ("GameTimer").GetComponent<AdController> ().OnSHowingADS ();
		#else
		bool checkNetwork = false;
		bool canCheckADs = GameObject.Find ("GameTimer").GetComponent<AdController> ().playingAds;
		if (canCheckADs == false) {
			if (GameObject.Find ("Options")) {
				checkNetwork = GameObject.Find ("Options").GetComponent<NetWorkCheck> ().networkState;
			}

			if (checkNetwork == false) {
				Debug.Log ("No Connection");
				Tittle_text [0].text = "Not Connected";
//				Tittle_text [1].text = "Not Connected";

				Message_text [0].text = "Zombie-pocalypse would be achieved if connected to the internet";
//				Message_text [1].text = "Capitalism runs more smoothly when connected to the world wide web!";

				NoAdsAvailable_PorTrait.SetActive (true);
				MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
			} else {
				bool adCheck = false;
				if (GameObject.Find ("GameTimer")) {
					adCheck = GameObject.Find ("GameTimer").GetComponent<AdController> ().OnCheckAds ();
				}

				if (adCheck == false) {
					Tittle_text [0].text = "No Ads Available!";
//					Tittle_text [1].text = "No Ads Available!";

					Message_text [0].text = "Sorry No Ads Available right now..Try again later";
//					Message_text [1].text = "Sorry No Ads Available right now..Try again later";

					NoAdsAvailable_PorTrait.SetActive (true);
					MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
				} else {
					MessageDispatcher.SendMessage (this, "OnPickAdToShow", adName, 0);
				}
			}
		}
		#endif
	}
}
