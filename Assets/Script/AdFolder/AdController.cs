﻿using System.Collections;
using System.Collections.Generic;
using com.ootii.Messages;
using UnityEngine;
using AppAdvisory.Ads;

//using SA.UltimateAds;
public class AdController : MonoBehaviour
{

	public string adNameUsed;
	string nameAdAvailable = "";

	//Vungle
	public string Vungle_iOSAppID = "587c7257205150140400048b";
	public string Vungle_androidAppID = "587c72983fd4073438000536";
	public string Vungle_iOSPlacement = "REWARDA94710";
	public string Vungle_androidPlacement = "REWARDA22922";
	Dictionary<string, bool> placements;
	List<string> placementIdList;
	string appID = "";
	string appPlacementID = "";

	//AppLovin
	public string AppLovinSDKKey = "4Ecd3GndLvdhEo_Z2r5RgE9R_8VFs0zxFZZIrKxADTsSLs0Ng6qMKzoK6IayJXuT6Rsyno6_LzRgqS3VZttvrf";

	public bool playingAds = false;

	void OnEnable ()
	{

		playingAds = false;
#if UNITY_IPHONE
		appID = Vungle_iOSAppID;
		appPlacementID = Vungle_iOSPlacement;
#elif UNITY_ANDROID
		appID = Vungle_androidAppID;
		appPlacementID = Vungle_androidPlacement;
#endif

		placements = new Dictionary<string, bool> {
			{ appPlacementID, false },
		};

		placementIdList = new List<string> (placements.Keys);

		AdsManager.OnVideoRewardedClosed += OnVideoInterstitialClosed;

		AppLovin.SetSdkKey (AppLovinSDKKey);
		AppLovin.InitializeSdk ();
		AppLovin.LoadRewardedInterstitial ();
		AppLovin.SetUnityAdListener (gameObject.name);

		string[] array = new string[placements.Keys.Count];

		placements.Keys.CopyTo (array, 0);
		Vungle.init (appID, array);

		initializeEventHandlers ();

		MessageDispatcher.AddListener ("OnAdFailToShow", OnAdFailToShow, true);
		MessageDispatcher.AddListener ("OnPickAdToShow", OnPickAdToShow, true);
		MessageDispatcher.AddListener ("OnAdDoneShow", OnAdDoneShow, true);
		MessageDispatcher.AddListener ("OnUnityShowAds", OnShowingAds, true);
	}

	void OnDisable ()
	{
		MessageDispatcher.RemoveListener ("OnAdFailToShow", OnAdFailToShow, true);
		MessageDispatcher.RemoveListener ("OnPickAdToShow", OnPickAdToShow, true);
		MessageDispatcher.RemoveListener ("OnAdDoneShow", OnAdDoneShow, true);
		MessageDispatcher.RemoveListener ("OnUnityShowAds", OnShowingAds, true);



	}

	void OnVideoInterstitialClosed ()
	{
		//		Debug.Log ("OnVideoInterstitialClosed");

	}

	private void OnRewardedUser (string itemids, int num)
	{
		//pausing the game
		//		Debug.Log ("On Done reward ads");
		MessageDispatcher.SendMessage (this, "OnAdDoneShow", 0, 0);
	}

	public void OnSHowingADS ()
	{

		Debug.Log (adNameUsed);
		string[] dataString;

		if (adNameUsed == "admultiplier") {
			MessageDispatcher.SendMessage (this, "OnUseAdsLimit", 0, 0);
		}

		if (adNameUsed == "offline") {
			MessageDispatcher.SendMessage (this, "OnDoubleScreen", 0, 1);
			dataString = new string[] { "OnDoneWatching", "Double Earining Offline" };
			MessageDispatcher.SendMessage (this, "OnGA_Report_Ads", "Tap:doubleEarningOfflinebtn", 0);
		} else if (adNameUsed == "freeReward") {
			MessageDispatcher.SendMessage (this, "OnShowReward", 0, 1);
			dataString = new string[] { "OnDoneWatching", "Free Reward" };
			MessageDispatcher.SendMessage (this, "OnGA_Report_Ads", "Tap:rewardbtn", 0);
		} else if (adNameUsed == "proTip") {
			MessageDispatcher.SendMessage (this, "OnShowAdvisor", 1, 1.5f);
			dataString = new string[] { "OnDoneWatching", "Adviser" };
			MessageDispatcher.SendMessage (this, "OnGA_Report_Ads", "Tap:adviserbtn", 0);
		} else if (adNameUsed == "LauchAds") {
			MessageDispatcher.SendMessage (this, "OnLaunchAds", 0, 1);
			dataString = new string[] { "OnDoneWatching", "Business Time Reduce" };
			MessageDispatcher.SendMessage (this, "OnGA_Report_Ads", "Tap:launchbtn", 0);
		} else if (adNameUsed == "WatchSpin") {
			MessageDispatcher.SendMessage (this, "OnWatchAndSpin", 0, 1);
			dataString = new string[] { "OnDoneWatching", "Watch and Spin" };
			MessageDispatcher.SendMessage (this, "OnGA_Report_Ads", "Tap:SpinBtn", 0);
		} else if (adNameUsed == "WatchSpinAddtime") {
			MessageDispatcher.SendMessage (this, "OnWatchAndSpinAddTime", 0, 1);
			dataString = new string[]{ "OnDoneWatching", "Watch Add spin Time" };
			MessageDispatcher.SendMessage (this, "OnGA_Report_Ads", "Tap:SpinTimeBtn", 0);
		} else if (adNameUsed == "BoostExploration") {
			MessageDispatcher.SendMessage (this, "OnDoneWatchingBoost", 0, 1);
			dataString = new string[] { "OnDoneWatching", "Watch Boost Eploration" };
			MessageDispatcher.SendMessage (this, "OnGA_Report_Ads", "Tap:BoostEploration", 0);
		} else if (adNameUsed == "WatchSpecialUfoOffer") {
			MessageDispatcher.SendMessage (this, "OnDoneWatchSpecialUfoOffer", 0, 1);
			dataString = new string[] { "OnDoneWatching", "WatchSpecialUfoOffer" };
			MessageDispatcher.SendMessage (this, "OnGA_Report_Ads", "Tap:WatchSpecialUfoOffer", 0);
		} else {
			MessageDispatcher.SendMessage (this, "OnStartAdMultiplierTime", 0, 1);
			dataString = new string[] { "OnDoneWatching", "Double Multiplier" };
			MessageDispatcher.SendMessage (this, "OnGA_Report_Ads", "Tap:x2btn", 0);

			MessageDispatcher.SendMessage (this, "OnShowTutorial", 11, 0.5f);
			MessageDispatcher.SendMessage (this, "OnCloseTutorial", 11, 5);
		}
		playingAds = false;
		MessageDispatcher.SendMessage (this, "OnMusicSuspend", 2, 1.5f);
		MessageDispatcher.SendMessage (this, "OnSendAnalytics_Unity", dataString, 0);

	}

	void OnAdDoneShow (IMessage rMessage)
	{//
		Debug.Log (adNameUsed);
		string[] dataString;
		if (adNameUsed == "offline") {
			MessageDispatcher.SendMessage (this, "OnDoubleScreen", 0, 1);
			dataString = new string[] { "OnDoneWatching", "Double Earining Offline" };
			MessageDispatcher.SendMessage (this, "OnGA_Report_Ads", "Tap:doubleEarningOfflinebtn", 0);
		} else if (adNameUsed == "freeReward") {
			MessageDispatcher.SendMessage (this, "OnShowReward", 0, 1);
			dataString = new string[] { "OnDoneWatching", "Free Reward" };
			MessageDispatcher.SendMessage (this, "OnGA_Report_Ads", "Tap:rewardbtn", 0);
		} else if (adNameUsed == "proTip") {
			MessageDispatcher.SendMessage (this, "OnShowAdvisor", 1, 1.5f);
			dataString = new string[] { "OnDoneWatching", "Adviser" };
			MessageDispatcher.SendMessage (this, "OnGA_Report_Ads", "Tap:adviserbtn", 0);
		} else if (adNameUsed == "LauchAds") {
			MessageDispatcher.SendMessage (this, "OnLaunchAds", 0, 1);
			dataString = new string[] { "OnDoneWatching", "Business Time Reduce" };
			MessageDispatcher.SendMessage (this, "OnGA_Report_Ads", "Tap:launchbtn", 0);
		} else if (adNameUsed == "WatchSpin") {
			MessageDispatcher.SendMessage (this, "OnWatchAndSpin", 0, 1);
			dataString = new string[] { "OnDoneWatching", "Watch and Spin" };
			MessageDispatcher.SendMessage (this, "OnGA_Report_Ads", "Tap:SpinBtn", 0);
		} else if (adNameUsed == "WatchSpinAddtime") {
			MessageDispatcher.SendMessage (this, "OnWatchAndSpinAddTime", 0, 1);
			dataString = new string[]{ "OnDoneWatching", "Watch Add spin Time" };
			MessageDispatcher.SendMessage (this, "OnGA_Report_Ads", "Tap:SpinTimeBtn", 0);
		} else if (adNameUsed == "BoostExploration") {
			MessageDispatcher.SendMessage (this, "OnDoneWatchingBoost", 0, 1);
			dataString = new string[] { "OnDoneWatching", "Watch Boost Eploration" };
			MessageDispatcher.SendMessage (this, "OnGA_Report_Ads", "Tap:BoostEploration", 0);
		} else if (adNameUsed == "WatchSpecialUfoOffer") {
			MessageDispatcher.SendMessage (this, "OnDoneWatchSpecialUfoOffer", 0, 1);
			dataString = new string[] { "OnDoneWatching", "WatchSpecialUfoOffer" };
			MessageDispatcher.SendMessage (this, "OnGA_Report_Ads", "Tap:WatchSpecialUfoOffer", 0);
		} else {
			MessageDispatcher.SendMessage (this, "OnStartAdMultiplierTime", 0, 1);
			dataString = new string[] { "OnDoneWatching", "Double Multiplier" };
			MessageDispatcher.SendMessage (this, "OnGA_Report_Ads", "Tap:x2btn", 0);

			MessageDispatcher.SendMessage (this, "OnShowTutorial", 11, 0.5f);
			MessageDispatcher.SendMessage (this, "OnCloseTutorial", 11, 5);
		}
		playingAds = false;
		MessageDispatcher.SendMessage (this, "OnMusicSuspend", 2, 1.5f);
		MessageDispatcher.SendMessage (this, "OnSendAnalytics_Unity", dataString, 0);
	}

	void OnPickAdToShow (IMessage rMessage)
	{
		if (playingAds == false) {
			playingAds = true;
			adNameUsed = (string)rMessage.Data;
			MessageDispatcher.SendMessage (this, "OnUnityShowAds", 0, 0);
		}
	}

	void OnShowingAds (IMessage rMessage)
	{
		if (string.IsNullOrEmpty (nameAdAvailable) == false) {
			MessageDispatcher.SendMessage (this, "OnMusicSuspend", 0, 0);
			if (adNameUsed == "admultiplier") {
				MessageDispatcher.SendMessage (this, "OnUseAdsLimit", 0, 0);
			}
			if (nameAdAvailable == "Vungle") {
				MessageDispatcher.SendMessage (this, "VungleRewardShow", 1, 0);
				Vungle.playAd (appPlacementID);
			} else if (nameAdAvailable == "AppLovin") {
				MessageDispatcher.SendMessage (this, "AdLovinRewardShow", 1, 0);
				AppLovin.ShowRewardedInterstitial ();
			} else {
				MessageDispatcher.SendMessage (this, "OnWatchingAds", 0, 0);
				AdsManager.instance.ShowRewardedVideo (Method);
				MessageDispatcher.SendMessage (this, "VerySimpleAdsRewardShow", 1, 0);
			}
		}
		nameAdAvailable = "";
	}


	void Method (bool success)
	{
		if (success) {
			//			Debug.Log ("give a reward to the player ");
			MessageDispatcher.SendMessage (this, "OnAdDoneShow", 0, 0.2f);
		} else {
			//			Debug.Log ("the player do not complete the video");
		}
	}

	public bool OnCheckAds ()
	{
		bool OnCheck = false;
		nameAdAvailable = "";
		//		Debug.Log (AdsManager.instance.IsReadyRewardedVideo () + " Reward ads");
		if (AdsManager.instance.IsReadyRewardedVideo ()) {
			nameAdAvailable = "simpleAds";
			OnCheck = true;
		} else {
			if (placements [appPlacementID]) {
				nameAdAvailable = "Vungle";
				OnCheck = true;
			} else {
				Vungle.loadAd (appPlacementID);
				if (AppLovin.IsIncentInterstitialReady ()) {
					nameAdAvailable = "AppLovin";
					OnCheck = true;
				} else {
					AppLovin.LoadRewardedInterstitial ();
					//					Debug.Log ("No rewarded ad is available.  Perform failover logic...");
				}
			}
		}

		//		#if UNITY_EDITOR
		//		OnCheck = true;
		//		Debug.Log(adNameUsed);
		//		if (adNameUsed == "admultiplier") {
		//			MessageDispatcher.SendMessage (this, "OnUseAdsLimit", 0, 0);
		//		}
		//		MessageDispatcher.SendMessage (this, "OnAdDoneShow", 0, 0.5f);
		//
		//		#else
		//
		//		#endif
		return OnCheck;
	}


	void OnAdFailToShow (IMessage rMessage)
	{
		string nameAd = (string)rMessage.Data;
	}


	// Setup EventHandlers for all available Vungle events
	void initializeEventHandlers ()
	{

		//Event triggered during when an ad is about to be played
		Vungle.onAdStartedEvent += (placementID) => {
			//			Debug.Log ("Ad " + placementID + " is starting!  Pause your game  animation or sound here.");
		};

		//Event is triggered when a Vungle ad finished and provides the entire information about this event
		//These can be used to determine how much of the video the user viewed, if they skipped the ad early, etc.
		Vungle.onAdFinishedEvent += (placementID, args) => {
			//			Debug.Log ("Ad finished - placementID " + placementID + ", was call to action clicked:" + args.WasCallToActionClicked + ", is completed view:"
			//				+ args.IsCompletedView);
			if (args.IsCompletedView) {
				MessageDispatcher.SendMessage (this, "OnAdDoneShow", 0, 0.5f);
				MessageDispatcher.SendMessage (this, "OnGA_Report_Ads", "vungleAdPlayed", 0);
			}
		};

		//Event is triggered when the ad's playable state has been changed
		//It can be used to enable certain functionality only accessible when ad plays are available
		Vungle.adPlayableEvent += (placementID, adPlayable) => {
			//			Debug.Log ("Ad's playable state has been changed! placementID " + placementID + ". Now: " + adPlayable);
			placements [placementID] = adPlayable;
		};

		//Fired log event from sdk
		Vungle.onLogEvent += (log) => {
			//			Debug.Log ("Log: " + log);
		};

		//Fired initialize event from sdk
		Vungle.onInitializeEvent += () => {
			//			Debug.Log ("SDK initialized");
		};
	}

	//AppLovin Event Handler
	void onAppLovinEventReceived (string ev)
	{
		if (ev.Contains ("REWARDAPPROVEDINFO")) {
			//			Debug.Log ("Lovin REWARDAPPROVEDINFO");
			MessageDispatcher.SendMessage (this, "OnAdDoneShow", 0, 0.5f);
			MessageDispatcher.SendMessage (this, "OnGA_Report_Ads", "appLovinAdPlayed", 0);
		} else if (ev.Contains ("LOADEDREWARDED")) {
			//			Debug.Log ("Lovin A rewarded video was successfully loaded.");
		} else if (ev.Contains ("LOADREWARDEDFAILED")) {
			//			Debug.Log ("Lovin A rewarded video failed to load.");

		} else if (ev.Contains ("HIDDENREWARDED")) {
			//			Debug.Log ("Lovin A rewarded video was closed.  Preload the next rewarded video.");
			AppLovin.LoadRewardedInterstitial ();
		}
	}

	void OnApplicationPause (bool pauseStatus)
	{
		if (pauseStatus == true) {

		} else if (pauseStatus == false) {
			//			MessageDispatcher.SendMessage (this, "OnMusicSuspend", 2, 1);
		}
	}
}
