﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;
using com.ootii.Messages;
public class UnityAnalyticsTracker : MonoBehaviour {

	// Use this for initialization
	void OnEnable () {

		MessageDispatcher.AddListener ("OnSendAnalytics_Unity", OnSendAnalytics_Unity, true);

	}

	void OnDisable(){
		MessageDispatcher.RemoveListener ("OnSendAnalytics_Unity", OnSendAnalytics_Unity, true);
		
	}

	void OnSendAnalytics_Unity(IMessage rMessage){
		string[] DataString = (string[])rMessage.Data;

		string nameAction = DataString [0];
		string nameGenerator = DataString [1];

		Analytics.CustomEvent(nameAction+" - "+nameGenerator);
//		Analytics.CustomEvent("nameAction", new Dictionary<string, object>
//			{
//				{ nameGenerator, nameAction }
//			});
		
	}
}
