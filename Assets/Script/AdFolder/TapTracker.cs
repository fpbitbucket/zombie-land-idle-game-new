﻿using UnityEngine;
using System;
using System.Collections;
using com.ootii.Messages;
public class TapTracker : MonoBehaviour {

	public int numTap = 0;

	void Awake () {
		OnCheckReward ();
		MessageDispatcher.AddListener ("OnTapGA_Report", OnReportTap, true);
	}

	void OnDisable(){
		MessageDispatcher.RemoveListener ("OnTapGA_Report", OnReportTap, true);
	}

	void OnReportTap(IMessage rMessage){
		string temp = (string)rMessage.Data; 
		numTap++;
		ES2.Save (numTap, "numTapped");
		MessageDispatcher.SendMessage (this, "OnGA_Report", "TrackingTap:"+temp+""+numTap.ToString(), 0);

	}

	private void OnCheckReward(){
		if (ES2.Exists ("TapDate")) {
			string stringDate = ES2.Load<string> ("TapDate");
			DateTime oldDate = Convert.ToDateTime (stringDate);
			DateTime newDate = System.DateTime.Now;
//			Debug.Log("LastDay: " + oldDate);
//			Debug.Log("CurrDay: " + newDate);
			numTap = ES2.Load<int> ("numTapped");

			TimeSpan difference = newDate.Subtract (oldDate);
			if (difference.Days > 1) {
//				Debug.Log ("New Day!");
				numTap = 0;
				ES2.Save (numTap, "numTapped");
				numTap = ES2.Load<int> ("numTapped");
				ES2.Save (Convert.ToString (System.DateTime.Now), "TapDate");
			} 


		} else {
			ES2.Save (numTap, "numTapped");
			ES2.Save (Convert.ToString (System.DateTime.Now), "TapDate");

		}
	}

	void OnApplicationPause( bool pauseStatus )
	{
		if (pauseStatus == true) {
			ES2.Save (numTap, "numTapped");
		} else if (pauseStatus == false) {
			OnCheckReward ();
		}
	}
}
