﻿using UnityEngine;
using System.Collections;
using com.ootii.Messages;
using GameAnalyticsSDK;

public class AdStatsTracker : MonoBehaviour {

	void OnEnable(){

		GameAnalytics.Initialize ();

		MessageDispatcher.AddListener ("VerySimpleAdsRewardShow", VerySimpleAdsRewardShow, true);
		MessageDispatcher.AddListener ("AdLovinRewardShow", AdLovinRewardShow, true);
		MessageDispatcher.AddListener ("VungleRewardShow", VungleRewardShow, true);
		MessageDispatcher.AddListener ("OnPlayingInGame", OnPlayingInGame, true);
		MessageDispatcher.AddListener ("OnGA_Report_Ads", OnGA_Report_Ads, true);
		MessageDispatcher.AddListener ("OnGA_Report_Iap", OnGA_Report_Iap, true);
		MessageDispatcher.AddListener ("OnGA_Report", OnGA_Report, true);
		MessageDispatcher.AddListener ("OnGA_ReportTime", OnGA_ReportTime, true);
		MessageDispatcher.AddListener ("OnGA_BusinessEvent", OnGA_BusinessEvent, true);
		MessageDispatcher.AddListener ("OnGA_SpinEvent", OnGA_SpinEvent, true);


	}
	
	void OnDisable(){
		MessageDispatcher.RemoveListener ("VerySimpleAdsRewardShow", VerySimpleAdsRewardShow, true);
		MessageDispatcher.RemoveListener ("AdLovinRewardShow", AdLovinRewardShow, true);
		MessageDispatcher.RemoveListener ("VungleRewardShow", VungleRewardShow, true);
		MessageDispatcher.RemoveListener ("OnPlayingInGame", OnPlayingInGame, true);
		MessageDispatcher.RemoveListener ("OnGA_Report_Ads", OnGA_Report_Ads, true);
		MessageDispatcher.RemoveListener ("OnGA_Report_Iap", OnGA_Report_Iap, true);
		MessageDispatcher.RemoveListener ("OnGA_Report", OnGA_Report, true);
		MessageDispatcher.RemoveListener ("OnGA_ReportTime", OnGA_ReportTime, true);
		MessageDispatcher.RemoveListener ("OnGA_SpinEvent", OnGA_SpinEvent, true);
	}

	void OnGA_SpinEvent(IMessage rMessage){
		int temp = (int)rMessage.Data; 
		GameAnalytics.NewDesignEvent("Spinner:SpinPerDay", (float)temp); 

	}
	void OnGA_ReportTime(IMessage rMessage){
		float temp = (float)rMessage.Data; 

	}

	void OnGA_BusinessEvent(IMessage rMessage){
		string[] temp = (string[])rMessage.Data; 
//		temp[0] = currencyString;
//		temp[1] = AmountString;
//		temp[2] = ItemTypeString;
//		temp[3] = ItemIdString;
//		temp[4] = cartType;
//		temp[5] = ReceiptString;
//		temp[6] = SignatureString;

		string currency = temp [0];
		int amount = int.Parse(temp [1]);
		string itemType = temp [2];
		string itemId = temp [3];
		string cartType = temp [4];
		string receipt = temp [5];
		string signature = temp [6];

		#if (UNITY_ANDROID)
		GameAnalytics.NewBusinessEventGooglePlay (currency,amount, itemType, itemId, cartType, receipt, signature);
		#endif

	}

	void OnPlayingInGame(IMessage rMessage){
		int temp = (int)rMessage.Data; 
		GameAnalytics.NewDesignEvent("OnPlayingInGame", (float)temp); 
	}

	void VungleRewardShow(IMessage rMessage){
		int temp = (int)rMessage.Data; 
		string[] dataString = new string[]{"Ads","VungleRewardShow"};
		MessageDispatcher.SendMessage (this, "OnSendAnalytics_Unity", dataString, 0);
	}

	void AdLovinRewardShow(IMessage rMessage){
		int temp = (int)rMessage.Data; 
//		GameAnalytics.NewDesignEvent("AdLovinRewardShow", temp); 
		string[] dataString = new string[]{"Ads","AdLovinRewardShow"};
		MessageDispatcher.SendMessage (this, "OnSendAnalytics_Unity", dataString, 0);	
	}

	void VerySimpleAdsRewardShow(IMessage rMessage){
		int temp = (int)rMessage.Data; 
		string[] dataString = new string[]{"Ads","VerySimpleAdsRewardShow"};
		MessageDispatcher.SendMessage (this, "OnSendAnalytics_Unity", dataString, 0);
	}
	void OnGA_Report(IMessage rMessage){
		string nameEvent = (string)rMessage.Data;
		string nameScene = nameEvent;
		float evenValue = 1f;
		GameAnalytics.NewDesignEvent(nameScene, evenValue); 
	}
	void OnGA_Report_Iap(IMessage rMessage){
		string nameEvent = (string)rMessage.Data;
		string nameScene = "IAP:"+nameEvent;
		float evenValue = 1f;
//		Debug.Log ("Send GA :" + nameScene + " Value: " + evenValue);
		GameAnalytics.NewDesignEvent(nameScene, evenValue); 
	}

	void OnGA_Report_Ads(IMessage rMessage){
		string nameEvent = (string)rMessage.Data;
		string nameScene = "";
		float evenValue = 1f;
		if (Application.loadedLevelName == "MainScene") {
			nameScene = "Crypto:AdShow:"+nameEvent;
		}else if (Application.loadedLevelName == "Banker") {
			nameScene = "Oldschool:AdShow:"+nameEvent;
		}else if (Application.loadedLevelName == "Resort") {
			nameScene = "Resort:AdShow:"+nameEvent;
		}
//		Debug.Log ("Send GA :" + nameScene + " Value: " + evenValue);
		GameAnalytics.NewDesignEvent(nameScene, evenValue); 
	}
}


//MessageDispatcher.SendMessage(this, "ADMOBINTERSTITIALSHOWN", 1 , 0);