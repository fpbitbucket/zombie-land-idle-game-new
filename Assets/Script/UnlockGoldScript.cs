﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using com.ootii.Messages;
using TMPro;

public class UnlockGoldScript : MonoBehaviour {
	public MoneyManager moneymanager;
	public MainGeneratorScript mainGen;
//	public Sprite iconSprite;
//	public Image iconPopup;
//	private string[] tempString = new string[]{"Isn't That Vierd?","Mogul","Oligarch","Tycoon","I Am Ya Fahza!","Adam Smith Award","Universal Banker","Theoretical Economist","The One True Investor","Shigar Und A Waffle?","Game Of Investments","That Can’t Be Good","Black Ink Inc","Lucky 777s","Lord Of Lobbyists","Midas","Literally Can’t Even","Yesh, Dat Is A Keepah!","Psyche!","There Can Only Be Ones","The Hang Of It","Invisible Hand High Five","Captain Capitalism","$ Shaped Pool","You’re The Boss","Perspective Annihilation","New Frontier Capitalism","It’s Never Enough","Nice Round Numbers","Frequent Updates","Punctual Launches","2222","Audience Rapport","A Top Hat Is You","A True AdVenture Capitalist","Toite Like A Toiger","Good Gracious","Gold Pressed Latinum","FASTER!","A Noteworthy Day","Adam Smith Is Proud","Keep Climbing","Upward Trend","Boom","3333","Magical Pixels","The Roaring 3500s","Take What You Can","Where's The Roof On This?","Symphon-Economics","Pecunia Vincit Omnia","The Legend Continues","An Amster-dam Good Time","Who Writes These Things?","Humongous Numbers","Penny Pincher","Glorious","Always I Will Believe In You","And Make Believe In You","And Live In Harmony","Battery Eater","Never Give Up","Achievement","I Love BITCOOOOOOOINS!"};
	public string[] nameString = {"Stocks","Bonds","Cars","Housing","Personal","Credit","Insurance","Debit","Savings","Space"} ;
	public string[] unlockName = {"Isn't That Vierd?","Mogul","Oligarch","Tycoon","I Am Ya Fahza!","Adam Smith Award","Universal Banker","Theoretical Economist","The One True Investor","Shigar Und A Waffle?","Game Of Loans","That Can’t Be Good","Black Ink Inc","Lucky 777s","Lord Of Lobbyists","Midas","Literally Can’t Even","Yesh, Dat Is A Keepah!","Psyche!","There Can Only Be Ones","The Hang Of It","Invisible Hand High Five","Captain Capitalism","$ Shaped Pool","You’re The Boss","Perspective Annihilation","New Frontier Capitalism","It’s Never Enough","Nice Round Numbers","Frequent Updates","Punctual Launches","2222","Audience Rapport","A Top Hat Is You","A True AdVenture Capitalist","Toite Like A Toiger","Good Gracious","Gold Pressed Latinum","FASTER!","A Noteworthy Day","Adam Smith Is Proud","Keep Climbing","Upward Trend","Boom","3333","Magical Pixels","The Roaring 3500s","Take What You Can","Where's The Roof On This?","Symphon-Economics","Pecunia Vincit Omnia","The Legend Continues","An Amster-dam Good Time","Who Writes These Things?","Humongous Numbers","Penny Pincher","Glorious","Always I Will Believe In You","And Make Believe In You","And Live In Harmony","Battery Eater","Never Give Up","Achievement","I Love GOOOOOOLD!"};
	public string[] unlockMessage = {"5 Free Gold","Profit Speed Doubled","Profit Speed Doubled","Profit Speed Doubled","5 Free Gold","Profit Speed Doubled","Profit Speed Doubled","Profit Speed Doubled","Profit Doubled","10 Free Gold","Profit Doubled","Profit Doubled","Profit Doubled","Profit Doubled","Profit Doubled","Profit Doubled","Profit Doubled\u00a0","15 Free Gold","Profit Doubled","Profit Doubled","Profit Doubled","Profit Doubled","Profit Doubled","Profit Doubled","Profit Doubled","Profit Doubled","Profit Doubled","Profit Doubled","Profit Doubled","Profit Doubled","Profit Doubled","Profit Doubled","Profit Doubled","Profit Doubled","Profit Doubled","20 Free Gold","Profit Doubled","Profit Doubled","Profit Doubled","Profit Doubled","Profit Doubled","Profit Doubled","Profit Doubled","Profit Doubled","Profit Doubled","Profit Doubled","Profit Doubled","Profit Doubled","Profit Doubled","Profit Doubled","Profit Doubled","Profit Doubled","30 Free Gold","Profit Doubled","Profit Doubled","Profit Doubled","Profit Doubled","Profit Doubled","Profit Doubled","Profit Doubled","Profit Doubled","Profit Doubled","Profit Doubled","50 Free Gold"};

//	public TextMeshProUGUI unlockTextUI;
//	public TextMeshProUGUI unlockTextUIMessage;
//	public GameObject objPopup;
//	public TextMeshProUGUI labelMainPort;
//	public GameObject[] nameblocksPort;
	public int[] numLevels = {1,25,50,100,100,200,300,400,500,500,600,666,700,777,800,900,1000,1000,1100,1111,1200,1300,1400,1500,1600,1700,1800,1900,2000,2100,2200,2222,2300,2400,2500,2500,2600,2700,2800,2900,3000,3100,3200,3300,3333,3400,3500,3600,3700,3800,3900,4000,4000,4100,4200,4300,4400,4500,4600,4700,4800,4900,5000,5000}; // will be a checker for level that will trigger the unlocks
//	public Sprite UnlockedSprite;
//	public Sprite LockedSprite;
	public int[] numLevelDoubleSpeed;  // will be a checker for level in double speed profit
	public int[] LevelProfitMultiplier; // will be a checker for levels to earning x3 x4 x5 profit ernings
	public int[] numLevelGoldReward = {1,100,500,1000,2500,4000,5000};
	public int[] numGoldReward = {5,5,10,15,20,30,50};
//	public TextMeshProUGUI textNumMultiplier,textNumMultiplierPort;
	private int numLevelUnlocked = 0;

	public int totalUnlocks;
	public int totalUnlocks_doubleSpeed;
	public int totalUnlocks_Multiplier;
	public int totalUnlocks_GoldReward;

	public bool[] GoldRewardTrigger; 
	public Color IconColor_Active, IconColor_Deactive;
//	private string[] temp_Name = new string[]{"Onward Men!","To Arms Men!","Mobilization!","All Hands on Deck!","With All Deliberate Speed Men!","Swiftly To Thy Course Men","Double Time Men","Look Alive","Let's Go! Let's Go!","Make It Snappy and Happy","Shake A Leg","Get The Lead Out Men","After Burners","Pedal To The Metal","Andele","Light The Fires","Go Overdrive!","Show Them The Meaning Of Haste"};
//	private int[] temp_level = new int[]{1,50,100,200,300,400,500,600,700,800,900,1000,1200,1400,1600,1800,2000,2500};

//	public GameObject mainUnlockBtn,mainUnlockBtn_Port;
	bool OnStartGame = false,hasUnlock = false;
	void OnEnable () {
		OnStartGame = true;
		hasUnlock = false;
//		audioFlag = false;
		nameString = mainGen.genNames_Code;
		MessageDispatcher.AddListener ("OnGoldCheckLevel", OnCheckLevelUnlocked, true);
		MessageDispatcher.AddListener ("OnGoldUnlockReset", OnGoldUnlockReset, true);
		MessageDispatcher.AddListener ("OnDoneAnimationReward", OnDoneAnimationReward, true);
//		unlockName = temp_Name;
//		numLevels = temp_level;

//		numLevels = new int[]{1,25,50,100,110,200,300,400,500,510,600,666,700,777,800,900,1000,1100,1110,1111,1200,1300,1400,1500,1600,1700,1800,1900,2000,2100,2200,2222,2300,2400,2500,2510,2600,2700,2800,2900,3000,3100,3200,3300,3333,3400,3500,3600,3700,3800,3900,4000,4100,4110,4200,4300,4400,4500,4600,4700,4800,4900,5000,5100
//			};
//
//		LevelProfitMultiplier = new int[]{500,600,666,700,777,800,900,1000,1100,1111,1200,1300,1400,1500,1600,1700,1800,1900,2000,2100,2200,2222,2300,2400,2500,2600,2700,2800,2900,3000,3100,3200,3300,3333,3400,3500,3600,3700,3800,3900,4000,4100,4200,4300,4400,4500,4600,4700,4800,4900,5000
//			};
//		numLevelDoubleSpeed = new int[] {25,50,100,200,300,400
//			};
//
//		numLevelGoldReward = new int[] {1,110,510,1110,2510,4110,5100
//			};
//
//		objPopup.SetActive (false);
		GoldRewardTrigger = new bool[numGoldReward.Length];
		if (ES2.Exists ("GoldUnlock_DoubleSpeed_"+moneymanager.worldName)) {
			totalUnlocks_doubleSpeed = ES2.Load<int> ("GoldUnlock_DoubleSpeed_"+moneymanager.worldName);
		} else {
			totalUnlocks_doubleSpeed = 0;
			ES2.Save(totalUnlocks_doubleSpeed, "GoldUnlock_DoubleSpeed_"+moneymanager.worldName);
		}


		if (ES2.Exists ("GoldUnlock_Multiplier_"+moneymanager.worldName)) {
			totalUnlocks_Multiplier = ES2.Load<int> ("GoldUnlock_Multiplier_"+moneymanager.worldName);
		} else {
			totalUnlocks_Multiplier = 0;
			ES2.Save(totalUnlocks_Multiplier, "GoldUnlock_Multiplier_"+moneymanager.worldName);
		}

		if (ES2.Exists ("GoldUnlock_GoldReward_"+moneymanager.worldName)) {
			totalUnlocks_GoldReward = ES2.Load<int> ("GoldUnlock_GoldReward_"+moneymanager.worldName);
		} else {
			totalUnlocks_GoldReward = 0;
			ES2.Save(totalUnlocks_GoldReward, "GoldUnlock_GoldReward_"+moneymanager.worldName);
		}

		for (int i = 0; i < numGoldReward.Length; i++) {
			if ( i < totalUnlocks_GoldReward) {
				GoldRewardTrigger [i] = true;
			} else {
				GoldRewardTrigger [i] = false;
			}
		}

		if (ES2.Exists ("GoldUnlock_"+moneymanager.worldName)) {
			totalUnlocks = ES2.Load<int> ("GoldUnlock_"+moneymanager.worldName);
		} else {
			totalUnlocks = 0;
			ES2.Save(totalUnlocks, "GoldUnlock_"+moneymanager.worldName);
		}

//		if (totalUnlocks == (nameblocksPort.Length-1)){
//			mainUnlockBtn.SetActive (false);
//			mainUnlockBtn_Port.SetActive (false);
//		}

//		for (int i = 0; i < nameblocksPort.Length; i++) {
//			int tempInt = i;
//			if (nameblocksPort [i].GetComponent<Button> () == false) {
//				nameblocksPort [i].AddComponent<Button> ();
//			}
//
//			nameblocksPort [i].GetComponent<Button> ().onClick.AddListener(() => { UnlockBtns(tempInt); });
//
//		}


		StartCoroutine (OnGeneratorLevelChecker ());
	}

	void OnDisable () {
		OnStartGame = false;
		MessageDispatcher.RemoveListener ("OnGoldCheckLevel", OnCheckLevelUnlocked, true);
	}

	void OnDoneAnimationReward (IMessage rMessage){
//		Debug.Log("OnDoneAnimationReward");
		hasUnlock = false;
	}


	void OnGoldUnlockReset (IMessage rMessage){
		if (ES2.Exists ("GoldUnlock_DoubleSpeed_"+moneymanager.worldName)) {
			totalUnlocks_doubleSpeed = ES2.Load<int> ("GoldUnlock_DoubleSpeed_"+moneymanager.worldName);
		} else {
			totalUnlocks_doubleSpeed = 0;
			ES2.Save(totalUnlocks_doubleSpeed, "GoldUnlock_DoubleSpeed_"+moneymanager.worldName);
		}


		if (ES2.Exists ("GoldUnlock_Multiplier_"+moneymanager.worldName)) {
			totalUnlocks_Multiplier = ES2.Load<int> ("GoldUnlock_Multiplier_"+moneymanager.worldName);
		} else {
			totalUnlocks_Multiplier = 0;
			ES2.Save(totalUnlocks_Multiplier, "GoldUnlock_Multiplier_"+moneymanager.worldName);
		}

//		mainUnlockBtn.SetActive (true);
//		mainUnlockBtn_Port.SetActive (true);
//		OnCheckUnlockAll_Reward ();

	}

	private IEnumerator OnGeneratorLevelChecker()
	{ 
		while (OnStartGame) {
			yield return new WaitForSeconds (0.5f);
			int checkNum = 0;

			for (int i = 0; i < nameString.Length; i++) {
				if (ES2.Exists ("btnName_" + nameString [i]) && ES2.Exists ("btnName_" + nameString [i] + "?tag=Level")) {
					int levelItem = ES2.Load<int> ("btnName_" + nameString [i] + "?tag=Level");
					totalUnlocks = ES2.Load<int> ("GoldUnlock_"+moneymanager.worldName);
					if (levelItem >= numLevels [totalUnlocks] && totalUnlocks != (numLevels.Length-1)) {
						checkNum++;
					} else {
						break;
					}
				} else {
					break;
				}
			}
//			Debug.Log ("checking Gold Unlock " + hasUnlock + " " +checkNum + "  " + numLevels [totalUnlocks] + " " + totalUnlocks);
			if (checkNum == 10 && hasUnlock == false) {
				int[] numberdata = new int[]{numLevels [totalUnlocks], totalUnlocks};
				MessageDispatcher.SendMessage (this, "OnGoldCheckLevel", numberdata, 0);
//				Debug.Log ("checking");
			}

			yield return OnStartGame;
		}
	}



	void OnCheckLevelUnlocked (IMessage rMessage){
		int[] numberdata = (int[])rMessage.Data;
		int LevelChecker = numberdata [0];
		int numTotalUnlockTemp =  numberdata [1];;
		hasUnlock = true;

		totalUnlocks++;
		int allunlock = ES2.Load<int> ("TotalUnlocks_"+moneymanager.worldName) + 1;
		ES2.Save(allunlock, "TotalUnlocks_"+moneymanager.worldName);
		ES2.Save(totalUnlocks, "GoldUnlock_"+moneymanager.worldName);

		if (totalUnlocks_Multiplier < numLevelDoubleSpeed.Length) {
			for (int i = 0; i < numLevelDoubleSpeed.Length; i++) {

				if (LevelChecker == numLevelDoubleSpeed [i]) {
					totalUnlocks_doubleSpeed++;
					ES2.Save (totalUnlocks_doubleSpeed, "GoldUnlock_DoubleSpeed_"+moneymanager.worldName); 
					for (int l = 0; l < nameString.Length; l++) {
						MessageDispatcher.SendMessage (this, "On_UpdateSpeed" + nameString [l], 0, 0);
					}
					string[] tempSendPopup = new string[]{ unlockName [numTotalUnlockTemp], unlockMessage [numTotalUnlockTemp], "DoubleSpeed" };
					MessageDispatcher.SendMessage (this, "OnShowGoldPopupReward", tempSendPopup, 0);
					break;
				}
			}
		}

		if (totalUnlocks_Multiplier < LevelProfitMultiplier.Length) {
			for (int i = 0; i < LevelProfitMultiplier.Length; i++) {
				//				Debug.Log (LevelChecker + " Multiplier " + LevelProfitMultiplier [i]);
				if (LevelChecker == LevelProfitMultiplier [i]) {
					totalUnlocks_Multiplier++;
					ES2.Save (totalUnlocks_Multiplier, "GoldUnlock_Multiplier_"+moneymanager.worldName);
					for (int l = 0; l < nameString.Length; l++) {
						MessageDispatcher.SendMessage (this, "On_UnlockedMultiplier_" + nameString [l], (double)2, 0);
					}
					string[] tempSendPopup = new string[]{ unlockName [numTotalUnlockTemp], unlockMessage [numTotalUnlockTemp], "ProfitDouble" };
					MessageDispatcher.SendMessage (this, "OnShowGoldPopupReward", tempSendPopup, 0);
					break;
				}
			}
		}

		if (totalUnlocks_GoldReward < numLevelGoldReward.Length) {
			for (int i = 0; i < numLevelGoldReward.Length; i++) {
				//				Debug.Log (LevelChecker + " GOLD " + numLevelGoldReward [i]);
				if (LevelChecker == numLevelGoldReward [i] && GoldRewardTrigger[i] == false) {
					GoldRewardTrigger [i] = true;
					MessageDispatcher.SendMessage (this, "OnIncrementGold", (double)numGoldReward [totalUnlocks_GoldReward], 0);	
					totalUnlocks_GoldReward++;
					ES2.Save (totalUnlocks_GoldReward, "GoldUnlock_GoldReward_"+moneymanager.worldName);
					string[] tempSendPopup = new string[]{ unlockName [numTotalUnlockTemp], unlockMessage [numTotalUnlockTemp], "Gold" };
					MessageDispatcher.SendMessage (this, "OnShowGoldPopupReward", tempSendPopup, 0);
					break;
				}
			}
		}
		hasUnlock = false;
	}

	public void CheckGalleryUnlock  (){
		int levelItem = ES2.Load<int> ("GoldUnlock_"+moneymanager.worldName);
		numLevelUnlocked = 0;

		for (int i = 0; i < numLevels.Length; i++) {
//			nameblocksPort [i].GetComponentInChildren<TextMeshProUGUI> ().text = numLevels [i].ToString ();
//			nameblocksPort [i].transform.GetChild(0).gameObject.SetActive (false);
//			nameblocksPort [i].transform.GetChild(1).gameObject.SetActive (false);


			if (levelItem >= numLevels [i]) {
				
				numLevelUnlocked++;
//				nameblocksPort [i].GetComponent<Button> ().targetGraphic = nameblocksPort [i].transform.Find ("Active/BigCircle").GetComponent<Image> ();
//				nameblocksPort [i].transform.Find ("SmallCircle").gameObject.GetComponent<Image> ().color = IconColor_Active;
//
//				nameblocksPort [i].transform.GetChild(0).gameObject.SetActive (true);

			} else {

//				nameblocksPort [i].GetComponent<Button> ().targetGraphic = nameblocksPort [i].transform.Find ("Active/BigCircle").GetComponent<Image> ();
//				nameblocksPort [i].transform.Find ("SmallCircle").gameObject.GetComponent<Image> ().color = IconColor_Deactive;
//
//				nameblocksPort [i].transform.GetChild(1).gameObject.SetActive (true);
			}
		}
		for (int i = 0; i < numLevelDoubleSpeed.Length; i++) {
			if (numLevelDoubleSpeed [i] != null){
				if (numLevels [numLevelUnlocked] == numLevelDoubleSpeed [i]) {
//					textNumMultiplier.text = "2x";
//					textNumMultiplierPort.text = "2x";
					break;
				}
			}
		}

		for (int i = 0; i < LevelProfitMultiplier.Length; i++) {
			if (LevelProfitMultiplier [i] != null) {
				if (numLevels [numLevelUnlocked] == LevelProfitMultiplier [i]) {
//					textNumMultiplier.text = "2x";
//					textNumMultiplierPort.text = "2x";
					break;
				}
			}
		}

//		labelMainPort.text = numLevels [numLevelUnlocked].ToString ();
	}




	public void UnlockBtns(int numBtn){
		MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
		string tempStringSub = numLevels [numBtn].ToString () + " - " + unlockName [numBtn] + " " + unlockMessage [numBtn];
		MessageDispatcher.SendMessage (this, "OnUpdateGallerySub", tempStringSub, 0);
	}


}
