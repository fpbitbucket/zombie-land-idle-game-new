﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using com.ootii.Messages;
using TMPro;
public class UnlockSript : MonoBehaviour {
	public MoneyManager moneymanager;
	public MainGeneratorScript mainGen;
	public int num_generator = 0;
//	public Sprite iconSprite;
	public string nameString;



//	private string[] tempName = new string[]{"Eight Digits Savings","Savings Reference","Just Go With It","Radioactive Savings","Savings in Disguise","Savings-phobia","Savings Wild Wild","Spins a Savings Any Size","Savings 360","Black Savings","Austrailian Savings Program","Salutations","Fire Won't Save You Now"};
//	private string[] tempName_1 = new string[]{"Profit x333","Profit x333","Profit x333","Profit x333","Profit x333","Profit x333","Profit x333","Profit x333","Profit x333","Profit x333","Profit x333","Profit x333","Profit x333"};
//	private int[] tempLevel = new int[]{33,66,99,222,333,444,555,666,777,888,999,1111,2222};
//
//
//
//	private int[] tempLevel_1 = new int[]{33,66,99,222,333,444,555,666,777,888,999,1111,2222};
//	private int[] tempLevel_2;
//
//	private double[] tempLevel_3 = new double[]{333,333,333,333,333,333,333,333,333,333,333,333,333};


//	private string[] unlockMessageTemp = new 

	public string[] unlockName;
	public string[] unlockMessage;
//	public TextMeshProUGUI labelMainPort;
//	public GameObject[] nameblocksPorts;
	public int[] numLevels; // will be a checker for level that will trigger the unlocks
//	public Sprite UnlockedSprite;
//	public Sprite LockedSprite;
	public int[] numLevelDoubleSpeed;  // will be a checker for level in double speed profit
	public int[] LevelProfitMultiplier; // will be a checker for levels to earning x3 x4 x5 profit ernings
	public double[] numProfitMultiplierAdd; // will be the number for multiplier x3 x4 x5 profit ernings
	public string[] nameProfitToGive;
//	public TextMeshProUGUI textNumMultiplierPort;
	private int numLevelUnlocked = 0;
	private int totalUnlocks;
	public Color IconColor_Active, IconColor_Deactive;
	//	public GameObject mainUnlockBtn_Port;
	public int TotalUnlockInGenerator = 0;
	public bool OnDoneUnlock = false;
	void OnEnable () {
		nameString = mainGen.genNames_Code [num_generator];
//		numLevels = new int[]{25,50,100,200,300,400,500,600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2250,2500,2750,3000,3250,3500,3750,4000,4250,4500,4750,5000,5250,5500,5750,6000,6250,6500,6750,7000,7250,7500,7750,8000,8250,8500,8750,9000,9250,9500,9750
//			};
//
//		LevelProfitMultiplier = new int[]{500,600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,5250,5500,5750,6000,6250,6500,6750,7000,7250,7500,7750,8000,8250,8500,8750,9000,9250,9500,9750
//			};
//		numProfitMultiplierAdd = new double[] {2,2,2,2,2,3,2,2,2,2,2,2,2,2,2,5,3,3,3,5,3,3,3,7,3,3,3,3,3,3,3,7,3,3,3 
//			};

		MessageDispatcher.AddListener ("On" + nameString + "CheckLevel", OnCheckLevelUnlocked, true);
		MessageDispatcher.AddListener ("OnResetTotalMoney", OnResetTotalMoney, true);
		if (ES2.Exists ("TotalUnlocks_"+moneymanager.worldName)) {
			totalUnlocks = ES2.Load<int> ("TotalUnlocks_"+moneymanager.worldName);
		} else {
			totalUnlocks = 0;
			ES2.Save(totalUnlocks, "TotalUnlocks_"+moneymanager.worldName);
		}

		int unlockNum = 0;
		if (ES2.Exists ("btnName_" + nameString + "?tag=Unlocked")) {
			unlockNum = ES2.Load<int> ("btnName_" + nameString + "?tag=Unlocked");
		} else {
			unlockNum = 0;
		}
		TotalUnlockInGenerator = unlockNum;
//		if (unlockNum == (nameblocksPorts.Length-1)){
//			mainUnlockBtn_Port.SetActive (false);
//		}
		if (unlockNum == numLevels.Length) {
			OnDoneUnlock = true;
		}
//		for (int i = 0; i < nameblocksPorts.Length; i++) {
//			int tempInt = i;
//
//			if (nameblocksPorts [i].GetComponent<Button> () == false) {
//				nameblocksPorts [i].AddComponent<Button> ();
//			}
//			nameblocksPorts [i].GetComponent<Button> ().onClick.AddListener(() => { UnlockBtns(tempInt); });
//
//		}
	}

	void OnDisable () {
		MessageDispatcher.RemoveListener ("On" + nameString + "CheckLevel", OnCheckLevelUnlocked, true);
		MessageDispatcher.RemoveListener ("OnResetTotalMoney", OnResetTotalMoney, true);
	}


	void OnResetTotalMoney (IMessage rMessage){
		OnDoneUnlock = false;
	}


	void OnCheckLevelUnlocked (IMessage rMessage){
		if (ES2.Exists ("btnName_" + nameString + "?tag=Level") && ES2.Exists ("btnName_" + nameString + "?tag=Unlocked")) {
			int unlockNum = ES2.Load<int> ("btnName_" + nameString + "?tag=Unlocked");
			if (unlockNum != numLevels.Length) {
				int levelItem = ES2.Load<int> ("btnName_" + nameString + "?tag=Level");

				int previewLevel = unlockNum;
				bool OntheLastLevel = false;

				int tempLevelCheckerTemp = numLevels [unlockNum];

				if (levelItem > tempLevelCheckerTemp) {
					while (levelItem >= tempLevelCheckerTemp && OntheLastLevel == false) {
						unlockNum++;
						//						Debug.Log (levelItem + " " + unlockNum + " " + tempLevelCheckerTemp + " " + numLevels.Length);
						if (unlockNum >= numLevels.Length) {
							OntheLastLevel = true;
							OnDoneUnlock = true;
							//							Debug.Log ("Last Unlock " + unlockNum);
						} else {
							//							Debug.Log ("Not Last Unlock " + unlockNum);
							tempLevelCheckerTemp = numLevels [unlockNum];
						}
					}

					totalUnlocks = ES2.Load<int> ("TotalUnlocks_" + moneymanager.worldName) + 1;
					ES2.Save (totalUnlocks, "TotalUnlocks_" + moneymanager.worldName);
					ES2.Save (unlockNum, "btnName_" + nameString + "?tag=Unlocked");
					for (int i = previewLevel; i < unlockNum; i++) {
						//						Debug.Log (i + " OnCheckLevelDoubleSpeed");
						OnCheckLevelDoubleSpeed (i);
					}
				} else if (levelItem == tempLevelCheckerTemp) {
					OnCheckLevelDoubleSpeed (unlockNum);
					unlockNum++;
					//				tempLevelCheckerTemp = numLevels [unlockNum];
					//				if (levelItem == tempLevelCheckerTemp) {
					//					unlockNum++;
					//					OnCheckLevelDoubleSpeed (unlockNum);
					//				}
					ES2.Save (unlockNum, "btnName_" + nameString + "?tag=Unlocked");
					totalUnlocks = ES2.Load<int> ("TotalUnlocks_" + moneymanager.worldName) + 1;
					ES2.Save (totalUnlocks, "TotalUnlocks_" + moneymanager.worldName);
				}
				TotalUnlockInGenerator = unlockNum;


				if (OnDoneUnlock == true) {
					
				} else {
					OnDoneUnlock = false;
				}
			}
		}
	}

	void OnCheckLevelDoubleSpeed(int numLevelGen){
		string typeUnlock = "";
		if (numLevelGen <= (numLevels.Length-1)) {
			for (int i = 0; i < numLevelDoubleSpeed.Length; i++) {
				if (numLevels [numLevelGen] == numLevelDoubleSpeed [i]) {
					typeUnlock = "Double Speed";
					MessageDispatcher.SendMessage (this, "On_UpdateSpeed" + nameString, 0, 0);
					break;
				}
			}
		}
		//		
		if (numLevelGen <= (numLevels.Length-1)) {
			for (int i = 0; i < LevelProfitMultiplier.Length; i++) {
				//				Debug.Log (numLevels [numLevelGen] + " " + LevelProfitMultiplier [i]);
				if (numLevels [numLevelGen] == LevelProfitMultiplier [i]) {
					for (int l = 0; l < mainGen.genNamesString.Length; l++) {
						if (mainGen.genNames_Code [l] == nameProfitToGive [i]) {
							typeUnlock = mainGen.genNamesString [l] + " Profit x"+numProfitMultiplierAdd [i];
							break;
						}
					}
					MessageDispatcher.SendMessage (this, "On_UnlockedMultiplier_" + nameProfitToGive [i], numProfitMultiplierAdd [i], 0);
					break;
				}
			}
		}

		int[] levelArray = new int[]{num_generator, numLevels [numLevelGen]};
		MessageDispatcher.SendMessage (this, "SmallUnlockTextUpgrade", typeUnlock, 0);
		MessageDispatcher.SendMessage (this, "OnShowPopupUnlocked", levelArray, 0);
	}
		


	public void CheckGalleryUnlock  (){
		int levelItem = ES2.Load<int> ("btnName_" + nameString + "?tag=Level");
		numLevelUnlocked = 0;

		for (int i = 0; i < numLevels.Length; i++) {
//			nameblocksPorts [i].GetComponentInChildren<TextMeshProUGUI> ().text = numLevels [i].ToString ();
//			nameblocksPorts [i].transform.GetChild(0).gameObject.SetActive (false);
//			nameblocksPorts [i].transform.GetChild(1).gameObject.SetActive (false);


			if (levelItem >= numLevels [i]) {
				
				numLevelUnlocked++;

//				nameblocksPorts [i].GetComponent<Button> ().targetGraphic = nameblocksPorts [i].transform.Find ("Active/BigCircle").GetComponent<Image> ();
//				nameblocksPorts [i].transform.Find ("SmallCircle").gameObject.GetComponent<Image> ().color = IconColor_Active;
//
//				nameblocksPorts [i].transform.GetChild(0).gameObject.SetActive (true);
			} else {
//				nameblocksPorts [i].GetComponent<Button> ().targetGraphic = nameblocksPorts [i].transform.Find ("Deactivate/BigCircle").GetComponent<Image> ();
//				nameblocksPorts [i].transform.Find ("SmallCircle").gameObject.GetComponent<Image> ().color = IconColor_Deactive;
//				nameblocksPorts [i].transform.GetChild(1).gameObject.SetActive (true);
			}
		}

		for (int i = 0; i < numLevelDoubleSpeed.Length; i++) {
			if (numLevelDoubleSpeed [i] != null){
				if (numLevels [numLevelUnlocked] == numLevelDoubleSpeed [i]) {
//					textNumMultiplierPort.text = "x2";
					break;
				}
			}
		}

		for (int i = 0; i < LevelProfitMultiplier.Length; i++) {
			if (LevelProfitMultiplier [i] != null) {
				if (numLevels [numLevelUnlocked] == LevelProfitMultiplier [i]) {
//					textNumMultiplierPort.text = "x"+numProfitMultiplierAdd[i].ToString();

					break;
				}
			}
		}
//		labelMainPort.text = numLevels [numLevelUnlocked].ToString ();
	}



	public void UnlockBtns(int numBtn){
		//		print ("update");
		MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
		string tempStringSub = numLevels [numBtn].ToString () + " - " + unlockName [numBtn] + " " + unlockMessage [numBtn];
		MessageDispatcher.SendMessage (this, "OnUpdateGallerySub", tempStringSub, 0);
	}

}
