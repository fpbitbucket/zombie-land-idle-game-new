﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;
using com.ootii.Messages;

public class AvatarOption : MonoBehaviour {
	public Sprite MaleSprite,FemaleSprite;
	public Sprite Maleprofile,Femaleprofile,AdvisorSprite;
	public Image avatartImagePort,profilePort;
//	public GameObject loadingCanvas;
	public Image adVisorPortrait;


	void Start () {
//		loadingCanvas.SetActive (false);
		avatartImagePort.sprite = MaleSprite;
		profilePort.sprite = Maleprofile;

		adVisorPortrait.sprite = AdvisorSprite;
	}
	
	public void avatarChange (int i){
		
		if (i == 0) {
			avatartImagePort.sprite = FemaleSprite;
			profilePort.sprite = Femaleprofile;
//			adVisorLandScape.sprite = Femaleprofile;
//			adVisorPortrait.sprite = Femaleprofile;
		} else {
			avatartImagePort.sprite = MaleSprite;
			profilePort.sprite = Maleprofile;
//			adVisorLandScape.sprite = AdvisorSprite;
//			adVisorPortrait.sprite = AdvisorSprite;
		}
	}


	public void OnChangeScene(string namestring){
//		SceneManager.LoadScene(namestring, LoadSceneMode.Single);
//		loadingsript.nameScene = namestring;
//		loadingCanvas.SetActive (true);
		MessageDispatcher.SendMessage (this, "OnStartChangeScene",namestring, 0);
	}
}
