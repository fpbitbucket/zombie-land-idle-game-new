﻿using UnityEngine;
using System.Collections;
using com.ootii.Messages;
public class AudioManager : MonoBehaviour {

	private static AudioManager _instance;
	private AudioSource audioPlayer;

	public static AudioManager instance
	{
		get
		{
			if(_instance == null)
			{
				_instance = GameObject.FindObjectOfType<AudioManager>();

				//Tell unity not to destroy this object when loading a new scene!
				DontDestroyOnLoad(_instance.gameObject);
			}

			return _instance;
		}
	}

	void Awake() 
	{
		MessageDispatcher.AddListener("PopButton", OnPopButton, true);
		if(_instance == null)
		{
			//If I am the first instance, make me the Singleton
			_instance = this;
			DontDestroyOnLoad(this);
		}
		else
		{
			//If a Singleton already exists and you find
			//another reference in scene, destroy it!
			if(this != _instance)
				Destroy(this.gameObject);
		}
	}

	void Start() {
		Debug.Log ("AudioManager Started");
		audioPlayer = gameObject.GetComponent<AudioSource> () as AudioSource;

	}

	void OnDisable(){
		MessageDispatcher.RemoveListener("PopButton", OnPopButton, true);
	}

	void OnPopButton(IMessage rMessage)
	{
		audioPlayer.Play ();
	}

	public static void playPopButton(){
		//audioPlayer.Play ();

	}
}
