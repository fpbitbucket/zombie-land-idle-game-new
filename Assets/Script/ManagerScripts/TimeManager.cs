﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using TMPro;
using com.ootii.Messages;
using DG.Tweening;
public class TimeManager : MonoBehaviour {

	public MoneyManager MoneyManagerObj;
	public MainGeneratorScript mainGen;
	public double numOfflineErn; //10% of earnings every 5secs ingame 
	public string[] nameGenerators; //name of the generators  that will be load
	public double offlineSeconds = 5; //coins earn offline per seconds
	private double rewardGet = 0; //total reward to get

//	public GameObject PopupTimeOutScreen;
	public TextMeshProUGUI onGameTextTimerPort;
	public TextMeshProUGUI onPopupTextTimerPort;
	public TextMeshProUGUI onPopupAdRemainingPort;
	public TextMeshProUGUI onPopupMessagePort;
	public GameObject watchbtnPort,continueBtnPort,tvboarderPort,tvFx;
//	public GameObject launchPort;
	public float adMultiplierTimer;
	public bool adMultiplierStatus = false;
	public int adMultiplierLimit = 6;
	public float adTimerHolder = 0;
	public double adMultiplier = 2;
	DateTime currentDate; //current time when get back
	DateTime oldDate; //old date will save

	public Color origColor,blinkColor;
	public Color lightLightColor;
	bool adplier = false;


	bool watchingAds = false;

	public double AmountBusiness;
	public double permanentAdMultiplier = 2;
	public double BoostGoldMultiplier = 0;
	public TextMeshProUGUI[] multiplierText;
	void OnEnable () {
		watchingAds = false;
		nameGenerators = mainGen.genNames_Code;
		MessageDispatcher.AddListener ("OnStartAdMultiplierTime", OnStartAdMultiplierTime, true); 
		MessageDispatcher.AddListener ("OnResetAllGenerator", OnResetAllGenerator, true); 
		MessageDispatcher.AddListener ("OnBoostWithGold", OnBoostWithGold, true); 
		MessageDispatcher.AddListener ("OnWatchingAds", OnWatchingAds, true); 
		MessageDispatcher.AddListener ("OnUseAdsLimit", OnUseAdsLimit, true); 
		adplier = true;
		tvFx.SetActive (false);
//		if (Application.loadedLevelName == "Banker") {
//			string nameWorld = "";
//			if (ES2.Exists ("BankerFist")) {
//				nameWorld = "Back:Resort";
//			}  else {
//				ES2.Save(1,"BankerFist");
//				nameWorld = "FisrtOpen:Resort";
//			}
//			MessageDispatcher.SendMessage (this, "OnGA_Report", nameWorld, 0);
//		}else if (Application.loadedLevelName == "Resort") {
//			string nameWorld = "";
//			if (ES2.Exists ("ResortFist")) {
//				nameWorld = "Back:Resort";
//			}  else {
//				ES2.Save(1,"ResortFist");
//				nameWorld = "FisrtOpen:Resort";
//			}
//			MessageDispatcher.SendMessage (this, "OnGA_Report", nameWorld, 0);
//		}


		if (ES2.Exists ("permanentAdMultiplier_"+MoneyManagerObj.worldName)) {
			permanentAdMultiplier = ES2.Load<double> ("permanentAdMultiplier_"+MoneyManagerObj.worldName);
		} else {
			permanentAdMultiplier = 2;
			ES2.Save(permanentAdMultiplier, "permanentAdMultiplier_"+MoneyManagerObj.worldName);
		}

		if (ES2.Exists ("BoostGoldMultiplier" + MoneyManagerObj.worldName)) {
			BoostGoldMultiplier = ES2.Load<double> ("BoostGoldMultiplier" + MoneyManagerObj.worldName);
		} else {
			BoostGoldMultiplier = 0;
			ES2.Save (BoostGoldMultiplier, "BoostGoldMultiplier" + MoneyManagerObj.worldName);
		}

		for (int i = 0; i < multiplierText.Length; i++) {
			multiplierText [i].text = "x" + (BoostGoldMultiplier + permanentAdMultiplier);
		}

		OnSetupAdMultiplierTimer ();
	}




	void OnDisable(){
		if (adMultiplierStatus) { 
			ES2.Save (adTimerHolder, "AdMultiplierTime_" + MoneyManagerObj.worldName); 
		}
		MessageDispatcher.RemoveListener ("OnStartAdMultiplierTime", OnStartAdMultiplierTime, true); 
		MessageDispatcher.RemoveListener ("OnResetAllGenerator", OnResetAllGenerator, true); 
		MessageDispatcher.RemoveListener ("OnBoostWithGold", OnBoostWithGold, true); 
		MessageDispatcher.RemoveListener ("OnWatchingAds", OnWatchingAds, true); 
		MessageDispatcher.RemoveListener ("OnUseAdsLimit", OnUseAdsLimit, true); 
	}

	void OnWatchingAds (IMessage rMessage){
		watchingAds = true;
	}
	void OnBoostWithGold (IMessage rMessage){
		double addBoost = (double)rMessage.Data;
		BoostGoldMultiplier += 2;
		ES2.Save (BoostGoldMultiplier, "BoostGoldMultiplier" + MoneyManagerObj.worldName);
		MessageDispatcher.SendMessage (this, "OnSetAdMultiplier",(double)( BoostGoldMultiplier+permanentAdMultiplier), 0.5f);
		UpdateMultiplier();
		OnUpdateAdMultiplier ();

	}
		

	void OnResetAllGenerator (IMessage rMessage){
//		tvFx.SetActive (true);
//		adMultiplierStatus = false;
//		adMultiplierLimit = 6;
//		adTimerHolder = 0;
//		ES2.Save (adMultiplierLimit, "AdLimitPerDay_"+MoneyManagerObj.worldName);
//		MessageDispatcher.SendMessage (this, "OnSetAdMultiplier", (double)( BoostGoldMultiplier+0), 0.5f);	
//		OnSetupAdMultiplierTimer ();
	}

	void OnUseAdsLimit  (IMessage rMessage){
		adMultiplierLimit--;
		OnUpdateAdMultiplier ();
	}


	void OnStartAdMultiplierTime (IMessage rMessage){
//		Debug.Log (adMultiplierStatus + " Multiplier TV ads");
		if (adMultiplierStatus == false) {
			//			adTimerHolder = adMultiplierTimer;
			adMultiplierStatus = true;
			MessageDispatcher.SendMessage (this, "OnSetAdMultiplier", (double)( BoostGoldMultiplier+permanentAdMultiplier), 0.5f);	
			tvFx.SetActive (false);
		} else {

		}
//		Debug.Log (adMultiplierStatus + " Multiplier TV ads");
		//		tvboarder.GetComponent<DOTweenAnimation> ().DOPause ();
		UpdateMultiplier();
		continueBtnPort.SetActive (false);
		watchbtnPort.SetActive (false);
		adTimerHolder += adMultiplierTimer;
		OnUpdateAdMultiplier ();
	}


	void OnUpdateAdMultiplier(){
		string bonuses = "bonuses";
		string morestring = "more";
		if (adMultiplierLimit > 0) {
			continueBtnPort.SetActive (false);
			watchbtnPort.SetActive (true);
			if (adMultiplierLimit == 1) {
				bonuses = "bonus";
			}
		} else {
			morestring = "";
			adMultiplierLimit = 0;
			continueBtnPort.SetActive (true);
			watchbtnPort.SetActive (false);
		}
		onPopupAdRemainingPort.text = "You have <#" + ColorUtility.ToHtmlStringRGB (lightLightColor) + ">" + adMultiplierLimit.ToString () + " "+morestring+"</color> ad "+bonuses+" that you can use today";

		ES2.Save (adMultiplierLimit, "AdLimitPerDay_" + MoneyManagerObj.worldName);
	}

	public void OpenAdMultiplerScreen(){

		double TempDisplay = BoostGoldMultiplier + permanentAdMultiplier;
		if (adMultiplierStatus == false) {
			TempDisplay = BoostGoldMultiplier + 0;
		}
		if (BoostGoldMultiplier == 0 && adMultiplierStatus == false) {
			TempDisplay = 2;
		}

		if (adMultiplierStatus == false) {
			onPopupMessagePort.text = "Watch fine quality ad and will recieve a x"+TempDisplay+" Profit boost for 4hrs";
		} else {
			onPopupMessagePort.text = "<#"+ColorUtility.ToHtmlStringRGB( lightLightColor )+">Watch another ad</color> to restart your timer to 4 hours again!";
		}
	}

	void UpdateMultiplier(){
		double TempDisplay = BoostGoldMultiplier + permanentAdMultiplier;
		if (adMultiplierStatus == false) {
			TempDisplay = BoostGoldMultiplier + 0;
		}
		if (BoostGoldMultiplier == 0 && adMultiplierStatus == false) {
			TempDisplay = 2;
		}
		for (int i = 0; i < multiplierText.Length; i++) {
			multiplierText [i].text = "x" + TempDisplay;
		}
	}

	private void OnSetupAdMultiplierTimer(){

		if (ES2.Exists ("HasDate_"+MoneyManagerObj.worldName)) {
			int temp = System.DateTime.Now.Day;
			if (ES2.Load<int> ("HasDate_"+MoneyManagerObj.worldName) == temp) {
				if (ES2.Exists ("AdLimitPerDay_"+MoneyManagerObj.worldName)) {
					adMultiplierLimit = ES2.Load<int> ("AdLimitPerDay_"+MoneyManagerObj.worldName);
				} else {
					adMultiplierLimit = 6;
					ES2.Save (adMultiplierLimit, "AdLimitPerDay_"+MoneyManagerObj.worldName);
				}
			} else {
				adMultiplierLimit = 6;
				ES2.Save (adMultiplierLimit, "AdLimitPerDay_"+MoneyManagerObj.worldName);
			}
		} else {
			adMultiplierLimit = 6;
			ES2.Save (adMultiplierLimit, "AdLimitPerDay_"+MoneyManagerObj.worldName);
			ES2.Save(System.DateTime.Now.Day, "HasDate_"+MoneyManagerObj.worldName);
		}

		continueBtnPort.SetActive (false);
		watchbtnPort.SetActive (false);

		if (adMultiplierLimit != 0) {

			continueBtnPort.SetActive (false);
			watchbtnPort.SetActive (true);
		} else {
			continueBtnPort.SetActive (true);
			watchbtnPort.SetActive (false);
		}

		if (ES2.Exists ("AdMultiplierTime_"+MoneyManagerObj.worldName)) {
			if (ES2.Load<float> ("AdMultiplierTime_"+MoneyManagerObj.worldName) == 0) {
				adMultiplierStatus = false;
				tvFx.SetActive (true);
			} else {
				if (ES2.Exists ("sysString")) {
					currentDate = System.DateTime.Now;
					long temp = Convert.ToInt64 (ES2.Load<string> ("sysString")); //load the time starts offline
					DateTime oldDate = DateTime.FromBinary (temp);
					TimeSpan difference = currentDate.Subtract (oldDate);
					int secondsInt = Convert.ToInt32 (difference.TotalSeconds); //seconds during offline
					int adMultiplierInt = Convert.ToInt32 (ES2.Load<float> ("AdMultiplierTime_"+MoneyManagerObj.worldName));

//					print (adMultiplierInt + " " + secondsInt);

					if (adMultiplierInt < secondsInt) { 
						print ("No Time Left");
						adTimerHolder = 0;
						MessageDispatcher.SendMessage (this, "OnSetAdMultiplier", (double)( BoostGoldMultiplier+0), 0.5f);	
					} else {
						print ("Still has Time Left");
						adMultiplierStatus = true;
						tvFx.SetActive (false);
						adTimerHolder = ES2.Load<float> ("AdMultiplierTime_"+MoneyManagerObj.worldName) - (float)difference.TotalSeconds;
						MessageDispatcher.SendMessage (this, "OnSetAdMultiplier", (double)( BoostGoldMultiplier+permanentAdMultiplier), 0.5f);	
					}
				}
				adMultiplierStatus = true;
				tvFx.SetActive (false);
			}
		} else {
			tvFx.SetActive (true);
			adMultiplierStatus = false;
			adTimerHolder = 0;
			ES2.Save(adTimerHolder, "AdMultiplierTime_"+MoneyManagerObj.worldName);
		}

		UpdateMultiplier ();
		UpdateTimeString ();
		OpenAdMultiplerScreen ();
		onPopupAdRemainingPort.text = "You have <#"+ColorUtility.ToHtmlStringRGB( lightLightColor )+">"+adMultiplierLimit.ToString()+" more</color> ad bonuses that you can use today";

	}

	void UpdateTimeString(){
		System.TimeSpan t = System.TimeSpan.FromSeconds (adTimerHolder);
		onGameTextTimerPort.text = ((int)t.TotalHours).ToString("d2")+":"+t.Minutes.ToString("d2")+":"+t.Seconds.ToString("d2");//string.Format ("{0:D2}:{1:D2}:{2:D2}", 0, 0, 0);
		onPopupTextTimerPort.text = ((int)t.TotalHours).ToString("d2")+":"+t.Minutes.ToString("d2")+":"+t.Seconds.ToString("d2");//string.Format ("{0:D2}:{1:D2}:{2:D2}", t.Hours, t.Minutes, t.Seconds) + " Remaining";

	}

	void Update()
	{
		if (adMultiplierStatus) { 
			float tempDeltaTime = Time.deltaTime;
			adTimerHolder -= tempDeltaTime;
			System.TimeSpan t = System.TimeSpan.FromSeconds (adTimerHolder);
			if (adTimerHolder < 0) {
				adTimerHolder = 0;
				adMultiplierStatus = false;
				onGameTextTimerPort.text = ((int)t.TotalHours).ToString("d2")+":"+t.Minutes.ToString("d2")+":"+t.Seconds.ToString("d2");//string.Format ("{0:D2}:{1:D2}:{2:D2}", 0, 0, 0);
				onPopupTextTimerPort.text = ((int)t.TotalHours).ToString("d2")+":"+t.Minutes.ToString("d2")+":"+t.Seconds.ToString("d2");//string.Format ("{0:D2}:{1:D2}:{2:D2}", 0, 0, 0);
				MessageDispatcher.SendMessage (this, "OnSetAdMultiplier",(double)( BoostGoldMultiplier+0), 0);	
				UpdateMultiplier ();
				tvFx.SetActive (true);
			} else {
				onGameTextTimerPort.text = ((int)t.TotalHours).ToString("d2")+":"+t.Minutes.ToString("d2")+":"+t.Seconds.ToString("d2");
				onPopupTextTimerPort.text = ((int)t.TotalHours).ToString("d2")+":"+t.Minutes.ToString("d2")+":"+t.Seconds.ToString("d2") + " Remaining";
				tvboarderPort.GetComponent<Image> ().color = origColor;
			}
		} else {
			tvboarderPort.GetComponent<Image>().color = Color.Lerp(origColor, blinkColor, Mathf.PingPong(Time.time, 1));
		}

	}

	public void CalculateReward(){ //calculate Rewards
		mainGen = GameObject.Find ("Generators").GetComponent<MainGeneratorScript> ();
		nameGenerators = mainGen.genNames_Code;
		string ActiveScene = GameObject.Find ("GameTimer").GetComponent<TimerSingleton> ().ActiveScene;
		if (ES2.Exists ("sysString_" + ActiveScene)) {
			numOfflineErn = 0;
			for (int i = 0; i < nameGenerators.Length; i++) {
				if (ES2.Exists ("btnName_" + nameGenerators[i]) && ES2.Load<bool> ("btnName_" + nameGenerators[i] + "?tag=HasManager")) {// get 10% of every Generator earnings 

					if (ES2.Load<bool> ("btnName_" + nameGenerators[i] + "?tag=Status") == true) { // check if the multiplier is already buy or not
						
						double basicEarning = ES2.Load<double> ("btnName_" + nameGenerators[i] + "?tag=Amount"); //basic earnings of generator 
						double unlockMultiplierOfProft = ES2.Load<double> ("btnName_" + nameGenerators[i] + "?tag=UnlockedprofitMultiplier"); //unlocked Multiplier of generator 
						double angelMultiplier = ES2.Load<double> ("btnName_" + nameGenerators[i] + "?tag=angelMultiplier");	//angel Multiplier of generator 
						double upgradeMultipliers = ES2.Load<double> ("btnName_" + nameGenerators[i] + "?tag=UpgradeprofitMultiplier");	//upgrade Multiplier of generator 

						double adMultipliers = 0; //ads multiplier

						int levelItem = ES2.Load<int> ("btnName_" + nameGenerators[i]  + "?tag=Level"); //get level of the generator

						if (ES2.Exists ("AdMultiplierTime_"+MoneyManagerObj.worldName)) {
							if (ES2.Load<float> ("AdMultiplierTime_"+MoneyManagerObj.worldName) == 0) { // check if the add multipler is still running or not
								adMultipliers = 1;
							} else {
								adMultipliers = 2;
							}
						}
//						Debug.Log ("Set AdMultiplierTime " + adMultipliers);

						double totalMultiplier = 0; // calculate the total multiplier
						if (unlockMultiplierOfProft == 0 && upgradeMultipliers == 0 && adMultipliers == 0) { //check if all multipler is 0 or else the answer for earnings will be always to 0
							totalMultiplier = 1;
						} else {
							totalMultiplier = unlockMultiplierOfProft + upgradeMultipliers + adMultipliers; // total of all multiplier
						}

						double TotalGenratorEarnCycle = (basicEarning * levelItem) * totalMultiplier * angelMultiplier; // get the total earnings for each generator

						numOfflineErn = numOfflineErn + (TotalGenratorEarnCycle * 0.10f); //get the earnings during offline and 10% of generator

					}
				}
			}


			currentDate = System.DateTime.Now;
			long temp = Convert.ToInt64(ES2.Load<string> ("sysString")); //load the time starts offline

			DateTime oldDate = DateTime.FromBinary(temp);
			TimeSpan difference = currentDate.Subtract(oldDate);

			int secondsInt = Convert.ToInt32(difference.TotalSeconds); //seconds during offline
			if (secondsInt > 59) { // check if the offline time is higher than 1min to earn reward
				double rewardIn = secondsInt / offlineSeconds; //getting the reward total seconds time during offline divid to offline per seconds (5)
				rewardGet = rewardIn*numOfflineErn;

				double[] dataRay = new double[]{ rewardGet ,secondsInt};
//				print ("reward " + rewardIn + " " + numOfflineErn);
				if (0.9f < rewardGet) {
					MessageDispatcher.SendMessage (this, "OnCheckReward", dataRay, 0.5f);
				} else {
					GameObject.Find ("RateReview").GetComponent<RateReviewScript> ().OnCheckIfHasRateReview ();
				}
			} else {
				GameObject.Find ("RateReview").GetComponent<RateReviewScript> ().OnCheckIfHasRateReview ();
			}

		} else {
			GameObject.Find ("RateReview").GetComponent<RateReviewScript> ().OnCheckIfHasRateReview ();

		}
	}




	void OnApplicationQuit()
	{
		ES2.Save(adTimerHolder, "AdMultiplierTime_"+MoneyManagerObj.worldName); 


		if (adMultiplierStatus == true) {
//			MyCustomMessage lMessage = MyCustomMessage.Allocate();
//			lMessage.Type = "OnSetCustomNotification";
//			lMessage.NotificationMessage = "Your Profit Doubler just ran out";
//			lMessage.NotificationTime = adTimerHolder;
//			lMessage.Sender = this;
//			lMessage.Delay = 1.0f;
//			MessageDispatcher.SendMessage(lMessage);
		}
	}

	void OnApplicationPause( bool pauseStatus )
	{
		
		if (pauseStatus == true) {
			adplier = false;
			ES2.Save (adTimerHolder, "AdMultiplierTime_" + MoneyManagerObj.worldName); 
		} else if (pauseStatus == false) {
			currentDate = System.DateTime.Now;
			if (watchingAds == false) {
				if (adplier == false) {
					adplier = true;
					OnSetupAdMultiplierTimer ();
				}
			} else {
				watchingAds = false;
			}
		}

	}

}

