﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using com.ootii.Messages;
public class BusinessManagerScript : MonoBehaviour {
	public MoneyManager MoneyManagerObj;
	public GameObject[] MissionControl,EventsObject;
	public GameObject[] launchPads;
	public float[] TimeBusinessUnlock;
	public double[] AmountBusiness;
	public bool Resort_Business = false,OSB_Business = false;
	public TextMeshProUGUI[] Resort_Message_1, Resort_Message_2_Time;
	public TextMeshProUGUI[] OldSchool_Message_1, OldSchool_Message_2_Time;
	public GameObject[] launchBtnResort,UnlockBtnResort;
	public GameObject[] launchBtnOldSchool,UnlockBtnOldSchool;

	DateTime currentDate; //current time when get back
	DateTime oldDate; //old date will save

	public bool[] timerLaunch;
	void Start () {
//		




		if (MoneyManagerObj.worldName == "Bitcoin") {
			if (ES2.Exists ("OpenResort")) {
				TimeBusinessUnlock [0] = ES2.Load<float> ("OpenResort");
				if (TimeBusinessUnlock [0] <= 0) {
					timerLaunch [0] = false;
//					Debug.Log ("Opened");
					Resort_Business = true;
					OnShowLaunchBtn_Resort ();
				} else {
					timerLaunch [0] = true;
//					Debug.Log ("Opening " + TimeBusinessUnlock [0]);
					Resort_Business = false;
					OnShowUnlockBtn_Resort ();
					OnCalculateTimeDuringOffline ();
				}
			} else {
				Debug.Log ("Lock");
				timerLaunch [0] = false;

				if (ES2.Exists ("OpenResort_Time")) {
					TimeBusinessUnlock [0] = ES2.Load<float> ("OpenResort_Time");
				}
				for (int i = 0; i < Resort_Message_1.Length; i++) {
					Resort_Message_1 [i].text = "Time";
					UnlockBtnResort [i].GetComponentInChildren<TextMeshProUGUI> ().text = "$" + MoneyManagerObj.GEtNumberNoDecimal (AmountBusiness [0]);
					System.TimeSpan t = System.TimeSpan.FromSeconds (TimeBusinessUnlock [0]);
//					Debug.Log ("time " + TimeBusinessUnlock [0]);
					Resort_Message_2_Time [i].text = ((int)t.TotalHours).ToString("d2")+":"+t.Minutes.ToString("d2")+":"+t.Seconds.ToString("d2");//string.Format ("{0:D2}:{1:D2}:{2:D2}", t.Hours, t.Minutes, t.Seconds);
				}

			}

			if (ES2.Exists ("OpenOldSchool")) {
				TimeBusinessUnlock [1] = ES2.Load<float> ("OpenOldSchool");
				if (TimeBusinessUnlock [1] <= 0) {
					timerLaunch [1] = false;
//					Debug.Log ("Opened");
					OSB_Business = true;
					OnShowLaunchBtn_OldSchool ();
				} else {
					timerLaunch [1] = true;
//					Debug.Log ("Opening " + TimeBusinessUnlock [1]);
					OSB_Business = false;
					OnShowUnlockBtn_OldSchool ();
					OnCalculateTimeDuringOffline ();
				}
			} else {
//				Debug.Log ("Lock");
				if (ES2.Exists ("OpenOldSchool_Time")) {
					TimeBusinessUnlock [1] = ES2.Load<float> ("OpenOldSchool_Time");
				}
				timerLaunch [1] = false;
				for (int i = 0; i < OldSchool_Message_1.Length; i++) {
					OldSchool_Message_1 [i].text = "Time";
					UnlockBtnOldSchool [i].GetComponentInChildren<TextMeshProUGUI> ().text = "$" + MoneyManagerObj.GEtNumberNoDecimal (AmountBusiness [1]);
					System.TimeSpan t = System.TimeSpan.FromSeconds (TimeBusinessUnlock [1]);
					OldSchool_Message_2_Time [i].text = ((int)t.TotalHours).ToString("d2")+":"+t.Minutes.ToString("d2")+":"+t.Seconds.ToString("d2");//string.Format ("{1:D2}:{1:D2}:{2:D2}", t.Hours, t.Minutes, t.Seconds);
				}

			}
		} else {
			for (int i = 0; i < launchPads.Length; i++) {
				launchPads [i].SetActive (false);
			}

			if (MoneyManagerObj.worldName == "Banker") {
				if (ES2.Exists ("OpenResort")) {
					float tempCheck = 0f;
					if (ES2.Exists ("OpenResort_Time")) {
						tempCheck = ES2.Load<float> ("OpenResort_Time");
						Debug.Log (tempCheck + " OpenResort_Time");
					} else {
						tempCheck = ES2.Load<float> ("OpenResort");
						Debug.Log (tempCheck + " OpenResort");
					}

					if (tempCheck <= 0) {
						for (int i = 0; i < launchPads.Length; i++) {
							launchPads [i].SetActive (true);
							OnShowLaunchBtn_Resort ();
						}
					}
				}
			}

			if (MoneyManagerObj.worldName == "Resort") {
				if (ES2.Exists ("OpenOldSchool")) {
					float tempCheck = 0f; 
					if (ES2.Exists ("OpenOldSchool_Time")) {
						tempCheck = ES2.Load<float> ("OpenOldSchool_Time");
						Debug.Log (tempCheck + " OpenOldSchool_Time");
					} else {
						tempCheck = ES2.Load<float> ("OpenOldSchool");
						Debug.Log (tempCheck + " OpenOldSchool");
					}
					if (tempCheck <= 0) {
//						Debug.Log (ES2.Load<float> ("OpenOldSchool"));
						for (int i = 0; i < launchPads.Length; i++) {
							launchPads [i].SetActive (true);
							OnShowLaunchBtn_OldSchool ();
						}
					}
				}
			}
		}


		for (int i = 0; i < MissionControl.Length; i++) {
			MissionControl [i].SetActive (true);
		}
		for (int i = 0; i < EventsObject.Length; i++) {
			EventsObject [i].SetActive (false);
		}



		MessageDispatcher.AddListener ("OnReduceTimeLaunch", OnReduceTimeLaunch, true);

	}

	void OnDisable(){

		if (MoneyManagerObj.worldName == "Bitcoin") {
			if (timerLaunch [0]) {
				ES2.Save (TimeBusinessUnlock [0], "OpenResort");
			}

			if (timerLaunch [1]) {
				ES2.Save (TimeBusinessUnlock [1], "OpenOldSchool");
			}
		}

		MessageDispatcher.RemoveListener ("OnReduceTimeLaunch", OnReduceTimeLaunch, true);
	}

	void OnReduceTimeLaunch (IMessage rMessage){
		float timeReduce = (float)rMessage.Data;

		if (ES2.Exists ("OpenResort")) {
			
			TimeBusinessUnlock [0] -= timeReduce;
			if (TimeBusinessUnlock [0] <= 0) {
				TimeBusinessUnlock [0] = 0;
			} else {
				System.TimeSpan t = System.TimeSpan.FromSeconds (TimeBusinessUnlock [0]);
				for (int j = 0; j < Resort_Message_2_Time.Length; j++) {
					Resort_Message_2_Time [j].text = ((int)t.TotalHours).ToString("d2")+":"+t.Minutes.ToString("d2")+":"+t.Seconds.ToString("d2");//string.Format ("{0:D2}:{1:D2}:{2:D2}", t.Hours, t.Minutes, t.Seconds);
				}
			}
			ES2.Save (TimeBusinessUnlock [0], "OpenResort");
			ES2.Save (TimeBusinessUnlock [0], "OpenResort_Time");
		}

		if (ES2.Exists ("OpenOldSchool")) {
			TimeBusinessUnlock [1] -= timeReduce;
			if (TimeBusinessUnlock [1] <= 0) {
				TimeBusinessUnlock [1] = 0;
			} else {
				System.TimeSpan a = System.TimeSpan.FromSeconds (TimeBusinessUnlock [1]);
				for (int l = 0; l < OldSchool_Message_2_Time.Length; l++) {
					OldSchool_Message_2_Time [l].text = ((int)a.TotalHours).ToString("d2")+":"+a.Minutes.ToString("d2")+":"+a.Seconds.ToString("d2");//string.Format ("{0:D2}:{1:D2}:{2:D2}", a.Hours, a.Minutes, a.Seconds);
				}
			}
			ES2.Save (TimeBusinessUnlock [1], "OpenOldSchool");
			ES2.Save (TimeBusinessUnlock [1], "OpenOldSchool_Time");
		}
	}

	public void OpenBusiness(int BusinessNum){
		
		if (MoneyManagerObj.totalMoney >= AmountBusiness [BusinessNum]) {
			MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
			MessageDispatcher.SendMessage (this, "OnDicrementMoney", AmountBusiness [BusinessNum], 0);	
			if (BusinessNum == 0) {
				OnShowUnlockBtn_Resort ();
				ES2.Save (TimeBusinessUnlock [0], "OpenResort");
				timerLaunch [0] = true;

			} else if (BusinessNum == 1) {
				OnShowUnlockBtn_OldSchool ();
				ES2.Save (TimeBusinessUnlock [1], "OpenOldSchool");
				timerLaunch [1] = true;
			}
			MessageDispatcher.SendMessage (this, "OnShowWatchLauch", 0, 0);	
		} else {
			MessageDispatcher.SendMessage (this, "PopButton", 2, 0f);
		}
	}
		
	//For RESORT _________________________________________________________
	void OnShowLaunchBtn_Resort(){
		
		for (int i = 0; i < launchBtnResort.Length; i++) {
			launchBtnResort [i].SetActive (true);
			Resort_Message_1[i].text = "OPEN FOR".ToUpper();
			Resort_Message_2_Time [i].text = "Business".ToUpper();
			ES2.Save (0f, "OpenResort");
			ES2.Save (0f, "OpenResort_Time");
			UnlockBtnResort [i].SetActive (false);
		}
		MessageDispatcher.SendMessage (this, "OnShowWatchLauch", 0, 0);	
	}

	void OnShowUnlockBtn_Resort(){
		
		for (int i = 0; i < UnlockBtnResort.Length; i++) {
			launchBtnResort [i].SetActive (false);
			UnlockBtnResort [i].SetActive (true);
			UnlockBtnResort [i].GetComponentInChildren<TextMeshProUGUI>().text = "OPENING";
		}
	}
	//---------------------------------------------------------------------------------------

	//For OLD SCHOOL _________________________________________________________
	void OnShowLaunchBtn_OldSchool(){
		for (int i = 0; i < launchBtnOldSchool.Length; i++) {
			launchBtnOldSchool [i].SetActive (true);
			OldSchool_Message_1[i].text = "OPEN FOR".ToUpper();
			OldSchool_Message_2_Time [i].text = "Business".ToUpper();
			ES2.Save (0f, "OpenOldSchool");
			ES2.Save (0f, "OpenOldSchool_Time");
			UnlockBtnOldSchool [i].SetActive (false);
		}
		MessageDispatcher.SendMessage (this, "OnShowWatchLauch", 0, 0);	
	}

	void OnShowUnlockBtn_OldSchool(){
		for (int i = 0; i < launchBtnResort.Length; i++) {
			launchBtnOldSchool [i].SetActive (false);
			UnlockBtnOldSchool [i].SetActive (true);
			UnlockBtnOldSchool [i].GetComponentInChildren<TextMeshProUGUI>().text = "OPENING";
		}

	}

	//---------------------------------------------------------------------------------------

	public void OnOpenMissionControl () {
		MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
		for (int i = 0; i < MissionControl.Length; i++) {
			MissionControl [i].SetActive (true);
		}
		for (int i = 0; i < EventsObject.Length; i++) {
			EventsObject [i].SetActive (false);
		}
	}

	public void OnOpenEvents(){
		MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
		for (int i = 0; i < MissionControl.Length; i++) {
			MissionControl [i].SetActive (false);
		}
		for (int i = 0; i < EventsObject.Length; i++) {
			EventsObject [i].SetActive (true);
		}
	}


	void OnCalculateTimeDuringOffline(){

		currentDate = System.DateTime.Now;
		long temp = Convert.ToInt64(ES2.Load<string> ("sysString")); //load the time starts offline

		DateTime oldDate = DateTime.FromBinary(temp);
		TimeSpan difference = currentDate.Subtract(oldDate);

		int secondsInt = Convert.ToInt32(difference.TotalSeconds); //seconds during offline

		if (timerLaunch [0]) {
			
//			if (ES2.Exists ("OpenResort_Time")) {
//				Debug.Log ("OpenResort_Time");
//				TimeBusinessUnlock [0] = ES2.Load<float> ("OpenResort_Time");
//			} else {
//				Debug.Log ("OpenResort");
//				TimeBusinessUnlock [0] = ES2.Load<float> ("OpenResort");
//			}

			Debug.Log(secondsInt + " " + TimeBusinessUnlock[0]);
			TimeBusinessUnlock[0] -= (float)secondsInt;

			if (TimeBusinessUnlock [0] <= 0) {
				timerLaunch [0] = false;
				Debug.Log ("Opened");
				Resort_Business = true;
				OnShowLaunchBtn_Resort ();
			}
		}


		if (timerLaunch [1]) {

//			if (ES2.Exists ("OpenOldSchool_Time")) {
//				TimeBusinessUnlock [1] = ES2.Load<float> ("OpenOldSchool_Time");
//			} else {
//				TimeBusinessUnlock [1] = ES2.Load<float> ("OpenOldSchool");
//			}

			Debug.Log(secondsInt + " " + TimeBusinessUnlock [1]);
			TimeBusinessUnlock [1] -= (float)secondsInt;
		
			if (TimeBusinessUnlock [1] <= 0) {
				timerLaunch [1] = false;
				Debug.Log ("Opened");
				OSB_Business = true;
				OnShowLaunchBtn_OldSchool ();
			}
		}

	}

	void Update(){
		if (MoneyManagerObj.worldName == "Bitcoin") {
			if (timerLaunch [0]) {
				float gamtetime = Time.deltaTime;
				TimeBusinessUnlock [0] -= gamtetime;
				if (TimeBusinessUnlock [0] < 0) {
					timerLaunch [0] = false;
					Resort_Business = true;
					TimeBusinessUnlock [0] = 0f;
					ES2.Save (0f, "OpenResort");
					OnShowLaunchBtn_Resort ();
				} else {
					System.TimeSpan t = System.TimeSpan.FromSeconds (TimeBusinessUnlock [0]);
					for (int i = 0; i < Resort_Message_2_Time.Length; i++) {
						Resort_Message_2_Time [i].text = ((int)t.TotalHours).ToString("d2")+":"+t.Minutes.ToString("d2")+":"+t.Seconds.ToString("d2");//string.Format ("{0:D2}:{1:D2}:{2:D2}", t.Hours, t.Minutes, t.Seconds);
					}

				}
			}
		}
		if (MoneyManagerObj.worldName == "Bitcoin") {
			if (timerLaunch [1]) {
				float gamtetime = Time.deltaTime;
				TimeBusinessUnlock [1] -= gamtetime;
				if (TimeBusinessUnlock [1] < 0) {
					timerLaunch [1] = false;
					OSB_Business = true;
					TimeBusinessUnlock [1] = 0f;
					ES2.Save (0f, "OpenOldSchool");
					OnShowLaunchBtn_OldSchool ();
				} else {
					System.TimeSpan t = System.TimeSpan.FromSeconds (TimeBusinessUnlock [1]);
					for (int i = 0; i < OldSchool_Message_2_Time.Length; i++) {
						OldSchool_Message_2_Time [i].text = ((int)t.TotalHours).ToString("d2")+":"+t.Minutes.ToString("d2")+":"+t.Seconds.ToString("d2");//string.Format ("{0:D2}:{1:D2}:{2:D2}", t.Hours, t.Minutes, t.Seconds);
					}
				}
			}
		}
	}

	void OnApplicationQuit()
	{
		if (MoneyManagerObj.worldName == "Bitcoin") {
			if (timerLaunch [0]) {
				ES2.Save (TimeBusinessUnlock [0], "OpenResort");
			}

			if (timerLaunch [1]) {
				ES2.Save (TimeBusinessUnlock [1], "OpenOldSchool");
			}
		}
	}

	void OnApplicationPause( bool pauseStatus )
	{
//		
		if (MoneyManagerObj.worldName == "Bitcoin") {
			if (pauseStatus == true) {

				if (timerLaunch [0]) {
					ES2.Save (TimeBusinessUnlock [0], "OpenResort");
				}
				if (timerLaunch [1]) {
					ES2.Save (TimeBusinessUnlock [1], "OpenOldSchool");
				}
			} else if (pauseStatus == false) {
				if (ES2.Exists ("sysString")) {
					OnCalculateTimeDuringOffline ();	
				}
					

			}
		}
	}

}
