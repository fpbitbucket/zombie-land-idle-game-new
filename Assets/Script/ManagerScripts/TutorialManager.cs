﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using com.ootii.Messages;
using TMPro;
#if UNITY_IOS
using NotificationServices = UnityEngine.iOS.NotificationServices;
using NotificationType = UnityEngine.iOS.NotificationType;
#endif

public class TutorialManager : MonoBehaviour
{
	public GameObject tutorialPanelPort;
	public GameObject[] tutorialObjPort, RestorGameSavePop;
	public GameObject IntoTutorial, intro_1, intro_2;
	//public GameObject[] forcenotTap;
//	public TextMeshProUGUI[] textPortrait;
	public string[] tutorialTextMessage, tutorialName;
	public bool[] tutorialStats;
	private int numAnim = 0;
	private bool tutorialShowing = false;
	public ScrollRect MainGenScroll;
	int numIntro = 0;
//	public string page2Message;
	public GameObject[] BottomObj;
	void OnEnable ()
	{
		MessageDispatcher.AddListener ("OnShowTutorial", OnShowTutorial, true); 
		MessageDispatcher.AddListener ("OnCloseTutorial", OnCloseTutorial, true); 
		MessageDispatcher.AddListener ("OnUpdateTextTutorial", OnUpdateTextTutorial, true); 
		MessageDispatcher.AddListener ("OnCloseTutorialTemp", OnCloseTutorialTemp, true); 
		MessageDispatcher.AddListener("OnShowRestorDataPopup", OnShowRestorDataPopup, true);
		MessageDispatcher.AddListener("OnCloseSkipSideTutorial", OnCloseSkipSideTutorial, true);

		for (int i = 0; i < BottomObj.Length; i++) {
			BottomObj[i].SetActive (false);
		}

		tutorialPanelPort.SetActive (false);
		IntoTutorial.SetActive (false);
		MainGenScroll.enabled = true;
		for (int i = 0; i < tutorialObjPort.Length; i++) {
			tutorialObjPort [i].transform.Find("SpeechBubble").GetComponentInChildren<TextMeshProUGUI>().text = tutorialTextMessage [i];
			tutorialObjPort [i].SetActive (false);
		}

		// for (int i = 0; i < forcenotTap.Length; i++) {
		// 	forcenotTap[i].SetActive(false);
		// }
		
		if (ES2.Exists ("TotalTutorial")) {
			tutorialStats = ES2.LoadArray<bool> ("TotalTutorial");
//			if (ES2.Exists ("TotalTutorialV2") == false) {
//				tutorialStats [9] = false;
//				tutorialStats [10] = false;
//				tutorialStats [11] = false;
//				ES2.Save (tutorialStats, "TotalTutorial");
//				ES2.Save ("", "TotalTutorialV2");
//			}

		} else {
			ES2.Save (tutorialStats, "TotalTutorial");
			tutorialPanelPort.SetActive (true);
			IntoTutorial.SetActive (true);
			intro_1.SetActive (true);
			intro_2.SetActive (false);
			numIntro = 0;
		}

		if (tutorialStats [3]) {
			BottomObj [0].SetActive (true);
		}

		if (tutorialStats [12]) {
			BottomObj [1].SetActive (true);

		}

		if (tutorialStats [11]) {
			BottomObj [2].SetActive (true);
			BottomObj [3].SetActive (true);
			BottomObj [4].SetActive (true);
			BottomObj [5].SetActive (true);
		} else {
			if (tutorialStats [9] == true && (tutorialStats [11] == false || tutorialStats [10] == false)) {
				BottomObj [2].SetActive (true);
				BottomObj [3].SetActive (true);
				BottomObj [4].SetActive (true);
				BottomObj [5].SetActive (true);

				tutorialStats [9] = true;
				tutorialStats [10] = true;
				tutorialStats [11] = true;
				ES2.Save (tutorialStats, "TotalTutorial");
			}
		}
	}

	void OnDisable ()
	{
		MessageDispatcher.RemoveListener ("OnShowTutorial", OnShowTutorial, true);
		MessageDispatcher.RemoveListener ("OnCloseTutorial", OnCloseTutorial, true);
		MessageDispatcher.RemoveListener ("OnUpdateTextTutorial", OnUpdateTextTutorial, true); 
		MessageDispatcher.RemoveListener ("OnCloseTutorialTemp", OnCloseTutorialTemp, true);
		MessageDispatcher.RemoveListener("OnShowRestorDataPopup", OnShowRestorDataPopup, true);
		MessageDispatcher.RemoveListener("OnCloseSkipSideTutorial", OnCloseSkipSideTutorial, true); 
	}

	void OnCloseTutorialTemp (IMessage rMessage)
	{
		tutorialShowing = false;
		for (int i = 0; i < tutorialObjPort.Length; i++) {
			tutorialObjPort [i].SetActive (false);
		}
	}

	void OnShowRestorDataPopup(IMessage rMessage)
	{
//		Debug.Log("OnShowRestorDataPopup");
//		if (GameObject.Find("TimeTracker") && Application.loadedLevelName == "MainScene")
//		{
//			if (GameObject.Find("TimeTracker").GetComponent<SignInOnClick>().isConnected)
//			{
//				if (GameObject.Find("TimeTracker").GetComponent<FirebaseDataBaseScript>().hasExistingData)
//				{
//					Debug.Log("OnShowRestorDataPopup" + GameObject.Find("TimeTracker").GetComponent<FirebaseDataBaseScript>().hasExistingData);
//					double chips = 0;
//					if (ES2.Exists("TotalGold"))
//					{
//						chips = ES2.Load<double>("TotalGold");
//					}
//					double chipsFirebase = GameObject.Find("TimeTracker").GetComponent<FirebaseDataBaseScript>().FirebaseDataChips();
//					double tempMoney = GameObject.Find("Managers/MoneyManager").GetComponent<MoneyManager>().totalMoney;
//					double tempD_ = GameObject.Find("TimeTracker").GetComponent<FirebaseDataBaseScript>().FirebaseDataCash();
//					string cloudCash = "$" + GameObject.Find("Managers/MoneyManager").GetComponent<MoneyManager>().GetStringForValue(tempD_);
//
//					Debug.Log(chipsFirebase + " Chips ");
//					Debug.Log(cloudCash + " Cash ");
//
//					for (int i = 0; i < RestorGameSavePop.Length; i++)
//					{
//						RestorGameSavePop[i].SetActive(true);
//						RestorGameSavePop[i].transform.GetChild(1).GetChild(1).GetChild(3).GetComponent<TextMeshProUGUI>().text = "= " + chips.ToString();
//
//						RestorGameSavePop[i].transform.GetChild(1).GetChild(1).GetChild(4).GetComponent<TextMeshProUGUI>().text = "$" + GameObject.Find("Managers/MoneyManager").GetComponent<MoneyManager>().GetStringForValue(tempMoney);
//						RestorGameSavePop[i].transform.GetChild(1).GetChild(1).GetChild(5).GetComponent<TextMeshProUGUI>().text = System.DateTime.Now.ToString();
//
//						RestorGameSavePop[i].transform.GetChild(1).GetChild(2).GetChild(3).GetComponent<TextMeshProUGUI>().text = "= " + chipsFirebase.ToString();
//						RestorGameSavePop[i].transform.GetChild(1).GetChild(2).GetChild(4).GetComponent<TextMeshProUGUI>().text = cloudCash;
//						RestorGameSavePop[i].transform.GetChild(1).GetChild(2).GetChild(5).GetComponent<TextMeshProUGUI>().text = GameObject.Find("TimeTracker").GetComponent<FirebaseDataBaseScript>().DataSave;
//					}
//				}
//				else
//				{
//					ES2.Save(1, "OnShowRestorDataPopup");
//					OnLocalDataLoad();
//				}
//			}
//		}
	}

	void OnCloseSkipSideTutorial(IMessage rMessage)
	{
		MessageDispatcher.SendMessage(this, "OnUpdateTextTutorial", 2, 0f);
		MessageDispatcher.SendMessage(this, "OnCloseTutorial", 11, 0f);
	}

	void OnUpdateTextTutorial(IMessage rMessage)
	{
		int num = (int)rMessage.Data;
		if (tutorialStats[11] == false)
		{
			if (num == 0)
			{
				ES2.Save(0, "SpinTutorialAgain");
				tutorialObjPort[11].transform.Find("SpeechBubble").GetComponentInChildren<TextMeshProUGUI>().text = "You can spin again to get higher profit boost or you can add 4 hours more to increase your timer!";
				tutorialObjPort[11].transform.GetChild(0).gameObject.SetActive(true);
				tutorialObjPort[11].transform.GetChild(1).gameObject.SetActive(false);
			}
			else if (num == 1)
			{
				tutorialObjPort[11].transform.GetChild(1).gameObject.SetActive(false);
				tutorialObjPort[11].transform.GetChild(0).gameObject.SetActive(false);
				tutorialObjPort[11].transform.GetChild(2).gameObject.SetActive(false);
				ES2.Save(1, "SpinTutorialAgain");
				tutorialObjPort[11].transform.Find("SpeechBubble").GetComponentInChildren<TextMeshProUGUI>().text = "Purrfect!";
				MessageDispatcher.SendMessage(this, "OnUpdateTextTutorial", 2, 2f);
				MessageDispatcher.SendMessage(this, "OnCloseTutorial", 11, 5f);
			}
			else if (num == 2)
			{
				ES2.Save(2, "SpinTutorialAgain");
				#if UNITY_IOS
				NotificationServices.RegisterForNotifications(
				NotificationType.Alert |
				NotificationType.Badge |
				NotificationType.Sound);
				#endif
				tutorialObjPort[11].transform.GetChild(1).gameObject.SetActive(false);
				tutorialObjPort[11].transform.GetChild(0).gameObject.SetActive(false);
				tutorialObjPort[11].transform.GetChild(2).gameObject.SetActive(false);
				tutorialObjPort[11].transform.Find("SpeechBubble").GetComponentInChildren<TextMeshProUGUI>().text = "I'll notify you when it runs out, so we can start it up again! Or you can watch more ads and continue getting boost for another 4 hours!";
			}
		}
	}

	public void OnShowFirst ()
	{
		intro_1.SetActive (false);
		intro_2.SetActive (true);
		numIntro++;
		if (numIntro == 2) {
			MessageDispatcher.SendMessage (this, "OnGA_Report", "Tutorial:Intro", 0);
			MessageDispatcher.SendMessage (this, "OnShowTutorial", 0, 0);
		}
	}

	void OnShowTutorial (IMessage rMessage)
	{
		int num = (int)rMessage.Data;
		if (tutorialShowing == false && tutorialStats [num] == false && Application.loadedLevelName == "MainScene") {
			tutorialShowing = true;
			tutorialObjPort [num].SetActive (true);
//			MessageDispatcher.SendMessage (this, "OnSoundFx", 8, 0f);
			if (num == 0) {
				IntoTutorial.SetActive (false);
				MainGenScroll.enabled = false;
			} else if (num == 1) {
				// forcenotTap [0].SetActive (true);
			} else if (num == 2) {
				// forcenotTap [1].SetActive (true);
				MainGenScroll.verticalNormalizedPosition = 1;
				MainGenScroll.enabled = false;
			} else if (num == 3) {
				BottomObj [0].SetActive (true);
			} else if (num == 12) {
				BottomObj [1].SetActive (true);
			} else if (num == 8) {
				MessageDispatcher.SendMessage (this, "OnShowMultiplierBtn", 0, 0);
			} 
			tutorialPanelPort.SetActive (true);



			// if (tutorialObjPort [num].transform.Find ("CHARACTER").gameObject.activeInHierarchy) {
			// 	Animator tempChar = tutorialObjPort[num].transform.Find ("CHARACTER").gameObject.GetComponent<Animator>();
			// 	int ranNum = Random.Range (0, 6);
			// 	if (ranNum == 0 || ranNum == 3 || ranNum == 5){
			// 		tempChar.Play ("Tutorial01");
			// 	}else {
			// 		tempChar.Play ("Tutorial02");
			// 	}
			// }

		}
	}
	public void OnCloseTutoriaSkip (int num)
	{
		if (num == 10) {
			MessageDispatcher.SendMessage (this, "OnCloseTutorial", 10, 0);
			MessageDispatcher.SendMessage (this, "OnShowTutorial", 11, 0);
			MessageDispatcher.SendMessage (this, "OnCloseTutorial", 11, 0);
			#if UNITY_IOS
					NotificationServices.RegisterForNotifications(
					NotificationType.Alert |
					NotificationType.Badge |
					NotificationType.Sound);
			#endif 
			
		}
	}
	public void OnCloseTutorialUsingChar (int num)
	{
		if (Application.loadedLevelName == "MainScene") {
			MessageDispatcher.SendMessage (this, "OnCloseTutorial", num, 0);
			if (num == 5) {
				MessageDispatcher.SendMessage (this, "OnShowTutorial", 6, 0);
			} else if (num == 6) {
				MessageDispatcher.SendMessage (this, "OnShowTutorial", 7, 0);
			} else if (num == 11) {
				if (tutorialStats [10] == false && tutorialShowing) {
					MessageDispatcher.SendMessage (this, "OnCloseTutorial", 10, 0);
					MessageDispatcher.SendMessage (this, "OnShowTutorial", 11, 0);
					MessageDispatcher.SendMessage (this, "OnCloseTutorial", 11, 0);
				}
			}
		}
	}

	void OnCloseTutorial (IMessage rMessage)
	{
		int num = (int)rMessage.Data;
		if (MainGenScroll.enabled == false) {
			MainGenScroll.enabled = true;
		}

		if (tutorialShowing == true && tutorialStats [num] == false) {
			
			for (int i = 0; i < tutorialObjPort.Length; i++) {
				
				tutorialObjPort [i].SetActive (false);
			}

			// for (int i = 0; i < forcenotTap.Length; i++) {
			// 	forcenotTap[i].SetActive(false);
			// }

			if (num == 11) {
				if (GameObject.Find ("SpinHandler")) {
					GameObject.Find ("SpinHandler").GetComponent<SpinnerDG> ().OnActiveCloseButton ();
				}
				BottomObj[2].SetActive(true);
        BottomObj[3].SetActive(true);
        BottomObj[4].SetActive(true);
				BottomObj [5].SetActive (true);
			}

			tutorialStats [num] = true;
			ES2.Save (tutorialStats, "TotalTutorial");
			tutorialShowing = false;
			string[] dataString = new string[]{ "OnDone", tutorialName [num] };
			MessageDispatcher.SendMessage (this, "OnSendAnalytics_Unity", dataString, 0);
//			MessageDispatcher.SendMessage (this, "OnGA_Report", num + ".Tutorial-" + tutorialName [num], 0);
			MessageDispatcher.SendMessage (this, "OnGA_Report", "Tutorialv2:"+num + "."+ tutorialName [num], 0);
		}
	}
}
