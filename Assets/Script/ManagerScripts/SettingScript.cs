﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Messages;
using TMPro;
public class SettingScript : MonoBehaviour {

	private AudioSource audioPlayer;
	public string iOSRateMeLink;
	public string GplayRateMeLink;
	public GameObject[] buttonColor;
	public GameObject[] settingPane,CreditPanel;
//	public AudioClip tap_1, tap_2;

	string nameScene;
	public bool canAudio = false,startOpen = false;
	void Start(){
		startOpen = false;
		audioPlayer = gameObject.GetComponent<AudioSource> () as AudioSource;
		canAudio = false;
		nameScene = Application.loadedLevelName;



		for (int j = 0; j < settingPane.Length; j++) {
			settingPane[j].SetActive (true);
		}

		for (int j = 0; j < CreditPanel.Length; j++) {
			CreditPanel[j].SetActive (false);
			CreditPanel [j].transform.GetChild (3).GetComponent<TextMeshProUGUI> ().text = "Ver "+Application.version.ToString();
		}

		if(ES2.Exists("SoundCheck_" + nameScene)){
			int musicNum = ES2.Load<int>("SoundCheck_" + nameScene);
			if (musicNum == 1){
				OnMusicBtnStatus(1);
				canAudio = true;
			}else if (musicNum == 2){
				OnMusicBtnStatus(0);
				canAudio = false;
			}

		}else{
			OnMusicBtnStatus(1);
			ES2.Save(1, "SoundCheck_" + nameScene);
			canAudio = true;
		}

	}


	public void OnMusicController(){
		int musicNum = ES2.Load<int>("SoundCheck_" + nameScene);
		Debug.Log (musicNum);
		if (musicNum == 1){
			ES2.Save(2, "SoundCheck_" + nameScene);
			canAudio = false;
			OnMusicBtnStatus(0);
		}else{
			canAudio = true;
			OnMusicBtnStatus(1);
			ES2.Save(1, "SoundCheck_" + nameScene);
		}
	}


	void OnMusicBtnStatus(int statsNum){
		MessageDispatcher.SendMessage(this, "OnTapGA_Report","SettingsMusicBtn", 0);
		if (statsNum == 0) {
			for (int j = 0; j < buttonColor.Length; j++) {
				buttonColor [j].transform.Find ("IconOn").gameObject.SetActive (false);
				buttonColor [j].transform.Find ("IconOff").gameObject.SetActive (true);
			}

		}else if (statsNum == 1) {
			for (int j = 0; j < buttonColor.Length; j++) {
				buttonColor [j].transform.Find ("IconOn").gameObject.SetActive (true);
				buttonColor [j].transform.Find ("IconOff").gameObject.SetActive (false);
			}
		}
	}

	void OnApplicationQuit() {
		int musicNum = ES2.Load<int>("SoundCheck_" + nameScene);
		if (musicNum == 2){
//			ES2.Save(1, "SoundCheck_" + nameScene);
		}else if (musicNum == 1){
//			ES2.Save(2, "SoundCheck_" + nameScene);
		}
	}

	void OnEnable() 
	{
		MessageDispatcher.AddListener("PopButton", OnPopButton, true);
	}

	void OnDisable(){
		MessageDispatcher.RemoveListener("PopButton", OnPopButton, true);
	}

	void OnPopButton(IMessage rMessage)
	{

		audioPlayer.Stop ();
		if ((int)rMessage.Data == 1) {
			audioPlayer.clip = GameObject.Find ("GameTimer").GetComponent<AudioFiles> ().audioFiles [0];
		} else if ((int)rMessage.Data == 2) {
			audioPlayer.clip = GameObject.Find ("GameTimer").GetComponent<AudioFiles> ().audioFiles [1];
		} else if (canAudio && (int)rMessage.Data == 3) {
			MessageDispatcher.SendMessage (this, "OnSoundFx", 2, 0f);
//			audioPlayer.clip = GameObject.Find ("GameTimer").GetComponent<AudioFiles> ().audioFiles [2];
		} else if (canAudio && (int)rMessage.Data == 4) {
			MessageDispatcher.SendMessage (this, "OnSoundFx", 3, 0f);
//			audioPlayer.clip = GameObject.Find ("GameTimer").GetComponent<AudioFiles> ().audioFiles [3];
		} else if (canAudio && (int)rMessage.Data == 5) {
			MessageDispatcher.SendMessage (this, "OnSoundFx", 4, 0f);
//			audioPlayer.clip = GameObject.Find ("GameTimer").GetComponent<AudioFiles> ().audioFiles [4];
		}else if ((int)rMessage.Data == 6) {
//			MessageDispatcher.SendMessage (this, "OnSoundFx", 5, 0f);
			audioPlayer.clip = GameObject.Find ("GameTimer").GetComponent<AudioFiles> ().audioFiles [5];
		}else if (canAudio && (int)rMessage.Data == 7) {
			MessageDispatcher.SendMessage (this, "OnSoundFx", 6, 0f);
//			audioPlayer.clip = GameObject.Find ("GameTimer").GetComponent<AudioFiles> ().audioFiles [6];
		}else if (canAudio && (int)rMessage.Data == 8) {
			MessageDispatcher.SendMessage (this, "OnSoundFx", 7, 0f);
//			audioPlayer.clip = GameObject.Find ("GameTimer").GetComponent<AudioFiles> ().audioFiles [7];
		}

		if (canAudio && startOpen && ((int)rMessage.Data == 1 || (int)rMessage.Data == 2 || (int)rMessage.Data == 6)) {
			audioPlayer.Play ();
		} else {
			if ((int)rMessage.Data == 1 || (int)rMessage.Data == 2 || (int)rMessage.Data == 6){
				StartCoroutine(canTapAudio());
			}
		}


//		startcou
	}

	IEnumerator canTapAudio(){
		yield return new WaitForSeconds (0.5f);
		startOpen = true;
	}

	public void BtnTapped (string btnName){
		AudioManager.playPopButton ();
		if (btnName == "likeus") {
			MessageDispatcher.SendMessage(this, "OnTapGA_Report","SettingsLikeusBtn", 0);
			Application.OpenURL ("https://www.facebook.com/FamilyPlayApps");
		} else if (btnName == "moregames") {
			#if UNITY_ANDROID
			Application.OpenURL ("https://play.google.com/store/apps/developer?id=Family%20Play%20ltd&hl=en");
			#endif

			#if UNITY_IOS
			Application.OpenURL ("https://itunes.apple.com/us/artist/family-play-pte.-ltd./id499477122");
			#endif
		} else if (btnName == "privacy") {
			MessageDispatcher.SendMessage(this, "OnTapGA_Report","SettingsPrivacyusBtn", 0);
			Application.OpenURL ("http://familyplay.co/privacy-policy-family-play/");
		} else if (btnName == "contactus") {
			MessageDispatcher.SendMessage(this, "OnTapGA_Report","SettingsContactusUsBtn", 0);
			string email = "support@kuronekogames.com";
			string subject = MyEscapeURL("Feedback for " + Application.productName);
			string body = MyEscapeURL("message");
			Application.OpenURL ("mailto:" + email + "?subject=" + subject + "&body=" + body);

		} else if (btnName == "restore") {
			Debug.Log ("restore");
		} else if (btnName == "rateme") {
			MessageDispatcher.SendMessage(this, "OnTapGA_Report","SettingsRateMeBtn", 0);
			#if UNITY_ANDROID
			Application.OpenURL (GplayRateMeLink);
			#endif
			#if UNITY_IOS
			Application.OpenURL (iOSRateMeLink);
			#endif
		} else if (btnName == "closecredits") {
			MessageDispatcher.SendMessage(this, "OnTapGA_Report","SettingsCloseCreditsBtn", 0);
			for (int j = 0; j < settingPane.Length; j++) {
				settingPane[j].SetActive (true);
			}

			for (int j = 0; j < CreditPanel.Length; j++) {
				CreditPanel[j].SetActive (false);
			}
		} else if (btnName == "credits") {
			MessageDispatcher.SendMessage(this, "OnTapGA_Report","SettingsOpenCreditsBtn", 0);
			for (int j = 0; j < settingPane.Length; j++) {
				settingPane[j].SetActive (false);
			}

			for (int j = 0; j < CreditPanel.Length; j++) {
				CreditPanel[j].SetActive (true);
			}
		}
	}
	string MyEscapeURL (string url)
	{
		return WWW.EscapeURL(url).Replace("+","%20");
	}
}
