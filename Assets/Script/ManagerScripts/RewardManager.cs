﻿using UnityEngine;
using System.Collections;
using com.ootii.Messages;
using TMPro;
public class RewardManager : MonoBehaviour {
//	public GameObject shoprewardPort,shoprewardPortDis;
	public GameObject rewardPopupClaimPort,rewardPopupPort;
	public GameObject rewardbtnPort;
	public float rewardTime;
	public TextMeshProUGUI rewardTextPort,rewardTextClaimPort;
	public string[] rewardName;
	public int[] rewardNum;
	public bool timeStart = false;
	public float temptime = 0;
	public bool hasReward = false;

	private int rewardSelect;
	void Awake () {
		hasReward = false;
		temptime = rewardTime;
		rewardPopupPort.SetActive (false);
		rewardPopupPort.transform.GetChild (0).gameObject.SetActive (false);
		rewardPopupPort.transform.GetChild (1).gameObject.SetActive (false);
		rewardPopupClaimPort.SetActive (false);

		rewardbtnPort.SetActive (false);

		StartCoroutine (OnloadRewardTime ());
	
		MessageDispatcher.AddListener ("OnShowReward", OnShowReward, true); 
	}

	void OnDisable(){
		MessageDispatcher.RemoveListener ("OnShowReward", OnShowReward, true); 
	}


	void OnShowReward (IMessage rMessage){
			rewardPopupClaimPort.SetActive (true);
	}

	IEnumerator OnloadRewardTime(){
		yield return new WaitForSeconds (1.5f);
		if (ES2.Exists ("hasReward") && ES2.Exists ("rewardtimeleft")) {
			temptime = ES2.Load<float> ("rewardtimeleft");
			if (temptime == 0) {
				timeStart = false;
				hasReward = true;

				if (GameObject.Find ("DailyRewardManager")) {
					if (GameObject.Find ("DailyRewardManager").GetComponent<DailyRewardScript> ().hasDailyReward) {
						
					} else {
						rewardbtnPort.SetActive (true);
					}
				}
//				Debug.Log ("has reward");
			} else {
				float timeOffline = TimerSingleton.instance.getTimeOfflineSeconds();          
				if (temptime < timeOffline) { // time offline is bigger than time remaining
					temptime = 0;
					timeStart = false;
					hasReward = true;
					if (GameObject.Find ("DailyRewardManager")) {
						if (GameObject.Find ("DailyRewardManager").GetComponent<DailyRewardScript> ().hasDailyReward) {
							
						} else {
							rewardbtnPort.SetActive (true);
						}
					}
				} else { // time remaining is bigger than time offline
					temptime = temptime - timeOffline;
					timeStart = true;
					rewardbtnPort.SetActive (false);
					hasReward = false;
				}
			}
		} else {
			rewardbtnPort.SetActive (false);
			hasReward = false; 
			timeStart = false;
			temptime = 0;
			ES2.Save (timeStart, "hasReward");
			ES2.Save(temptime, "rewardtimeleft"); 
		}

	}
	void Update () {
		if (timeStart) {
			temptime -= Time.deltaTime;

			if (temptime < 0) {
				timeStart = false;
				ES2.Save((float)0, "rewardtimeleft"); 
				temptime = rewardTime;
				hasReward = true;
				if (GameObject.Find ("DailyRewardManager")) {
					if (GameObject.Find ("DailyRewardManager").GetComponent<DailyRewardScript> ().hasDailyReward) {

					} else {
						rewardbtnPort.SetActive (true);
					}
				}
			}
		}
	}

	public void OnCloseNoReward(){
		rewardPopupPort.SetActive (false);
		rewardPopupPort.transform.GetChild (0).gameObject.SetActive (false);
		rewardPopupPort.transform.GetChild (1).gameObject.SetActive (false);
	}

	public void OnCloseClaimReward(){
		rewardPopupClaimPort.SetActive (false);
		MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
		if (rewardName [rewardSelect].ToLower () == "chips") {
			MessageDispatcher.SendMessage (this, "OnIncrementGold", (double)rewardNum [rewardSelect], 0);	
		} else if (rewardName [rewardSelect].ToLower () == "timewarp") {
		
		}
	}

	public void OnStartRewardTime(string fromTap){
		MessageDispatcher.SendMessage(this, "OnTapGA_Report","FreeRewardBtnOpen_"+fromTap, 0);
		rewardPopupPort.SetActive (true);
		rewardPopupPort.transform.GetChild (0).gameObject.SetActive (false);
		rewardPopupPort.transform.GetChild (1).gameObject.SetActive (false);
		if (hasReward) {
			OnSelectReward ();
			rewardPopupPort.transform.GetChild (0).gameObject.SetActive (true);
		} else {
			rewardPopupPort.transform.GetChild (1).gameObject.SetActive (true);
		}

	}

	public void OncloseRewardPopup(){
		
		MessageDispatcher.SendMessage(this, "OnTapGA_Report","FreeRewardBtnClose", 0);
		rewardbtnPort.SetActive (false);
		rewardPopupPort.SetActive (false);
		rewardPopupPort.transform.GetChild (0).gameObject.SetActive (false);
		rewardPopupPort.transform.GetChild (1).gameObject.SetActive (false);

		hasReward = false; 
		if (GameObject.Find ("DailyRewardManager")) {
			if (GameObject.Find ("DailyRewardManager").GetComponent<DailyRewardScript> ().hasDailyReward) {
				GameObject.Find ("DailyRewardManager").GetComponent<DailyRewardScript> ().dailyRewardbtnPort.SetActive (true);
			}
		}
		temptime = rewardTime;
		timeStart = true;
	}

	private void OnSelectReward(){
		rewardSelect = Random.Range (0, rewardName.Length);

		string nameReward = "";
		if (rewardName [rewardSelect].ToLower () == "chips") {
			nameReward = "Brains";
		}
		rewardTextPort.text = rewardNum [rewardSelect].ToString () + " " + nameReward;
		rewardTextClaimPort.text = rewardNum [rewardSelect].ToString () + " " + nameReward;

	}

	void OnApplicationQuit()
	{
		if (timeStart == true) {
			ES2.Save(temptime, "rewardtimeleft"); 
		}
	}

	void OnApplicationPause( bool pauseStatus )
	{
		if (pauseStatus == true) {
			if (timeStart == true) {
				ES2.Save(temptime, "rewardtimeleft"); 
			}
		} else {
			if (timeStart == true) {
				StartCoroutine (OnloadRewardTime ());
			}
		}
	}
}
