﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using com.ootii.Messages;
using DG.Tweening;
using TMPro;
public class UnlockManager : MonoBehaviour {
	public MainGeneratorScript mainGen;
	public MoneyManager moneymanager;
	public UnlockSript[] allUnlock;
	public UnlockGoldScript goldunlock;
//	public TextMeshProUGUI textTotalUnlockedPort;
//	public TextMeshProUGUI textTotalUnlockedGalleryPort;
//	public TextMeshProUGUI textGalleryText,textGalleryTextPort;
//	public TextMeshProUGUI textGallerySubPort;
	public int numAllUnlocks;
	public int totalUnlocks;
	public GameObject objPopupPort;
	public GameObject objGoldPopupPort;
//	public Image[] mainbtnIcon_Portrait;
//	public TextMeshProUGUI[] mainbtnName_Portrait;
	public GameObject refPort,refPortBot;

	float timeFade = 0;
	bool startFade = false,OnStartGame = false,popupFlag = false;

	public GameObject[] GenGlowingObj;
//	public GameObject[] textUnlock;
//	float[] textypost;
	void OnEnable () {
		OnStartGame = true;
//		audioFlag = false;
//		textypost = new float[textUnlock.Length];
//		for (int i = 0; i < textUnlock.Length; i++) {
//			textypost [i] = textUnlock [i].transform.localPosition.y;
//			textUnlock [i].GetComponent<TextMeshProUGUI> ().text = "";
//			textUnlock [i].SetActive (false);
//		}

		int totalUnlocks_All = 0;
		for (int i = 0; i < allUnlock.Length; i++) {
			totalUnlocks_All += allUnlock [i].numLevels.Length;
		}
		totalUnlocks_All += goldunlock.numLevels.Length;
		numAllUnlocks = totalUnlocks_All;

		objPopupPort.SetActive (false);
		objGoldPopupPort.SetActive (false);
		startFade = false;
		if (ES2.Exists ("TotalUnlocks_"+moneymanager.worldName)) {
			totalUnlocks = ES2.Load<int> ("TotalUnlocks_"+moneymanager.worldName);

//			textTotalUnlockedPort.text = totalUnlocks.ToString() + "/" + numAllUnlocks.ToString();
//			textTotalUnlockedGalleryPort.text = textTotalUnlockedPort.text;
		} else {

//			textTotalUnlockedPort.text = "0/" + numAllUnlocks.ToString();
//			textTotalUnlockedGalleryPort.text = textTotalUnlockedPort.text;
		}
//		textGallerySubPort.text = "Unlock these achievements for bonuses & surprises!";

		MessageDispatcher.AddListener ("OnShowGoldPopupReward", OnShowGoldPopupReward, true);
		MessageDispatcher.AddListener ("OnResetTotalMoney", OnResetMoney, true); 
		MessageDispatcher.AddListener ("OnShowPopupUnlocked", OnShowPopupUnlocked, true);
		MessageDispatcher.AddListener ("OnShowPopupSetIcon", OnShowPopupSetIcon, true); 
		MessageDispatcher.AddListener ("OnUpdateGallerySub", OnUpdateGallerySub, true); 
		MessageDispatcher.AddListener ("SmallUnlockTextUpgrade", SmallUnlockTextUpgrade, true); 
	}

	void OnDisable(){
		OnStartGame = false;
		MessageDispatcher.RemoveListener ("OnShowGoldPopupReward", OnShowGoldPopupReward, true);
		MessageDispatcher.RemoveListener ("OnResetTotalMoney", OnResetMoney, true); 
		MessageDispatcher.RemoveListener ("OnShowPopupUnlocked", OnShowPopupUnlocked, true); 
		MessageDispatcher.RemoveListener ("OnShowPopupSetIcon", OnShowPopupSetIcon, true); 
		MessageDispatcher.RemoveListener ("OnUpdateGallerySub", OnUpdateGallerySub, true); 
		MessageDispatcher.RemoveListener ("SmallUnlockTextUpgrade", SmallUnlockTextUpgrade, true); 
	}

	IEnumerator OnCloseRewardScreenDelay(){
		yield return new WaitForSeconds (2f);
		if (popupFlag == false) {
			OnCloseReward_ ();
		}
	}

	public void OnCloseGoldRewardScreen(){
		MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
		MessageDispatcher.SendMessage(this, "OnTapGA_Report","BrainRewardCollectBtn", 0);
		popupFlag = false;
		OnCloseReward_ ();
	}
	void OnCloseReward_ (){
//		audioFlag = false;
//		objGoldPopup.SetActive (false);
		objGoldPopupPort.SetActive (false);
		MessageDispatcher.SendMessage (this, "OnDoneAnimationReward",0, 0f);
	}

	void OnShowGoldPopupReward (IMessage rMessage){
		string[] stringText = (string[])rMessage.Data;
//		if (audioFlag == false) {
//			audioFlag = true;
			MessageDispatcher.SendMessage (this, "PopButton", 4, 0f);
//		}
		objGoldPopupPort.SetActive (true);
//		objGoldPopupPort.transform.Find ("CongratsBigBg").transform.GetChild (0).gameObject.SetActive (false);
//		objGoldPopupPort.transform.Find ("CongratsBigBg").transform.GetChild (1).gameObject.SetActive (true);
//		objGoldPopupPort.transform.Find ("CongratsBigBg").transform.GetChild (2).gameObject.SetActive (false);
//		objGoldPopupPort.transform.Find ("CongratsBigBg").transform.GetChild (3).gameObject.SetActive (false);
//		objGoldPopupPort.transform.Find ("CongratsBigBg").transform.GetChild (4).gameObject.SetActive (false);
//
//		objGoldPopupPort.transform.Find ("CongratsBigBg").transform.GetChild (1).gameObject.GetComponent<TextMeshProUGUI>().text = stringText[0];

		if (stringText [2] == "Gold") {


//			objGoldPopupPort.transform.Find ("CongratsBigBg").transform.GetChild (0).gameObject.SetActive (true);
//			objGoldPopupPort.transform.Find ("CongratsBigBg").transform.GetChild (2).gameObject.SetActive (true);
//			objGoldPopupPort.transform.Find ("CongratsBigBg").transform.GetChild (3).gameObject.SetActive (true);
//
			objGoldPopupPort.transform.GetChild (2).gameObject.GetComponent<TextMeshProUGUI>().text = stringText[1];


//			StopCoroutine (OnCloseRewardScreenDelay ());
		} else if (stringText [2] == "DoubleSpeed" || stringText [2] == "ProfitDouble") {
//			objGoldPopupPort.transform.Find ("CongratsBigBg").transform.GetChild (4).gameObject.SetActive (true);
			objGoldPopupPort.transform.GetChild (2).gameObject.GetComponent<TextMeshProUGUI>().text = stringText[1];
			

		}
		popupFlag = true;
//		StopCoroutine (OnCloseRewardScreenDelay ());
//		StartCoroutine (OnCloseRewardScreenDelay ());
	}

	void OnUpdateGallerySub (IMessage rMessage){
		string subText = (string)rMessage.Data;

//		textGallerySubPort.text = "Level "+subText;
	}

	void OnShowPopupSetIcon (IMessage rMessage){
		Sprite IconSrpit = (Sprite)rMessage.Data;
//		objPopupPort.transform.Find ("CircleIcon").transform.Find ("Icon").GetComponent<Image> ().sprite = IconSrpit;
	}

	string HoldUnlockText= "";
	void SmallUnlockTextUpgrade (IMessage rMessage){
		HoldUnlockText = (string)rMessage.Data;
	}
	void OnShowPopupUnlocked (IMessage rMessage){
//		string[] stringText = (string[])rMessage.Data;
		int[] num_generator = (int[])rMessage.Data;
		startFade = false;
		timeFade = 0;
		GenGlowingObj [num_generator [0]].transform.Find ("GeneratorBaseGrp").transform.GetComponent<Image> ().color = Color.white;
		Color tempColor = GenGlowingObj[num_generator[0]].transform.Find ("GeneratorBaseGrp").transform.GetComponent<Image> ().color;
		GenGlowingObj[num_generator[0]].transform.Find ("GeneratorBaseGrp").transform.GetComponent<Image> ().DOColor (Color.red,1).SetEase (Ease.Linear).SetLoops (11, LoopType.Yoyo).OnComplete(()=>MyCallback(num_generator[0], tempColor));


//		textUnlock [num_generator].GetComponent<TextMeshProUGUI> ().text = HoldUnlockText;
//		HoldUnlockText = "";
//		textUnlock [num_generator].SetActive (true);
//		textUnlock [num_generator].transform.DOLocalMoveY(textypost[num_generator] + 100.5f,1f).OnComplete(() => { 
//			textUnlock [num_generator].transform.DOLocalMoveY(0,0);
//			textUnlock [num_generator].SetActive (false);
//		});

		objPopupPort.SetActive (true);
		string nameGenerator = mainGen.genNamesString[num_generator[0]];
		objPopupPort.transform.GetChild (1).GetChild (0).GetComponent<TextMeshProUGUI> ().text = nameGenerator +" Level "+num_generator[1]+"\n"+"<color=red>"+HoldUnlockText;
		objPopupPort.transform.GetChild (1).gameObject.SetActive (false);
//		

//		objPopupPort.transform.Find ("Title").GetComponent<TextMeshProUGUI>().text = stringText[0];
//		objPopupPort.transform.Find ("Text").GetComponent<TextMeshProUGUI>().text = stringText[1];
		objPopupPort.GetComponent<CanvasGroup> ().alpha = 1;
		objPopupPort.transform.DOMove (refPortBot.transform.position, 0f);
		objPopupPort.transform.DOMove (refPort.transform.position, 0.5f).OnComplete (OnStartFading);

//		if (audioFlag == false) {
//			audioFlag = true;
			MessageDispatcher.SendMessage (this, "PopButton", 3, 0f);
//		}

	}

	void MyCallback(int numGen,Color TempColor){
		GenGlowingObj[numGen].transform.Find ("GeneratorBaseGrp").transform.GetComponent<Image> ().color = TempColor;
	}

	void OnStartFading(){
		objPopupPort.transform.GetChild (1).gameObject.SetActive (true);
		StopCoroutine (OnDisablePopup ());
		StartCoroutine (OnDisablePopup ());
	}


	IEnumerator OnDisablePopup(){
		yield return new WaitForSeconds (1);
		startFade = true;
	}

	void OnResetMoney (IMessage rMessage){
		totalUnlocks = 0;
		ES2.Save(totalUnlocks, "TotalUnlocks_"+moneymanager.worldName);
	}


	void Update () {
		if (OnStartGame) {
			if (ES2.Exists ("TotalUnlocks_" + moneymanager.worldName)) {
				totalUnlocks = ES2.Load<int> ("TotalUnlocks_" + moneymanager.worldName);

//				textTotalUnlockedPort.text = totalUnlocks.ToString () + "/" + numAllUnlocks.ToString ();
//				textTotalUnlockedGalleryPort.text = textTotalUnlockedPort.text;
			} else {

//				textTotalUnlockedPort.text = "0/" + numAllUnlocks.ToString ();
//				textTotalUnlockedGalleryPort.text = textTotalUnlockedPort.text;
			}

			if (startFade) {
				float tempDeltaTime = Time.deltaTime;
				timeFade += 0.5f * tempDeltaTime;
				if (timeFade > 2) {
					startFade = false;
//					audioFlag = false;
					objPopupPort.GetComponent<CanvasGroup> ().alpha = 1;
//					objPopupPort.SetActive (false);
					timeFade = 0;
					objPopupPort.transform.position = refPortBot.transform.position;
					//		objGoldPopup.transform.position = refLandBot.transform.position;

				} else {
//					objPopupPort.GetComponent<CanvasGroup> ().alpha = Mathf.Lerp (1, 0, timeFade);

				}
			}
		}
	}
}
