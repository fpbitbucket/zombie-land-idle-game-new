﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;	
using com.ootii.Messages;
public class HelpManagerScript : MonoBehaviour {
	public GameObject[] HelPopUp;
	public TextMeshProUGUI[] UI_Title,UI_Message;
	public string[] helpMesssage,TitleOfMessage;
	void Start () {
		for (int i = 0; i < HelPopUp.Length; i++) {
			HelPopUp[i].SetActive (false);
		}
	}
	
	// Update is called once per frame
	public void OnShow (int numMessage) {
		MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
		for (int i = 0; i < HelPopUp.Length; i++) {
			HelPopUp[i].SetActive (true);
			UI_Title [i].text = TitleOfMessage[numMessage];
			UI_Message[i].text = helpMesssage[numMessage];
		}
		if (numMessage == 0) {//Manager
			MessageDispatcher.SendMessage(this, "OnTapGA_Report","ManagerHelpBtn", 0);
		}else if (numMessage == 1) {//Cash Upgrade
			MessageDispatcher.SendMessage(this, "OnTapGA_Report","CashUpgradeHelpBtn", 0);

		}else if (numMessage == 2) {//Zombie Idol Upgrade
			MessageDispatcher.SendMessage(this, "OnTapGA_Report","ZombieIdolHelpBtn", 0);
		}else if (numMessage == 3) {//Zombie Idol Upgrade

		}else if (numMessage == 4) {//Zombie Idol Upgrade

		}else if (numMessage == 5) {//Shop
			MessageDispatcher.SendMessage(this, "OnTapGA_Report","ShopHelpBtn", 0);
		}
	}

	public void OnCloseAllHelp(){
		MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
		for (int i = 0; i < HelPopUp.Length; i++) {
			HelPopUp[i].SetActive (false);
		}

	}

}
