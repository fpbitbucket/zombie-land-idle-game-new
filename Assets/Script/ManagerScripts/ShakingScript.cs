﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class ShakingScript : MonoBehaviour {

	public float timerFloat = 20f;
	float tempTime = 0f;
	bool StartAnim = false;

	void OnEnable (){
		tempTime = timerFloat;
		StartAnim = true;
		gameObject.transform.DOLocalRotate (new Vector3 (0, 0, -10), 0);
	}

	void Update () {
		if (StartAnim) { 
			float tempDeltaTime = Time.deltaTime;
			timerFloat -= tempDeltaTime;
			System.TimeSpan t = System.TimeSpan.FromSeconds (tempDeltaTime);
			if (timerFloat < 0) {
				StartAnim = false;
				timerFloat = tempTime;
				gameObject.GetComponent<DOTweenAnimation> ().DOPause ();
				gameObject.transform.DOLocalRotate (new Vector3 (0, 0, 0), 0);
				StartCoroutine (OnStartAnimation ());
			}
		}
	}


	IEnumerator OnStartAnimation(){
		yield return new WaitForSeconds (20);
		gameObject.transform.DOLocalRotate (new Vector3 (0, 0, -10), 0);
		gameObject.GetComponent<DOTweenAnimation> ().DORestart ();
		StartAnim = true;
	}
}
