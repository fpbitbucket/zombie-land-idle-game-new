﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;
using com.ootii.Messages;
public class BonusController : MonoBehaviour {
	public GameObject bonusPrefb_Pill;
	public GeneratorScript[] generators;
//	public GameObject[] coinRef;
//	public GameObject[] coinObj;
	public GameObject tmproPrefab,coinPrefab;

	public double[] randomCoin;
	public float[] timeCoin;
	public float[] delayTimeCoin;
	public float timeMoveCoin;
	public bool startTime,checkIfcanServe;
	public float usedTime;


	Vector3[] posCoin;
	void Start () {
//		BottomBunos[0].SetActive (false);
//		BottomBunos[1].SetActive (false);
//		posCoin = new Vector3[2]; 
//		posCoin[0] = coinObj[0].transform.localPosition;
//		posCoin[1] = coinObj[1].transform.localPosition;
		startTime = false;
		checkIfcanServe = true;
		StartCoroutine(OnCheckingStatus5Gen ());
//		Instantiate (bonusPrefb_Pill,);
//		Instantiate(bingoBox,new Vector3(0f,0f,0f),Quaternion.identity) as GameObject;
//		Instantiate(bonusPrefb_Pill,new Vector3(Random.Range(-2.5f,2.5f),6.35f,0),Quaternion.identity);

	}


	void OnEnable (){
		MessageDispatcher.AddListener ("OnBunosTouch", OnBunosTouch, true);
	}

	void OnDisable () {
		MessageDispatcher.RemoveListener ("OnBunosTouch", OnBunosTouch, true);
	}

	void OnBunosTouch (IMessage rMessage){
		Vector3 ObjCoin = (Vector3 )rMessage.Data;
		OnBonusCoins (ObjCoin);
	}

	IEnumerator OnSelectTime(){
		yield return new WaitForSeconds (delayTimeCoin[Random.Range (0, delayTimeCoin.Length)]);
//		coinObj[0].transform.localPosition = posCoin[0];
//		coinObj[1].transform.localPosition = posCoin[1];
		usedTime = timeCoin[Random.Range (0, timeCoin.Length)];
		startTime = true;
	}

	private IEnumerator OnCheckingStatus5Gen()
	{ 
		while (checkIfcanServe) {
			yield return new WaitForSeconds (0.5f);
			int checkNum = 0;
			for (int i = 0; i < generators.Length; i++) {
				if (generators[i].levelItem >= 25) {
					checkNum++;
				} else {
					break;
				}
			}
			if (checkNum == 5) {
				Debug.Log ("ON Start Bonus Running");
				checkIfcanServe = false;
				StartCoroutine(OnSelectTime ());
			}
			yield return checkIfcanServe;
		}
	}

	void Update () {
		if (startTime) {
			float tempDeltaTime = Time.deltaTime;
			usedTime -= tempDeltaTime;
			if (usedTime < 0) {
				usedTime = 0;
				startTime = false;
				StartCoroutine( OnStartRollingCoin ());
			}
		}
	}


	IEnumerator OnStartRollingCoin(){
		MessageDispatcher.SendMessage (this, "PopButton", 5, 0f);
		for (int i = 0; i < 3; i++) {
			Instantiate(bonusPrefb_Pill,new Vector3(Random.Range(-2.5f,2.5f),6.35f,0),Quaternion.identity);
			yield return new WaitForSeconds (1.5f);
		}

//		BottomBunos[0].SetActive (true);
//		BottomBunos[1].SetActive (true);
//		coinObj[0].transform.DOLocalMoveX (coinRef[0].transform.localPosition.x, timeMoveCoin).OnComplete (OnDoneRollingCoin);
//		coinObj[1].transform.DOLocalMoveX (coinRef[1].transform.localPosition.x, timeMoveCoin).OnComplete (OnDoneRollingCoin);
	}



	void OnDoneRollingCoin(){
//		BottomBunos[0].SetActive (false);
//		BottomBunos[1].SetActive (false);
		StartCoroutine(OnSelectTime ());
	}

	public void OnBonusCoins(Vector3 btnObjVector3){
//		MessageDispatcher.SendMessage (this, "PopButton", 6, 0f);
		double numCoinGive = randomCoin[Random.Range(0,randomCoin.Length)];
		tmproPrefab.GetComponent<TextMeshPro> ().text = "$"+numCoinGive;
		MessageDispatcher.SendMessage (this, "btnObjVector3",numCoinGive, 0);
		GameObjectUtil.Instantiate(coinPrefab, btnObjVector3);
		GameObject tempObj_1 = GameObject.Instantiate (tmproPrefab);
		tempObj_1.transform.position = btnObjVector3;
	}

}
