﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using com.ootii.Messages;


public class LaunchTimeManager : MonoBehaviour {
	public GameObject[] launchPopup;
	public GameObject[] launchbtnMain;
	float timeToReduce = 10800f;
	int numLunchAds = 5;


	void OnEnable () {
		int unlockNum = 0;
		if (ES2.Exists ("LaunchTimeAds")) {
			numLunchAds = ES2.Load<int> ("LaunchTimeAds");
		}
		MessageDispatcher.AddListener ("OnLaunchAds", OnLaunchAds, true);
		MessageDispatcher.AddListener ("OnShowWatchLauch", OnShowWatchLauch, true);
//		OnCloseLaunchPopup ();
		for (int i = 0; i < launchPopup.Length; i++) {
			launchPopup [i].SetActive (false);
		}
		OnCheckingButton ();
		dayCheck ();
	}

	void OnDisable(){
		MessageDispatcher.RemoveListener ("OnLaunchAds", OnLaunchAds, true);
		MessageDispatcher.RemoveListener ("OnShowWatchLauch", OnShowWatchLauch, true);
	}

	void OnShowWatchLauch (IMessage rMessage){
		OnCheckingButton ();
	}

	void OnLaunchAds (IMessage rMessage){
		numLunchAds--;
		if (numLunchAds <= 0) {
			numLunchAds = 0;
		}
		ES2.Save(numLunchAds, "LaunchTimeAds"); 
		MessageDispatcher.SendMessage(this, "OnReduceTimeLaunch", 10800f, 0);
		OnCheckingButton ();
	}

	void OnCheckingButton () {
		if (numLunchAds <= 0) {
			LaunchBtn (0);
		} else {
			if (ES2.Exists ("OpenResort")) {
				if (ES2.Load<float> ("OpenResort") <= 0) {
					LaunchBtn (0);
					OnCheckOldSchool ();
				} else {
					LaunchBtn (1);
				}
			} else if (ES2.Exists ("OpenOldSchool")) {
				if (ES2.Load<float> ("OpenOldSchool") <= 0) {
					LaunchBtn (0);
					OnCheckResort ();
				} else {
					LaunchBtn (1);
				}
			} else {
				LaunchBtn (0);
			}
		}
	}

	void OnCheckOldSchool(){
		if (ES2.Exists ("OpenOldSchool")) {
			if (ES2.Load<float> ("OpenOldSchool") <= 0) {
				LaunchBtn (0);
			} else {
				LaunchBtn (1);
			}
		} else {
			LaunchBtn (0);
		}
	}

	void OnCheckResort(){
		if (ES2.Exists ("OpenResort")) {
			if (ES2.Load<float> ("OpenResort") <= 0) {
				LaunchBtn (0);
			} else {
				LaunchBtn (1);
			}
		} else {
			LaunchBtn (0);
		}
	}


	void LaunchBtn(int Status){
		if (Status == 0) {
			for (int i = 0; i < launchbtnMain.Length; i++) {
				launchbtnMain [i].SetActive (false);
			}
		} else {
			for (int i = 0; i < launchbtnMain.Length; i++) {
				launchbtnMain [i].SetActive (true);
			}
		}
	}

	void dayCheck(){
		if (ES2.Exists ("Launchbutton")) {
			string stringDate = ES2.Load<string> ("Launchbutton");
			DateTime oldDate = Convert.ToDateTime (stringDate);
			DateTime newDate = System.DateTime.Now;
//			Debug.Log ("LastDay: " + oldDate);
//			Debug.Log ("CurrDay: " + newDate);

			TimeSpan difference = newDate.Subtract (oldDate);
			if (difference.Days >= 1) {
//				Debug.Log ("New Reward!");
				string newStringDate = Convert.ToString (newDate);
				ES2.Save(newStringDate, "Launchbutton");
//				PlayerPrefs.SetString ("OnLaunchBonus", newStringDate);
				numLunchAds = 5;
				OnCheckingButton ();
			}
		} else {
			ES2.Save(Convert.ToString(System.DateTime.Now), "Launchbutton");
		}
	}

	void OnShowPopUp(){
		string textMessage = "You have " + numLunchAds.ToString ()+" more ads you can watch today!";
		for (int i = 0; i < launchPopup.Length; i++) {
			launchPopup [i].SetActive (true);
			launchPopup [i].transform.Find ("LookingToLunch/MessageText").GetComponent<TextMeshProUGUI> ().text = textMessage;
		}
	}

	public void OnShowLaunchPopup(){
		MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
		OnShowPopUp ();
	}

	public void OnCloseLaunchPopup(){
		MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
		for (int i = 0; i < launchPopup.Length; i++) {
			launchPopup [i].SetActive (false);
		}
	}


	void OnApplicationQuit()
	{


	}

	void OnApplicationPause( bool pauseStatus )
	{
		if (pauseStatus == true) {

		} else if (pauseStatus == false) {
			dayCheck ();	

		}
	}

}
