﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using com.ootii.Messages;

public class MusicManager : MonoBehaviour {
	public GameObject[] buttonColor;
	string nameScene;
	int musicNum = 0;
	void Start(){
		
		nameScene = Application.loadedLevelName;
		if(ES2.Exists("musicCheck_1" + nameScene)){
			
			if (ES2.Load<int>("musicCheck_1" + nameScene) == 1){
				OnMusicBtnStatus(1);
				musicNum = 1;
				gameObject.GetComponent<AudioSource>().Play();
			}else if (ES2.Load<int>("musicCheck_1" + nameScene) == 2){
				OnMusicBtnStatus(0);
				musicNum = 2;
				gameObject.GetComponent<AudioSource>().Stop();
			}

		}else{
			musicNum = 1;
			OnMusicBtnStatus(1);
			ES2.Save(1, "musicCheck_1" + nameScene);
			gameObject.GetComponent<AudioSource>().Play();

		}

//		print("Music " + musicNum);
	}


	void OnEnable(){
		MessageDispatcher.AddListener ("OnMusicSuspend", OnMusicSuspend, true);
	}

	void OnDisable () {
		MessageDispatcher.RemoveListener ("OnMusicSuspend", OnMusicSuspend, true);
	}

	void OnMusicSuspend (IMessage rMessage){
		int numSuspend = (int)rMessage.Data;
		if (numSuspend == 0) {
			if (musicNum == 1) {
				gameObject.GetComponent<AudioSource> ().Stop ();
			}
		} else if (numSuspend == 2) {
			if (musicNum == 1) {
				gameObject.GetComponent<AudioSource> ().Play ();
			}
		}

	}


	public void OnMusicController(){
		Debug.Log (musicNum);
		if (musicNum == 1){
			musicNum = 2;
			ES2.Save(2, "musicCheck_1" + nameScene);
			gameObject.GetComponent<AudioSource>().Stop();
			OnMusicBtnStatus(0);
		}else{
			OnMusicBtnStatus(1);
			musicNum = 1;
			ES2.Save(1, "musicCheck_1" + nameScene);
			gameObject.GetComponent<AudioSource>().Play(); 
		}
	}


	void OnMusicBtnStatus(int statsNum){
		if (statsNum == 0) {
			for (int j = 0; j < buttonColor.Length; j++) {
				buttonColor [j].transform.Find ("IconOn").gameObject.SetActive (false);
				buttonColor [j].transform.Find ("IconOff").gameObject.SetActive (true);
			}

		}else if (statsNum == 1) {
			for (int j = 0; j < buttonColor.Length; j++) {
				buttonColor [j].transform.Find ("IconOn").gameObject.SetActive (true);
				buttonColor [j].transform.Find ("IconOff").gameObject.SetActive (false);
			}
		}
	}

	void OnApplicationQuit() {
//		if (musicNum == 2){
//			ES2.Save(0, "musicCheck_1" + nameScene);
//		}else if (musicNum == 1){
//			ES2.Save(1, "musicCheck_1" + nameScene);
//		}
		Debug.Log("On Disable Music");
	}
}
