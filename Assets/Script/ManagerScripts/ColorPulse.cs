﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorPulse : MonoBehaviour {
	public Color origColor,blinkColor;
	// Use this for initialization
	void Start () {
		
	}

	// Update is called once per frame
	void Update () {
		gameObject.GetComponent<Image>().color = Color.Lerp(origColor, blinkColor, Mathf.PingPong(Time.time, 1));
	}
}
