﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AdviserChecker : MonoBehaviour {

	public bool checkIfcanServe = false,hasAdvise;
	public GameObject[] TipBtn;
	public Color origColor,blinkColor;

	void Start () {
		hasAdvise = false;
		StartCoroutine (OnCheckingStatus5Gen ());
	}
	

	private IEnumerator OnCheckingStatus5Gen()
	{ 
		while (checkIfcanServe) {
			if (gameObject.GetComponent<AdvisorScript> ().OnCheckCanAdvise ()) {
				hasAdvise = true;
			} else {
				for (int i = 0; i < TipBtn.Length; i++) {
					TipBtn [i].GetComponent<Image> ().color = origColor;
				}
				hasAdvise = false;
			}
			yield return new WaitForSeconds (5f);
			yield return checkIfcanServe;
		}
	}


	void Update () {
		if (hasAdvise) {
			for (int i = 0; i < TipBtn.Length; i++) {
				TipBtn [i].GetComponent<Image> ().color = Color.Lerp (origColor, blinkColor, Mathf.PingPong (Time.time, 1));
			}
		}
	}
}
