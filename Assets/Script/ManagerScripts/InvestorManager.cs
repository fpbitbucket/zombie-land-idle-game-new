﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using TMPro;
using com.ootii.Messages;
public class InvestorManager : MonoBehaviour {
//	Are you sure you want to reset
//	and gain  <#fff200>1</color>  bank support?
	public MoneyManager MoneyManagerObj;
	public TextMeshProUGUI TotalAngelTextPort,TotalAngelTextUpgrade;
	public TextMeshProUGUI TotalAngelToClaimTextPort;
	public TextMeshProUGUI TotalAngelbonusPercentPort;
	public GameObject popNoInvestorPort;

	public GameObject[] ClaimChipsPopup,ClaimWithChipsPopup,OnShowNotEnoughGold;

	public double numAngelInvestor = 0;
	public double TotalAngel = 0;
	public double TotalAngelSacrifice = 0;
	public int TotalReset = 0; 
	public double numAngelInvestorEffectiveness = 2;
	private double seasonEarnings = 0;
	public double lifeTimeEarnings = 0;
	public double totalclaimAngle = 0;

	//L - Lifetime Earnings
	//S - Session Earnings 
	//K - Sacrificed Angels
	//(150*(sqrt(L)-sqrt(L-S)))/(sqrt(10^15))-K
	//Mathf.Sqrt()


	public double tempInvestorNum = 0;
	public bool noresetClaim = false;
	private double noresetGoldAmount = 0;
	private string nameWorld = "";
	void Start () {
		nameWorld = MoneyManagerObj.worldName;
		popNoInvestorPort.SetActive (false);

		OnCloseClaimPopup ();
		OnCloseClaimWithChipsPopup ();

		for (int i = 0;i < OnShowNotEnoughGold.Length;++i){
			OnShowNotEnoughGold [i].SetActive (false);
		}

		MessageDispatcher.AddListener ("OnCheckInvestors", OnCheckInvestors, true); 
		MessageDispatcher.AddListener ("OnDicrementAngel", OnDicrementAngel, true); 
		MessageDispatcher.AddListener ("OnIncrementAngel", OnIncrementAngel, true); 
		MessageDispatcher.AddListener ("OnUpgradePerBonus", OnUpgradePerBonus, true); 
		MessageDispatcher.AddListener ("OnClaimWithNoReset", OnClaimWithNoReset, true); 

		seasonEarnings = MoneyManagerObj.seasonEarnings;

		if (ES2.Exists ("TotalAngelClaim_"+nameWorld)) {
			totalclaimAngle = ES2.Load<double> ("TotalAngelClaim_"+nameWorld);
		} else {
			totalclaimAngle = 0;
		}

		if (ES2.Exists ("TotalAngel_" + MoneyManagerObj.worldName)) {
			TotalAngel = ES2.Load<double> ("TotalAngel_" + MoneyManagerObj.worldName);
		} else {
			TotalAngel = 0;
			ES2.Save(TotalAngel, "TotalAngel_" + MoneyManagerObj.worldName);
		}

		if (ES2.Exists ("TotalAngelSacrifice_" + MoneyManagerObj.worldName)) {
			TotalAngelSacrifice = ES2.Load<double> ("TotalAngelSacrifice_" + MoneyManagerObj.worldName);
		} else {
			TotalAngelSacrifice = 0;
			ES2.Save(TotalAngelSacrifice, "TotalAngelSacrifice_" + MoneyManagerObj.worldName);
		}

		if (ES2.Exists ("TotalReset")) {
			TotalReset = ES2.Load<int> ("TotalReset");
		} else {
			TotalReset = 0;
			ES2.Save(TotalReset, "TotalReset");
		}

		if (ES2.Exists ("BonusPerAngel"+nameWorld)) {
			numAngelInvestorEffectiveness = ES2.Load<double> ("BonusPerAngel"+nameWorld);
		} else {
			numAngelInvestorEffectiveness = 2;
			ES2.Save(numAngelInvestorEffectiveness, "BonusPerAngel"+nameWorld);
		}
			
		lifeTimeEarnings = MoneyManagerObj.lifeTimeEarnings;
//		double sep = 12 - 15;
	
		//L- lifeTimeEarnings  S - seasonEarnings K- TotalAngelSacrifice;
		double tempNumber = 150 * (Math.Sqrt (lifeTimeEarnings/Math.Pow(10,15))) - totalclaimAngle;
//		tempNumber = (150 * ((Math.Sqrt (lifeTimeEarnings) - Math.Sqrt (lifeTimeEarnings - seasonEarnings))) )/ (Math.Sqrt (Math.Pow(10, 15))) - TotalAngelSacrifice;

//		double tempOldnumInvest = (150 * (Math.Sqrt (lifeTimeEarnings/Math.Pow(10,15))));
		if (tempNumber <= 0) {
			numAngelInvestor = 0;
		} else {
			numAngelInvestor = Math.Floor(tempNumber);
		}
//		Debug.Log (numAngelInvestor + " claim " + totalclaimAngle);
		numAngelInvestor = Math.Floor(tempNumber);

//		TextClaimPopup.text = "Are you sure you want to reset and gain  <#fff200>" + MoneyManagerObj.GetStringForValueNumbers(numAngelInvestor)+ "</color>  bank support?";
//		TextClaimPopupPort.text = "Are you sure you want to reset and gain  <#fff200>" + MoneyManagerObj.GetStringForValueNumbers(numAngelInvestor)+ "</color>  bank support?";
		TotalAngelToClaimTextPort.text =  MoneyManagerObj.GetStringForValueTemp(numAngelInvestor);

//		TotalAngelText_NoReset

		StartCoroutine (OnTotalAngelUpdater ());

		TotalAngelbonusPercentPort.text = numAngelInvestorEffectiveness.ToString () +"%";
	}

	void OnDisable(){
		
		MessageDispatcher.RemoveListener ("OnCheckInvestors", OnCheckInvestors, true);
		MessageDispatcher.RemoveListener ("OnDicrementAngel", OnDicrementAngel, true);
		MessageDispatcher.RemoveListener ("OnIncrementAngel", OnIncrementAngel, true);
		MessageDispatcher.RemoveListener ("OnUpgradePerBonus", OnUpgradePerBonus, true);
		MessageDispatcher.RemoveListener ("OnClaimWithNoReset", OnClaimWithNoReset, true);
	}

	public void OnCloseClaimPopup(){
		MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
		for (int i = 0; i < ClaimChipsPopup.Length; i++) {
			ClaimChipsPopup [i].SetActive (false);
		}
	}

	public void OnCloseClaimWithChipsPopup (){
		MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
		noresetClaim = false;
		for (int i = 0; i < ClaimWithChipsPopup.Length; i++) {
			ClaimWithChipsPopup [i].SetActive (false);
		}
	}

	public void OnOpenClaimWithChipsPopup (){
		MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
		for (int i = 0; i < ClaimWithChipsPopup.Length; i++) {
			ClaimWithChipsPopup [i].SetActive (true);
		}
	}

	public void OnOpenClaimPopup(){
		MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
//		MessageDispatcher.SendMessage(this, "OnShowTutorial",6, 0);
		string used_ = "You have "+MoneyManagerObj.GetStringForValueTemp(numAngelInvestor) + " Zombie Idols waiting to join and boost your profits!To hire them you will need to reset your progress or use your Golden Brains. Choose wisely!";
		for (int i = 0; i < ClaimChipsPopup.Length; i++) {
			ClaimChipsPopup [i].SetActive (true);
			ClaimChipsPopup [i].transform.Find ("BG/EnjoyText").GetComponent<TextMeshProUGUI> ().text = used_;
		}
	}

	void OnUpgradePerBonus(IMessage rMessage){
		double tempIncre = (double)rMessage.Data;

		numAngelInvestorEffectiveness = numAngelInvestorEffectiveness + tempIncre;
		ES2.Save(numAngelInvestorEffectiveness, "BonusPerAngel"+nameWorld);
		TotalAngelbonusPercentPort.text = numAngelInvestorEffectiveness.ToString ()+"%";
	}

	void OnIncrementAngel(IMessage rMessage){
		double tempIncre = (double)rMessage.Data;

		if (ES2.Exists ("TotalAngelClaim_"+nameWorld)) {
			totalclaimAngle = ES2.Load<double> ("TotalAngelClaim_"+nameWorld) + tempIncre;
		} else {
			totalclaimAngle = tempIncre;

		}

		ES2.Save(totalclaimAngle, "TotalAngelClaim_"+nameWorld);
//		numAngelInvestor = totalclaimAngle;

		TotalAngel = TotalAngel + tempIncre;
		ES2.Save(TotalAngel, "TotalAngel_" + MoneyManagerObj.worldName);
	
	}

	void OnDicrementAngel(IMessage rMessage){

		double tempIncre = (double)rMessage.Data;
		TotalAngel = TotalAngel - tempIncre;
		if (TotalAngel <= 0){
			TotalAngel = 0;
		}
		ES2.Save(TotalAngel, "TotalAngel_" + MoneyManagerObj.worldName);

		// totalclaimAngle = TotalAngel - tempIncre;
		// ES2.Save(totalclaimAngle, "TotalAngelClaim_"+nameWorld);

		TotalAngelSacrifice = tempIncre + TotalAngelSacrifice;
		ES2.Save(TotalAngelSacrifice, "TotalAngelSacrifice_" + MoneyManagerObj.worldName);


	}
	private IEnumerator OnTotalAngelUpdater()
	{ 
		while (true) {
			yield return new WaitForSeconds (0.5f);
			if (tempInvestorNum != TotalAngel) {
				TotalAngelTextPort.text = MoneyManagerObj.GetStringForValueTemp(TotalAngel);
				TotalAngelTextUpgrade.text = "YOUR ZOMBIE IDOLS: "+MoneyManagerObj.GetStringForValueTemp(TotalAngel);
				tempInvestorNum = TotalAngel;
			}

			yield return true;
		}
	}
	double tempNumInvestor = 0;
	void OnCheckInvestors(IMessage rMessage){
		double totalSeasonMoney = (double)rMessage.Data;
		seasonEarnings = MoneyManagerObj.seasonEarnings;
		lifeTimeEarnings = MoneyManagerObj.lifeTimeEarnings;
		TotalAngelSacrifice = ES2.Load<double> ("TotalAngelSacrifice_" + MoneyManagerObj.worldName);
//		Debug.Log
		double tempOldnumInvest = 150 * (Math.Sqrt (lifeTimeEarnings/Math.Pow(10,15))) - totalclaimAngle;
//		Debug.Log (tempOldnumInvest + " === " + totalclaimAngle);
		if (Math.Floor (tempOldnumInvest) <= 0) {
			numAngelInvestor = 0;
		} else if (Math.Floor(tempOldnumInvest) >  Math.Floor(totalclaimAngle)) {
			if (Math.Floor (tempOldnumInvest) > numAngelInvestor) {
				numAngelInvestor = Math.Floor (tempOldnumInvest);
				MessageDispatcher.SendMessage (this, "OnTriggerMenuNotifyOn", 2, 0);
			}
		}

		TotalAngelToClaimTextPort.text =  MoneyManagerObj.GetStringForValueTemp(numAngelInvestor);

	}

	public void tempAngel(){
		MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
		MessageDispatcher.SendMessage(this, "OnIncrementAngel",(double)100, 0);
	}

	public void tempAngel2(){
		MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
		if (TotalAngel == 0) {
			TotalAngel = 1;
		}
		double tempAngel = TotalAngel * 10;
		MessageDispatcher.SendMessage(this, "OnIncrementAngel",tempAngel, 0);
	}

	public void OnClaimInvestors(){
		
		noresetClaim = false;
		OnCheckIfcanbuy ();
	}

	public void OnClaimInvestorsConfirmation(string name){
		if (name == "yes") {
			
			double tempnum = 0;
//			ES2.Save(tempnum, "TotalSeasonEarning");
			MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);

			TotalReset = ES2.Load<int> ("TotalReset") + 1;
			ES2.Save(TotalReset, "TotalReset");
			OnCloseClaimPopup ();
			OnCloseClaimWithChipsPopup ();

			//K- TotalAngelSacrifice;
			//C - numAngelInvestor to claim
			// A - TotalAngel
			
			double TotalInvestorCalculate = (numAngelInvestor + TotalAngel) - TotalAngelSacrifice;
			if (TotalInvestorCalculate <= 0){
				TotalInvestorCalculate = (numAngelInvestor + TotalAngel);
			}
			double totalAngelMultiplier =  Math.Floor(TotalInvestorCalculate) * (numAngelInvestorEffectiveness / 100);

			ES2.Save(0, "TotalUpgrades");
			ES2.Save(0, "TotalAngelUnlock");
		
			MessageDispatcher.SendMessage(this, "OnIncrementAngel",numAngelInvestor, 0);

			numAngelInvestor = 0;
			tempInvestorNum = 0;

			TotalAngelToClaimTextPort.text =  MoneyManagerObj.GetStringForValueTemp(numAngelInvestor);

			if (noresetClaim == false) {
				MessageDispatcher.SendMessage(this, "OnTapGA_Report","OnClaimZombieIdols", 0);
				MessageDispatcher.SendMessage(this, "OnResetAllGenerator",totalAngelMultiplier, 0);	
				MessageDispatcher.SendMessage (this, "OnResetTotalMoney", 0, 0f);
				MessageDispatcher.SendMessage (this, "OnResetAllDataAngle", 0, 0);
				MessageDispatcher.SendMessage (this, "OnResetAllData", 0, 0);
				MessageDispatcher.SendMessage (this, "OnRestMenuTriggers", 0, 0);

				ES2.Save(0, "GoldUnlock_"+nameWorld);

				ES2.Save(0, "GoldUnlock_DoubleSpeed_" + nameWorld);
				ES2.Save(0, "GoldUnlock_Multiplier_" + nameWorld);

				numAngelInvestorEffectiveness = 2;
				ES2.Save(numAngelInvestorEffectiveness, "BonusPerAngel"+nameWorld);
				TotalAngelbonusPercentPort.text = numAngelInvestorEffectiveness.ToString ()+"%";

				Debug.Log ("reset");
			} else {
				MessageDispatcher.SendMessage(this, "OnTapGA_Report","OnClaimWithNoZombieIdols", 0);
				MessageDispatcher.SendMessage (this, "OnDicrementGold", noresetGoldAmount, 0);	
				Debug.Log (" no reset");
			}

		}else if (name == "no") {
			MessageDispatcher.SendMessage(this, "OnTapGA_Report","OnCloseClaimZombieIdols", 0);
			MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
			OnCloseClaimPopup ();
			OnCloseClaimWithChipsPopup ();
			popNoInvestorPort.SetActive (false);
		}
	}

	public void OnClaimPopupNoReset_Chips(){
		
		MessageDispatcher.SendMessage (this, "OnClaimWithNoReset", (double)20, 0);	
	}

	void OnClaimWithNoReset(IMessage rMessage){
		if (MoneyManagerObj.TotalGold >= (double)20) {
			double numGold = (double)rMessage.Data;
			noresetGoldAmount = numGold;
			noresetClaim = true;
			OnOpenClaimWithChipsPopup ();
			MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
		} else {
			MessageDispatcher.SendMessage (this, "PopButton", 2, 0f);
			for (int i = 0;i < OnShowNotEnoughGold.Length;++i){
				OnShowNotEnoughGold [i].SetActive (true);
			}
			OnCloseClaimWithChipsPopup ();
		}
	}

	void OnCheckIfcanbuy(){
		if (0 < Math.Floor(numAngelInvestor)) {
			OnOpenClaimPopup ();
			MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
		} else {
			MessageDispatcher.SendMessage (this, "PopButton", 2, 0f);
			popNoInvestorPort.SetActive (true);
		}
	}

	public void TempResetAll(){
		MessageDispatcher.SendMessage(this, "OnResetAllGenerator",(double)0, 0);
		MessageDispatcher.SendMessage(this, "OnResetTotalMoney",(double)0, 0);
		ES2.Save(0, "TotalReset");
		TotalAngel = (double)0;
		totalclaimAngle = (double)0;
		ES2.Save(TotalAngel, "TotalAngel_" + MoneyManagerObj.worldName);
		ES2.Save(totalclaimAngle, "TotalAngelClaim_"+nameWorld);
//		ES2.Save((double)0, "TotalSeasonEarning");
		ES2.Save((double)0, "TotalLifeTimeEarning" + nameWorld);
		ES2.Save((double)0, "TotalAngelScrifice" + nameWorld);

		ES2.Save(0, "TotalUpgrades" + nameWorld);
		ES2.Save(0, "TotalAngelUnlock" + nameWorld);

		ES2.Save(0, "GoldUnlock_"+nameWorld);

		ES2.Save(0, "GoldUnlock_DoubleSpeed_" + nameWorld);
		ES2.Save(0, "GoldUnlock_Multiplier_" + nameWorld);

		MessageDispatcher.SendMessage(this, "OnRestUpgradesAngel",0, 0);
		MessageDispatcher.SendMessage(this, "OnRestUpgradesCash",0, 0);

		MessageDispatcher.SendMessage(this, "OnIncrementAngel",(double)0, 0);

//		MessageDispatcher.SendMessage(this, "OnCheckInvestors",(double)0, 0);	
	}


}
