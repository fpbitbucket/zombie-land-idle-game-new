﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using DG.Tweening;
public class ArrowLocation : MonoBehaviour {
	public GameObject arrowObj;
	// Use this for initialization
	void OnEnable () {
		gameObject.GetComponent<Image>().enabled = false;
		StartCoroutine(OnSetPosition());
	}
	
	IEnumerator OnSetPosition(){
		yield return new WaitForSeconds(1);
		gameObject.GetComponent<Image>().enabled = true;
			gameObject.transform.DOLocalMoveX(gameObject.transform.localPosition.x + 10f,0.5f).SetLoops(999,LoopType.Yoyo).SetEase(Ease.Linear);
		gameObject.transform.DOLocalMoveY(arrowObj.transform.localPosition.y,0);//.SetDelay(1).OnComplete(()=>{});
	}
	
}
