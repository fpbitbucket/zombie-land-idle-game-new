﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CalculatorScript : MonoBehaviour {
	public MoneyManager moneymonager;
	public TextMeshProUGUI textAmount,textLevel,textTimer,textEarn,multiplierText;
	public GameObject cardLock;
	public double basicCost; 
	public int current_level;
	public double upgrade_Amount;
	public double rate_percentage;
	public int multiplierNum = 1;

	bool isUnlock = false;
	void OnEnable () {
		if (ES2.Exists ("text_Level")) {
			current_level = ES2.Load<int> ("text_Level");
		} else {
			ES2.Save(current_level, "text_Level");
		}
		textLevel.text = (current_level).ToString ();

		if (current_level == 0) {
			cardLock.SetActive (true);
			isUnlock = false;
		} else {
			cardLock.SetActive (false);
			isUnlock = true;
		}

		StartCoroutine( OnCheckUpgrade ());
	}


	void updateEarning(){
		double TotalGenratorEarnCycle = (basicCost * current_level);
		textEarn.text = moneymonager.GetStringForValue (TotalGenratorEarnCycle).ToString ();
	}



	double compound_amount(double principle,double rate_percent,int level_){
		double return_amount;
		return_amount = principle * Mathf.Pow ((1f + (float)rate_percent), (float)level_);
		return return_amount;
	}


	double cost_total(int currenLevel,double principle,double rate_percent,int level_togo){
		double total_cost = 0;
		int temp_level = currenLevel;
		for (int i = 0; i < (level_togo - currenLevel); i++) {
			total_cost += compound_amount (principle, rate_percent, temp_level);
			temp_level += 1;
		}

		return total_cost;
	}

	int highest_level (int current_level,double principle,double rate_percent,double current_cash){
		double total_cost = 0;
		int temp_level = current_level;

		while (total_cost < current_cash) {
			total_cost += compound_amount(principle,rate_percent,temp_level);
			temp_level++;
		}
		return temp_level-1-current_level;
	}

	IEnumerator OnCheckUpgrade(){
		while (true && isUnlock) {
			yield return new WaitForSeconds (0.3f);
			double amountUpgrade = 0;
			if (multiplierNum == 0) {
				int maxLevel = highest_level (current_level, upgrade_Amount, rate_percentage, moneymonager.totalMoney);
				amountUpgrade = cost_total (current_level, upgrade_Amount, rate_percentage, maxLevel);
				textAmount.text = moneymonager.GetStringForValue (amountUpgrade).ToString ();
			} else if (multiplierNum == 1) {
				amountUpgrade = cost_total (current_level, upgrade_Amount, rate_percentage, current_level + multiplierNum);
				textAmount.text = moneymonager.GetStringForValue (amountUpgrade).ToString ();
			} else {
				amountUpgrade = cost_total (current_level, upgrade_Amount, rate_percentage, current_level * multiplierNum);
				textAmount.text = moneymonager.GetStringForValue (amountUpgrade).ToString ();
			}

			if (current_level == 0) {
				cardLock.SetActive (true);
			} else {
				cardLock.SetActive (false);
			}

			yield return true;
		}
	}


	public void OnChangeMultiplier(){
		if (multiplierNum == 1) {
			multiplierNum = 10;
			multiplierText.text = "x10";
		}else if (multiplierNum == 10) {
			multiplierNum = 100;
			multiplierText.text = "x100";
		}else if (multiplierNum == 100) {
			multiplierNum = 0;
			multiplierText.text = "MAX";
		}else if (multiplierNum == 0) {
			multiplierNum = 1;
			multiplierText.text = "x1";
		}

	}



}
