﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using com.ootii.Messages;
using TMPro;
public class AdvisorScript : MonoBehaviour {
	public MainGeneratorScript mainGen;
	public MoneyManager MoneyManager;
	public GoldUpGradeScript GoldUpgrade;
	public UnlockSript[] genUnlocks_Levels;
	public GameObject[] tipPop;
	public GameObject[] landScapeAdvisor;
	public GameObject[] NoAdVisorAvailble;
	public TextMeshProUGUI[] textMessage;

	public GameObject[] multiplierAdvise;
	public GameObject[] adviseOkaybtn_Port;

	private string[] nameGen;
	private string[] nameGenString;

	private string genStringToUpgrade,typeUpgrade;
	private double numberOfUpgrade;
	int numAdvice = 0;
	public bool ActiveMultiplerAdvise = false;
	void OnEnable () {
		nameGen = mainGen.genNames_Code;
		nameGenString = mainGen.genNamesString;
		landScapeAdvisor[0].SetActive (false);
//		landScapeAdvisor[1].SetActive (false);
		for (int i = 0; i < NoAdVisorAvailble.Length; i++) {
			NoAdVisorAvailble[i].SetActive (false);
		}

		if (ActiveMultiplerAdvise) {
			multiplierAdvise[0].SetActive (false);
//			multiplierAdvise[1].SetActive (false);
			for (int i = 0; i < adviseOkaybtn_Port.Length; i++) {
				adviseOkaybtn_Port [i].SetActive (false);
				string tempString = "";
				if (i == 0) {
					tempString = "Upgrade";
				} else if (i == 1) {
					tempString = "Unlock";
				} else if (i == 2) {
					tempString = "HireManager";
				} else if (i == 3) {
					tempString = "UpgradeOffline";
				} else if (i == 4) {
					tempString = "UpgradeChips";
				}	
				adviseOkaybtn_Port [i].SetActive (false);
				adviseOkaybtn_Port [i].transform.Find ("ItemBtn").GetComponent<Button> ().onClick.AddListener (() => {
					OnUpgradeMultipler (tempString);
				});
			}
		}

//		OnCloseAdviser ();
		landScapeAdvisor[0].SetActive (false);
//		landScapeAdvisor[1].SetActive (false);

		if (ActiveMultiplerAdvise) {
			multiplierAdvise[0].SetActive (false);
//			multiplierAdvise[1].SetActive (false);
		} 
		MessageDispatcher.AddListener ("OnShowAdvisor", OnShowAdvisor, true);
	}

	void OnDisable () {
		MessageDispatcher.RemoveListener ("OnShowAdvisor", OnShowAdvisor, true);
	}

	void OnShowAdvisor (IMessage rMessage){
		MessageDispatcher.SendMessage(this, "OnTapGA_Report","AdviserPopupOpen", 0);
		if (ActiveMultiplerAdvise && numAdvice > 1) {
			multiplierAdvise[0].SetActive (true);
//			multiplierAdvise[1].SetActive (true);
		} else {
			landScapeAdvisor [0].SetActive (true);
//			landScapeAdvisor [1].SetActive (true);
		}
	}

	// cost for advice to upgrade generator
	double cost_To_Advise(){
		double cost_temp = 0;
		double lowestCost = 0;
		int indxLowest = 0;
		int numGenCanAdvice = 0;
		int numGenActive = 0;
		bool canAdvise = false;
		for (int i = 0; i < nameGen.Length; i++) {
			int unlockNum = 0;

			if (ES2.Exists ("btnName_" + nameGen [i])) {
				numGenActive++;
				int levelItem = ES2.Load<int> ("btnName_" + nameGen [i] + "?tag=Level");
				double principle = ES2.Load<double> ("btnName_" + nameGen [i] + "?tag=AmountUpgrade");
				double rate_percent = ES2.Load<double> ("btnName_" + nameGen [i] + "?tag=percentIncrease");
				unlockNum = ES2.Load<int> ("btnName_" + nameGen [i] + "?tag=Unlocked");
				int levelTogo = 0;
				bool checkLevelBool = false;
				if (genUnlocks_Levels [i].OnDoneUnlock == true) {
					//					Debug.Log (unlockNum + " Done");
					levelTogo = 0;
					checkLevelBool = false;
				} else {
					checkLevelBool = true;
					//					Debug.Log (unlockNum + "not Done");
					levelTogo = genUnlocks_Levels [i].numLevels [unlockNum];
				}

				cost_temp = cost_total (levelItem, principle, rate_percent, levelTogo);
				int tempLevel = levelTogo - levelItem;
				if ((MoneyManager.totalMoney - cost_temp) >= 0 && levelItem > 0 && checkLevelBool == true) {
					canAdvise = true;
					if (numGenCanAdvice == 0) {
						lowestCost = cost_temp;
						indxLowest = i;
						numberOfUpgrade = levelTogo - levelItem;
					} else {
						if (numberOfUpgrade > (levelTogo - levelItem)) {
							lowestCost = cost_temp;
							indxLowest = i;
							numberOfUpgrade = levelTogo - levelItem;
						}
					}
					numGenCanAdvice++;
				}
			}
			if (i == (nameGen.Length - 1) && canAdvise) {
				cost_temp = lowestCost;
				genStringToUpgrade = nameGen [indxLowest];
				unlockNum = ES2.Load<int> ("btnName_" + nameGen [indxLowest] + "?tag=Unlocked");
				string messageUnlock = genUnlocks_Levels [indxLowest].unlockMessage [indxLowest];
				textMessage [0].text = "Upgrade your " + nameGenString [indxLowest] + " to Level " + genUnlocks_Levels [indxLowest].numLevels [unlockNum].ToString () + " and you get " + messageUnlock;
				typeUpgrade = "Upgrade";

				if (ActiveMultiplerAdvise) {
					
					adviseOkaybtn_Port [0].SetActive (true);
					adviseOkaybtn_Port [0].transform.Find ("ItemBgGrp/Text_Title").GetComponent<TextMeshProUGUI> ().text = "Upgrade " + nameGenString [indxLowest] + " to Level " + genUnlocks_Levels [indxLowest].numLevels [unlockNum].ToString (); //Name of Upgrade
					adviseOkaybtn_Port [0].transform.Find ("ItemBgGrp/Text_Discript").GetComponent<TextMeshProUGUI> ().text = MoneyManager.GetStringForValue (cost_temp) +" Pills"; //Amount of Upgrade
					adviseOkaybtn_Port [0].transform.Find ("ItemBgGrp/Text_Amount").GetComponent<TextMeshProUGUI> ().text = messageUnlock;
					adviseOkaybtn_Port [0].transform.Find ("ItemBtn/Text").GetComponent<TextMeshProUGUI> ().text = "UPGRADE";
				}

				landScapeAdvisor[0].transform.Find("Advisor/okaybtn").GetComponentInChildren<TextMeshProUGUI>().text = "UPGRADE";
//				landScapeAdvisor[1].transform.Find("Advisor/okaybtn").GetComponentInChildren<TextMeshProUGUI>().text = "UPGRADE";
				//					break;
			} else {
				if (i == (nameGen.Length - 1) && canAdvise == false) {
					if (ActiveMultiplerAdvise) {
						adviseOkaybtn_Port [0].SetActive (false);
					}
//					Debug.Log ("NO To Advice Check if Active Gen");
//					Debug.Log ("NO. " + numGenActive + " Total Gen." + (nameGen.Length-1));
					cost_temp = 0;
				}
			}
		}
		return cost_temp;
	}

	// cost for advice to Open a generator if still no available
	double cost_To_UnlockGenerator(){ 
		double cost_temp = 0;
		double lowestCost = 0;
		bool canAdvise = false;
		int indxLowest = 0;
		for (int i = 0; i < nameGen.Length; i++) {
			if (ES2.Exists ("btnName_" + nameGen [i])) {
				int levelItem = ES2.Load<int> ("btnName_" + nameGen [i] + "?tag=Level");
				double principle = ES2.Load<double> ("btnName_" + nameGen [i] + "?tag=AmountUpgrade");
				double rate_percent = ES2.Load<double> ("btnName_" + nameGen [i] + "?tag=percentIncrease");
				if (levelItem == 0) {
					cost_temp = cost_total (levelItem, principle, rate_percent, 1);
					if ((MoneyManager.totalMoney - cost_temp) >= 0) {
						canAdvise = true;
						if (lowestCost == 0) {
							lowestCost = cost_temp;
							indxLowest = i;
						} else {
							if (lowestCost > cost_temp) {
								lowestCost = cost_temp;
								indxLowest = i;
							}
						}
						numberOfUpgrade = 1;
					}
				}
			}
			if (i == (nameGen.Length - 1) && canAdvise) {
				cost_temp = lowestCost;
				genStringToUpgrade = nameGen [indxLowest];
				string messageUnlock = "Unlock "+nameGenString [indxLowest]+" and earn more";
				textMessage [0].text = messageUnlock;
//				textMessage [1].text = messageUnlock;
				typeUpgrade = "Unlock";
				landScapeAdvisor[0].transform.Find("Advisor/okaybtn").GetComponentInChildren<TextMeshProUGUI>().text = "UNLOCK";
//				landScapeAdvisor[1].transform.Find("Advisor/okaybtn").GetComponentInChildren<TextMeshProUGUI>().text = "UPGRADE";


				if (ActiveMultiplerAdvise) {
					
					adviseOkaybtn_Port [1].SetActive (true);
					adviseOkaybtn_Port [1].transform.Find ("ItemBgGrp/Text_Title").GetComponent<TextMeshProUGUI> ().text = messageUnlock; //Name of Upgrade
					adviseOkaybtn_Port [1].transform.Find ("ItemBgGrp/Text_Discript").GetComponent<TextMeshProUGUI> ().text = MoneyManager.GetStringForValue (cost_temp) + " Pills"; //Amount of Upgrade
					adviseOkaybtn_Port [1].transform.Find ("ItemBgGrp/Text_Amount").GetComponent<TextMeshProUGUI> ().text = "";
					adviseOkaybtn_Port [1].transform.Find ("ItemBtn/Text").GetComponent<TextMeshProUGUI> ().text = "UNLOCK";
				}

			} else {
				if (i == (nameGen.Length - 1) && canAdvise == false) {
//					Debug.Log ("Cant afford any generator to unlock");
					if (ActiveMultiplerAdvise) {
						adviseOkaybtn_Port [1].SetActive (false);
					}
					cost_temp = 0;
				}
			}
		}
		return cost_temp;
	}


	double cost_To_HireManagerGenerator(){
		double cost_temp = 0;
		double lowestCost = 0;
		int indxLowest = 0;
		int numGenCanAdvice = 0;
		int numGenActive = 0;
		bool canAdvise = false;
		ManagerBtnScript managerScript = GameObject.Find ("Managers/HireManager").GetComponent<ManagerBtnScript> ();
		for (int i = 0; i < nameGen.Length; i++) {
			int unlockNum = 0;
			if (ES2.Exists ("btnName_" + nameGen [i])) {
				int levelItem = ES2.Load<int> ("btnName_" + nameGen [i] + "?tag=Level");

				if (levelItem > 0) {
					bool hasManagerGen = ES2.Load<bool> ("btnName_" + nameGen [i] + "?tag=HasManager");
					if (hasManagerGen == false) {
						cost_temp = managerScript.AmountManager [i];
						if ((MoneyManager.totalMoney - cost_temp) >= 0) {
							canAdvise = true;
							if (lowestCost == 0) {
								lowestCost = cost_temp;
								indxLowest = i;
								numberOfUpgrade = i;
							} else {
								if (lowestCost > cost_temp) {
									lowestCost = cost_temp;
									indxLowest = i;
								}
							}
						}
					}
				}
			}
			if (i == (nameGen.Length - 1) && canAdvise) {
				cost_temp = lowestCost;
				genStringToUpgrade = nameGen [indxLowest];
				string messageUnlock = "Hire " + managerScript.nameManagerString[indxLowest] + " to manage " + nameGenString[indxLowest];
				numberOfUpgrade = indxLowest;
				textMessage [0].text = messageUnlock;
//				textMessage [1].text = messageUnlock;
				typeUpgrade = "HireManager";

				if (ActiveMultiplerAdvise) {
					
					adviseOkaybtn_Port [2].SetActive (true);
					adviseOkaybtn_Port [2].transform.Find ("ItemBgGrp/Text_Title").GetComponent<TextMeshProUGUI> ().text = nameGenString [indxLowest]; //Name of Upgrade
					adviseOkaybtn_Port [2].transform.Find ("ItemBgGrp/Text_Discript").GetComponent<TextMeshProUGUI> ().text = MoneyManager.GetStringForValue (cost_temp) +" Pills"; //Amount of Upgrade
					adviseOkaybtn_Port [2].transform.Find ("ItemBgGrp/Text_Amount").GetComponent<TextMeshProUGUI> ().text = "Hire " + managerScript.nameManagerString[indxLowest];
					adviseOkaybtn_Port [2].transform.Find ("ItemBtn/Text").GetComponent<TextMeshProUGUI> ().text = "HIRE";
				}

				landScapeAdvisor[0].transform.Find("Advisor/okaybtn").GetComponentInChildren<TextMeshProUGUI>().text = "HIRE";
//				landScapeAdvisor[1].transform.Find("Advisor/okaybtn").GetComponentInChildren<TextMeshProUGUI>().text = "HIRE";
				//					break;
			} else {
				if (i == (nameGen.Length - 1) && canAdvise == false) {
					if (ActiveMultiplerAdvise) {
						adviseOkaybtn_Port [2].SetActive (false);
					}
					cost_temp = 0;
				}
			}
		}
		return cost_temp;
	}

	double cost_To_UpgradeOffline(){ 
		double cost_temp;
		cost_temp = 10;
		if ((MoneyManager.TotalGold - cost_temp) >= 0) {
			string messageUnlock = "Would you like to increase your offline multiplier to x5 for " + cost_temp + " Brains?";
			textMessage [0].text = messageUnlock;
//			textMessage [1].text = messageUnlock;
			typeUpgrade = "UpgradeOffline";
			landScapeAdvisor[0].transform.Find("Advisor/okaybtn").GetComponentInChildren<TextMeshProUGUI>().text = "10 Brains";
//			landScapeAdvisor[1].transform.Find("Advisor/okaybtn").GetComponentInChildren<TextMeshProUGUI>().text = "10 Chips";


			if (ActiveMultiplerAdvise) {
				
				adviseOkaybtn_Port [3].SetActive (true);
				adviseOkaybtn_Port [3].transform.Find ("ItemBgGrp/Text_Title").GetComponent<TextMeshProUGUI> ().text = "Increase your offline multiplier to x5"; //Name of Upgrade
				adviseOkaybtn_Port [3].transform.Find ("ItemBgGrp/Text_Discript").GetComponent<TextMeshProUGUI> ().text = cost_temp + " Brains"; //Amount of Upgrade
				adviseOkaybtn_Port [3].transform.Find ("ItemBgGrp/Text_Amount").GetComponent<TextMeshProUGUI> ().text = "";
				adviseOkaybtn_Port [3].transform.Find ("ItemBtn/Text").GetComponent<TextMeshProUGUI> ().text = "10 Brains";

			}

		} else {
			if (ActiveMultiplerAdvise) {
				
				adviseOkaybtn_Port [3].SetActive (false);
			}
			cost_temp = 0;
		}

		return cost_temp;
	}

	double cost_To_UpgradeChips(){ 
		double cost_temp = 0;
		double lowestCost = 0;
		int indxLowest = 0;
		bool canAdvise = false;
		for (int i = 0; i < GoldUpgrade.nameString.Length; i++) {
			if (GoldUpgrade.nameString [i] == "multiplier" || GoldUpgrade.nameString [i] == "TimeWarp") {
				cost_temp = GoldUpgrade.upgradeCost [i];
				if ((MoneyManager.TotalGold - cost_temp) >= 0) {
					canAdvise = true;
					if (lowestCost == 0) {
						lowestCost = cost_temp;
						indxLowest = i;
					} else {
						if (lowestCost > cost_temp) {
							lowestCost = cost_temp;
							indxLowest = i;
						}
					}
				}
			}
			if (i == (GoldUpgrade.nameString.Length - 1) && canAdvise) {
				cost_temp = lowestCost;
				genStringToUpgrade = nameGen [indxLowest];
				string messageUnlock = "Upgrade " + GoldUpgrade.nameUpgradeString[indxLowest] + ", " + GoldUpgrade.nameUpgradeStringDiscript[indxLowest];
				numberOfUpgrade = indxLowest;
				textMessage [0].text = messageUnlock;
//				textMessage [1].text = messageUnlock;
				typeUpgrade = "UpgradeChips";
				landScapeAdvisor[0].transform.Find("Advisor/okaybtn").GetComponentInChildren<TextMeshProUGUI>().text = lowestCost+" Brains";
//				landScapeAdvisor[1].transform.Find("Advisor/okaybtn").GetComponentInChildren<TextMeshProUGUI>().text = lowestCost+" Chips";


				if (ActiveMultiplerAdvise) {
					
					adviseOkaybtn_Port [4].SetActive (true);
					adviseOkaybtn_Port [4].transform.Find ("ItemBgGrp/Text_Title").GetComponent<TextMeshProUGUI> ().text = GoldUpgrade.nameUpgradeString[indxLowest]; //Name of Upgrade
					adviseOkaybtn_Port [4].transform.Find ("ItemBgGrp/Text_Discript").GetComponent<TextMeshProUGUI> ().text = lowestCost + " Brains"; //Amount of Upgrade
					adviseOkaybtn_Port [4].transform.Find ("ItemBgGrp/Text_Amount").GetComponent<TextMeshProUGUI> ().text = GoldUpgrade.nameUpgradeStringDiscript[indxLowest];
					adviseOkaybtn_Port [4].transform.Find ("ItemBtn/Text").GetComponent<TextMeshProUGUI> ().text = lowestCost+" Brains";


				}

			} else {
				if (i == (GoldUpgrade.nameString.Length - 1) && canAdvise == false) {
					cost_temp = 0;
					if (ActiveMultiplerAdvise) {
						adviseOkaybtn_Port [4].SetActive (false);
					}
				}
			}
		}
		return cost_temp;
	} 

	public void OnUpgradeMultipler(string stringName){
		MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);

		typeUpgrade = stringName;
		OnUpgrade ();
	}

	public void OnUpgrade(){
		if (typeUpgrade == "Unlock") {
			MessageDispatcher.SendMessage (this, "PopButton", 3, 0f);
			MessageDispatcher.SendMessage (this, "OnAdvisorUnlock" + genStringToUpgrade, numberOfUpgrade, 0);
		} else if (typeUpgrade == "Upgrade") {
			MessageDispatcher.SendMessage (this, "PopButton", 3, 0f);
			MessageDispatcher.SendMessage (this, "OnAdvisorUpgrade" + genStringToUpgrade, numberOfUpgrade, 0);
		} else if (typeUpgrade == "HireManager") {
			MessageDispatcher.SendMessage (this, "PopButton", 3, 0f);
			MessageDispatcher.SendMessage (this, "OnHiremanagerFromAdvisor", (int)numberOfUpgrade, 0);
		} else if (typeUpgrade == "UpgradeOffline") {
			MessageDispatcher.SendMessage (this, "PopButton", 3, 0f);
			MessageDispatcher.SendMessage (this, "OnBoostWithGold", (double)5, 0);	
			MessageDispatcher.SendMessage (this, "OnDicrementGold", (double)10, 0);
		} else if (typeUpgrade == "UpgradeChips") {
			MessageDispatcher.SendMessage (this, "OnUpgradeAdviser", (int)numberOfUpgrade, 0);	
		}
		MessageDispatcher.SendMessage(this, "OnTapGA_Report","AdviserAcceptTipBtn_"+typeUpgrade, 0);
//		Debug.Log (genStringToUpgrade + " " + typeUpgrade);
		OnCloseAdviser ();
	}

	public void OnCloseNoAdviserAvailable(){
		MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
		for (int i = 0; i < NoAdVisorAvailble.Length; i++) {
			NoAdVisorAvailble[i].SetActive (false);
		}
	}

	public void OnCloseAdviser(){
		MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
		landScapeAdvisor[0].SetActive (false);
//		landScapeAdvisor[1].SetActive (false);
		MessageDispatcher.SendMessage(this, "OnTapGA_Report","AdviserPopupClose", 0);
		if (ActiveMultiplerAdvise) {
			multiplierAdvise[0].SetActive (false);
//			multiplierAdvise[1].SetActive (false);
		} 
	}

	public void OnCloseTipPopup(){
//		MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
		for (int i = 0; i < tipPop.Length; i++) {
			tipPop [i].SetActive (false);
		}
	}
	public bool canAdvise = false;
	public void OnShowTip(){
		MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
		if (gameObject.GetComponent<AdviserChecker> ()) {
			gameObject.GetComponent<AdviserChecker> ().hasAdvise = false;
		}
		canAdvise = false;
		numAdvice = 0;
		if (ActiveMultiplerAdvise) {
			if (cost_To_Advise () != 0) {
				canAdvise = true;
				numAdvice++;
			}
			if ( cost_To_UnlockGenerator () != 0) {
				canAdvise = true;
				numAdvice++;
			}
			if (cost_To_HireManagerGenerator () != 0) {
				canAdvise = true;
				numAdvice++;
			}
			if (cost_To_UpgradeOffline () != 0) {
				canAdvise = true;
				numAdvice++;
			}
			if (cost_To_UpgradeChips () != 0) {
				canAdvise = true;
				numAdvice++;
			}


		} else {
			double[] Tempcost = new double[3];

			Tempcost [0] = cost_To_Advise ();
			Tempcost [1] = cost_To_UnlockGenerator ();
			Tempcost [2] = cost_To_HireManagerGenerator ();


			double lowestCost = 0;
			int indxLowest = 0;
//			Debug.Log ("1." + Tempcost [0] + "\n" + "2." + Tempcost [1] + "\n" + "3." + Tempcost [2]);

			for (int i = 0; i < Tempcost.Length; i++) {
				if (i == 0) {
					if (Tempcost [i] != 0) {
						lowestCost = Tempcost [i];
						indxLowest = i;
						canAdvise = true;
					}
				} else {
					if (Tempcost [i] > Tempcost [i - 1]) {
						lowestCost = Tempcost [i];
						indxLowest = i;
						canAdvise = true;
					}
				}
			}

//			Debug.Log ("3 Advise " + canAdvise);

			if (canAdvise == false) {
				double cost_1 = cost_To_UpgradeOffline ();
				double cost_2 = cost_To_UpgradeChips ();
				if (cost_1 != 0 && cost_2 != 0) {
					canAdvise = true;
					if (cost_1 < cost_2) {
						cost_To_UpgradeOffline ();
					} else {
						cost_To_UpgradeChips ();
					}
				} else {
					if (cost_To_UpgradeChips () != 0) {
						canAdvise = true;
//						Debug.Log ("cost_To_UpgradeChips " + cost_To_UpgradeChips ());
					} else if (cost_To_UpgradeOffline () != 0) {
						canAdvise = true;
//						Debug.Log ("cost_To_UpgradeOffline " + cost_To_UpgradeOffline ());
					}
				}
			} else {
				if (indxLowest == 0) {
					cost_To_Advise ();
				} else if (indxLowest == 1) {
					cost_To_UnlockGenerator ();
				} else if (indxLowest == 2) {
					cost_To_HireManagerGenerator ();
				}
			}

//			Debug.Log ("can Advise " + canAdvise);
		}
		if (canAdvise) {
			for (int i = 0; i < tipPop.Length; i++) {
				tipPop [i].SetActive (true);
			}
		} else {
			for (int j = 0; j < NoAdVisorAvailble.Length; j++) {
				NoAdVisorAvailble[j].SetActive (true);
			}
		}
	}

	public bool OnCheckCanAdvise(){
		bool hasAdvise = false;
		if (canAdvise == false) {
			if (cost_To_Advise () != 0) {
				hasAdvise = true;
			}
			if (cost_To_UnlockGenerator () != 0) {
				hasAdvise = true;
			}
			if (cost_To_HireManagerGenerator () != 0) {
				hasAdvise = true;
			}
			if (cost_To_UpgradeOffline () != 0) {
				hasAdvise = true;
			}
			if (cost_To_UpgradeChips () != 0) {
				hasAdvise = true;
			}
		}
		return hasAdvise;
	}

	double compound_amount(double principle,double rate_percent,int level_){
		double return_amount;
		return_amount = principle * Mathf.Pow ((1f + (float)rate_percent), (float)level_);
		return return_amount;
	}


	double cost_total(int currenLevel,double principle,double rate_percent,int level_togo){
		double total_cost = 0;
		int temp_level = currenLevel;
		for (int i = 0; i < (level_togo - currenLevel); i++) {
			total_cost += compound_amount (principle, rate_percent, temp_level);
			temp_level += 1;
		}

		return total_cost;
	}

	int highest_level (int current_level,double principle,double rate_percent,double current_cash){
		double total_cost = 0;
		int temp_level = current_level;

		while (total_cost < current_cash) {
			total_cost += compound_amount(principle,rate_percent,temp_level);
			temp_level++;
		}


		return temp_level-1-current_level;
	}

}
