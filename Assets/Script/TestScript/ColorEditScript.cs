﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorEditScript : MonoBehaviour {
	public GameObject[] GameObj;
	public Color[] GameObjColor;

	void Start () {
		for (int i = 0; i < GameObj.Length; i++) {
			if (GameObj [i] != null && GameObjColor [i] != null) {
				GameObj [i].GetComponent<Image> ().color = GameObjColor [i];
			}
		}
	}
}
