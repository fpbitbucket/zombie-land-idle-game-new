﻿using UnityEngine;
using System;
using System.Collections;

public class MoneyUnitHandler : MonoBehaviour {

	public double[] millionsNumbers = new double[] {Math.Pow(10,6)-1,Math.Pow(10,9)-1,Math.Pow(10,12)-1,Math.Pow(10,15)-1,Math.Pow(10,18)-1,Math.Pow(10,21)-1,Math.Pow(10,24)-1,Math.Pow(10,27)-1,Math.Pow(10,30)-1,Math.Pow(10,33)-1,Math.Pow(10,36)-1,Math.Pow(10,39)-1,Math.Pow(10,42)-1,Math.Pow(10,45)-1,Math.Pow(10,48)-1,Math.Pow(10,51)-1,Math.Pow(10,54)-1,Math.Pow(10,57)-1,Math.Pow(10,60)-1,Math.Pow(10,63)-1,Math.Pow(10,66)-1,Math.Pow(10,69)-1,Math.Pow(10,72)-1,Math.Pow(10,75)-1,Math.Pow(10,78)-1,Math.Pow(10,81)-1,Math.Pow(10,84)-1,Math.Pow(10,87)-1,Math.Pow(10,90)-1,Math.Pow(10,93)-1,Math.Pow(10,96)-1,Math.Pow(10,99)-1,Math.Pow(10,102)-1,Math.Pow(10,105)-1,Math.Pow(10,108)-1,Math.Pow(10,111)-1,Math.Pow(10,114)-1,Math.Pow(10,117)-1,Math.Pow(10,120)-1,Math.Pow(10,123)-1,Math.Pow(10,126)-1,Math.Pow(10,129)-1,Math.Pow(10,132)-1,Math.Pow(10,135)-1,Math.Pow(10,138)-1,Math.Pow(10,141)-1,Math.Pow(10,144)-1,Math.Pow(10,147)-1,Math.Pow(10,150)-1,Math.Pow(10,153)-1,Math.Pow(10,156)-1,Math.Pow(10,159)-1,Math.Pow(10,162)-1,Math.Pow(10,165)-1,Math.Pow(10,168)-1,Math.Pow(10,171)-1,Math.Pow(10,174)-1,Math.Pow(10,177)-1,Math.Pow(10,180)-1,Math.Pow(10,183)-1,Math.Pow(10,186)-1,Math.Pow(10,189)-1,Math.Pow(10,192)-1,Math.Pow(10,195)-1,Math.Pow(10,198)-1,Math.Pow(10,201)-1,Math.Pow(10,204)-1,Math.Pow(10,207)-1,Math.Pow(10,210)-1,Math.Pow(10,213)-1,Math.Pow(10,216)-1,Math.Pow(10,219)-1,Math.Pow(10,222)-1,Math.Pow(10,225)-1,Math.Pow(10,228)-1,Math.Pow(10,231)-1,Math.Pow(10,234)-1,Math.Pow(10,237)-1,Math.Pow(10,240)-1,Math.Pow(10,243)-1,Math.Pow(10,246)-1,Math.Pow(10,249)-1,Math.Pow(10,252)-1,Math.Pow(10,255)-1,Math.Pow(10,258)-1,Math.Pow(10,261)-1,Math.Pow(10,264)-1,Math.Pow(10,267)-1,Math.Pow(10,270)-1,Math.Pow(10,273)-1,Math.Pow(10,276)-1,Math.Pow(10,279)-1,Math.Pow(10,282)-1,Math.Pow(10,285)-1,Math.Pow(10,288)-1,Math.Pow(10,291)-1,Math.Pow(10,294)-1,Math.Pow(10,297)-1,Math.Pow(10,300)-1,Math.Pow(10,303)-1};
	public double[] numberCounter = new double[] {Math.Pow(10,6),Math.Pow(10,9),Math.Pow(10,12),Math.Pow(10,15),Math.Pow(10,18),Math.Pow(10,21),Math.Pow(10,24),Math.Pow(10,27),Math.Pow(10,30),Math.Pow(10,33),Math.Pow(10,36),Math.Pow(10,39),Math.Pow(10,42),Math.Pow(10,45),Math.Pow(10,48),Math.Pow(10,51),Math.Pow(10,54),Math.Pow(10,57),Math.Pow(10,60),Math.Pow(10,63),Math.Pow(10,66),Math.Pow(10,69),Math.Pow(10,72),Math.Pow(10,75),Math.Pow(10,78),Math.Pow(10,81),Math.Pow(10,84),Math.Pow(10,87),Math.Pow(10,90),Math.Pow(10,93),Math.Pow(10,96),Math.Pow(10,99),Math.Pow(10,102),Math.Pow(10,105),Math.Pow(10,108),Math.Pow(10,111),Math.Pow(10,114),Math.Pow(10,117),Math.Pow(10,120),Math.Pow(10,123),Math.Pow(10,126),Math.Pow(10,129),Math.Pow(10,132),Math.Pow(10,135),Math.Pow(10,138),Math.Pow(10,141),Math.Pow(10,144),Math.Pow(10,147),Math.Pow(10,150),Math.Pow(10,153),Math.Pow(10,156),Math.Pow(10,159),Math.Pow(10,162),Math.Pow(10,165),Math.Pow(10,168),Math.Pow(10,171),Math.Pow(10,174),Math.Pow(10,177),Math.Pow(10,180),Math.Pow(10,183),Math.Pow(10,186),Math.Pow(10,189),Math.Pow(10,192),Math.Pow(10,195),Math.Pow(10,198),Math.Pow(10,201),Math.Pow(10,204),Math.Pow(10,207),Math.Pow(10,210),Math.Pow(10,213),Math.Pow(10,216),Math.Pow(10,219),Math.Pow(10,222),Math.Pow(10,225),Math.Pow(10,228),Math.Pow(10,231),Math.Pow(10,234),Math.Pow(10,237),Math.Pow(10,240),Math.Pow(10,243),Math.Pow(10,246),Math.Pow(10,249),Math.Pow(10,252),Math.Pow(10,255),Math.Pow(10,258),Math.Pow(10,261),Math.Pow(10,264),Math.Pow(10,267),Math.Pow(10,270),Math.Pow(10,273),Math.Pow(10,276),Math.Pow(10,279),Math.Pow(10,282),Math.Pow(10,285),Math.Pow(10,288),Math.Pow(10,291),Math.Pow(10,294),Math.Pow(10,297),Math.Pow(10,300),Math.Pow(10,303)};
	public string[] moneyUnit = new string[] {"million","billion","trillion","quadrillion","quintillion","sextillion","septillion","octillion","nonillion","decillion","undecillion","duodecillion","tredecillion","quattuordecillion","quindecillion","sexdecillion","septendecillion","octodecillion","novemdecillion","vigintillion","unvigintillion","duovigintillion","trevigintillion","quattuorvigintillion","quinvigintillion","sexvigintillion","septenvigintillion","octovigintillion","novemvigintillion","trigintillion","untrigintillion","duotrigintillion","tretrigintillion","quattuortrigintillion","quintrigintillion","sextrigintillion","septentrigintillion","octotrigintillion","novemtrigintillion","quadragintillion","unquadragintillion","duoquadragintillion","trequadragintillion","quattuorquadragintillion","quinquadragintillion","sexquadragintillion","septenquadragintillion","octoquadragintillion","novemquadragintillion","quinquagintillion","unquinquagintillion","duoquinquagintillion","trequinquagintillion","quattuorquinquagintillion","quinquinquagintillion","sexquinquagintillion","septenquinquagintillion","octoquinquagintillion","novemquinquagintillion","sexagintillion","unsexagintillion","duosexagintillion","tresexagintillion","quattuorsexagintillion","quinsexagintillion","sexsexagintillion","septensexagintillion","octosexagintillion","novemsexagintillion","septuagintillion","unseptuagintillion","duoseptuagintillion","treseptuagintillion","quattuorseptuagintillion","quinseptuagintillion","sexseptuagintillion","septenseptuagintillion","octoseptuagintillion","novemseptuagintillion","octogintillion","unoctogintillion","duooctogintillion","treoctogintillion","quattuoroctogintillion","quinoctogintillion","sexoctogintillion","septenoctogintillion","octooctogintillion","novemoctogintillion","nonagintillion","unnonagintillion","duononagintillion","trenonagintillion","quattuornonagintillion","quinnonagintillion","sexnonagintillion","septennonagintillion","octononagintillion","novemnonagintillion","centillion"};

	public string GetStringForValue(double value){
		double newMoneyAmount;
		string moneySuffix = ""; 
		if (value > millionsNumbers[99]) {
			// Decillion
			moneySuffix = " "+moneyUnit[99];
			newMoneyAmount = (double)value / numberCounter[99];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[98]) {
			// Decillion
			moneySuffix = " "+moneyUnit[98];
			newMoneyAmount = (double)value / numberCounter[98];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[97]) {
			// Decillion
			moneySuffix = " "+moneyUnit[97];
			newMoneyAmount = (double)value / numberCounter[97];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[96]) {
			// Decillion
			moneySuffix = " "+moneyUnit[96];
			newMoneyAmount = (double)value / numberCounter[96];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[95]) {
			// Decillion
			moneySuffix = " "+moneyUnit[95];
			newMoneyAmount = (double)value / numberCounter[95];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[94]) {
			// Decillion
			moneySuffix = " "+moneyUnit[94];
			newMoneyAmount = (double)value / numberCounter[94];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[93]) {
			// Decillion
			moneySuffix = " "+moneyUnit[93];
			newMoneyAmount = (double)value / numberCounter[93];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[92]) {
			// Decillion
			moneySuffix = " "+moneyUnit[92];
			newMoneyAmount = (double)value / numberCounter[92];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[91]) {
			// Decillion
			moneySuffix = " "+moneyUnit[91];
			newMoneyAmount = (double)value / numberCounter[91];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[90]) {
			// Decillion
			moneySuffix = " "+moneyUnit[90];
			newMoneyAmount = (double)value / numberCounter[90];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[89]) {
			// Decillion
			moneySuffix = " "+moneyUnit[89];
			newMoneyAmount = (double)value / numberCounter[89];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[88]) {
			// Decillion
			moneySuffix = " "+moneyUnit[88];
			newMoneyAmount = (double)value / numberCounter[88];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[87]) {
			// Decillion
			moneySuffix = " "+moneyUnit[87];
			newMoneyAmount = (double)value / numberCounter[87];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[86]) {
			// Decillion
			moneySuffix = " "+moneyUnit[86];
			newMoneyAmount = (double)value / numberCounter[86];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[85]) {
			// Decillion
			moneySuffix = " "+moneyUnit[85];
			newMoneyAmount = (double)value / numberCounter[85];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[84]) {
			// Decillion
			moneySuffix = " "+moneyUnit[84];
			newMoneyAmount = (double)value / numberCounter[84];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[83]) {
			// Decillion
			moneySuffix = " "+moneyUnit[83];
			newMoneyAmount = (double)value / numberCounter[83];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[82]) {
			// Decillion
			moneySuffix = " "+moneyUnit[82];
			newMoneyAmount = (double)value / numberCounter[82];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[81]) {
			// Decillion
			moneySuffix = " "+moneyUnit[81];
			newMoneyAmount = (double)value / numberCounter[81];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[80]) {
			// Decillion
			moneySuffix = " "+moneyUnit[80];
			newMoneyAmount = (double)value / numberCounter[80];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[79]) {
			// Decillion
			moneySuffix = " "+moneyUnit[79];
			newMoneyAmount = (double)value / numberCounter[79];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[78]) {
			// Decillion
			moneySuffix = " "+moneyUnit[78];
			newMoneyAmount = (double)value / numberCounter[78];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[77]) {
			// Decillion
			moneySuffix = " "+moneyUnit[77];
			newMoneyAmount = (double)value / numberCounter[77];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[76]) {
			// Decillion
			moneySuffix = " "+moneyUnit[76];
			newMoneyAmount = (double)value / numberCounter[76];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[75]) {
			// Decillion
			moneySuffix = " "+moneyUnit[75];
			newMoneyAmount = (double)value / numberCounter[75];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[74]) {
			// Decillion
			moneySuffix = " "+moneyUnit[74];
			newMoneyAmount = (double)value / numberCounter[74];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[73]) {
			// Decillion
			moneySuffix = " "+moneyUnit[73];
			newMoneyAmount = (double)value / numberCounter[73];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[72]) {
			// Decillion
			moneySuffix = " "+moneyUnit[72];
			newMoneyAmount = (double)value / numberCounter[72];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[71]) {
			// Decillion
			moneySuffix = " "+moneyUnit[71];
			newMoneyAmount = (double)value / numberCounter[71];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[70]) {
			// Decillion
			moneySuffix = " "+moneyUnit[70];
			newMoneyAmount = (double)value / numberCounter[70];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[69]) {
			// Decillion
			moneySuffix = " "+moneyUnit[69];
			newMoneyAmount = (double)value / numberCounter[69];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[68]) {
			// Decillion
			moneySuffix = " "+moneyUnit[68];
			newMoneyAmount = (double)value / numberCounter[68];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[67]) {
			// Decillion
			moneySuffix = " "+moneyUnit[67];
			newMoneyAmount = (double)value / numberCounter[67];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[66]) {
			// Decillion
			moneySuffix = " "+moneyUnit[66];
			newMoneyAmount = (double)value / numberCounter[66];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[65]) {
			// Decillion
			moneySuffix = " "+moneyUnit[65];
			newMoneyAmount = (double)value / numberCounter[65];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[64]) {
			// Decillion
			moneySuffix = " "+moneyUnit[64];
			newMoneyAmount = (double)value / numberCounter[64];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[63]) {
			// Decillion
			moneySuffix = " "+moneyUnit[63];
			newMoneyAmount = (double)value / numberCounter[63];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[62]) {
			// Decillion
			moneySuffix = " "+moneyUnit[62];
			newMoneyAmount = (double)value / numberCounter[62];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[61]) {
			// Decillion
			moneySuffix = " "+moneyUnit[61];
			newMoneyAmount = (double)value / numberCounter[61];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[60]) {
			// Decillion
			moneySuffix = " "+moneyUnit[60];
			newMoneyAmount = (double)value / numberCounter[60];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[59]) {
			// Decillion
			moneySuffix = " "+moneyUnit[59];
			newMoneyAmount = (double)value / numberCounter[59];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[58]) {
			// Decillion
			moneySuffix = " "+moneyUnit[58];
			newMoneyAmount = (double)value / numberCounter[58];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[57]) {
			// Decillion
			moneySuffix = " "+moneyUnit[57];
			newMoneyAmount = (double)value / numberCounter[57];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[56]) {
			// Decillion
			moneySuffix = " "+moneyUnit[56];
			newMoneyAmount = (double)value / numberCounter[56];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[55]) {
			// Decillion
			moneySuffix = " "+moneyUnit[55];
			newMoneyAmount = (double)value / numberCounter[55];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[54]) {
			// Decillion
			moneySuffix = " "+moneyUnit[54];
			newMoneyAmount = (double)value / numberCounter[54];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[53]) {
			// Decillion
			moneySuffix = " "+moneyUnit[53];
			newMoneyAmount = (double)value / numberCounter[53];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[52]) {
			// Decillion
			moneySuffix = " "+moneyUnit[52];
			newMoneyAmount = (double)value / numberCounter[52];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[51]) {
			// Decillion
			moneySuffix = " "+moneyUnit[51];
			newMoneyAmount = (double)value / numberCounter[51];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[50]) {
			// Decillion
			moneySuffix = " "+moneyUnit[50];
			newMoneyAmount = (double)value / numberCounter[50];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[49]) {
			// Decillion
			moneySuffix = " "+moneyUnit[49];
			newMoneyAmount = (double)value / numberCounter[49];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[48]) {
			// Decillion
			moneySuffix = " "+moneyUnit[48];
			newMoneyAmount = (double)value / numberCounter[48];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[47]) {
			// Decillion
			moneySuffix = " "+moneyUnit[47];
			newMoneyAmount = (double)value / numberCounter[47];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[46]) {
			// Decillion
			moneySuffix = " "+moneyUnit[46];
			newMoneyAmount = (double)value / numberCounter[46];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[45]) {
			// Decillion
			moneySuffix = " "+moneyUnit[45];
			newMoneyAmount = (double)value / numberCounter[45];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[44]) {
			// Decillion
			moneySuffix = " "+moneyUnit[44];
			newMoneyAmount = (double)value / numberCounter[44];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[43]) {
			// Decillion
			moneySuffix = " "+moneyUnit[43];
			newMoneyAmount = (double)value / numberCounter[43];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[42]) {
			// Decillion
			moneySuffix = " "+moneyUnit[42];
			newMoneyAmount = (double)value / numberCounter[42];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[41]) {
			// Decillion
			moneySuffix = " "+moneyUnit[41];
			newMoneyAmount = (double)value / numberCounter[41];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[40]) {
			// Decillion
			moneySuffix = " "+moneyUnit[40];
			newMoneyAmount = (double)value / numberCounter[40];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[39]) {
			// Decillion
			moneySuffix = " "+moneyUnit[39];
			newMoneyAmount = (double)value / numberCounter[39];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[38]) {
			// Decillion
			moneySuffix = " "+moneyUnit[38];
			newMoneyAmount = (double)value / numberCounter[38];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[37]) {
			// Decillion
			moneySuffix = " "+moneyUnit[37];
			newMoneyAmount = (double)value / numberCounter[37];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[36]) {
			// Decillion
			moneySuffix = " "+moneyUnit[36];
			newMoneyAmount = (double)value / numberCounter[36];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[35]) {
			// Decillion
			moneySuffix = " "+moneyUnit[35];
			newMoneyAmount = (double)value / numberCounter[35];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[34]) {
			// Decillion
			moneySuffix = " "+moneyUnit[34];
			newMoneyAmount = (double)value / numberCounter[34];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[33]) {
			// Decillion
			moneySuffix = " "+moneyUnit[33];
			newMoneyAmount = (double)value / numberCounter[33];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[32]) {
			// Decillion
			moneySuffix = " "+moneyUnit[32];
			newMoneyAmount = (double)value / numberCounter[32];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[31]) {
			// Decillion
			moneySuffix = " "+moneyUnit[31];
			newMoneyAmount = (double)value / numberCounter[31];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[30]) {
			// Decillion
			moneySuffix = " "+moneyUnit[30];
			newMoneyAmount = (double)value / numberCounter[30];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[29]) {
			// Decillion
			moneySuffix = " "+moneyUnit[29];
			newMoneyAmount = (double)value / numberCounter[29];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[28]) {
			// Decillion
			moneySuffix = " "+moneyUnit[28];
			newMoneyAmount = (double)value / numberCounter[28];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[27]) {
			// Decillion
			moneySuffix = " "+moneyUnit[27];
			newMoneyAmount = (double)value / numberCounter[27];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[26]) {
			// Decillion
			moneySuffix = " "+moneyUnit[26];
			newMoneyAmount = (double)value / numberCounter[26];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[25]) {
			// Decillion
			moneySuffix = " "+moneyUnit[25];
			newMoneyAmount = (double)value / numberCounter[25];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[24]) {
			// Decillion
			moneySuffix = " "+moneyUnit[24];
			newMoneyAmount = (double)value / numberCounter[24];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[23]) {
			// Decillion
			moneySuffix = " "+moneyUnit[23];
			newMoneyAmount = (double)value / numberCounter[23];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[22]) {
			// Decillion
			moneySuffix = " "+moneyUnit[22];
			newMoneyAmount = (double)value / numberCounter[22];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[21]) {
			// Decillion
			moneySuffix = " "+moneyUnit[21];
			newMoneyAmount = (double)value / numberCounter[21];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[20]) {
			// Decillion
			moneySuffix = " "+moneyUnit[20];
			newMoneyAmount = (double)value / numberCounter[20];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[19]) {
			// Decillion
			moneySuffix = " "+moneyUnit[19];
			newMoneyAmount = (double)value / numberCounter[19];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[18]) {
			// Decillion
			moneySuffix = " "+moneyUnit[18];
			newMoneyAmount = (double)value / numberCounter[18];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[17]) {
			// Decillion
			moneySuffix = " "+moneyUnit[17];
			newMoneyAmount = (double)value / numberCounter[17];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[16]) {
			// Decillion
			moneySuffix = " "+moneyUnit[16];
			newMoneyAmount = (double)value / numberCounter[16];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[15]) {
			// Decillion
			moneySuffix = " "+moneyUnit[15];
			newMoneyAmount = (double)value / numberCounter[15];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[14]) {
			// Decillion
			moneySuffix = " "+moneyUnit[14];
			newMoneyAmount = (double)value / numberCounter[14];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[13]) {
			// Decillion
			moneySuffix = " "+moneyUnit[13];
			newMoneyAmount = (double)value / numberCounter[13];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[12]) {
			// Decillion
			moneySuffix = " "+moneyUnit[12];
			newMoneyAmount = (double)value / numberCounter[12];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[11]) {
			// Decillion
			moneySuffix = " "+moneyUnit[11];
			newMoneyAmount = (double)value / numberCounter[11];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[10]) {
			// Decillion
			moneySuffix = " "+moneyUnit[10];
			newMoneyAmount = (double)value / numberCounter[10];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[9]) {
			// Decillion
			moneySuffix = " "+moneyUnit[9];
			newMoneyAmount = (double)value / numberCounter[9];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[9]) {
			// Decillion
			moneySuffix = " "+moneyUnit[9];
			newMoneyAmount = (double)value / numberCounter[9];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[8]) {
			// Nonillion
			moneySuffix = " "+moneyUnit[8];//Quint
			newMoneyAmount = (double)value / numberCounter[8];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[7]) {
			//Octillion
			moneySuffix = " "+moneyUnit[7];
			newMoneyAmount = (double)value / numberCounter[7];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[6]) {
			// Sextillion
			moneySuffix = " "+moneyUnit[6];//Quint
			newMoneyAmount = (double)value / numberCounter[6];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[5]) {
			// Septillion
			moneySuffix = " "+moneyUnit[5];
			newMoneyAmount = (double)value / numberCounter[5];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[4]) {
			// Quintillion
			moneySuffix = " "+moneyUnit[4];//Quin
			newMoneyAmount = (double)value / numberCounter[4];
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[3]) {//999999999999999
			// quadrillion
			moneySuffix = " "+moneyUnit[3];//Quad
			newMoneyAmount = (double)value / numberCounter[3];//((double)1000000000000000;
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[2]) {//999999999999
			// trillion
			moneySuffix = " "+moneyUnit[2];//Tril
			newMoneyAmount = (double)value / numberCounter[2];//((double)1000000000000;
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[1]) {//999999999
			// billion
			moneySuffix = " "+moneyUnit[1];//Bil
			newMoneyAmount = (double)value / numberCounter[1];//((double)1000000000;
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else if (value > millionsNumbers[0]) {//999999
			// million
			moneySuffix = " "+moneyUnit[0];//Mil
			newMoneyAmount = (double)value / numberCounter[0];//((double)1000000;
			return  ((string.Format ("{0:n2}", newMoneyAmount) + moneySuffix));

		} else {
			return  (string.Format ("{0:n2}", value) + moneySuffix); // string.Format("{0:#.00}", Convert.ToDecimal(totalMoney.ToString ()) / 100);

		}
	}
}
