﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using TMPro;
public class UpgradeCalutulator : MonoBehaviour {
	public MoneyUnitHandler moneyHandler;
	public Button upBtn;
	public TextMeshProUGUI upText;
	public double temptotalMoney;
	public double upgradeAmount;
	public double percentUpgrade;
	public double moneyNeed = 0;
	public int multiplier;
	public int level;
	public bool OnStart = false;

	private double temptotalMoneyHolder;
	private double upgradeAmountHolder;
	private double percentUpgradeHolder;
	int tempMultiplier = 0;



	void Start () {
		temptotalMoneyHolder = temptotalMoney;
		upgradeAmountHolder = upgradeAmount;
		percentUpgradeHolder = percentUpgrade;

		upBtn.interactable = false;

	}
	
	// Update is called once per frame
	void Update () {
		if (OnStart) {


			temptotalMoney = temptotalMoneyHolder;
			upgradeAmount = upgradeAmountHolder;
			percentUpgrade = percentUpgradeHolder;
			tempMultiplier = 0;
//			if (multiplier == 0) { // if multiplier for max 
				
			if (multiplier == 1) {//multipler for x1
				print ("multiplier for x1 ");
				upgradeAmount = upgradeAmount * percentUpgrade;
				tempMultiplier = 1;

			}else { //multipler for x10 x100
				bool cont = true;
				double tempUpGrade = upgradeAmountHolder + (upgradeAmountHolder * percentUpgrade);
				double tempPecent = 1;

				do {
					if (temptotalMoney >= tempUpGrade) {
						tempPecent = percentUpgrade;
						double tempUpGrade1 = (tempUpGrade * tempPecent);
						print ("New 1 " + tempUpGrade1);
						tempUpGrade = tempUpGrade + tempUpGrade1;
						print ("New 2 " + tempUpGrade);
						tempMultiplier++;

					}else{
						cont = false; 
					}

				} while(cont);

				if (tempMultiplier == 0 && multiplier == 0) {
					upgradeAmount = upgradeAmount + (upgradeAmount * percentUpgrade);
				} else {

					if (multiplier == 10 || multiplier == 100) {
						tempMultiplier = multiplier + 1;
					}
					upgradeAmount = upgradeAmount + (upgradeAmount * percentUpgrade);
					tempPecent = 1;
					double tempHolder = 0;
					for (int i = 1; i < tempMultiplier; i++) {
						tempPecent = percentUpgrade;
						double tempUpGrade1 = (upgradeAmount * tempPecent);
						upgradeAmount = upgradeAmount + tempUpGrade1;
						tempHolder = tempHolder + upgradeAmount;
					}
				}
			}


			if (temptotalMoney >= upgradeAmount) {
				print ("can buy");
				upBtn.interactable = true;
			} else {
				print ("cant buy");
				upBtn.interactable = false;
			}

			upText.text = "$"+moneyHandler.GetStringForValue(upgradeAmount);

		}



	}



	public void OnReduce(){
		temptotalMoney = temptotalMoney - upgradeAmount;
		upgradeAmount = upgradeAmount * percentUpgrade;
	}

}
