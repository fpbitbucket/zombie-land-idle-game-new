﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using com.ootii.Messages;

public class SceneLoading : MonoBehaviour
{

  public GameObject portrat,fillObj;
  bool OnDone = false;
  void OnEnable()
  {
    OnDone = false;
    MessageDispatcher.AddListener("OnStartChangeScene", OnExitScene, true);
  }

  void OnDisable()
  {
    MessageDispatcher.RemoveListener("OnStartChangeScene", OnExitScene, true);
  }

  void OnExitScene(IMessage rMessage)
  {
    OnShowScreen();
  }

  void Start()
  {
    OnDone = false;
    OnShowScreen();
    StartCoroutine(OnRemoveSplashScreen());
  }

  void OnShowScreen()
  {
    portrat.SetActive(true);
  }



  IEnumerator OnRemoveSplashScreen()
  {
    yield return new WaitForSeconds(5);
    OnDone = true;
    fillObj.GetComponent<Image>().fillAmount = 0;
    portrat.SetActive(false);
    // gameObject.SetActive(false);
  }
}
