﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using com.ootii.Messages;


public class LoadingScript : MonoBehaviour {
//	public Image loadObjLand,loadObjPort;
	public GameObject loadObjPort;
	public string nameScene;
	void OnEnable (){
		

//		landPanel.SetActive (true);
		loadObjPort.SetActive (true);
		nameScene = GameObject.Find ("GameTimer").GetComponent<TimerSingleton> ().ActiveScene;
		StartCoroutine(AsynchronousLoad(nameScene));

	}

	void OnDisable(){
//		MessageDispatcher.RemoveListener ("OnStartChangeScene", OnStartChangeScene, true); 
	}


	void OnStartChangeScene (IMessage rMessage){
		nameScene = (string)rMessage.Data;
		MessageDispatcher.SendMessage (this, "OnPlayingInGame",1, 0);
		StartCoroutine(AsynchronousLoad(nameScene));

	}


	void Update() {
//		if ((Input.deviceOrientation == DeviceOrientation.LandscapeLeft || Input.deviceOrientation == DeviceOrientation.LandscapeRight)) {
//			landPanel.SetActive (true);
//			loadObjPort.SetActive (false);
//		} else if ((Input.deviceOrientation == DeviceOrientation.Portrait || Input.deviceOrientation == DeviceOrientation.PortraitUpsideDown)) {
//			landPanel.SetActive (false);
//			loadObjPort.SetActive (true);
//		}
	}
	IEnumerator AsynchronousLoad (string scene)
	{
		yield return null;

		AsyncOperation ao = Application.LoadLevelAsync (scene);//SceneManager.LoadSceneAsync(scene);
		ao.allowSceneActivation = false;
		while (! ao.isDone)
		{
			// [0, 0.9] > [0, 1]
			float progress = Mathf.Clamp01(ao.progress / 0.9f);
			if (ao.progress == 0.9f)
			{
				System.GC.Collect();
				ao.allowSceneActivation = true;
			}

			yield return null;
		}
	}
}
