﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TMPro;
using com.ootii.Messages;
public class MenuNotifyScript : MonoBehaviour {
	public string name;
	public TextMeshProUGUI btnText;
	public Color disable;
	private Color availColor;


	void OnEnable () {

		MessageDispatcher.AddListener ("On_"+name, OnturnOnbutton, true);

	}

	void OnDisable () {
		MessageDispatcher.RemoveListener ("On_"+name, OnturnOnbutton, true);
	}

	private void OnturnOnbutton(IMessage rMessage){
		
	}
}
