﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using com.ootii.Messages;
public class GeneratorScript_OLD : MonoBehaviour {
	public UnlockSript getNumLevels;
	public MoneyManager getMoneyUnit;
	public GameObject fillBarAnim;
	public Transform fillbar;
	public GameObject lockObj;
	public GameObject fillLevel;
	public GameObject upBtn;
	public Button buyBtn;
	public Image IconnBg;
	public string nameString; // name of generator
	public float basicTime; // time for progress bar
	public TextMeshProUGUI textMultiplier;
	public TextMeshProUGUI textTime;
	public TextMeshProUGUI startAmountText;
	public TextMeshProUGUI itemAmountText;
	public TextMeshProUGUI levelText;
	public TextMeshProUGUI upTextAmount;
	public double basicCost = 1; //Money Earning
	public double percentIncrease;
	public double upgradeAmount = 0; //Money Upgrade
	public int levelItem = 1; // Level of generator
	public bool isActiveGen = false; //active Generator

	private double xTimesAmount = 1; // multiplier

	public Color colorAvail; // color of  the button for unlocked generator
	public Color iconColor; // color of the icon bg button for buy
	public Color readycolorBuy; // color of the bg button for upgrade generator
	private bool timerStart = false; // bool for time to start for text timer

	private float timeleftOffline = 0f;

	private float origTime; // a original time for basic time use for calculator double speed upgrade
	private float tempTime; // a temporary time for basic time for reset cycle

	private int tempLevel = 0; // a temp level use for fillbar for level
	private double mainMultiplier = 0;
	private double TempAmount;
	private double startTingPercentage = 1;
	float tempScaler = 0;


	private int Orig_Level;
	private float Orig_basicTime;
	private double Orig_basicCost;
	private double Orig_percentIncrease;
	private double Orig_upgradeAmount;
	private bool Orig_isActiveGen;

	private double unlockMultiplierOfProft = 0;
	private double upgradeMultipliers  = 0;

	private double adMultipliers = 0;
	private double angelMultiplier  = 1;
	private double totalMultiplier = 1;

	private bool OnStartGame = false;
	private bool OnSuspend = true;

	private bool hasManagerGen = false;

	void Start(){
		IconnBg.color = iconColor;

		Orig_Level = levelItem;
		Orig_basicCost = basicCost;
		Orig_basicTime = basicTime;
		Orig_percentIncrease = Orig_basicCost;
		Orig_upgradeAmount = upgradeAmount;
		Orig_isActiveGen = isActiveGen;
		tempTime = basicTime;
		timeleftOffline = 0f;
		origTime = basicTime;
		tempLevel = Orig_Level;
		fillbar.GetComponent<Image> ().fillAmount = 0;
		fillBarAnim.SetActive (false);
		MessageDispatcher.AddListener ("OnUpdateMultiplier", OnUpdateMultiplier, true); //update multipeler for upgrade
		MessageDispatcher.AddListener ("OnHireManager" + nameString, OnHireManager, true); // function for hire manager and upgrade profit
		MessageDispatcher.AddListener ("On_UpdateSpeed" + nameString, OnDoubleSpeedEarning, true); // function for double speed for unlocks
		MessageDispatcher.AddListener ("On_UnlockedMultiplier_" + nameString, OnunLockUpgradeProfitEarining, true); //update amount earning from Unlocked
		MessageDispatcher.AddListener ("OnResetAllGenerator", OnResetAllGenerator,true);
		MessageDispatcher.AddListener ("OnSetAdMultiplier", OnSetAdMultiplier,true);
		MessageDispatcher.AddListener ("OnUpgradeMultiplier" + nameString, OnSetUpgradeMultiplier,true);
		MessageDispatcher.AddListener ("OnAngelMultiplier" + nameString, OnAngelMultiplier,true);
		MessageDispatcher.AddListener ("OnLevelUpgrade_" + nameString + "_Level", OnLevelUpgrade,true);
		MessageDispatcher.AddListener ("OnRefreshGenerator", OnRefreshGenerator, true);


		MessageDispatcher.AddListener ("OnUnlocked_Button_" + nameString, OnUnlockedButton, true);
		MessageDispatcher.AddListener ("OnUpgrade_Button_" + nameString, OnUpgradeButton, true);
		MessageDispatcher.AddListener ("OnBuy_Button_" + nameString, OnBuyButton, true);

		OnSetupGenerators ();
	}

	void OnEnable(){
//		OnSetupGenerators ();
//		if (OnStartGame == true) {
//			OnLoadDatas ();
//		}

//		print ("enable");
//		Orig_Level = levelItem;
//		Orig_basicCost = basicCost;
//		Orig_basicTime = basicTime;
//		Orig_percentIncrease = Orig_basicCost;
//		Orig_upgradeAmount = upgradeAmount;
//		Orig_isActiveGen = isActiveGen;
//		tempTime = basicTime;
//		timeleftOffline = 0f;
//		origTime = basicTime;
//		tempLevel = Orig_Level;
//		fillbar.GetComponent<Image> ().fillAmount = 0;
//		fillBarAnim.SetActive (false);
//		MessageDispatcher.AddListener ("OnUpdateMultiplier", OnUpdateMultiplier, true); //update multipeler for upgrade
//		MessageDispatcher.AddListener ("OnHireManager" + nameString, OnHireManager, true); // function for hire manager and upgrade profit
//		MessageDispatcher.AddListener ("On_UpdateSpeed" + nameString, OnDoubleSpeedEarning, true); // function for double speed for unlocks
//		MessageDispatcher.AddListener ("On_UnlockedMultiplier_" + nameString, OnunLockUpgradeProfitEarining, true); //update amount earning from Unlocked
//		MessageDispatcher.AddListener ("OnResetAllGenerator", OnResetAllGenerator,true);
//		MessageDispatcher.AddListener ("OnSetAdMultiplier", OnSetAdMultiplier,true);
//		MessageDispatcher.AddListener ("OnUpgradeMultiplier" + nameString, OnSetUpgradeMultiplier,true);
//		MessageDispatcher.AddListener ("OnAngelMultiplier" + nameString, OnAngelMultiplier,true);
//		MessageDispatcher.AddListener ("OnLevelUpgrade_" + nameString + "_Level", OnLevelUpgrade,true);
//		MessageDispatcher.AddListener ("OnRefreshGenerator", OnRefreshGenerator, true);
//
//
//		MessageDispatcher.AddListener ("OnUnlocked_Button_" + nameString, OnUnlockedButton, true);
//		MessageDispatcher.AddListener ("OnUpgrade_Button_" + nameString, OnUpgradeButton, true);
//		MessageDispatcher.AddListener ("OnBuy_Button_" + nameString, OnBuyButton, true);
	}

	void OnDisable(){
//		print ("Disable");
//		MessageDispatcher.RemoveListener ("OnUpdateMultiplier", OnUpdateMultiplier, true);
//		MessageDispatcher.RemoveListener ("OnHireManager" + nameString, OnHireManager, true);
//		MessageDispatcher.RemoveListener ("On_UpdateSpeed" + nameString, OnDoubleSpeedEarning, true);
//		MessageDispatcher.RemoveListener ("On_UnlockedMultiplier_" + nameString, OnunLockUpgradeProfitEarining, true);
//		MessageDispatcher.RemoveListener ("OnSetAdMultiplier", OnSetAdMultiplier, true);
//		MessageDispatcher.RemoveListener ("OnResetAllGenerator", OnResetAllGenerator, true);
//		MessageDispatcher.RemoveListener ("OnUpgradeMultiplier" + nameString, OnSetUpgradeMultiplier, true);
//		MessageDispatcher.RemoveListener ("OnAngelMultiplier" + nameString, OnAngelMultiplier,true);
//		MessageDispatcher.RemoveListener ("OnLevelUpgrade_" + nameString + "_Level", OnLevelUpgrade,true);
//		MessageDispatcher.RemoveListener ("OnRefreshGenerator", OnRefreshGenerator,true);
//
//
//
//		MessageDispatcher.RemoveListener ("OnUnlocked_Button_" + nameString, OnUnlockedButton, true);
//		MessageDispatcher.RemoveListener ("OnUpgrade_Button_" + nameString, OnUpgradeButton, true);
//		MessageDispatcher.RemoveListener ("OnBuy_Button_" + nameString, OnBuyButton, true);
	}

	void OnRefreshGenerator(IMessage rMessage){
		OnSetupGenerators ();
	}

	void OnLevelUpgrade(IMessage rMessage){
		double numUpMultiplier = (double)rMessage.Data;
		levelItem = levelItem + (int)numUpMultiplier;
		ES2.Save(levelItem, "btnName_" + nameString+"?tag=Level");
//		levelItem = ES2.Load<int> ("btnName_" + nameString + "?tag=Level");
		levelText.text = levelItem.ToString();
		OnEarningFunction ("Update");
	}
	void OnAngelMultiplier(IMessage rMessage){
		double numUpMultiplier = (double)rMessage.Data;

		if (angelMultiplier == 1) {
			angelMultiplier = 0;
		} 

//		print ("send Multiplier" + numUpMultiplier);
		angelMultiplier = angelMultiplier + numUpMultiplier;
		ES2.Save(angelMultiplier, "btnName_" + nameString+"?tag=angelMultiplier");
		OnEarningFunction ("Update");
	}

	void OnSetUpgradeMultiplier(IMessage rMessage){
		int numUpMultiplier = (int)rMessage.Data;

		upgradeMultipliers = upgradeMultipliers + numUpMultiplier;
		ES2.Save(upgradeMultipliers, "btnName_" + nameString+"?tag=UpgradeprofitMultiplier");
		OnEarningFunction ("Update");
	}

	void OnSetAdMultiplier(IMessage rMessage){ 
		bool activeAdMultiplier = (bool)rMessage.Data;
		if (activeAdMultiplier) {
			adMultipliers = 2;
		} else {
			adMultipliers = 0;
		}
		OnEarningFunction ("update");
	}

	private void OnResetAllGenerator(IMessage rMessage){ 
		double numInvestor = (double)rMessage.Data;
		timerStart = false;

		levelItem = Orig_Level;
		Orig_basicCost = Orig_basicCost + numInvestor;
		basicCost = Orig_basicCost;
		ES2.Save(basicCost, "btnName_" + nameString+"?tag=Amount");
	
		basicTime = Orig_basicTime;
		tempTime = Orig_basicTime;

		Orig_basicCost = Orig_percentIncrease;
		upgradeAmount = Orig_upgradeAmount;
		isActiveGen = Orig_isActiveGen;
		tempLevel = Orig_Level;

		unlockMultiplierOfProft = 0;
		upgradeMultipliers  = 0;
		angelMultiplier  = 1;
		if (nameString == "Stocks") {
			Orig_isActiveGen = true;
		} else {
			Orig_isActiveGen = false;
		}
		ES2.Delete("btnName_" + nameString);
		OnSetupGenerators ();
	
	}

	void OnSetupGenerators(){ //setup Generator data 
		
		System.TimeSpan t = System.TimeSpan.FromSeconds(basicTime);
		textTime.text = string.Format("{0:D2}:{1:D2}:{2:D2}", t.Hours, t.Minutes, t.Seconds);

		if (ES2.Exists ("btnName_" + nameString)) {
			OnLoadDatas ();
			timeleftOffline = ES2.Load<float> ("btnName_" + nameString + "?tag=time_GenLeft");
			if (isActiveGen == true) {
				if (hasManagerGen == true) {
					
					timerStart = false;
					buyBtn.enabled = false;
					IconnBg.color = Color.white;
					float tempFloatGetLeft = TimerSingleton.instance.getTimeManager (nameString);
					basicTime = tempFloatGetLeft;
					float percentTime = (basicTime / tempTime);
					print (basicTime + " " + tempTime);
					print ((float)System.TimeSpan.FromSeconds (1 - (basicTime / tempTime)).TotalSeconds + " Time");
					tempScaler =  (float)System.TimeSpan.FromSeconds (1 - (basicTime/tempTime)).TotalSeconds;
					timerStart = true;

				} else {
					
					lockObj.SetActive (false);
					float tempFloatGetLeft = TimerSingleton.instance.getTimeLeft (nameString);
					if (tempFloatGetLeft == 0f) {
						if (timeleftOffline != 0){
							OnEarningFunction ("earning");
							timeleftOffline = 0;
							ES2.Save(timeleftOffline, "btnName_" + nameString+"?tag=time_GenLeft");
						}
					} else {
						timerStart = false;
						buyBtn.enabled = false;
						IconnBg.color = Color.white;
						basicTime = tempFloatGetLeft;
						float percentTime = (basicTime / tempTime);
						tempScaler =  (float)System.TimeSpan.FromSeconds (1 - (basicTime/tempTime)).TotalSeconds;

						timerStart = true;
					}
				}
			} else {
				ES2.Save(false, "btnName_" + nameString+"?tag=Status");
				isActiveGen = false;
				timerStart = false;
				lockObj.SetActive (true);
			}
		} else {
			timerStart = false;
			lockObj.SetActive (true);
			buyBtn.enabled = true;
			fillBarAnim.SetActive (false);
			fillbar.GetComponent<Image> ().fillAmount = 0;
			ES2.Save(isActiveGen, "btnName_" + nameString+ "?tag=Status");
			ES2.Save(hasManagerGen, "btnName_" + nameString+"?tag=HasManager");
			ES2.Save(levelItem, "btnName_" + nameString+"?tag=Level");
			ES2.Save(tempLevel, "btnName_" + nameString+"?tag=TempLevel");
			ES2.Save(basicCost, "btnName_" + nameString+"?tag=Amount");
			ES2.Save(upgradeAmount, "btnName_" + nameString+"?tag=AmountUpgrade");
			ES2.Save(upgradeAmount, "btnName_" + nameString+"?tag=LockedAmount");
			ES2.Save(percentIncrease, "btnName_" + nameString+"?tag=percentIncrease");
			ES2.Save(basicTime, "btnName_" + nameString+"?tag=progressTime");
			ES2.Save(timeleftOffline, "btnName_" + nameString+"?tag=time_GenLeft");
			ES2.Save(0, "btnName_" + nameString+"?tag=UpgradeLevel");
			ES2.Save(0, "btnName_" + nameString+"?tag=Unlocked");

			ES2.Save(upgradeMultipliers, "btnName_" + nameString+"?tag=UpgradeprofitMultiplier");
			ES2.Save(unlockMultiplierOfProft, "btnName_" + nameString+"?tag=UnlockedprofitMultiplier");
			ES2.Save(angelMultiplier, "btnName_" + nameString+"?tag=angelMultiplier");
			OnLoadDatas ();
		}

		levelText.text = levelItem.ToString();
		textMultiplier.text = "x"+xTimesAmount.ToString ();
		TempAmount = upgradeAmount;
		//calculate the level fill progress bar
		int numUnlock = ES2.Load<int> ("btnName_" + nameString + "?tag=Unlocked"); //number of level was unlocked
		tempLevel = ES2.Load<int> ("btnName_" + nameString + "?tag=TempLevel");
		int tempLevelChecker = 0;
		if (numUnlock < 1) {
			tempLevelChecker = getNumLevels.numLevels [numUnlock];
		} else {
			tempLevelChecker = getNumLevels.numLevels [numUnlock] - getNumLevels.numLevels [numUnlock-1];
		}

		fillLevel.transform.DOScaleX((((float)1/(float)tempLevelChecker))*(float)tempLevel,0);

		StartCoroutine (OnCheckTotalMoney ());

		OnEarningFunction ("update");

		OnStartGame = true;
//		print ("Gen Start " + gameObject.activeInHierarchy);
	}

	private void OnLoadDatas(){
		isActiveGen = ES2.Load<bool> ("btnName_" + nameString + "?tag=Status");
		if (isActiveGen == true) {
			lockObj.SetActive (false);
		}
		//load eveything saves 
		levelItem = ES2.Load<int> ("btnName_" + nameString + "?tag=Level");
		basicCost = ES2.Load<double> ("btnName_" + nameString + "?tag=Amount");
		upgradeAmount = ES2.Load<double> ("btnName_" + nameString + "?tag=AmountUpgrade");

		hasManagerGen = ES2.Load<bool> ("btnName_" + nameString + "?tag=HasManager");

		unlockMultiplierOfProft = ES2.Load<double> ("btnName_" + nameString + "?tag=UnlockedprofitMultiplier");

		angelMultiplier = ES2.Load<double> ("btnName_" + nameString + "?tag=angelMultiplier");

		upgradeMultipliers = ES2.Load<double> ("btnName_" + nameString + "?tag=UpgradeprofitMultiplier");
	}

	IEnumerator OnCheckTotalMoney(){ //check total money if available to upgrade or unlock if locked
		yield return new WaitForSeconds (0.3f);
		if (ES2.Exists ("btnName_" + nameString)) {
			double temptotalMoney = getMoneyUnit.totalMoney;
			if (ES2.Exists ("TotalMoney")) {
				TempAmount = upgradeAmount;
				if (xTimesAmount == 1) {// multiplier for x1 
					mainMultiplier = xTimesAmount;
					TempAmount = TempAmount + (TempAmount * percentIncrease);
					upTextAmount.text = "$" + getMoneyUnit.GetStringForValue (TempAmount).ToString ();//HasDecimalValue (TempAmount).ToString ();
					textMultiplier.text = "x" + mainMultiplier.ToString ();


				}else {
					int numTempPlier = 0; // number of multiplier
					bool cont = true;
					double tempUpGrade = upgradeAmount + (upgradeAmount * percentIncrease);
					double tempPecent = 1;
					double tempHolder = 0;
					do {
						
						if (temptotalMoney >= tempHolder) {
							tempPecent = percentIncrease;
							double tempUpGrade1 = tempUpGrade * tempPecent;
							tempUpGrade = tempUpGrade + tempUpGrade1;
							tempHolder = tempHolder + tempUpGrade;
							numTempPlier++;
						}else{
							cont = false; 
						}

					} while(cont);
					if (numTempPlier == 1 && xTimesAmount == 0) {
						
						TempAmount = TempAmount + (TempAmount * percentIncrease);
						mainMultiplier = 1;
						numTempPlier = 1;
//						print ("ZERO");
					} else {
						if (xTimesAmount == 10 || xTimesAmount == 100) {
							numTempPlier = Convert.ToInt32 (xTimesAmount);
						} else {
							numTempPlier--;
						}
						mainMultiplier = numTempPlier;
						TempAmount = upgradeAmount + (upgradeAmount * percentIncrease);
						tempPecent = percentIncrease;
						double tempHolder1 = 0;
						for (int i = 0; i < numTempPlier; i++) {
							double tempUpGrade1 = (TempAmount * tempPecent);
							TempAmount = TempAmount + tempUpGrade1;
							tempHolder1 = tempHolder1 + TempAmount;

						}
						TempAmount = tempHolder1;
					}

					textMultiplier.text = "x" + mainMultiplier.ToString ();
					upTextAmount.text = "$" + getMoneyUnit.GetStringForValue (TempAmount).ToString ();	

//					print ("multiplier for max " + TempAmount + " " + numTempPlier + " " + xTimesAmount);
				}
					
				double startAmount = 0;
				if (mainMultiplier == 1) {
					startAmount = upgradeAmount;
					startAmountText.text = "$"+ getMoneyUnit.GetStringForValue (upgradeAmount).ToString ();
				} else {
					startAmount = TempAmount;
					startAmountText.text = "$"+ getMoneyUnit.GetStringForValue (TempAmount).ToString ();
				}

				if (temptotalMoney >= TempAmount) {// check if can upgrade/buy 
					MessageDispatcher.SendMessage(this, "OnShowTutorial",1, 0);
					upBtn.GetComponent<Image> ().color = readycolorBuy;
					upBtn.GetComponent<Button> ().enabled = true;
				} else {
					upBtn.GetComponent<Image> ().color = Color.gray;
					upBtn.GetComponent<Button> ().enabled = false;

				}
					
				if (isActiveGen == false) {// check if can unlocked is locked
					if (temptotalMoney >= startAmount) {
						MessageDispatcher.SendMessage(this, "OnShowTutorial",2, 0);
						lockObj.GetComponent<Button> ().interactable = true;
						lockObj.transform.Find ("Gray").GetComponent<Image> ().color = colorAvail;
					} else {
						lockObj.GetComponent<Button> ().interactable = false;
						lockObj.transform.Find ("Gray").GetComponent<Image> ().color = Color.gray;
					}
				}
			}
		}
		StartCoroutine (OnCheckTotalMoney ());
	}

	void OnunLockUpgradeProfitEarining(IMessage rMessage){ // upgrade profit earnings from unlock
		
		int multiplierUnlocked = (int)rMessage.Data;
		unlockMultiplierOfProft = unlockMultiplierOfProft + multiplierUnlocked;
		ES2.Save(unlockMultiplierOfProft, "btnName_" + nameString+"?tag=UnlockedprofitMultiplier");
		OnEarningFunction ("Update");

	}

	private void OnEarningFunction (string callName){
		
		if (unlockMultiplierOfProft == 0 && upgradeMultipliers == 0 && adMultipliers == 0) {
			totalMultiplier = 1;
		} else {
			totalMultiplier = unlockMultiplierOfProft + upgradeMultipliers + adMultipliers;
		}

		double TotalGenratorEarnCycle = (basicCost * levelItem) * totalMultiplier * angelMultiplier;
		itemAmountText.text = "$" + getMoneyUnit.GetStringForValue (TotalGenratorEarnCycle).ToString ();//HasDecimalValue (TotalGenratorEarnCycle);
		if (callName == "earning") {
			MessageDispatcher.SendMessage(this, "OnIncrementMoney",TotalGenratorEarnCycle, 0);	
		}
	}


	void OnDoubleSpeedEarning(IMessage rMessage){ //double speed from unlocked
		int checkMaxfillLevel = ES2.Load<int> ("btnName_" + nameString + "?tag=UpgradeLevel"); //level for upgrade
		checkMaxfillLevel++;
		ES2.Save(checkMaxfillLevel, "btnName_" + nameString+"?tag=UpgradeLevel");
		float finalTimeAnswer = 0;
		if (nameString == "Stocks") {//calculation for stocks because its different from other generator
			float getPow = Mathf.Pow (origTime, ((float)checkMaxfillLevel+1)/1);
			finalTimeAnswer = getPow;

		} else { //calculation for double speed for generator when upgrade
			float getPow = Mathf.Pow (2, (float)checkMaxfillLevel);
			float getCal = origTime / getPow;
			finalTimeAnswer = getCal;
		}
		//-----------------------------------------------------------------

		//formula for 2 - 10 generators 
		if (timerStart == false) {
			basicTime = finalTimeAnswer;
			tempTime = finalTimeAnswer;
			System.TimeSpan t = System.TimeSpan.FromSeconds (basicTime);
			textTime.text = string.Format ("{0:D2}:{1:D2}:{2:D2}", t.Hours, t.Minutes, t.Seconds);
		} else {
			timerStart = false; //turn off 1st to get the time left and get the percentage so can set to the new time double speed

			float tempNumers = tempTime - basicTime; //get time left (fulltime - coutingdowntime)
			float tempnum2 = tempNumers / tempTime; //get percent for old time fill progress;

			float timededuc = finalTimeAnswer - (finalTimeAnswer * tempnum2);
			basicTime = timededuc; //get percent left for new time
			tempTime = finalTimeAnswer;
			timerStart = true;

		}
		//-----------------------------------------------------------------
		ES2.Save(finalTimeAnswer, "btnName_" + nameString+"?tag=progressTime");	
	}
	void OnUpgradeButton (IMessage rMessage){
		
		if (isActiveGen == true) {
			double templong = 0;
			double percentIncrease = ES2.Load<double>("btnName_" + nameString+"?tag=percentIncrease");
			double tempPecent = 1;
			TempAmount = upgradeAmount;
			if (mainMultiplier == 1) {
				templong = TempAmount + (TempAmount * percentIncrease);
			} else {
				if (xTimesAmount == 0 && xTimesAmount == 1) {
					mainMultiplier = 1;
					TempAmount = TempAmount + (TempAmount * percentIncrease);
					textMultiplier.text = "x1";
				} else {
					int numTempPlier = Convert.ToInt32 (mainMultiplier);

					TempAmount = upgradeAmount + (upgradeAmount * percentIncrease);
					tempPecent = percentIncrease;
					double tempHolder = 0;
					for (int i = 0; i < numTempPlier; i++) {
						double tempUpGrade1 = (TempAmount * tempPecent);
						TempAmount = TempAmount + tempUpGrade1;
						tempHolder = tempHolder + TempAmount;
					}
					templong = tempHolder;
				}
			}

			double totalMoney = getMoneyUnit.totalMoney;
			if (totalMoney >= templong) { //check money if is available to buy
				//				print(templong);
				MessageDispatcher.SendMessage (this, "OnCloseTutorial", 1, 0);
				int tempLevelChecker = 0;
				int numUnlock = ES2.Load<int> ("btnName_" + nameString + "?tag=Unlocked");
				//				print (mainMultiplier + " plier" + templong);
				for (int i = 0; i < mainMultiplier; i++) {//use loop for multiplier
					//upgrade the earning every cyle
					levelItem++;
					tempLevel++;
					if (numUnlock < 1) {
						tempLevelChecker = getNumLevels.numLevels [numUnlock];
					} else {
						tempLevelChecker = getNumLevels.numLevels [numUnlock] - getNumLevels.numLevels [numUnlock-1];
					}
					fillLevel.transform.DOKill ();
					fillLevel.transform.DOScaleX((((float)1/(float)tempLevelChecker))*(float)tempLevel,0);

					ES2.Save (levelItem, "btnName_" + nameString + "?tag=Level"); //save immediately level for checking for unlocks
					levelText.text = levelItem.ToString ();

					if (tempLevel == tempLevelChecker) {
						tempLevel = 0;
						if (i == (mainMultiplier - 1)) {
							fillLevel.transform.DOScaleX(0,0).SetDelay (1f);
						}
						MessageDispatcher.SendMessage(this, "On" + nameString + "CheckLevel",0, 0);//send update to Unlocked Manager that unlock was achieve every +25 levels
						numUnlock = ES2.Load<int> ("btnName_" + nameString + "?tag=Unlocked");
					}

					//------------------------------------------------------------------
					upgradeAmount = upgradeAmount + (upgradeAmount * percentIncrease);
					OnEarningFunction ("Update");
				}

				MessageDispatcher.SendMessage(this, "OnDicrementMoney",templong, 0);	
				ES2.Save(tempLevel, "btnName_" + nameString+"?tag=TempLevel");
				ES2.Save (upgradeAmount, "btnName_" + nameString + "?tag=AmountUpgrade");
			} else {

			}
		}
	}

	public void OnUpgradeEarningCyle(){ //upgrade/buy 
		MessageDispatcher.SendMessage (this, "OnUpgrade_Button_" + nameString, 0, 0);
	}

	void OnUnlockedButton (IMessage rMessage){
		print ("OnUnlockedButton");
		if (isActiveGen == false) {
			double templong = 0;
			double totalMoney = getMoneyUnit.totalMoney;
			double tempPecent = 1;

			TempAmount = ES2.Load<double>("btnName_" + nameString+"?tag=LockedAmount");
			if (mainMultiplier == 1) {
				templong = TempAmount;
			} else {
				if (xTimesAmount == 0 && xTimesAmount == 1) {
					mainMultiplier = 1;
					TempAmount = TempAmount + (TempAmount * percentIncrease);
					textMultiplier.text = "x1";
				} else {
					int numTempPlier = Convert.ToInt32 (mainMultiplier);

					TempAmount = upgradeAmount + (upgradeAmount * percentIncrease);
					tempPecent = percentIncrease;
					double tempHolder = 0;
					for (int i = 0; i < numTempPlier; i++) {
						double tempUpGrade1 = (TempAmount * tempPecent);
						TempAmount = TempAmount + tempUpGrade1;
						tempHolder = tempHolder + TempAmount;
					}
					templong = tempHolder;
				}
			}


			if (totalMoney >= templong) { //check if can buy or not
				MessageDispatcher.SendMessage (this, "OnCloseTutorial", 2, 0);
				MessageDispatcher.SendMessage(this, "OnUnlockedGenerator",0, 0); //send trigger from mony script for stats
				lockObj.SetActive (false);
				isActiveGen = true;
//				levelItem = ES2.Load<int> ("btnName_" + nameString + "?tag=Level");
				ES2.Save (isActiveGen, "btnName_" + nameString + "?tag=Status");

				int tempLevelChecker = 0;
				int numUnlock = ES2.Load<int> ("btnName_" + nameString + "?tag=Unlocked");

				if (Convert.ToInt32 (xTimesAmount) == 1) {
					MessageDispatcher.SendMessage (this, "OnDicrementMoney", upgradeAmount, 0);
					levelItem++;
					tempLevel++;
				} else {
					for (int i = 0; i < mainMultiplier; i++) {
						levelItem++;
						tempLevel++;
						ES2.Save (levelItem, "btnName_" + nameString + "?tag=Level");

						if (numUnlock < 1) {
							tempLevelChecker = getNumLevels.numLevels [numUnlock];
						} else {
							tempLevelChecker = getNumLevels.numLevels [numUnlock] - getNumLevels.numLevels [numUnlock-1];
						}
						fillLevel.transform.DOKill ();
						fillLevel.transform.DOScaleX((((float)1/(float)tempLevelChecker))*(float)tempLevel,0);

						if (tempLevelChecker == tempLevel) {
							tempLevel = 0;
							if (i == (mainMultiplier - 1)) {
								fillLevel.transform.DOScaleX(0,0).SetDelay (1f);
							}
							MessageDispatcher.SendMessage(this, "On" + nameString + "CheckLevel",0, 0);//send update to Unlocked Manager that unlock was achieve every +25 levels
							numUnlock = ES2.Load<int> ("btnName_" + nameString + "?tag=Unlocked");
						}
						upgradeAmount = upgradeAmount + (upgradeAmount * percentIncrease);
					}

					MessageDispatcher.SendMessage(this, "OnDicrementMoney",templong, 0);
				}

				OnEarningFunction ("Update");
				ES2.Save(tempLevel, "btnName_" + nameString+"?tag=TempLevel");
				//save Level current
				levelText.text = levelItem.ToString ();
				ES2.Save (levelItem, "btnName_" + nameString + "?tag=Level");

				//set generator as active
				ES2.Save (upgradeAmount, "btnName_" + nameString + "?tag=AmountUpgrade");
				upTextAmount.text = "$" + HasDecimalValue (upgradeAmount).ToString ();

				if (hasManagerGen == true) {
					timerStart = true;
					buyBtn.enabled = false;
					IconnBg.color = Color.white;
				}
			}
		}

	}

	public void OnUnlockItem(){ //unlock the generator during the start
		MessageDispatcher.SendMessage(this, "OnUnlocked_Button_" + nameString,0, 0);	
	}
		
	private void OnUpdateMultiplier(IMessage rMessage){ //update multiplier for upgrade
		double numMulti = (double)rMessage.Data;
		if (numMulti == 0) {// if select as Max
			xTimesAmount = 0;
		} else {
			xTimesAmount = numMulti;
		}
	}

	private void OnHireManager(IMessage rMessage){ //dispatch for hiremanager
		hasManagerGen = true;
		ES2.Save (true, "btnName_" + nameString + "?tag=HasManager");
		if (isActiveGen == true) {
			buyBtn.enabled = false;
			IconnBg.color = Color.white;
			if (timerStart == false) {
				timerStart = true;
				if (0.125f >= tempTime) { //limit the progress bar cause when the number speed is too small the progress bar will run fast and not look good
					fillbar.GetComponent<Image> ().fillAmount = 1;
					fillBarAnim.SetActive (true);
				}
			}
		}
	}

	private IEnumerator OnEndFill(){ //progress bar cycle
		
		OnEarningFunction ("earning");
		yield return new WaitForSeconds (0.1f);

		if (0.125f >= tempTime) {//check time if will do the moving progress bar using or just animation fill
			
			if (hasManagerGen == true) { 
				fillbar.GetComponent<Image> ().fillAmount = 1;
				fillBarAnim.SetActive (true);
			} else {
				fillbar.GetComponent<Image> ().fillAmount = 0;
				fillBarAnim.SetActive (false);
			}
		} else {
			fillbar.GetComponent<Image> ().fillAmount = 0;
			fillBarAnim.SetActive (false);
		}

		hasManagerGen = ES2.Load<bool> ("btnName_" + nameString + "?tag=HasManager");
		if (hasManagerGen == true) { //check if there is manager to the cycle will continue
			timerStart = true;
			buyBtn.enabled = false;
			IconnBg.color = Color.white;
		} else {
			IconnBg.color = iconColor;
			buyBtn.enabled = true;
		}

		System.TimeSpan t = System.TimeSpan.FromSeconds(tempTime);
		textTime.text =  string.Format("{0:D2}:{1:D2}:{2:D2}", t.Hours, t.Minutes, t.Seconds);
	}

	void OnBuyButton (IMessage rMessage){
		if (timerStart == false && isActiveGen) {
			fillbar.GetComponent<Image> ().fillAmount = 0;
			fillBarAnim.SetActive (false);
			timerStart = true;
			buyBtn.enabled = false;
			IconnBg.color = Color.white;
			MessageDispatcher.SendMessage (this, "OnCloseTutorial", 0, 0);
		}
	}

	public void OnBuy(){ // buy for earning when no manger yet
		MessageDispatcher.SendMessage (this, "OnBuy_Button_" + nameString, 0, 0);
	}

	void Update()
	{
		
		if (timerStart && isActiveGen && OnSuspend) { //couting for progress bar
			float tempDeltaTime = Time.deltaTime;

			//calculation for fill bar so it will not base in dotween and better that separating progress bar
			basicTime -= tempDeltaTime; //couting down time
			float tempNumers = tempTime - basicTime; //get time left (fulltime - coutingdowntime)
			float tempnum2 = tempNumers / tempTime; //get percent for the fill progress

			timeleftOffline = basicTime; //store the time for the offline time to continue during suspend

			float tempFiller = (float)System.TimeSpan.FromSeconds (tempnum2).TotalSeconds;
			bool checkhasManager = hasManagerGen; //check if already hire manager

			if (checkhasManager == false) { //if no manager
				if ( 0.125f >= tempTime) {
					fillbar.GetComponent<Image> ().fillAmount = 1;
					fillBarAnim.SetActive (true);
				} else {
					fillbar.GetComponent<Image> ().fillAmount = Mathf.Lerp (tempScaler, 1, tempFiller);
				}
			} else {
				if (0.125f >= tempTime) {
					fillbar.GetComponent<Image> ().fillAmount = 1;
					fillBarAnim.SetActive (true);
				} else {
					fillbar.GetComponent<Image> ().fillAmount = Mathf.Lerp (tempScaler, 1, tempFiller);
				}
			}

			System.TimeSpan t = System.TimeSpan.FromSeconds(basicTime);
			if (basicTime < 0) { // end time progress bar
				tempTime = ES2.Load<float> ("btnName_" + nameString + "?tag=progressTime");
				basicTime = tempTime;
				timeleftOffline = 0;
				tempScaler = 0;
				ES2.Save(timeleftOffline, "btnName_" + nameString+"?tag=time_GenLeft");
				t = System.TimeSpan.FromSeconds(basicTime);
				textTime.text =  string.Format("{0:D2}:{1:D2}:{2:D2}", t.Hours, t.Minutes, t.Seconds);
				timerStart = false;
				StartCoroutine(OnEndFill ());
			} else {
				textTime.text =  string.Format("{0:D2}:{1:D2}:{2:D2}", t.Hours, t.Minutes, t.Seconds);
			}
		}

	}

	private string HasDecimalValue(double value) { // string for use for Generators so it will different than Total money format cause total money format has no 3 decimal places 
		double newMoneyAmount;
		string moneySuffix = "";
		if (value > 999999999999999) {
			// quadrillion
			moneySuffix = " Quad";
			newMoneyAmount = (double)value / (double)1000000000000000;
			return ((string.Format ("{0:n3}", newMoneyAmount) + moneySuffix));

		} else if (value > 999999999999) {
			// trillion
			moneySuffix = " Tril";
			newMoneyAmount = (double)value / (double)1000000000000;
			return ((string.Format ("{0:n3}", newMoneyAmount) + moneySuffix));

		} else if (value > 999999999) {
			// billion
			moneySuffix = " Bil";
			newMoneyAmount = (double)value / (double)1000000000;
			return ((string.Format ("{0:n3}", newMoneyAmount) + moneySuffix));

		} else if (value > 999999) {
			// million
			moneySuffix = " Mil";
			newMoneyAmount = (double)value / (double)1000000;
			return ((string.Format ("{0:n3}", newMoneyAmount) + moneySuffix));

		} else {
			return (string.Format ("{0:n2}", value) + moneySuffix); // string.Format("{0:#.00}", Convert.ToDecimal(totalMoney.ToString ()) / 100);
		}
	}


	void OnApplicationQuit()
	{
		print ("ASSA");
		ES2.Save((float)System.TimeSpan.FromSeconds (timeleftOffline).TotalSeconds, "btnName_" + nameString+"?tag=time_GenLeft"); //save time when application quit
	}

	void OnApplicationPause( bool pauseStatus )
	{
		
		if (pauseStatus == true) {
			if (timerStart == true) {
				OnSuspend = false;
			}
			OnStartGame = false;
			ES2.Save((float)System.TimeSpan.FromSeconds (timeleftOffline).TotalSeconds, "btnName_" + nameString+"?tag=time_GenLeft");//save time when application suspend
//			print(timeleftOffline + " TIME!!");


		} else if (pauseStatus == false && OnStartGame == false) {
			OnSuspend = true;
			if (timerStart == true) {
				OnSetupGenerators ();//load game when start or back from suspend
			} else {
				if (ES2.Exists ("btnName_" + nameString)) {
					if (isActiveGen == true) {
						if (hasManagerGen == true) {
							OnSetupGenerators ();
						}
					}
				}
			}
		}
	}

}
