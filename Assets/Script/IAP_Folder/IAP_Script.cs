﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using com.ootii.Messages;
using UnityEngine.Analytics;

using LitJson;
// Placing the Purchaser class in the CompleteProject namespace allows it to interact with ScoreManager, 
// one of the existing Survival Shooter scripts.
//namespace CompleteProject
//{
	// Deriving the Purchaser class from IStoreListener enables it to receive messages from Unity Purchasing.
public class IAP_Script : MonoBehaviour, IStoreListener
	{
		private static IStoreController m_StoreController;          // The Unity Purchasing system.
		private static IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.

		// Product identifiers for all products capable of being purchased: 
		// "convenience" general identifiers for use with Purchasing, and their store-specific identifier 
		// counterparts for use with and outside of Unity Purchasing. Define store-specific identifiers 
		// also on each platform's publisher dashboard (iTunes Connect, Google Play Developer Console, etc.)

		// General product identifiers for the consumable, non-consumable, and subscription products.
		// Use these handles in the code to reference which product to purchase. Also use these values 
		// when defining the Product Identifiers on the store. Except, for illustration purposes, the 
		// kProductIDSubscription - it has custom Apple and Google identifiers. We declare their store-
		// specific mapping to Unity Purchasing's AddProduct, below.
	public string[] kProductIDConsumable;   
	public double[] numGoldReward;
	private int numIdConsumable = 0;
	public string[] StringtempCurrency;
	string buttonTap;
//	public string[] moneyPrefix = new string[]{"ALL","USD","AFN","ARS","AWG","AUD","AZN","BSD","BBD","BYR","EUR","BZD","BMD","BOB","BAM","BWP","BGN","BRL","GBP","BND","KHR","CAD","KYD","CLP","CNY","COP","CRC","HRK","CUP","EUR","CZK","DKK","DOP","XCD","EGP","SVC","GBP","EUR","FKP","FJD","EUR","GHC","GIP","EUR","GTQ","GGP","GYD","EUR","HNL","HKD","HUF","ISK","INR","IDR","IRR","EUR","IMP","ILS","EUR","JMD","JPY","JEP","KZT","KPW","KRW","KGS","LAK","LVL","LBP","LRD","CHF","LTL","EUR","MKD","MYR","EUR","MUR","MXN","MNT","MZN","NAD","NPR","ANG","EUR","NZD","NIO","NGN","KPW","NOK","OMR","PKR","PAB","PYG","PEN","PHP","PLN","QAR","RON","RUB","SHP","SAR","RSD","SCR","SGD","EUR","SBD","SOS","ZAR","KRW","EUR","LKR","SEK","CHF","SRD","SYP","TWD","THB","TTD","TRY","TRL","TVD","UAH","GBP","USD","UYU","UZS","EUR","VEF","VND","YER","ZWD"};
//	public string[] moneySymBol = new string[]{"Lek","$","؋","$","ƒ","$","ман","$","$","p.","€","BZ$","$","$b","KM","P","лв","R$","£","$","៛","$","$","$","¥","$","₡","kn","₱","€","Kč","kr","RD$","$","£","$","£","€","£","$","€","¢","£","€","Q","£","$","€","L","$","Ft","kr","Rp","Rp","﷼","€","£","₪","€","J$","¥","£","лв","₩","₩","лв","₭","Ls","£","$","CHF","Lt","€","ден","RM","€","₨","$","₮","MT","$","₨","ƒ","€","$","C$","₦","₩","kr","﷼","₨","B/.","Gs","S/.","Php","zł","﷼","lei","руб","£","﷼","Дин.","₨","$","€","$","S","R","₩","€","₨","kr","CHF","$","£","NT$","฿","TT$","TL","£","$","₴","£","$","$U","лв","€","Bs","₫","﷼","Z$"};
	string purchaseType = "";

	public string[] WeekendDealIDs;
	int numDealIdConsumable = 0;
	public string[] WeekendDealCurrency;
	public bool OnLoadedAMount = false;
	void Start()
	{
//		Gender gender = Gender.Female;
//		Analytics.SetUserGender(gender);
//
//		int birthYear = 2014;
//		Analytics.SetUserBirthYear(birthYear);

//		Analytics.Transaction("12345abcde", 0.99m, "USD", null, null);
		// If we haven't set up the Unity Purchasing reference
		if (m_StoreController == null)
		{
			// Begin to configure our connection to Purchasing
			InitializePurchasing();
		}
	}

	void OnEnable(){
		MessageDispatcher.AddListener ("OnBuyChips", OnBuyChips, true); 
		MessageDispatcher.AddListener ("OnBuyWeekendDeal", OnBuyWeekendDeal, true); 

	}

	void OnDisable(){
		MessageDispatcher.RemoveListener ("OnBuyChips", OnBuyChips, true); 
		MessageDispatcher.AddListener ("OnBuyWeekendDeal", OnBuyWeekendDeal, true); 

	}

	void OnBuyWeekendDeal (IMessage rMessage){
		purchaseType = "WeekDealIap";
		string iapBundle = (string)rMessage.Data;
		for (int i = 0; i < WeekendDealIDs.Length; i++) {
			if (iapBundle == WeekendDealIDs [i]) {
				numDealIdConsumable = i;
				break;
			}
		}

		BuyProductID(iapBundle);
	}


	void OnBuyChips (IMessage rMessage){
		int num = (int)rMessage.Data;
		purchaseType = "NormalIap";
		numIdConsumable = num;

		if (num == 0) {
			MessageDispatcher.SendMessage(this, "OnTapGA_Report","BuyBtnGold25", 0);
		} else if (num == 1) {
			MessageDispatcher.SendMessage(this, "OnTapGA_Report","BuyBtnGold60", 0);
		}else if (num == 2) {
			MessageDispatcher.SendMessage(this, "OnTapGA_Report","BuyBtnGold120", 0);
		}else if (num == 3) {
			MessageDispatcher.SendMessage(this, "OnTapGA_Report","BuyBtnGold245", 0);
		}else if (num == 4) {
			MessageDispatcher.SendMessage(this, "OnTapGA_Report","BuyBtnGold630", 0);
		}else if (num == 5) {
			MessageDispatcher.SendMessage(this, "OnTapGA_Report","BuyBtnGold1350", 0);
		}else if (num == 6) {
			MessageDispatcher.SendMessage(this, "OnTapGA_Report","BuyBtnSpecialOffer", 0);
		}else if (num == 7) {
			MessageDispatcher.SendMessage(this, "OnTapGA_Report","BuyBtnStarterPack", 0);
		}

//		if (numIdConsumable == 6) {
//			MessageDispatcher.SendMessage(this, "OnIncrementGold",numGoldReward[num],0);	
//			MessageDispatcher.SendMessage(this, "OnCloseSpecialOffer",0,0);	
//			MessageDispatcher.SendMessage (this, "OnPermanentMultiplier", (double)12, 0);	
//
//		} else {
//			MessageDispatcher.SendMessage(this, "OnIncrementGold",numGoldReward[num],0);	
//		}
		BuyProductID(kProductIDConsumable[num]);
	}

	public void InitializePurchasing() 
	{
		// If we have already connected to Purchasing ...
		if (IsInitialized())
		{
			// ... we are done here.
			return;
		}

		// Create a builder, first passing in a suite of Unity provided stores.
		var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

		// Add a product to sell / restore by way of its identifier, associating the general identifier
		// with its store-specific identifiers.
		for (int i = 0;i < kProductIDConsumable.Length;i++){
			
			builder.AddProduct(kProductIDConsumable[i], ProductType.Consumable);
		}
		for (int i = 0;i < WeekendDealIDs.Length;i++){
			builder.AddProduct(WeekendDealIDs[i], ProductType.Consumable);
		}

		// Kick off the remainder of the set-up with an asynchrounous call, passing the configuration 
		// and this class' instance. Expect a response either in OnInitialized or OnInitializeFailed.
		UnityPurchasing.Initialize(this, builder);
	}


	private bool IsInitialized()
	{
		// Only say we are initialized if both the Purchasing references are set.
		return m_StoreController != null && m_StoreExtensionProvider != null;
	}


	public void BuyConsumable(int num)
	{
		// Buy the consumable product using its general identifier. Expect a response either 
		// through ProcessPurchase or OnPurchaseFailed asynchronously.
		numIdConsumable = num;
//		if (numIdConsumable == 6) {
//			MessageDispatcher.SendMessage(this, "OnIncrementGold",numGoldReward,0);	
//			MessageDispatcher.SendMessage(this, "OnCloseSpecialOffer",0,0);	
//
//		} else {
//			MessageDispatcher.SendMessage(this, "OnIncrementGold",numGoldReward,0);	
//		}
		BuyProductID(kProductIDConsumable[num]);
	}


	void BuyProductID(string productId)
	{
		// If Purchasing has been initialized ...
		if (IsInitialized())
		{
			// ... look up the Product reference with the general product identifier and the Purchasing 
			// system's products collection.
			Product product = m_StoreController.products.WithID(productId);

			// If the look up found a product for this device's store and that product is ready to be sold ... 
			if (product != null && product.availableToPurchase)
			{
				Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
				// ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed 
				// asynchronously.
				m_StoreController.InitiatePurchase(product);
			}
			// Otherwise ...
			else
			{
				// ... report the product look-up failure situation  
				Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
			}
		}
		// Otherwise ...
		else
		{
			// ... report the fact Purchasing has not succeeded initializing yet. Consider waiting longer or 
			// retrying initiailization.
			Debug.Log("BuyProductID FAIL. Not initialized.");
		}
	}


	// Restore purchases previously made by this customer. Some platforms automatically restore purchases, like Google. 
	// Apple currently requires explicit purchase restoration for IAP, conditionally displaying a password prompt.
	public void RestorePurchases()
	{
		// If Purchasing has not yet been set up ...
		if (!IsInitialized())
		{
			// ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
			Debug.Log("RestorePurchases FAIL. Not initialized.");
			return;
		}

		// If we are running on an Apple device ... 
		if (Application.platform == RuntimePlatform.IPhonePlayer || 
			Application.platform == RuntimePlatform.OSXPlayer)
		{
			// ... begin restoring purchases
			Debug.Log("RestorePurchases started ...");
			 
			// Fetch the Apple store-specific subsystem.
			var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
			// Begin the asynchronous process of restoring purchases. Expect a confirmation response in 
			// the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
			apple.RestoreTransactions((result) => {
				// The first phase of restoration. If no more responses are received on ProcessPurchase then 
				// no purchases are available to be restored.
				Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
			});
		}
		// Otherwise ...
		else
		{
			// We are not running on an Apple device. No work is necessary to restore purchases.
			Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
		}
	}


	//  
	// --- IStoreListener
	//

	public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
	{
		// Purchasing has succeeded initializing. Collect our Purchasing references.
		Debug.Log("OnInitialized: PASS");

		// Overall Purchasing system, configured with products for this application.
		m_StoreController = controller;
		// Store specific subsystem, for accessing device-specific store features.
		m_StoreExtensionProvider = extensions;

		StringtempCurrency = new string[kProductIDConsumable.Length];
		for (int i = 0;i < kProductIDConsumable.Length;i++){
			string princeString = m_StoreController.products.WithID (kProductIDConsumable [i]).metadata.isoCurrencyCode;
			string princeIso =  m_StoreController.products.WithID (kProductIDConsumable [i]).metadata.localizedPrice.ToString();
			StringtempCurrency [i] = princeString + " " + princeIso;
//			Debug.Log (m_StoreController.products.WithID(kProductIDConsumable[i]).metadata.localizedTitle + " localizedTitle");
//			Debug.Log (m_StoreController.products.WithID(kProductIDConsumable[i]).metadata.localizedDescription + " localizedDescription");
//			Debug.Log (m_StoreController.products.WithID(kProductIDConsumable[i]).metadata.localizedPriceString + " localizedPriceString");
//			Debug.Log (m_StoreController.products.WithID(kProductIDConsumable[i]).metadata.isoCurrencyCode + " isoCurrencyCode");
//			Debug.Log (m_StoreController.products.WithID(kProductIDConsumable[i]).metadata.localizedPrice + " localizedPrice");
//			Debug.Log(string.Format ("{0:n2}", 5050.01));
//			Debug.Log(string.Format ("{0:n2}", 5050));
		}

		WeekendDealCurrency = new string[WeekendDealIDs.Length];
		for (int i = 0; i < WeekendDealIDs.Length; i++) {
			string princeString = m_StoreController.products.WithID (WeekendDealIDs [i]).metadata.isoCurrencyCode;
			string princeIso =  m_StoreController.products.WithID (WeekendDealIDs [i]).metadata.localizedPrice.ToString();
			WeekendDealCurrency [i] = princeString + " " + princeIso;

		}
		OnLoadedAMount = true;
	}


	public void OnInitializeFailed(InitializationFailureReason error)
	{
		// Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
		Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
	}


	public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args) 
	{
		// A consumable product has been purchased by this user.
		bool validPurchase = false; // Presume valid for platforms with no R.V.
		string[] listGAString;

		#if UNITY_IOS
		listGAString = new string[6];
		#endif
		#if UNITY_ANDROID
		listGAString = new string[7];
		#endif

		if (String.Equals (args.purchasedProduct.definition.id, kProductIDConsumable [numIdConsumable], StringComparison.Ordinal)) {
			Debug.Log (string.Format ("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));

			validPurchase = true;


			Analytics.Transaction (args.purchasedProduct.definition.id, args.purchasedProduct.metadata.localizedPrice,
				args.purchasedProduct.metadata.isoCurrencyCode, null,
				null);
//			#if UNITY_ANDROID || UNITY_IOS
			string receipt = args.purchasedProduct.receipt;
			var wrapper = JsonMapper.ToObject (receipt);
			var payload = (string)wrapper ["Payload"];
			var details = JsonMapper.ToObject (payload);
			var receiptJson = (string)details ["json"];

			string currencyString = args.purchasedProduct.metadata.isoCurrencyCode.ToString ();
			string AmountString = args.purchasedProduct.metadata.localizedPrice.ToString ();
			string ItemTypeString = "Gold";
			string ItemIdString = numGoldReward [numIdConsumable] + "Gold";
			string ReceiptString = args.purchasedProduct.receipt;
			string SignatureString = (string)details ["signature"];
			string cartType = "";
//			#endif

			string IapAmount = "";
			string GA_IAP_Report = "";
			if (numIdConsumable == 6) {
				MessageDispatcher.SendMessage (this, "OnIncrementGold", numGoldReward [numIdConsumable], 0);
				MessageDispatcher.SendMessage (this, "OnPermanentMultiplier", (double)12, 0);	
				MessageDispatcher.SendMessage (this, "OnCloseSpecialOffer", 0, 0);	

				buttonTap = "Tap:BtnSpecialOffer";
				GA_IAP_Report = "SpecialOffer";
				cartType = "SpecialOffer";
			} else if (numIdConsumable == 7) {
				MessageDispatcher.SendMessage (this, "OnIncrementGold", numGoldReward [numIdConsumable], 0);
				MessageDispatcher.SendMessage (this, "OnPermanentMultiplier", (double)3, 0);	
				MessageDispatcher.SendMessage (this, "OnSuccessBuyStarterPack", 0, 0);
				MessageDispatcher.SendMessage (this, "OnCloseStarterPack", 0, 0);	
				buttonTap = "Tap:BtnStartPack";
				GA_IAP_Report = "StarterPack";
				cartType = "StarterPack";
			} else {
				MessageDispatcher.SendMessage (this, "OnIncrementGold", numGoldReward [numIdConsumable], 0);
				buttonTap = "Tap:BtnChipBank" + numIdConsumable;
				GA_IAP_Report = "BtnChipBank_" + numIdConsumable;
				cartType = "ChipBank_"+numGoldReward [numIdConsumable].ToString();
			}
			IapAmount = "Amount:" + StringtempCurrency [numIdConsumable];
//			(string currency, int amount, string itemType, string itemId, string cartType, string receipt, string signature)
			listGAString[0] = currencyString;
			listGAString[1] = AmountString;
			listGAString[2] = ItemTypeString;
			listGAString[3] = ItemIdString;
			listGAString[4] = cartType;
			listGAString[5] = ReceiptString;
			listGAString[6] = SignatureString;

			MessageDispatcher.SendMessage (this, "OnGA_Report_Iap", IapAmount, 0);
			MessageDispatcher.SendMessage (this, "OnGA_Report_Iap", buttonTap, 0);
			MessageDispatcher.SendMessage (this, "OnGA_Report", "Purchase:" + GA_IAP_Report, 0);
		} else if (String.Equals (args.purchasedProduct.definition.id, WeekendDealIDs [numDealIdConsumable], StringComparison.Ordinal)) {

			DateTime date = DateTime.Now;
			string dateToday = date.ToString ("d");
			DayOfWeek day = DateTime.Now.DayOfWeek;
			string dayToday = day.ToString ();

			validPurchase = true;
			string receipt = args.purchasedProduct.receipt;
			var wrapper = JsonMapper.ToObject (receipt);
			var payload = (string)wrapper ["Payload"];
			var details = JsonMapper.ToObject (payload);
			var receiptJson = (string)details ["json"];

			string currencyString = args.purchasedProduct.metadata.isoCurrencyCode.ToString ();
			string AmountString = args.purchasedProduct.metadata.localizedPrice.ToString ();
			string ItemTypeString = "WeekendDeal_"+dayToday;
			string ItemIdString = "WeekendDeal_"+dayToday;
			string ReceiptString = args.purchasedProduct.receipt;
			string SignatureString = (string)details ["signature"];
			string cartType = "WeekendDeal";
			Debug.Log ("Currency " + currencyString);
			Debug.Log ("Amount String " + AmountString);
			Debug.Log ("Item Type String " + ItemTypeString);
			Debug.Log ("Item Id String " + ItemIdString);
			Debug.Log ("Receipt String " + ReceiptString);
			Debug.Log ("Signature String " + SignatureString);

			listGAString[0] = currencyString;
			listGAString[1] = AmountString;
			listGAString[2] = ItemTypeString;
			listGAString[3] = ItemIdString;
			listGAString[4] = cartType;
			listGAString[5] = ReceiptString;
			listGAString[6] = SignatureString;


			MessageDispatcher.SendMessage (this, "OnSuccessBuyDeals", WeekendDealIDs [numDealIdConsumable], 0);


		}else 
		{
			Debug.Log(string.Format("ProcessPurchase: FAIL. Unrecognized product: '{0}'", args.purchasedProduct.definition.id));
			string failpurchase = string.Format ("ProcessPurchase: FAIL. product: '{0}'", args.purchasedProduct.definition.id);
			MessageDispatcher.SendMessage (this, "OnGA_Report_Iap", "failpurchase", 0);
		}

		if (validPurchase) {
			MessageDispatcher.SendMessage (this, "OnGA_BusinessEvent", listGAString, 0);
		}

		// Return a flag indicating whether this product has completely been received, or if the application needs 
		// to be reminded of this purchase at next app launch. Use PurchaseProcessingResult.Pending when still 
		// saving purchased products to the cloud, and when that save is delayed. 
		return PurchaseProcessingResult.Complete;
	}


	public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
	{
		// A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing 
		// this reason with the user to guide their troubleshooting actions.
		Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
	}
}
//}