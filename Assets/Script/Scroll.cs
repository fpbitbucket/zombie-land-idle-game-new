﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TMPro;
using com.ootii.Messages;
public class Scroll : MonoBehaviour {
	public MainGeneratorScript mainGen;
	public MoneyManager getMoneyUnit;
	public GeneratorScript[] generators;
	private string[] nameString;
	private string[] nameDisplayString;
//	public GameObject tinkerText1,tinkerText2;
	public GameObject tinkerText1port,tinkerText2port;
	public int speed;
	private int lastItem;

	public double adMultipliers;
	private float endPosX;
	private bool OnStart = false;
	private bool OnSwitch = false,OnSwitch2 = false;

	void OnEnable(){
		nameString = mainGen.genNames_Code;
		nameDisplayString = mainGen.genNamesString;
		lastItem = 1;
		MessageDispatcher.AddListener ("OnSetAdMultiplier", OnSetAdMultiplier, true);
		MessageDispatcher.AddListener ("OnUpdateTickerData", OnUpdateTickerData, true);


//		string TempString = "";
//		for (int i = 0; i < nameString.Length; i++) {
//			double TotalGenratorEarnCycle = 0;
////			if (ES2.Exists ("btnName_" + nameString [i])) {
////				double unlockMultiplierOfProft = ES2.Load<double> ("btnName_" + nameString [i] + "?tag=UnlockedprofitMultiplier");
////				double angelMultiplier = ES2.Load<double> ("btnName_" + nameString [i] + "?tag=angelMultiplier");
////				double upgradeMultipliers = ES2.Load<double> ("btnName_" + nameString [i] + "?tag=UpgradeprofitMultiplier");
////				double totalMultiplier = 0;
////				int levelItem = ES2.Load<int> ("btnName_" + nameString [i] + "?tag=Level");
////				double basicCost = ES2.Load<double> ("btnName_" + nameString [i] + "?tag=Amount");
//////				double adMultipliers = 0;
////
////				if (unlockMultiplierOfProft == 0 && upgradeMultipliers == 0 && adMultipliers == 0) {
////					totalMultiplier = 1;
////				} else {
////					totalMultiplier = unlockMultiplierOfProft + upgradeMultipliers + adMultipliers;
////				}
////				TotalGenratorEarnCycle = (basicCost * levelItem) * totalMultiplier * angelMultiplier;
////			} else {
////				
////			}
////			TotalGenratorEarnCycle = 0;
//			TempString = TempString + nameDisplayString [i] + " $" + getMoneyUnit.GetStringForValue (TotalGenratorEarnCycle).ToString () + " <sprite name=PriceUp> ";
//			Debug.Log ("Init Ticker " + TempString);
//		}
//
//		tinkerText1.GetComponent<TextMeshProUGUI> ().text = TempString;
//		tinkerText2.GetComponent<TextMeshProUGUI> ().text = TempString;
//		tinkerText1port.GetComponent<TextMeshProUGUI> ().text = TempString;
//		tinkerText2port.GetComponent<TextMeshProUGUI> ().text = TempString;

//		StartCoroutine (OnUpdateTextRunning ());

		OnStart = true;
		MessageDispatcher.SendMessage(this, "OnUpdateTickerData",0, 1f);	
	}

	void OnDisable(){
		OnStart = false;
		MessageDispatcher.RemoveListener ("OnSetAdMultiplier", OnSetAdMultiplier, true);
		MessageDispatcher.RemoveListener ("OnUpdateTickerData", OnUpdateTickerData, true);
	}

	void OnUpdateTickerData(IMessage rMessage){ 
//		string nameGenerator = (bool)rMessage.Data;

		string TempString = "";
		if (OnStart) {
			for (int i = 0; i < generators.Length; i++) {
				double TotalGenratorEarnCycle = generators [i].earningsPerCycle;
//				if (ES2.Exists ("btnName_" + nameString [i])) {
//					double unlockMultiplierOfProft = ES2.Load<double> ("btnName_" + nameString [i] + "?tag=UnlockedprofitMultiplier");
//
//					double angelMultiplier = ES2.Load<double> ("btnName_" + nameString [i] + "?tag=angelMultiplier");
//
//					double upgradeMultipliers = ES2.Load<double> ("btnName_" + nameString [i] + "?tag=UpgradeprofitMultiplier");
//
//					double totalMultiplier = 0;
//
//					int levelItem = ES2.Load<int> ("btnName_" + nameString [i] + "?tag=Level");
//
//					double basicCost = ES2.Load<double> ("btnName_" + nameString [i] + "?tag=Amount");
//
////				double adMultipliers = 0;
//					if (unlockMultiplierOfProft == 0 && upgradeMultipliers == 0 && adMultipliers == 0) {
//						totalMultiplier = 1;
//					} else {
//						totalMultiplier = unlockMultiplierOfProft + upgradeMultipliers + adMultipliers;
//					}
//					TotalGenratorEarnCycle = (basicCost * levelItem) * totalMultiplier * angelMultiplier;
////				Debug.Log (TotalGenratorEarnCycle);
////				Debug.Log (unlockMultiplierOfProft + " " + upgradeMultipliers + " " + adMultipliers);
//
//				} else {
//					TotalGenratorEarnCycle = 0;
//				}

//				TotalGenratorEarnCycle

				TempString = TempString + nameDisplayString [i] + " $" + getMoneyUnit.GetStringForValue (TotalGenratorEarnCycle).ToString () + " <sprite name=PriceUp> ";
			}
//			tinkerText1.GetComponent<TextMeshProUGUI> ().text = TempString;
//			tinkerText2.GetComponent<TextMeshProUGUI> ().text = TempString;

			tinkerText1port.GetComponent<TextMeshProUGUI> ().text = TempString;
			tinkerText2port.GetComponent<TextMeshProUGUI> ().text = TempString;
		}
	}

	void OnSetAdMultiplier(IMessage rMessage){ 
		adMultipliers = (double)rMessage.Data;
//		if (activeAdMultiplier) {
//			adMultipliers = 2;
//		} else {
//			adMultipliers = 0;
//		}
//		Debug.Log ("On Set Ticker Multiplier " + adMultipliers);
		MessageDispatcher.SendMessage(this, "OnUpdateTickerData",0, 1f);	
	}


	private IEnumerator OnUpdateTextRunning()
	{ 
		while (OnStart) {
			yield return new WaitForSeconds (2f);
			string TempString = "";
			for (int i = 0; i < generators.Length; i++) {
//				if (ES2.Exists ("btnName_" + nameString [i])) {
//					double unlockMultiplierOfProft = ES2.Load<double> ("btnName_" + nameString [i] + "?tag=UnlockedprofitMultiplier");
//					double angelMultiplier = ES2.Load<double> ("btnName_" + nameString [i] + "?tag=angelMultiplier");
//					double upgradeMultipliers = ES2.Load<double> ("btnName_" + nameString [i] + "?tag=UpgradeprofitMultiplier");
//					double totalMultiplier = 0;
//					int levelItem = ES2.Load<int> ("btnName_" + nameString [i] + "?tag=Level");
//					double basicCost = ES2.Load<double> ("btnName_" + nameString [i] + "?tag=Amount");
//
//					if (unlockMultiplierOfProft == 0 && upgradeMultipliers == 0 && adMultipliers == 0) {
//						totalMultiplier = 1;
//					} else {
//						totalMultiplier = unlockMultiplierOfProft + upgradeMultipliers + adMultipliers;
//					}
				double TotalGenratorEarnCycle = generators [i].earningsPerCycle;//(basicCost * levelItem) * totalMultiplier * angelMultiplier;

					TempString = TempString + nameDisplayString [i] + " $" + getMoneyUnit.GetStringForValue (TotalGenratorEarnCycle).ToString () + " <sprite name=PriceUp> ";
//				}
			}
//			tinkerText1.GetComponent<TextMeshProUGUI> ().text = TempString;
//			tinkerText2.GetComponent<TextMeshProUGUI> ().text = TempString;

			tinkerText1port.GetComponent<TextMeshProUGUI> ().text = TempString;
			tinkerText2port.GetComponent<TextMeshProUGUI> ().text = TempString;
			yield return OnStart;
		}
	}

	void Update(){
		
//		if (tinkerText1port.activeInHierarchy) {
//			float width = tinkerText1port.GetComponent<TextMeshProUGUI> ().preferredWidth;
//
//			if (tinkerText1port.GetComponent<TextMeshProUGUI> ().havePropertiesChanged) {
//				width = tinkerText1port.GetComponent<TextMeshProUGUI> ().preferredWidth;
//				}
//			if (Mathf.Abs (tinkerText1port.transform.localPosition.x + 400) > width) {
//					OnSwitch = true;
//			} else if (Mathf.Abs (tinkerText2port.transform.localPosition.x + 400) > width) {
//					OnSwitch = false;
//				}
//				if (OnSwitch == false) {
//				float t = tinkerText1port.transform.localPosition.x - (Time.deltaTime * speed);
//				tinkerText1port.transform.localPosition = new Vector3 (t, tinkerText1port.transform.localPosition.y, tinkerText1port.transform.localPosition.z);
//				float posX = tinkerText1port.transform.localPosition.x + width + 10;
//					tinkerText2.transform.localPosition = new Vector3 (posX, tinkerText2.transform.localPosition.y, tinkerText2.transform.localPosition.z);
//				} else {
//					float t = tinkerText2.transform.localPosition.x - (Time.deltaTime * speed);
//					tinkerText2.transform.localPosition = new Vector3 (t, tinkerText2.transform.localPosition.y, tinkerText2.transform.localPosition.z);
//					float posX = tinkerText2.transform.localPosition.x + width + 10;
//				tinkerText1port.transform.localPosition = new Vector3 (posX, tinkerText1port.transform.localPosition.y, tinkerText1port.transform.localPosition.z);
//				}
//			}

			if (tinkerText1port.activeInHierarchy) {
				float width2 = tinkerText1port.GetComponent<TextMeshProUGUI> ().preferredWidth;

				if (tinkerText1port.GetComponent<TextMeshProUGUI> ().havePropertiesChanged) {
					width2 = tinkerText1port.GetComponent<TextMeshProUGUI> ().preferredWidth;
				}
				if (Mathf.Abs (tinkerText1port.transform.localPosition.x + 400) > width2) {
					OnSwitch2 = true;
				} else if (Mathf.Abs (tinkerText2port.transform.localPosition.x + 400) > width2) {
					OnSwitch2 = false;
				}
				if (OnSwitch2 == false) {
					float t = tinkerText1port.transform.localPosition.x - (Time.deltaTime * speed);
					tinkerText1port.transform.localPosition = new Vector3 (t, tinkerText1port.transform.localPosition.y, tinkerText1port.transform.localPosition.z);
					float posX = tinkerText1port.transform.localPosition.x + width2 + 10;
					tinkerText2port.transform.localPosition = new Vector3 (posX, tinkerText2port.transform.localPosition.y, tinkerText2port.transform.localPosition.z);
				} else {
					float t = tinkerText2port.transform.localPosition.x - (Time.deltaTime * speed);
					tinkerText2port.transform.localPosition = new Vector3 (t, tinkerText2port.transform.localPosition.y, tinkerText2port.transform.localPosition.z);
					float posX = tinkerText2port.transform.localPosition.x + width2 + 10;
					tinkerText1port.transform.localPosition = new Vector3 (posX, tinkerText1port.transform.localPosition.y, tinkerText1port.transform.localPosition.z);
				}
			}
	}
}
