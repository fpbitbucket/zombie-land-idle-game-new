﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TMPro;
using com.ootii.Messages;
public class ShopManager : MonoBehaviour {
	public GeneratorScript[] generatorsObj;
	public MainGeneratorScript mainGen;
	public MoneyManager MoneyManagerObj;
	public TextMeshProUGUI numberEarnText;//goldNumberTextPort
	public GameObject onConfimationPopup,OnDonthaveGold;
	private string[] nameString;
	public double[] timeWarpGold = new double[] {10,25,40};
	private double[] timeWarpNumDays = new double[]{1,7,14};
	public int[] numDays = new int[]{1,7,24};
	private double goldNumTotal;
	private int numWarp = 0;
	private string buyname = "";
	double timewarpEarned = 0,goldtoSpent;
	double multiplier_Num = 2;
	public TextMeshProUGUI TestTextPreMinute;
	public string[] defaultAmountUsd,amountGOld;
	public GameObject[] iapBtnsPort;
//	public GameObject[] specialBtn;

	public ScrollRect ShopScroll;
	public GameObject ShopSpaceObj;
	void OnEnable(){
		nameString = mainGen.genNames_Code;
		goldtoSpent = 0;
//		ShopSpaceObj.SetActive (false);
		MessageDispatcher.AddListener ("OnTimeWarpGoldUpgrade", OnTimeWarpGoldUpgrade, true); 
		MessageDispatcher.AddListener ("OnAddPermanentMultiplierShop", OnAddPermanentMultiplierShop, true); 
		MessageDispatcher.AddListener ("OnDealsTimeWarp", OnDealsTimeWarp, true); 
		MessageDispatcher.AddListener ("OnDealsTimeWarpInHour", OnDealsTimeWarpInHour, true); 

		for (int i = 0; i < iapBtnsPort.Length; i++) {
			iapBtnsPort [i].transform.GetChild(2).GetComponentInChildren<TextMeshProUGUI> ().text = defaultAmountUsd [i];
			iapBtnsPort [i].transform.GetChild(3).GetComponentInChildren<TextMeshProUGUI> ().text = amountGOld [i];
		}

		onConfimationPopup.SetActive (false);
		OnDonthaveGold.SetActive (false);
		StartCoroutine (OnCheckCurrencyShop ());
		StartCoroutine (OnsetupScroller ());
	}

	void OnDisable(){
		MessageDispatcher.RemoveListener ("OnAddPermanentMultiplierShop", OnAddPermanentMultiplierShop, true); 
		MessageDispatcher.RemoveListener ("OnTimeWarpGoldUpgrade", OnTimeWarpGoldUpgrade, true); 
		MessageDispatcher.RemoveListener ("OnDealsTimeWarp", OnDealsTimeWarp, true); 
		MessageDispatcher.RemoveListener ("OnDealsTimeWarpInHour", OnDealsTimeWarpInHour, true); 
	}
	private IEnumerator OnsetupScroller()
	{
		yield return new WaitForSeconds (2f);
		ShopScroll.verticalNormalizedPosition = 1;
	}
	private IEnumerator OnCheckCurrencyShop()
	{ 
		bool OnCheckCurreny = true;
		int numIapCurrency = GameObject.Find ("GameTimer").GetComponent<IAP_Script> ().StringtempCurrency.Length;
		while (OnCheckCurreny) {
			numIapCurrency = GameObject.Find ("GameTimer").GetComponent<IAP_Script> ().StringtempCurrency.Length;
			yield return new WaitForSeconds (2f);
			Debug.Log (numIapCurrency + " Currency Count"); 
			if (numIapCurrency == 8){
				OnCheckCurreny = false;
				OnUpdateCurreny ();
			}
		}
	}

	void OnUpdateCurreny(){
		int numIapCurrency = GameObject.Find ("GameTimer").GetComponent<IAP_Script> ().StringtempCurrency.Length;
		for (int i = 0; i < iapBtnsPort.Length; i++) {
			iapBtnsPort [i].transform.GetChild(2).GetComponentInChildren<TextMeshProUGUI> ().text = GameObject.Find ("GameTimer").GetComponent<IAP_Script> ().StringtempCurrency [i];
		}
//		for (int i = 0; i < specialBtn.Length; i++) {
//			specialBtn[i].GetComponent<TextMeshProUGUI> ().text = GameObject.Find ("GameTimer").GetComponent<IAP_Script> ().StringtempCurrency [6];
//		}
	}

	private IEnumerator OnTotalGoldUpdater()
	{ 
		while (true) {
			yield return new WaitForSeconds (0.3f);
//			goldNumTotal = ES2.Load<double> ("TotalGold");
//			goldNumberTextPort.text = goldNumTotal.ToString ();
			yield return true;
		}
	}



	public void buytimeWarp(int numGold){

		MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
		timewarpEarned = 0;
		numWarp = numGold;
		goldtoSpent = timeWarpGold [numWarp];
		buyname = "";
		if (MoneyManagerObj.TotalGold >= goldtoSpent) {
			onConfimationPopup.SetActive (true);
			buyname = "TimeWarp";
			/*
			for (int i = 0; i < nameString.Length; i++) {
				double basicCost = ES2.Load<double> ("btnName_" + nameString [i] + "?tag=Amount");
				double unlockMultiplierOfProft = ES2.Load<double> ("btnName_" + nameString [i] + "?tag=UnlockedprofitMultiplier");
				double upgradeMultipliers = ES2.Load<double> ("btnName_" + nameString [i] + "?tag=UpgradeprofitMultiplier");
				double angelMultiplier = ES2.Load<double> ("btnName_" + nameString [i] + "?tag=angelMultiplier");
				int levelItem = ES2.Load<int> ("btnName_" + nameString [i] + "?tag=Level");
				float progressTime = ES2.Load<float> ("btnName_" + nameString [i] + "?tag=progressTime");
				double totalMultiplier = 0;
				double adMultipliers = 0;
				if (unlockMultiplierOfProft == 0 && upgradeMultipliers == 0 && adMultipliers == 0) {
					totalMultiplier = 1;
				} else {
					totalMultiplier = unlockMultiplierOfProft + upgradeMultipliers + adMultipliers;
				}
				double TotalGenratorEarnCycle = (basicCost * levelItem) * totalMultiplier * angelMultiplier;
				Debug.Log (TotalGenratorEarnCycle + " " + nameString [i]);
			}
			*/

			for (int i = 0; i < generatorsObj.Length; i++) {

				if (generatorsObj [i].hasManagerGen){
					double TotalGenratorEarnCycle = generatorsObj [i].earningsPerCycle;
					float progressTime = generatorsObj [i].tempTime;
					double GetEarnPerMinute = (TotalGenratorEarnCycle / progressTime) * 60;
					double GetEarnPerHour = GetEarnPerMinute * 60;
					timewarpEarned +=  GetEarnPerHour*(24*timeWarpNumDays [numGold]); // days 

					Debug.Log ("PerMinute: " + GetEarnPerMinute + " PerHour: " + GetEarnPerHour + " " + timeWarpNumDays [numGold] + " Days: GEN " + i + " = "  + GetEarnPerHour*(24*timeWarpNumDays [numGold]));
				}

			}
			Debug.Log ("Total Earnings in " +timeWarpNumDays [numGold]+ " Days : " + timewarpEarned);
			numberEarnText.text = "Are you ready to spend " + goldtoSpent.ToString () + " Brains and gain " + MoneyManagerObj.GetStringForValue (timewarpEarned) + " Pills?";
		} else {
			OnDonthaveGold.SetActive (true);
		}
	}

	void Update(){
		double TotalEarningPerMinute = 0;

		for (int i = 0; i < generatorsObj.Length; i++) {

			if (generatorsObj [i].hasManagerGen){
				float progressTime = generatorsObj [i].tempTime;
				double TotalGenratorEarnCycle = generatorsObj [i].earningsPerCycle/progressTime;
				TotalEarningPerMinute += TotalGenratorEarnCycle*60;
			}

		}

		TestTextPreMinute.text = MoneyManagerObj.GetStringForValue (TotalEarningPerMinute) + " Per Minute";

	}

	public void OnBuyGold(int numGold){
		MessageDispatcher.SendMessage (this, "OnBuyChips", numGold, 0);	
	}


	public void OnBuyAdd_AdMultiplier(int numberGold){
		MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
		goldtoSpent = (double)numberGold;
		buyname = "";
		Debug.Log ("OnBuyAdd_AdMultiplier");
		if (MoneyManagerObj.TotalGold >= goldtoSpent) {
			buyname = "add_adMultiplier";
			onConfimationPopup.SetActive (true);
			numberEarnText.text = "Are you ready to spend " + goldtoSpent.ToString () + " Brains and to add x"+multiplier_Num+" to all profit?";
		} else {
			OnDonthaveGold.SetActive (true);
		}

	}

	public void OnBoostGold(int numberGold){
		MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
		goldtoSpent = (double)numberGold;
		buyname = "";
		if (MoneyManagerObj.TotalGold >= goldtoSpent) {
			buyname = "Ad_Boost";
			onConfimationPopup.SetActive (true);
			numberEarnText.text = "Are you ready to spend " + goldtoSpent.ToString () + " Brains to add x2 to your ad Boost to all Business?";
		} else {
			if (ES2.Exists ("NotFirstInstall")) {
				OnDonthaveGold.SetActive (true);
			} else {
				ES2.Save(1, "NotFirstInstall");
				GameObject.Find ("Managers/GameScreenManager").GetComponent<GameManager> ().OnShowScreen (14);
				GameObject.Find ("ShopManager").GetComponent<ShopManager> ().OnConfirmBuyWarp (1);
			}

		}
	}

	public void OnConfirmBuyWarp(int num){
		
		onConfimationPopup.SetActive (false);
		OnDonthaveGold.SetActive (false);

		if (num == 0) {
			if (string.IsNullOrEmpty(buyname) == false) {
				MessageDispatcher.SendMessage (this, "PopButton", 3, 0f);
				if (buyname == "TimeWarp") {
					MessageDispatcher.SendMessage (this, "OnIncrementMoney", timewarpEarned, 0);	
				} else if (buyname == "add_adMultiplier") {
					Debug.Log ("OnAddpermanentAdMultiplier " + multiplier_Num);
//					Debug.Log
					MessageDispatcher.SendMessage (this, "OnPermanentMultiplier", multiplier_Num, 0);
//					MessageDispatcher.SendMessage (this, "OnAddpermanentAdMultiplier", multiplier_Num, 0);	
//					multiplier_Num = 2;
				}	else if (buyname == "Ad_Boost") {
					MessageDispatcher.SendMessage (this, "OnBoostWithGold", (double)2, 0);	
				}

				MessageDispatcher.SendMessage (this, "OnDicrementGold", goldtoSpent, 0);
			}
		} else {
			MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
		}
	}

	void OnAddPermanentMultiplierShop (IMessage rMessage){
		double[] numWarpNew = (double[])rMessage.Data;
		multiplier_Num = numWarpNew [0];
		OnBuyAdd_AdMultiplier((int)numWarpNew [1]);
	}

	void OnTimeWarpGoldUpgrade (IMessage rMessage){
		double numWarpNew = (double)rMessage.Data;

//		numWarp = numWarpNew[0];
//		goldtoSpent = numWarpNew[1];
		buytimeWarp((int)numWarpNew);
	}


	void OnDealsTimeWarpInHour (IMessage rMessage)
	{
		int numHours = (int)rMessage.Data;
		for (int i = 0; i < generatorsObj.Length; i++) {

			if (generatorsObj [i].hasManagerGen) {
				double TotalGenratorEarnCycle = generatorsObj [i].earningsPerCycle;
				float progressTime = generatorsObj [i].tempTime;
				double GetEarnPerMinute = (TotalGenratorEarnCycle / progressTime) * 60;
				double GetEarnPerHour = GetEarnPerMinute * 60;
				timewarpEarned += GetEarnPerHour * numHours; // days 
			}
		}
		MessageDispatcher.SendMessage (this, "OnIncrementMoney", timewarpEarned, 0);	
	}

	void OnDealsTimeWarp (IMessage rMessage)
	{
		int numDays_1 = (int)rMessage.Data;

		for (int i = 0; i < generatorsObj.Length; i++) {

			if (generatorsObj [i].hasManagerGen) {
				double TotalGenratorEarnCycle = generatorsObj [i].earningsPerCycle;
				float progressTime = generatorsObj [i].tempTime;
				double GetEarnPerMinute = (TotalGenratorEarnCycle / progressTime) * 60;
				double GetEarnPerHour = GetEarnPerMinute * 60;
				timewarpEarned += GetEarnPerHour * (24 * numDays_1); // days 
			}
		}
		MessageDispatcher.SendMessage (this, "OnIncrementMoney", timewarpEarned, 0);	
	}
}
