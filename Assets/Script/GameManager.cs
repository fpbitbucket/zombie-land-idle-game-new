﻿using UnityEngine;
//using System;
using UnityEngine.UI;
using System.Collections;
using TMPro;
using com.ootii.Messages;
public class GameManager : MonoBehaviour {
	public ScreenManager screenManagerObj;
	public MoneyManager MoneyManagerObj;
//	public GameObject menuPopup;
	public GameObject launchPopPort;
	public GameObject portraitPanel;
	public GameObject[] GameScreensPort;
//	public GameObject[] UpgradeScreenPort;
	public GameObject multiplierBtnPort;
	private double multiplierNum = 1;
	public TimeManager adLimiter;
//	public GameObject[] upBtnPort;
	public GameObject popupDoubleErningPort;
	public GameObject MultiplierBg;
//	public GameObject[] menucirclePort;
//	public GameObject menuMainCircle;
	public TextMeshProUGUI[] timeTextObj;
	public GameObject BigtimeReward;
	private string[] tipsMessage = new string[]{"It's well worth taking the time to think of a plan to invade the Earth successfully than just invading now and zombies lose.",
		"Hire a crazy zombie to watch your minions while training.",
		"Zombiepocalypse is near! Finally, a world for zombies!",
		"Zombies are waiting to attack! We need and eat human brains.",
		"Doomsday coming! Zombies of the Zombieland will be the victor!"};
	private bool menuNotify = false;

	public Color UpgradebtnActive = Color.white;
	public Color UpgradebtnDeactive = Color.black;
	void Start(){
//		Application.targetFrameRate = 60;
		BigtimeReward.SetActive(false);
		multiplierNum = 1;
		if (GameObject.Find ("Managers/TutorialManager").GetComponent<TutorialManager> ().tutorialStats.Length > 12) {
			if (GameObject.Find ("Managers/TutorialManager").GetComponent<TutorialManager> ().tutorialStats [8] == true) {
				multiplierBtnPort.transform.GetChild(0).gameObject.GetComponent<TextMeshProUGUI>().text = "x1";
				multiplierBtnPort.GetComponent<Button>().interactable  = true;
			} else {
				multiplierBtnPort.transform.GetChild(0).gameObject.GetComponent<TextMeshProUGUI>().text = "";
				multiplierBtnPort.GetComponent<Button>().interactable  = false;
			}
		} 

		launchPopPort.SetActive (false);
		menuNotify = false;

		popupDoubleErningPort.SetActive (false);
		OnCloseSpecialoffer ();

		OnChangeMultiplierStatus (0);

	}

	void OnSetTipMessage(){
		int numTip = Random.Range (0,tipsMessage.Length);
		for (int i = 0; i < timeTextObj.Length; i++) {
			timeTextObj [i].text = tipsMessage [numTip];
		}
	}

	public void OnCloseSpecialoffer(){
//		MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
//		for (int i = 0; i < specialOfferPopup.Length; i++) {
//			specialOfferPopup [i].SetActive (false);
//		}
//		GameScreensPort [14].SetActive (false);
	}

	public void OnSpecialoffer(){
//		MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
//		for (int i = 0; i < specialOfferPopup.Length; i++) {
//			specialOfferPopup [i].SetActive (true);
//		}
//		GameScreensPort [14].SetActive (true);
	}

	public void OnSwitchScreen(int numScreen){
		MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
//		for (int i = 0; i < upBtnPort.Length; i++) {
//			UpgradeScreenPort [i].SetActive (false);
//			upBtnPort [i].transform.Find ("Name/Name").GetComponent<TextMeshProUGUI> ().color = UpgradebtnDeactive;
//		}
			
//		upBtnPort [numScreen].transform.Find ("Name/Name").GetComponent<TextMeshProUGUI> ().color = UpgradebtnActive;
//		UpgradeScreenPort [numScreen].SetActive (true);
	}

	public void OnShowLaunchPop(){
		MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
		launchPopPort.SetActive (true);
	}

	public void OnCloseLaunchPop(){
		MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
		launchPopPort.SetActive (false);
	}


	void OnEnable () {
		for (int i = 1; i < GameScreensPort.Length; i++) {
			GameScreensPort [i].SetActive (false);
		}
		MessageDispatcher.AddListener ("OnCheckReward", OnCheckReward, true);
		MessageDispatcher.AddListener ("OnDoubleScreen", OnDoubleScreen, true);
		MessageDispatcher.AddListener ("OnTriggerMenuNotifyOn", OnTriggerMenuNotifyOn, true);
		MessageDispatcher.AddListener ("OnTriggerMenuNotifyOff", OnTriggerMenuNotifyOff, true);
		MessageDispatcher.AddListener ("OnRestMenuTriggers", OnRestMenuTriggers, true);
		MessageDispatcher.AddListener ("OnCloseSpecialOffer", OnCloseSpecialOffer, true);
		MessageDispatcher.AddListener ("OnShowMultiplierBtn", OnShowMultiplierBtn, true);
		MessageDispatcher.AddListener ("OnBigTimeReward", OnBigTimeReward, true);
	}

	void OnDisable () {
		MessageDispatcher.RemoveListener ("OnCheckReward", OnCheckReward, true);
		MessageDispatcher.RemoveListener ("OnDoubleScreen", OnDoubleScreen, true);
		MessageDispatcher.RemoveListener ("OnTriggerMenuNotifyOn", OnTriggerMenuNotifyOn, true);
		MessageDispatcher.RemoveListener ("OnTriggerMenuNotifyOff", OnTriggerMenuNotifyOff, true);
		MessageDispatcher.RemoveListener ("OnRestMenuTriggers", OnRestMenuTriggers, true);
		MessageDispatcher.RemoveListener ("OnCloseSpecialOffer", OnCloseSpecialOffer, true);
		MessageDispatcher.RemoveListener ("OnShowMultiplierBtn", OnShowMultiplierBtn, true);
		MessageDispatcher.RemoveListener ("OnBigTimeReward", OnBigTimeReward, true);
	}

	void OnBigTimeReward(IMessage rMessage){
		BigtimeReward.SetActive(true);
	}

	public void OnCloseBigTimeReward(){
		BigtimeReward.SetActive(false);
		MessageDispatcher.SendMessage(this, "OnIncrementMoney",(double)10000000, 0);	
		ES2.Save(1,"BigTimeReward");
	}

	void OnShowMultiplierBtn(IMessage rMessage){
		multiplierBtnPort.transform.GetChild(0).gameObject.GetComponent<TextMeshProUGUI>().text = "x1";
		multiplierBtnPort.GetComponent<Button>().interactable  = true;
	}


	void OnCloseSpecialOffer(IMessage rMessage){
		OnCloseSpecialoffer ();
//		Debug.Log ("close All");
	}
	void OnRestMenuTriggers(IMessage rMessage){
//		for (int i = 0; i < menucirclePort.Length; i++) {
//			menucirclePort [i].SetActive (false);
//		}
	}

	void Update() {
//		if (menuNotify) {
////			Color tempColor = menuMainCircle.GetComponent<Image> ().color;
//			Color alpha_0 = new Color (tempColor.r, tempColor.g, tempColor.b, 0);
//
////			menuMainCircle.GetComponent<Image> ().color = Color.Lerp (Color.green, alpha_0, Mathf.PingPong (Time.time, 1));
//		} else {
//			Color tempColor = menuMainCircle.GetComponent<Image> ().color;
//			Color alpha_0 = new Color (tempColor.r, tempColor.g, tempColor.b, 0);
////			menuMainCircle.GetComponent<Image> ().color = alpha_0;
//		}
	}

	public void OnShowMenu () {
		MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
//		menuPopup.SetActive (true);
		menuNotify = false;
		MessageDispatcher.SendMessage (this, "OnCloseTutorial", 3, 0);
	}

	public void OnCloseMenu () {
		MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
		menuNotify = false;
//		menuMainCircle.SetActive (false);
//		menuPopup.SetActive (false);
	}

	private double OfflineNum = 0;
	int numUpG = 0;

	void OnTriggerMenuNotifyOn (IMessage rMessage){
		int tempData = (int)rMessage.Data;
		menuNotify = true;
		if (11 == tempData || 12 == tempData || 13 == tempData) {
			numUpG = tempData;
			tempData = 0;
		}

//		menuMainCircle.SetActive (true);
//		menucirclePort [tempData].SetActive (true);
	}

	bool bolUpg = false;
	void OnTriggerMenuNotifyOff(IMessage rMessage){
		int tempData = (int)rMessage.Data;
		if (11 == tempData || 12 == tempData || 13 == tempData) {
			int tempInt = tempData;
			if (numUpG == tempData) {
				bolUpg = true;
				tempData = 0;
			}else {
				bolUpg = false;
			}
			if (bolUpg == true) {
//				menucirclePort [0].SetActive (false);
			} 
		} else {
//			menucirclePort [tempData].SetActive (false);
		}

	}


	void OnDoubleScreen (IMessage rMessage){
		OnShowScreen (0);
		popupDoubleErningPort.SetActive (false);
		OnSetTipMessage ();
		popupDoubleErningPort.SetActive (true);
		popupDoubleErningPort.transform.Find ("OfflineEarningSmallBg").transform.Find ("MoneyEarn").GetComponent<TextMeshProUGUI>().text = MoneyManagerObj.GetStringForValue ((OfflineNum * (double)2)) + "  Pills";


	}

	public void OnClaimDoubleEarningOffline(){
		MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
		MessageDispatcher.SendMessage(this, "OnIncrementMoney",(OfflineNum*(double)2), 0);	
		popupDoubleErningPort.SetActive (false);
		GameObject.Find ("RateReview").GetComponent<RateReviewScript> ().OnCheckIfHasRateReview ();
	}

	void OnCheckReward (IMessage rMessage){
		double[] tempData = (double[])rMessage.Data;
		OfflineNum = tempData [0];
//		Debug.Log ("On ShowReward");
		System.TimeSpan t = System.TimeSpan.FromSeconds(tempData [1]);

		GameScreensPort [0].SetActive (true);
		GameScreensPort [0].transform.Find ("BG/TimeText").GetComponent<TextMeshProUGUI>().text = "You were offline for "+string.Format("{0:D2}:{1:D2}:{2:D2}", t.Hours, t.Minutes, t.Seconds);
		GameScreensPort [0].transform.Find ("BG/MoneyEarn").GetComponent<TextMeshProUGUI>().text = MoneyManagerObj.GetStringForValue(OfflineNum) + " Pills";
		GameScreensPort [0].transform.Find ("BG/DoubleEarnText").GetComponent<TextMeshProUGUI>().text = "Watch Ad for <color=#00FF01FF>double earnings </color> ("+MoneyManagerObj.GetStringForValue((OfflineNum*(double)2))+") Pills";

	}

	public void OnChangeMultiplier(){
		MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
		if (multiplierNum == 1) {
			multiplierNum = 10;
			MessageDispatcher.SendMessage(this, "OnUpdateMultiplier",multiplierNum, 0);	
			multiplierBtnPort.transform.GetChild(0).gameObject.GetComponent<TextMeshProUGUI>().text = "x10";
			OnChangeMultiplierStatus (1);
		}else if (multiplierNum == 10) {
			multiplierNum = 100;
			MessageDispatcher.SendMessage(this, "OnUpdateMultiplier",multiplierNum, 0);	
			multiplierBtnPort.transform.GetChild(0).gameObject.GetComponent<TextMeshProUGUI>().text = "x100";
			MessageDispatcher.SendMessage(this, "OnTapGA_Report","MultiplierBtnX100", 0);
			OnChangeMultiplierStatus (2);
		}else if (multiplierNum == 100) {
			multiplierNum = 0;
			MessageDispatcher.SendMessage(this, "OnUpdateMultiplier",multiplierNum, 0);	
			multiplierBtnPort.transform.GetChild(0).gameObject.GetComponent<TextMeshProUGUI>().text = "MAX";
			OnChangeMultiplierStatus (3);
			MessageDispatcher.SendMessage(this, "OnTapGA_Report","MultiplierBtnMAX", 0);
		}else if (multiplierNum == 0) {
			multiplierNum = 1;
			MessageDispatcher.SendMessage(this, "OnUpdateMultiplier",multiplierNum, 0);	
			multiplierBtnPort.transform.GetChild(0).gameObject.GetComponent<TextMeshProUGUI>().text = "x1";
			OnChangeMultiplierStatus (0);
			MessageDispatcher.SendMessage(this, "OnTapGA_Report","MultiplierBtnX1", 0);
		}
			
		MessageDispatcher.SendMessage (this, "OnCloseTutorial", 8, 0);
		MessageDispatcher.SendMessage (this, "OnStartSpinBtn", 0, 5f);
//		MessageDispatcher.SendMessage (this, "OnShowTutorial", 9, 10);
	}

	public void OnChangeMultiplierStatus(int numStatus){
		MultiplierBg.transform.GetChild (0).gameObject.SetActive (false);
		MultiplierBg.transform.GetChild (1).gameObject.SetActive (false);
		MultiplierBg.transform.GetChild (2).gameObject.SetActive (false);
		MultiplierBg.transform.GetChild (3).gameObject.SetActive (false);

		MultiplierBg.transform.GetChild (numStatus).gameObject.SetActive (true);
	}

	public void OnCollectOffline(){
		MessageDispatcher.SendMessage(this, "OnIncrementMoney",OfflineNum, 0);	
		MessageDispatcher.SendMessage (this, "OnTapGA_Report", "OfflineEarningContinueClose", 0);
		GameObject.Find ("RateReview").GetComponent<RateReviewScript> ().OnCheckIfHasRateReview ();
	}
	int CurrentScreenActive = 0;
	public void OnShowScreen(int numScreen){
		MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
		double totalMoney = MoneyManagerObj.lifeTimeEarnings;//ES2.Load<double> ("TotalSeasonEarning");
//		menuPopup.SetActive (false);
//		menuNotify = false;
//		Debug.Log(numScreen);

		string NameAction = "";
		for (int i = 0; i < GameScreensPort.Length; i++) {
			GameScreensPort [i].SetActive (false);
		}

		if (numScreen == 0) {
			NameAction = "Close";
		} else {
			NameAction = "Open";
		}

		if (numScreen == 2) {//big offline earning
			GameScreensPort [0].SetActive (true);
			if (NameAction == "Close") {
				
			}
		} else if (numScreen == 8) {//tv button
			MessageDispatcher.SendMessage(this, "OnTapGA_Report","TvBtn"+NameAction, 0);
			if (adLimiter.adMultiplierLimit != 0) {
				GameScreensPort [1].SetActive (true);
			} else {
				GameScreensPort [2].SetActive (true);
			}

			MessageDispatcher.SendMessage (this, "OnCloseTutorial", 9, 0);
			MessageDispatcher.SendMessage (this, "OnShowTutorial", 10, 0.5f);
		} else if (numScreen == 12) {
			GameScreensPort [3].SetActive (true);
		} else if (numScreen == 13) {
			MessageDispatcher.SendMessage(this, "OnTapGA_Report","DailyReward"+NameAction, 0);
			GameScreensPort [4].SetActive (true);
		} else if (numScreen == 14) {
			screenManagerObj.OnShowScreen (4);
//			screenManagerObj.OnActiveStopScreen (1);
		} else if (numScreen == 5) {
			GameScreensPort [5].SetActive (true);
		}
	}



	public void OnUpgradeProfit(string nameString){
		MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
		if (ES2.Load<bool> ("btnName_" + nameString + "?tag=Status") == true) {
			double templong = ES2.Load<double>("btnName_" + nameString+"?tag=profit_x3"); 
			double totalMoney = MoneyManagerObj.totalMoney;//ES2.Load<double> ("TotalMoney");

			if (totalMoney >= templong) {
				MessageDispatcher.SendMessage(this, "OnDicrementMoney",templong, 0);	
				MessageDispatcher.SendMessage(this, "On" + nameString,"OnBuy", 0);

			} else {

			}
		}
	}

	public void OnBuyManager(string nameString){
		MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
		if (ES2.Load<bool> ("btnName_" + nameString + "?tag=Status") == true) {
			if (ES2.Load<bool> ("btnName_" + nameString + "?tag=HasManager") == false) {
				MessageDispatcher.SendMessage(this, "On" + nameString,"HireManager", 0);//buy manager
			}
		}
	}

}
