﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;
using System.Collections;
using TMPro;
using com.ootii.Messages;
using DG.Tweening;
public class TimeTrackerScript : MonoBehaviour {



	private static TimeTrackerScript _instance;
	public static TimeTrackerScript instance
	{
		get
		{
			if(_instance == null)
			{
				_instance = GameObject.FindObjectOfType<TimeTrackerScript>();

				//Tell unity not to destroy this object when loading a new scene!
				DontDestroyOnLoad(_instance.gameObject);
			}

			return _instance;
		}
	}

	public float TimeSpentInApp = 0f;


	void Awake() 
	{
		



		if(_instance == null)
		{
			//If I am the first instance, make me the Singleton
			_instance = this;
			DontDestroyOnLoad(this);
		}
		else
		{
			//If a Singleton already exists and you find
			//another reference in scene, destroy it!
			if(this != _instance)
			Destroy(this.gameObject);
		}
		OnLoadFiles ();
	}

	void OnLoadFiles(){
		bool canResetTime = false;
		if (ES2.Exists ("DayCheckToday")) {
			string stringDate = ES2.Load<string> ("DayCheckToday");
			DateTime oldDate = Convert.ToDateTime (stringDate);
			DateTime newDate = System.DateTime.Now;
			TimeSpan difference = newDate.Subtract (oldDate);
			if (difference.Days >= 1) {
				TimeSpentInApp = 0f;
				ES2.Save (TimeSpentInApp, "TimeSpentInApp");
				canResetTime = true;
			} else {
				TimeSpentInApp = ES2.Load<float> ("TimeSpentInApp");
				canResetTime = false;
			}
		} else {
			ES2.Save (TimeSpentInApp, "TimeSpentInApp");
			ES2.Save (Convert.ToString (System.DateTime.Now), "TapSpinDate");
			canResetTime = false;
		}
	}

	void Update(){
		TimeSpentInApp += Time.deltaTime;

	}

	void OnApplicationQuit()
	{
		ES2.Save (TimeSpentInApp, "TimeSpentInApp");
		ES2.Save (Convert.ToString (System.DateTime.Now), "DayCheckToday");
	}

	void OnApplicationPause( bool pauseStatus )
	{
		if (pauseStatus == true) {
			
			ES2.Save (Convert.ToString (System.DateTime.Now), "DayCheckToday");
			ES2.Save (TimeSpentInApp, "TimeSpentInApp");
		} else if (pauseStatus == false) {
			OnLoadFiles ();
		}
	}

}
