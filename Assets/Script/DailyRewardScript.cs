﻿using UnityEngine;
using System;
using System.Collections;
using TMPro;
using com.ootii.Messages;
public class DailyRewardScript : MonoBehaviour {
	public GameObject dailyRewardbtnPort;
	public TextMeshProUGUI[] rewardTextPort; 
	public GameObject[] coverRewardPort;
	public int[] rewardNum;
	bool onloadDay = false;
	private int numReward = 0;
	public bool hasDailyReward = false;
	void Awake(){
		dailyRewardbtnPort.SetActive (false);

		onloadDay = true;
		hasDailyReward = false;
		for (int i = 0; i < rewardTextPort.Length; i++) {
			rewardTextPort [i].text = rewardNum[i].ToString() + " Brains";
		}
		OnCheckReward ();
	}

	private void OnCheckReward(){
		if (ES2.Exists ("PlayDate")) {
			string stringDate = ES2.Load<string> ("PlayDate");
			DateTime oldDate =  Convert.ToDateTime(stringDate);
			DateTime newDate = System.DateTime.Now;
//			Debug.Log("LastDay: " + oldDate);
//			Debug.Log("CurrDay: " + newDate);
			numReward = ES2.Load<int> ("numReward");
			Debug.Log (numReward);
			for (int i = 0; i < coverRewardPort.Length; i++) {
				if (i > numReward) {
					coverRewardPort [i].SetActive (true);
				} else {
					coverRewardPort [i].SetActive (false);
				}
			}

			for (int i = 0; i < numReward; i++) {
				rewardTextPort [i].text = "COLLECTED";
			}
			TimeSpan difference = newDate.Subtract(oldDate);
//			print (difference.Days + " days");
			if (difference.Days == 1) {
//				Debug.Log ("New Reward!");
				rewardTextPort [numReward].text = rewardNum[numReward].ToString() + " Brains";
				coverRewardPort [numReward].SetActive (false);
				dailyRewardbtnPort.SetActive (true);
				hasDailyReward = true;
			} else if (difference.Days > 1) {
//				print ("more than 1 day");
				numReward = 0;
				ES2.Save(numReward, "numReward");
				numReward = ES2.Load<int> ("numReward");
				for (int i = 0; i < coverRewardPort.Length; i++) {
					coverRewardPort [i].SetActive (true);
				}
				rewardTextPort [numReward].text = rewardNum[numReward].ToString() + " Brains";
				coverRewardPort [numReward].SetActive (false);
				dailyRewardbtnPort.SetActive (true);
				hasDailyReward = true;

				StartCoroutine (OnCheckFreeReward ());

			} else {
				hasDailyReward = false;
				coverRewardPort [numReward].SetActive (true);
				dailyRewardbtnPort.SetActive (false);
			}


		} else {
			ES2.Save(numReward, "numReward");
			ES2.Save(Convert.ToString(System.DateTime.Now), "PlayDate");
			for (int i = 0; i < coverRewardPort.Length; i++) {
				coverRewardPort [i].SetActive (true);
			}

		}
	}

	IEnumerator OnCheckFreeReward(){
		yield return new WaitForSeconds (1);
		if (GameObject.Find ("RewardManager") != null) {
			if (GameObject.Find ("RewardManager").GetComponent<RewardManager> ().hasReward == true) {
				GameObject.Find ("RewardManager").GetComponent<RewardManager> ().rewardbtnPort.SetActive(false);
			}
		}
	}

	void OnApplicationQuit()
	{


	}

	public void OnCollectReward(){
		if (hasDailyReward) {
			MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
			hasDailyReward = false;
			MessageDispatcher.SendMessage (this, "OnIncrementGold", (double)rewardNum [numReward], 0);	

			rewardTextPort [numReward].text = "COLLECTED";
			numReward++;
			DateTime newDate = System.DateTime.Now;

			if (GameObject.Find ("RewardManager").GetComponent<RewardManager> ().hasReward) {
				GameObject.Find ("RewardManager").GetComponent<RewardManager> ().rewardbtnPort.SetActive (true);
			}
			if (numReward == 6) {
				numReward = 0;
			} 

			ES2.Save (numReward, "numReward");
			string newStringDate = Convert.ToString (newDate);
			ES2.Save (newStringDate, "PlayDate");
		} else {
		
		}
		dailyRewardbtnPort.SetActive (false);
	}

	void OnApplicationPause( bool pauseStatus )
	{
		if (pauseStatus == true) {
			onloadDay = false;
		} else if (pauseStatus == false) {
			if (onloadDay == false) {
				OnCheckReward ();
			}
		}
	}


}
