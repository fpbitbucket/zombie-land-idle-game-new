﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using com.ootii.Messages;
using TMPro;
public class QuickBuyManager : MonoBehaviour {
	public MoneyManager mainMoney;
	public GameObject[] quickBuyBtn;
	public GameObject[] quickBuyBtn_Angle;
//	public TextMeshProUGUI textNumberItem;
	public Color ColorAvail,ColorNotAvail;
	void OnEnable(){
		MessageDispatcher.AddListener ("OnShowQuickBuyUpgrades", OnShowQuickBuyUpgrades, true); 
		MessageDispatcher.AddListener ("OnChangeStatusBuy", OnChangeStatusBuy, true); 
		MessageDispatcher.AddListener ("OnChangeStatusBuy_Angle", OnChangeStatusBuy_Angle, true); 

		if (ES2.Exists ("OnShowQuickBuyUpgrades_" + mainMoney.worldName)) {
			OnActiveQuickBuy ();
		} else {
			for (int i = 0; i < quickBuyBtn.Length; i++) {
				quickBuyBtn [i].SetActive (false);
			}

			for (int i = 0; i < quickBuyBtn_Angle.Length; i++) {
				quickBuyBtn_Angle [i].SetActive (false);
			}
		}
	}
	void OnDisable () {
		MessageDispatcher.RemoveListener ("OnShowQuickBuyUpgrades", OnShowQuickBuyUpgrades, true); 
		MessageDispatcher.RemoveListener ("OnChangeStatusBuy", OnChangeStatusBuy, true); 
		MessageDispatcher.RemoveListener ("OnChangeStatusBuy_Angle", OnChangeStatusBuy_Angle, true); 
	}

	void OnChangeStatusBuy_Angle (IMessage rMessage){
		double[] numState = (double[])rMessage.Data;

		if (numState[0] == 0) {
			for (int i = 0; i < quickBuyBtn.Length; i++) {
				quickBuyBtn_Angle [i].transform.Find ("Black").transform.Find ("amount").GetComponent<TextMeshProUGUI> ().text = 0.ToString ();
				quickBuyBtn_Angle [i].transform.Find ("BuyBtn").transform.Find ("Base").GetComponent<Image> ().color = ColorNotAvail;
				quickBuyBtn_Angle [i].transform.Find ("BuyBtn").GetComponent<Button> ().enabled = false;
			}
		}else if (numState[0] == 1) {
			for (int i = 0; i < quickBuyBtn_Angle.Length; i++) {
				quickBuyBtn_Angle [i].transform.Find ("Black").transform.Find ("amount").GetComponent<TextMeshProUGUI> ().text = mainMoney.GetStringForValueNumbers(numState [1]).ToString ();
				quickBuyBtn_Angle [i].transform.Find ("BuyBtn").transform.Find ("Base").GetComponent<Image> ().color = ColorAvail;
				quickBuyBtn_Angle [i].transform.Find ("BuyBtn").GetComponent<Button> ().enabled = true;
			}
		}
	}

	void OnChangeStatusBuy (IMessage rMessage){
		double[] numState = (double[])rMessage.Data;
		if (numState[0] == 0) {
			for (int i = 0; i < quickBuyBtn.Length; i++) {
				quickBuyBtn [i].transform.Find ("Black").transform.Find ("amount").GetComponent<TextMeshProUGUI> ().text = 0.ToString ();
				quickBuyBtn [i].transform.Find ("BuyBtn").transform.Find ("Base").GetComponent<Image> ().color = ColorNotAvail;
				quickBuyBtn [i].transform.Find ("BuyBtn").GetComponent<Button> ().enabled = false;
			}
		}else if (numState[0] == 1) {
			for (int i = 0; i < quickBuyBtn.Length; i++) {
				quickBuyBtn [i].transform.Find ("Black").transform.Find ("amount").GetComponent<TextMeshProUGUI> ().text = mainMoney.GetStringForValueNumbers(numState [1]).ToString ();
				quickBuyBtn [i].transform.Find ("BuyBtn").transform.Find ("Base").GetComponent<Image> ().color = ColorAvail;
				quickBuyBtn [i].transform.Find ("BuyBtn").GetComponent<Button> ().enabled = true;
			}
		}
	}


	void OnShowQuickBuyUpgrades (IMessage rMessage){
		Debug.Log ("On Active Quick Buy");
		ES2.Save(1, "OnShowQuickBuyUpgrades_" + mainMoney.worldName);
		OnActiveQuickBuy ();
	}

	void OnActiveQuickBuy(){
		for (int i = 0; i < quickBuyBtn.Length; i++) {
			quickBuyBtn [i].SetActive (true);
		}

		for (int i = 0; i < quickBuyBtn_Angle.Length; i++) {
			quickBuyBtn_Angle [i].SetActive (true);
		}
	}

}
