﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using com.ootii.Messages;
using TMPro;
public class EzBuyUpgradeScript : MonoBehaviour {
	public MoneyManager mainMoney;
	public GameObject[] EzBuyBtn;
	public Color ColorAvail,ColorNotAvail;
	void OnEnable(){
		MessageDispatcher.AddListener ("OnShowEzBuyUpgrades", OnShowEzBuyUpgrades, true); 
		MessageDispatcher.AddListener ("OnChangeStatusEzBuy", OnChangeStatusEzBuy, true); 
		MessageDispatcher.AddListener ("OnChangeIconEzBuy", OnChangeIconEzBuy, true); 

		if (ES2.Exists ("OnShowEzBuyUpgrades_" + mainMoney.worldName)) {
			OnActiveQuickBuy ();
		} else {
			for (int i = 0; i < EzBuyBtn.Length; i++) {
				EzBuyBtn [i].SetActive (false);
			}
		}
	}

	void OnDisable () {
		MessageDispatcher.RemoveListener ("OnShowEzBuyUpgrades", OnShowEzBuyUpgrades, true); 
		MessageDispatcher.RemoveListener ("OnChangeStatusEzBuy", OnChangeStatusEzBuy, true); 
		MessageDispatcher.RemoveListener ("OnChangeIconEzBuy", OnChangeIconEzBuy, true); 

	}

	void OnChangeIconEzBuy (IMessage rMessage){
		Sprite iconSprite = (Sprite)rMessage.Data;
		for (int i = 0; i < EzBuyBtn.Length; i++) {
			EzBuyBtn [i].transform.Find ("Icon").GetComponent<Image> ().sprite = iconSprite;
		}

	}

	void OnShowEzBuyUpgrades (IMessage rMessage){
		Debug.Log ("On Active Quick Buy");
		ES2.Save(1, "OnShowEzBuyUpgrades_" + mainMoney.worldName);
		OnActiveQuickBuy ();
	}

	void OnChangeStatusEzBuy (IMessage rMessage){
		double[] numState = (double[])rMessage.Data;
	
		if (numState[0] == 0) {
			for (int i = 0; i < EzBuyBtn.Length; i++) {
				EzBuyBtn [i].transform.Find ("Name").transform.Find ("xMultiplier").GetComponent<TextMeshProUGUI> ().text = "x"+numState [1];
				EzBuyBtn [i].transform.Find ("buyBTn").GetComponent<Image> ().color = ColorNotAvail;
				EzBuyBtn [i].transform.Find ("buyBTn").GetComponent<Button> ().enabled = false;
			}
		}else if (numState[0] == 1) {
			for (int i = 0; i < EzBuyBtn.Length; i++) {
				EzBuyBtn [i].transform.Find ("Name").transform.Find ("xMultiplier").GetComponent<TextMeshProUGUI> ().text = "x"+numState [1];
				EzBuyBtn [i].transform.Find ("buyBTn").GetComponent<Image> ().color = ColorAvail;
				EzBuyBtn [i].transform.Find ("buyBTn").GetComponent<Button> ().enabled = true;
			}
		}
	}

	void OnActiveQuickBuy () {
		for (int i = 0; i < EzBuyBtn.Length; i++) {
			EzBuyBtn [i].SetActive (true);
		}
	}
}
