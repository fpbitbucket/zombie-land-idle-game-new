﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TMPro;
using com.ootii.Messages;
public class GoldUpGradeScript : MonoBehaviour {
	public MoneyManager MoneyManagerObj;
	public string[] nameString;
	public string[] nameUpgradeString = new string[] {"Micro-Manager Upgrade","Micro-Manager","x3 Multiplier","x12 Multiplier","x27 Multiplier","x99 Multiplier","x501 Multiplier","Tycoon Claim","1 Day Time Warp","7 Day Time Warp","14 Day Time Warp","Flux Capitalor","Flux Capitalor Deluxe","Gold Suit","Blue Suit","White Suit","Green Suit","Marvin's Theorem","Martian Law"};
	public string[] nameUpgradeStringDiscript = new string[] {"Adds a x2 Speed Buff to the current planet's Micro-Manager.","Increases the speed of investments by 300% when you click it","Applies a permanent 3x profit multiplier to the planet you are on","Applies a permanent 12x profit multiplier to the planet you are on","Applies a permanent 27x profit multiplier to the planet you are on","Applies a permanent 99x profit multiplier to the planet you are on","Applies a permanent 501x profit multiplier to the planet you are on","Immediately claim your pending\u00a0Tycoon Investors\u00a0without having to reset.","Immediately earn\u00a0Money\u00a0and\u00a0Tycoon Investors\u00a0as if you'd idled for one day.","Immediately earn\u00a0Money\u00a0and\u00a0Tycoon Investors\u00a0as if you'd idled for one week.","Immediately earn\u00a0Money\u00a0and\u00a0Tycoon Investors\u00a0as if you'd idled for two weeks.","Applies a permanent 121% profit speed boost for a new multiplier of (1+1.21)/100=2.21x to the planet you are on. ","Applies a permanent 363% profit speed boost for a new multiplier of (1+3.63)/100=4.63x to the planet you are on.","Applies a 2x profit multiplier when worn on any or all planets. ","Applies a 3x profit multiplier when worn on any or all planets.","Applies a 2x profit speed boost when worn on any or all planets. Cannot be worn on event planets.","Applies +10%\u00a0Tycoon Investor\u00a0effectiveness when worn on any or all planets.","Cut Martian wait time by 1/2 and add x1 to their surge.","Cut Martian wait time by 1/32 and add x5 to their surge."};
	public double[] upgradeCost = new double[] {10,45,20,50,100,300,1200,20,10,25,40,40,80,40,50,140,240,50,200};
	public double[] numProfit;
	public Sprite[] btnIcon;
	public GameObject[] GoldUpgradeObjectPort;
	private int numUpgrade;

	public Color availToBuy, notAvailToBuy;


	private int numStartCount, numLastCout;
	bool OnStarChecking = false;
	void Start(){
		OnStarChecking = true;
		MessageDispatcher.AddListener ("OnRestUpgradesGold", OnRestUpgrades, true); 
		MessageDispatcher.AddListener ("OnUpgradeAdviser", OnUpgradeAdviser, true); 
		OnStartSetup ();
		MessageDispatcher.SendMessage(this, "OnRestUpgradesGold",0, 0);	
	}

	void OnDisable(){
		OnStarChecking = false;
		MessageDispatcher.RemoveListener ("OnRestUpgradesGold", OnRestUpgrades, true);
		MessageDispatcher.RemoveListener ("OnUpgradeAdviser", OnUpgradeAdviser, true);
	}

	private void OnStartSetup(){
		for (int i = 0; i < GoldUpgradeObjectPort.Length; i++) {
			int tempInt = i;
			int numSprite = 0;
			if (nameString [i] == "Multiplier") {
				numSprite = 0;
			}else if (nameString [i] == "ClaimWithnoReset") {
				numSprite = 1;
			}else if (nameString [i] == "TimeWarp") {
				numSprite = 2;
			}
			GoldUpgradeObjectPort [i].transform.Find ("ItemBgGrp/Icon").GetComponent<Image> ().sprite = btnIcon[numSprite];
			GoldUpgradeObjectPort [i].transform.Find ("ItemBgGrp/Icon").GetComponent<Image> ().sprite = btnIcon[numSprite];
			GoldUpgradeObjectPort[i].transform.Find ("ItemBtn").GetComponent<Button>().onClick.AddListener(() => { OnUpProfit(tempInt); });
		}
	}



	void OnUpgradeAdviser (IMessage rMessage){
		int numUpgrade = (int)rMessage.Data;
		OnUpProfit (numUpgrade);

	}
	void OnRestUpgrades (IMessage rMessage){
		
//		if (ES2.Exists ("TotalGoldUnlock")) {
//			numUpgrade = ES2.Load<int> ("TotalGoldUnlock");
//		} else {
//			numUpgrade = 0;
//			ES2.Save(numUpgrade, "TotalGoldUnlock");
//		}
//		
		for (int i = 0; i < GoldUpgradeObjectPort.Length; i++) {
			int tempInt = i;

			GoldUpgradeObjectPort [i].transform.Find ("ItemBgGrp/Title_Text").GetComponent<TextMeshProUGUI> ().text = nameUpgradeString[i]; //Name of Upgrade
			GoldUpgradeObjectPort [i].transform.Find ("ItemBgGrp/Discript_Text").GetComponent<TextMeshProUGUI> ().text = nameUpgradeStringDiscript [i]; //Amount of Upgrade
			GoldUpgradeObjectPort [i].transform.Find ("ItemBtn/Text").GetComponent<TextMeshProUGUI> ().text = " <#fff200>"+string.Format ("{0:n0}", upgradeCost[i]);

			GoldUpgradeObjectPort[i].transform.Find ("ItemBtn").GetComponent<Image> ().color = notAvailToBuy;
		}
		StartCoroutine (OnCheckBuy ());

	}

	IEnumerator OnCheckBuy(){
		

		while (OnStarChecking) {
			yield return new WaitForSeconds (1f);
			double TotalGold = 0;
			if (ES2.Exists ("TotalGold")) {
				TotalGold = ES2.Load<double> ("TotalGold");
			} else {
				TotalGold = 0;
			}

			int numTotalManager = GoldUpgradeObjectPort.Length - 1;
			bool checker = false;
			for (int i = 0; i < GoldUpgradeObjectPort.Length; i++) {
				
				if (TotalGold >= upgradeCost[i]) {
					checker = true;
					if (GoldUpgradeObjectPort [i].transform.Find ("ItemBtn").GetComponent<Image> ().color == notAvailToBuy && GoldUpgradeObjectPort [i].activeSelf == true) {
						MessageDispatcher.SendMessage(this, "OnTriggerMenuNotifyOn",3, 0);
					}
					GoldUpgradeObjectPort [i].transform.Find ("ItemBtn").GetComponent<Image> ().color = availToBuy;
				} else {
					GoldUpgradeObjectPort[i].transform.Find ("ItemBtn").GetComponent<Image> ().color = notAvailToBuy;
				}

			}

			if (checker == false) {
				MessageDispatcher.SendMessage(this, "OnTriggerMenuNotifyOff",3, 0);
			}

			yield return OnStarChecking;
		}
	}

	public void OnUpProfit(int numbtn){
//		if (MoneyManagerObj.TotalGold >= upgradeCost [numbtn]) {
			MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
//			numUpgrade++;

//			numStartCount++;
//			numLastCout++;

//			ES2.Save (numUpgrade, "TotalGoldUnlock");
			if (nameString [numbtn] == "TimeWarp") {
				MessageDispatcher.SendMessage (this, "OnTimeWarpGoldUpgrade", numProfit [numbtn], 0);	
			} else if (nameString [numbtn] == "ClaimWithnoReset") {
				MessageDispatcher.SendMessage (this, "OnClaimWithNoReset", upgradeCost [numbtn], 0);	
			} else if (nameString [numbtn] == "Multiplier") {
				double[] tempDouble = new double[]{ numProfit [numbtn], upgradeCost [numbtn] };
				MessageDispatcher.SendMessage (this, "OnAddPermanentMultiplierShop", tempDouble, 0);		

			}
			MessageDispatcher.SendMessage(this, "OnTapGA_Report","BuyBrainsUpgradeHasGold", 0);
//		} 
//		else {
//			MessageDispatcher.SendMessage(this, "OnTapGA_Report","BuyBrainsUpgradeNoGold", 0);
//			MessageDispatcher.SendMessage (this, "PopButton", 2, 0f);
//
//		}
	}

}
