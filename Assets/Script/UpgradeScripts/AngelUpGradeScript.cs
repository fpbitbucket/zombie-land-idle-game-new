﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TMPro;
using com.ootii.Messages;
public class AngelUpGradeScript : MonoBehaviour {
	public MoneyManager MoneyManagerObj;
//	private double[] tempstring = new double[]{3,2,2,5,10,10,10,10,9,50,50,50,50,50,50,50,50,11,3,3,3,3,3,3,3,3,3,3,15,75,75,75,75,75,75,75,75,75,75,100,100,10,15,3,5,5,50,4,6,3,3,3,3,3,3,3,3,10,3,3,3,3,3,3,3,3,3,3,30,30,30,30,30,5,3,3,3,3,3,3,3,3,3,3,7,3,3,3,3,3,3,3,3,3,3,50,7.777777,10,10,10,10,10,10,10,10,10,10,3,3,3,3,3,3,3,3,3,3,25,25,25,25,25,25,25,25,25,25,3,3,3,3,3,3,3,3,3,3,13.11,5,3,4,5,25,25,25,25,25,25,25,25,25,25,3,3,3,3,3,3,3,3,3,3,3,25,25,25,25,25,25,25,25,25,25,3,3,3,3,3,3,3,3,3,3,19,25,25,25,25,25,25,25,25,25,25};

	public MainGeneratorScript mainGen;
//	public TextMeshProUGUI[] Text_TotalAngel;
	public string[] nameString;
	public string[] nameUpgradeString = {"Angel Sacrifice","Angelic Mutiny","Angelic Rebellion","Angelic Selection","Newspaper Swap","Car Wash Swap","Pizza Swap","Donut Swap","Divine Intervention","Newspaper Surge","Car Wash Surge","Pizza Surge","Donut Surge","Newspaper Merger","Car Wash Merger","Pizza Merger","Donut Merger","Rapture Contingent","Newspaperion Ultrus","Washicus Maximus","Pizzeria Primus","Ultimus Donuticus","Shrimpus Glorious","Puckus Alotus","Cinemaxima Spiritus","Fundorium Sanctum","Oil Vincit Omnia","Lemonus Supremus","Buy Earth","Newspaper Takeover","Car Wash Takeover","Pizza Takeover","Donut Takeover","Shrimp Takeover","Newspaper Assimilation","Car Wash Assimilation","Pizza Assimilation","Donut Assimilation","Shrimp Assimilation","First Amen-dment","Unrefusable Offer","Paradise Lost And Found","Black Friday the 13th","Divine Write Off","In Brightest Day...","In Darkest Night...","The Good News","Synergize Suds","Feta Beta-Testing","Pumpkin Spice","Cocktail Parties","Free Jerseys","In-Universe Continuity","Dolla Bills Ya'll","Anti-Solar Research","Divine Squeeze","Perfumed Pages","Hark!","Fold Into Hats","Free Breakfast","Little Nero's","Donut Isotopes","Son Of A Shrimp","Quack!","Rotten Potatoes","Money Bin Life Guards","Texas Tea","Lemonster","Recycling Boom","Redundant Facilities","No Competitors","Donut Boutiques","Hokey Honky Hockey","Sacred Trust Fund","Effective Marketing","Car Dirty-ers","Guaranteed Horse Free","Cartoon Endorsements","Government Subsidy","Union Busting","Hire Viewers","Sue Everything","Accident Free Week","Fund Favorable Studies","Hallelujah!","Heavenly News","Discounts For Investors","Ascended Pizza","Holy Donut Holes","Shrimp Saints","Angelic Refs","Angel Best Boys","Seraph Saturdays","The Cleansing Fires","Thirsty Cherubs","It Is Done","Profit-dence","Emergency Reserves","Bite The Cost","A Little Further","Desperate Measures","Impulse Buy","Seize Every Inch","Small Opportunity","Off Shore Refuge","Shell Corporations","Expensive","Manufacture The News","Freak Dust Storms","Famine Zone Delivery","Heritage Appeal","Onboard Orchestra","Pay Per View","Enough Parking Spots","Catapults","Large Hadron Pumps","Oktoberlemon","Propped Up","Aunty Idol","Pizza Clicker","Donut Box","Clicker Shrimps","Dark Hockey","Farley's Angels","Aggressive Negotiation","Worth It?","Lemon Scented Angels","Loyal Readers","Mud Enthusiasts","Gamers","Police Officers","Voracious Tourists","Canadian Angels","Movie Buffs","Up And Coming Capitalists","Off Roaders","Thirsty Neighbors","Proverbs","Pile Of Haloes","Last Lunch Special","Holy Guacamole","Low Sinterest Rates","Newspaper Dealing","Car Wash Dealing","Pizza Dealing","Donut Dealing","Shrimp Dealing","Hockey Dealing","Movie Dealing","Bank Dealing","Oil Dealing","Lemon Dealing","Ultra Mega Death Holiness","Angel-Sourced Ingredients","Blessed Fuzzy Dice","Holy Moly Guacamole","Heavenly Sprinkles","Tridents","Turn Water Into Teeth","Answered Movie Prayers","Security Guardians","Peppy Laborforce","Awe-Inspiring Toasts","Newspaper Transaction","Car Wash Transaction","Pizza Transaction","Donut Transaction","Shrimp Transaction","Hockey Transaction","Movie Transaction","Bank Transaction","Oil Transaction","Lemon Transaction","Venerated Bike Baskets","Venerated Hoses","Venerated Pizza Savers","Venerated Novelty Mugs","Venerated Rain Caps","Venerated Hockey Tape","Venerated Clackers","Venerated Spreadsheets","Venerated Sippy Cups","Venerated Lunch Boxes","Forever And Ever","Let There Be Clean","Let There Be Cocktails","Let There Be Games","Let There Be Cashflow","Let There Be Feasts","Let There Be Lemons","Let There Be Stories","Let There Be Pastries","Let There Be Classics","Let There Be Blood"};
	public string[] nameUpgradeStringDiscript = {"All profits x3","Angel Investor Effectiveness +2%","Angel Investor Effectiveness +2%","All profits x5","+10 Newspaper Delivery","+10 Car Wash","+10 Pizza Delivery","+10 Donut Shop","All profits x9","+50 Newspaper Delivery","+50 Car Wash","+50 Pizza Delivery","+50 Donut Shop","+50 Newspaper Delivery","+50 Car Wash","+50 Pizza Delivery","+50 Donut Shop","All profits x11","Newspaper Delivery profit x3","Car Wash profit x3","Pizza Delivery profit x3","Donut Shop profit x3","Shrimp Boat profit x3","Hockey Team profit x3","Movie Studio profit x3","Bank profit x3","Oil Company profit x3","Lemonade Stand profit x3","All profits x15","+75 Newspaper Delivery","+75 Car Wash","+75 Pizza Delivery","+75 Donut Shop","+75 Shrimp Boat","+75 Newspaper Delivery","+75 Car Wash","+75 Pizza Delivery","+75 Donut Shop","+75 Shrimp Boat","+100 Newspaper Delivery","+100 Car Wash","Angel Investor Effectiveness +10%","All profits x15","All profits x3","All profits x5","All profits x5","+50 Newspaper Delivery","Car Wash profit x4","Pizza Delivery profit x6","Donut Shop profit x3","Shrimp Boat profit x3","Hockey Team profit x3","Movie Studio profit x3","Bank profit x3","Oil Company profit x3","Lemonade Stand profit x3","Newspaper Delivery profit x3","Angel Investor Effectiveness +10%","Newspaper Delivery profit x3","Car Wash profit x3","Pizza Delivery profit x3","Donut Shop profit x3","Shrimp Boat profit x3","Hockey Team profit x3","Movie Studio profit x3","Bank profit x3","Oil Company profit x3","Lemonade Stand profit x3","+30 Newspaper Delivery","+30 Car Wash","+30 Pizza Delivery","+30 Donut Shop","+30 Hockey Team","All profits x5","Newspaper Delivery profit x3","Car Wash profit x3","Pizza Delivery profit x3","Donut Shop profit x3","Shrimp Boat profit x3","Hockey Team profit x3","Movie Studio profit x3","Bank profit x3","Oil Company profit x3","Lemonade Stand profit x3","All profits x7","Newspaper Delivery profit x3","Car Wash profit x3","Pizza Delivery profit x3","Donut Shop profit x3","Shrimp Boat profit x3","Hockey Team profit x3","Movie Studio profit x3","Bank profit x3","Oil Company profit x3","Lemonade Stand profit x3","+50 Newspaper Delivery","All profits x7.777777","+10 Newspaper Delivery","+10 Car Wash","+10 Pizza Delivery","+10 Donut Shop","+10 Shrimp Boat","+10 Hockey Team","+10 Movie Studio","+10 Bank","+10 Oil Company","+10 Lemonade Stand","Newspaper Delivery profit x3","Car Wash profit x3","Pizza Delivery profit x3","Donut Shop profit x3","Shrimp Boat profit x3","Hockey Team profit x3","Movie Studio profit x3","Bank profit x3","Oil Company profit x3","Lemonade Stand profit x3","+25 Car Wash","+25 Newspaper Delivery","+25 Pizza Delivery","+25 Donut Shop","+25 Shrimp Boat","+25 Hockey Team","+25 Movie Studio","+25 Bank","+25 Oil Company","+25 Lemonade Stand","Newspaper Delivery profit x3","Car Wash profit x3","Pizza Delivery profit x3","Donut Shop profit x3","Shrimp Boat profit x3","Hockey Team profit x3","Movie Studio profit x3","Bank profit x3","Oil Company profit x3","Lemonade Stand profit x3","All profits x13.11","All profits x5","All profits x3","All profits x4","All profits x5","+25 Newspaper Delivery","+25 Car Wash","+25 Pizza Delivery","+25 Donut Shop","+25 Shrimp Boat","+25 Hockey Team","+25 Movie Studio","+25 Bank","+25 Oil Company","+25 Lemonade Stand","All profits x3","Newspaper Delivery profit x3","Car Wash profit x3","Pizza Delivery profit x3","Donut Shop profit x3","Shrimp Boat profit x3","Hockey Team profit x3","Movie Studio profit x3","Bank profit x3","Oil Company profit x3","Lemonade Stand profit x3","+25 Newspaper Delivery","+25 Car Wash","+25 Pizza Delivery","+25 Donut Shop","+25 Shrimp Boat","+25 Hockey Team","+25 Movie Studio","+25 Bank","+25 Oil Company","+25 Lemonade Stand","Newspaper Delivery profit x3","Car Wash profit x3","Pizza Delivery profit x3","Donut Shop profit x3","Shrimp Boat profit x3","Hockey Team profit x3","Movie Studio profit x3","Bank profit x3","Lemonade Stand profit x3","Oil Company profit x3","All profits x19","+25 Car Wash","+25 Shrimp Boat","+25 Hockey Team","+25 Bank","+25 Pizza Delivery","+25 Lemonade Stand","+25 Newspaper Delivery","+25 Donut Shop","+25 Movie Studio","+25 Oil Company"};
	public double[] upgradeCost;
	public double[] numProfit;
	public Sprite[] btnIcon;
	public GameObject[] AngelUpgradeObjectPort;

	public GameObject[] angelSpenTpopUp;
	private int numUpgrade,NumToUpgrade;

//	private string[] tempStrings = new string[]{"All","All","Stocks","Bonds","Cars","Housing","Personal","Credit","Insurance","Debit","Savings","Space","All","Stocks","Bonds","Cars","Housing","Personal","Credit","Insurance","Debit","Savings","Space","All","All","Stocks","Bonds","Cars","Housing","Personal","Credit","Insurance","Debit","Savings","Space","All","Stocks","Bonds","Cars","Housing","Personal","Credit","Insurance","Debit","Savings","Space","All","All","Stocks","Bonds","Cars","Housing","Personal","Credit","Insurance","Debit","Savings","Space","All","All","Stocks","Bonds","Cars","Housing","Personal","Credit","Insurance","Debit","Savings","Space","All","All","Stocks","Bonds","Cars","Housing","Personal","Credit","Insurance","Debit","Savings","Space","All","All"};
//	private string[] tempStrings_1 = new string[]{"Help From Up There","More Help From Above","Holy Stocks","Holy Bonds","Car Loans","Holy Housing Loans","Conflict of Interest","Holy Credit Cards","Holy Insurance","Holy CameraCar Loans","Holy Savings","Space Program X","Double Up","Angelic Stocks Crew","Bonds Enthusiasts","Car Loans Consultants","Housing Loans Massagers","Personal Loans Guards","Little Credit Cards Servers","Disposable Insurance","Early Debit-ers","Savings Addict","Anti-Solar Research","Delivery Angels","Boosters Housing Loans","Stocks Agents","Bonds Cashiers","Car Loans Fanners","Housing Loans Pokes","If You Say So","Dignitary Credit Cardholders","Agents","Debit Installers","Savings Bait","Texas Tea","Wandering Agents","Stocks Quality Assurance","Bonds Injectors","Car Loands-lords","Housing Loans Milkers","ContainCar Loanst Units","Secret Agents","Insurance Fixers","Debit People","Savings Testers","Accident Free Week","Banker Angel, Will You Be Mine","Holy Banker-oni","Stocks On","Bonds Testers","Holy Cars","Housing Loans Washers","Personal Loans Galore","Debit Guards","Insurance Members","Debit Givers","Savings Catchers","The Cleansing Fires","Holy Synergy","Banker Rising","Helpful Hand","Burn Proof Workers","Got Your Back","Good With Animals","Still Not Comfortable","Secret Service","Insurance Idea","Debit Decorators","Baby Savings","Worth It?","Here To Save","That Glorious Hour","The Last Stocks Upgrade","The Last Bonds Upgrade","The Last Car Loans Upgrade","The Last Housing Loans Upgrade","The Last Personal Loans Upgrade","The Last Credit Card Upgrade","The Last Insurance Upgrade","The Last Debit Upgrade","The Last Savings Upgrade","Off Roaders","The End In Sight","Praise Bankers"};
//	private string[] tempStrings_2 = new string[]{"All profits x3","All profits x3","Stocks profit x5","Bonds profit x5","Car Loans profit x5","Housing Loans profit x5","Personal Loans profit x5","Credit Cards profit x5","Insurance profit x5","Debit profit x5","Savings profit x5","Space Program Profit x5","All profits x3","Stocks profit x7","Bonds profit x7","Car Loans profit x7","Housing Loans profit x7","Personal Loans profit x7","Credit Cards profit x7","Insurance profit x7","Debit profit x7","Savings profit x7","Space Program Profit x7","All profits x5","All profits x3","Stocks profit x3","Bonds profit x3","Car Loans profit x3","Housing Loans profit x3","Personal Loans profit x3","Credit Cards profit x3","Insurance profit x3","Debit profit x3","Savings profit x3","Space Program Profit x3","All profits x5","Stocks profit x5","Bonds profit x5","Car Loans profit x5","Housing Loans profit x5","Personal Loans profit x5","Credit Cards profit x5","Insurance profit x5","Debit profit x5","Savings profit x5","Space Program Profit x5","All profits x5","All profits x7","Stocks profit x9","Bonds profit x9","Car Loans profit x9","Housing Loans profit x9","Personal Loans profit x9","Credit Cards profit x9","Insurance profit x9","Debit profit x9","Savings profit x9","Space Program Profit x9","All profits x9","All profits x9","Stocks profit x15","Bonds profit x15","Car Loans profit x15","Housing Loans profit x15","Personal Loans profit x15","Credit Cards profit x15","Insurance profit x15","Debit profit x15","Savings profit x15","Space Program Profit x15","All profits x15","All profits x9","Stocks profit x21","Bonds profit x21","Car Loans profit x21","Housing Loans profit x21","Personal Loans profit x21","Credit Cards profit x21","Insurance profit x21","Debit profit x21","Savings profit x21","Space Program Profit x21","All profits x9","All profits x777"};
//	private double[] tempNumCost = new double[]{100,100,1,2,4,8,16,32,64,128,256,400,1,1,3,9,27,100,200,400,600,900,10,1,1,3,12,29,136,311,555,789,25,100,500,100,1,5,45,66,99,175,280,420,700,900,5,100,1,10,20,100,200,400,800,16,222,333,666,1,2,14,56,112,179,298,434,620,808,889,1,9,34,6,12,24,69,105,214,333,500,800,1,777};
//	private double[] tempNunProf = new double[]{3,3,5,5,5,5,5,5,5,5,5,5,3,7,7,7,7,7,7,7,7,7,7,5,3,3,3,3,3,3,3,3,3,3,3,5,5,5,5,5,5,5,5,5,5,5,5,7,9,9,9,9,9,9,9,9,9,9,9,9,15,15,15,15,15,15,15,15,15,15,15,9,21,21,21,21,21,21,21,21,21,21,9,777};


//
//	private string[] tempString_1 = new string[]{"all profits x3","Beach Drinks profit x3","Pool Cabana profit x3","Scuba Lessons profit x3","Parasailing profit x3","Sunset Restaurant profit x3","Island Day Tours profit x3","Private Boating profit x3","Beach Hotel profit x3","Resort Chain profit x3","Airline profit x3","All profits x3","10 more Pool Cabana","10 more Parasailing","10 more Island Day Tours","10 more Beach Hotel","10 more Airline","Beach Drinks profit x3","Pool Cabana profit x3","Scuba Lessons profit x3","Parasailing profit x3","Sunset Restaurant profit x3","Island Day Tours profit x3","Private Boating profit x3","Beach Hotel profit x3","Resort Chain profit x3","Airline profit x3","all profits x3","10 more Pool Cabana","10 more Parasailing","10 more Island Day Tours","10 more Beach Hotel","10 more Airline","Beach Drinks profit x2","Pool Cabana profit x2","Scuba Lessons profit x2","Parasailing profit x2","Sunset Restaurant profit x2","Island Day Tours profit x2","Private Boating profit x2","Beach Hotel profit x2","Resort Chain profit x2","Airline profit x2","all profits x3","10 more Pool Cabana","10 more Parasailing","10 more Island Day Tours","10 more Beach Hotel","10 more Airline","Beach Drinks profit x3","Pool Cabana profit x3","Scuba Lessons profit x3","Parasailing profit x3","Sunset Restaurant profit x3","Island Day Tours profit x3","Private Boating profit x3","Beach Hotel profit x3","Resort Chain profit x3","Airline profit x3","all profits x3","10 more Pool Cabana","10 more Parasailing","10 more Island Day Tours","10 more Beach Hotel","10 more Airline","Beach Drinks profit x5","Pool Cabana profit x5","Scuba Lessons profit x5","Parasailing profit x5","Sunset Restaurant profit x5","Island Day Tours profit x5","Private Boating profit x5","Beach Hotel profit x5","Resort Chain profit x5","Airline profit x5","all profits x9","all profits x9","50 more Beach Drinks","50 more Pool Cabana","50 more Scuba Lessons","50 more Parasailing","50 More Sunset Restaurant","50 more Island Day Tours","50 More Private Boating","50 More Beach Hotel","50 More Werewolf Colonies","50 more Airline","Beach Drinks profit x3","Pool Cabana profit x3","Scuba Lessons profit x3","Parasailing profit x3","Sunset Restaurant profit x3","Island Day Tours profit x3","Private Boating profit x3","Beach Hotel profit x3","Resort Chain profit x3","Airline profit x3","all profits x3","75 more","75 more","75 more","75 more","75 more","75 more","75 more","75 more","75 more","75 more","Beach Drinks profit x3","Pool Cabana profit x3","Scuba Lessons profit x3","Parasailing profit x3","Sunset Restaurant profit x3","Island Day Tours profit x3","Private Boating profit x3","Beach Hotel profit x3","Resort Chain profit x3","Airline profit x3","all profits x3","50 more Pool Cabana","50 more","100 more","100 more","25 more","Beach Drinks profit x7","Pool Cabana profit x7","Scuba Lessons profit x7","Parasailing profit x7","Sunset Restaurant profit x7","Island Day Tours profit x7 ","Private Boating profit x7","Beach Hotel profit x7","Resort Chain profit x7","Airline profit x7","all profits x7","100 more Parasailing","200 more Sunset Restaurant","300 more Private Boating","Beach Drinks profit x2","Pool Cabana profit x2","Scuba Lessons profit x2","Parasailing profit x2","Sunset Restaurant profit x2","Island Day Tours profit x2 ","Private Boating profit x2","Beach Hotel profit x2","Resort Chain profit x2","Airline profit x2","All profits x5"};

	public Color availToBuy, notAvailToBuy;
	private int numStartCount;
//	mainGen.string

	private bool OnStarChecking = false;
	private int[] ArrayTocheck;
	private int tempNumToQuickBuy = 0;
	bool OnQuickUpgrade = false;
	void OnEnable(){
		MessageDispatcher.AddListener ("OnRestUpgradesAngel", OnRestUpgrades, true); 
		MessageDispatcher.AddListener ("OnResetAllDataAngle", OnResetAllDataAngle, true); 
//		nameUpgradeStringDiscript = tempStrings_2;
//		nameUpgradeString = tempStrings_1;
//		nameString = tempStrings;
//		numProfit = tempNunProf;
//		upgradeCost = tempNumCost;

//		nameUpgradeStringDiscript = tempString_1;
//		for (int i = 0; i < tempNunProf.Length; i++) {
//			nameString [i] = tempStrings [i];
//			nameUpgradeStringDiscript [i] = tempString_1 [i];
//			upgradeCost[i] = tempNunProf[i];
//		}
//
		for (int i = 0; i < angelSpenTpopUp.Length; i++) {
			angelSpenTpopUp [i].SetActive (false);
		}
		OnStartSetup ();
		MessageDispatcher.SendMessage(this, "OnRestUpgradesAngel",0, 0);


//		mainGen

//		for (int i = 0; i < numProfit.Length; i++) {
//			if (nameString [i] == "Miner") {
//				nameString [i] = mainGen.genNames_Code[i];
//			}else if (nameString [i] == "ETH") {
//				nameString [i] = mainGen.genNames_Code[i];
//			}else if (nameString [i] == "DodgeCoin") {
//				nameString [i] = mainGen.genNames_Code[i];
//			}else if (nameString [i] == "BTCCash") {
//				nameString [i] = mainGen.genNames_Code[i];
//			}else if (nameString [i] == "LiteCoin") {
//				nameString [i] = mainGen.genNames_Code[i];
//			}else if (nameString [i] == "Ripple") {
//				nameString [i] = mainGen.genNames_Code[i];
//			}else if (nameString [i] == "NEO") {
//				nameString [i] = mainGen.genNames_Code[i];
//			}else if (nameString [i] == "SmartContracts") {
//				nameString [i] = mainGen.genNames_Code[i];
//			}else if (nameString [i] == "BlockChain") {
//				nameString [i] = mainGen.genNames_Code[i];
//			}else if (nameString [i] == "ICO") {
//				nameString [i] = mainGen.genNames_Code[i];
//			}else if (nameString [i] == "Miner_Level") {
//				nameString [i] = mainGen.genNames_Code[i]+"_Level";
//			}else if (nameString [i] == "ETH_Level") {
//				nameString [i] = mainGen.genNames_Code[i]+"_Level";
//			}else if (nameString [i] == "DodgeCoin_Level") {
//				nameString [i] = mainGen.genNames_Code[i]+"_Level";
//			}else if (nameString [i] == "BTCCash_Level") {
//				nameString [i] = mainGen.genNames_Code[i]+"_Level";
//			}else if (nameString [i] == "LiteCoin_Level") {
//				nameString [i] = mainGen.genNames_Code[i]+"_Level";
//			}else if (nameString [i] == "Ripple_Level") {
//				nameString [i] = mainGen.genNames_Code[i]+"_Level";
//			}else if (nameString [i] == "NEO_Level") {
//				nameString [i] = mainGen.genNames_Code[i]+"_Level";
//			}else if (nameString [i] == "SmartContracts_Level") {
//				nameString [i] = mainGen.genNames_Code[i]+"_Level";
//			}else if (nameString [i] == "BlockChain_Level") {
//				nameString [i] = mainGen.genNames_Code[i]+"_Level";
//			}else if (nameString [i] == "ICO_Level") {
//				nameString [i] = mainGen.genNames_Code[i]+"_Level";
//			}
//		}
	}

	void OnDisable(){
		OnStarChecking = false;
		MessageDispatcher.RemoveListener ("OnRestUpgradesAngel", OnRestUpgrades, true);
		MessageDispatcher.RemoveListener ("OnResetAllDataAngle", OnResetAllDataAngle, true);
	}

	void OnResetAllDataAngle (IMessage rMessage){
		numUpgrade = 0;
		if (ES2.Exists ("TotalAngelUnlock_" + MoneyManagerObj.worldName)) {
			ES2.Save(numUpgrade, "TotalAngelUnlock_" + MoneyManagerObj.worldName);
		} 
		for (int i = 0; i < AngelUpgradeObjectPort.Length; i++) {
			if (ES2.Exists ("AngleUpgrades_"+nameString [i] + "-upNum:"+i + "_" + MoneyManagerObj.worldName)) {
				ES2.Delete("AngleUpgrades_"+nameString [i] + "-upNum:"+i + "_" + MoneyManagerObj.worldName);
			}
			AngelUpgradeObjectPort[i].transform.Find ("ItemBtn").GetComponent<Button>().onClick.RemoveAllListeners();

		}

		OnStartSetup ();
		MessageDispatcher.SendMessage(this, "OnRestUpgradesAngel",0, 0);	
	}

	void OnRestUpgrades (IMessage rMessage){

		int numToActive = 0;
		ArrayTocheck = new int[10];//10
		for (int i = 0; i < AngelUpgradeObjectPort.Length; i++) {
			if (ES2.Exists ("AngleUpgrades_"+nameString [i] + "-upNum:"+i + "_" + MoneyManagerObj.worldName)) {
				AngelUpgradeObjectPort [i].SetActive (false);
			} else {
				if (numToActive < 10) {
					ArrayTocheck [numToActive] = i;
					numToActive++;
					AngelUpgradeObjectPort [i].SetActive (true);
				} else {
					AngelUpgradeObjectPort [i].SetActive (false);
				}
			}
		}
		OnStarChecking = true;
		StartCoroutine (OnCheckBuy ());
	}
	private void OnStartSetup(){
		
		if (ES2.Exists ("TotalAngelUnlock_" + MoneyManagerObj.worldName)) {
			numUpgrade = ES2.Load<int> ("TotalAngelUnlock_" + MoneyManagerObj.worldName);
		} else {
			numUpgrade = 0;
			ES2.Save(numUpgrade, "TotalAngelUnlock_" + MoneyManagerObj.worldName);
		}

		for (int i = 0; i < AngelUpgradeObjectPort.Length; i++) {
			int tempInt = i;
			AngelUpgradeObjectPort [i].transform.Find ("ItemBgGrp/Icon").GetComponent<Image> ().sprite = btnIcon[10];
			AngelUpgradeObjectPort[i].transform.Find ("ItemBtn").GetComponent<Button>().onClick.AddListener(() => { OnUpProfit(tempInt); });

			if (ES2.Exists ("AngleUpgrades_"+nameString [i] + "-upNum:"+i + "_" + MoneyManagerObj.worldName)) {
				numStartCount++;
			}

			AngelUpgradeObjectPort [i].transform.Find ("ItemBgGrp/Title_Text").GetComponent<TextMeshProUGUI> ().text = nameUpgradeString[i]; //Name of Upgrade
			AngelUpgradeObjectPort [i].transform.Find ("ItemBgGrp/Discript_Text").GetComponent<TextMeshProUGUI> ().text = nameUpgradeStringDiscript [i] +" <#fff200>"; //Amount of Upgrade
			AngelUpgradeObjectPort [i].transform.Find ("ItemBgGrp/Amount_Text").GetComponent<TextMeshProUGUI> ().text = MoneyManagerObj.GetStringForValueNumbers(upgradeCost[i]) + "</color> Zombie Idols";
			AngelUpgradeObjectPort [i].transform.Find ("ItemBtn").GetComponent<Image> ().color = notAvailToBuy;
		}



	}

	IEnumerator OnCheckBuy(){
		

		while (OnStarChecking) {
			yield return new WaitForSeconds (1f);
			double TotalAngel = 0;
			if (ES2.Exists ("TotalAngel_" + MoneyManagerObj.worldName)) {
				TotalAngel = ES2.Load<double> ("TotalAngel_" + MoneyManagerObj.worldName);
			} else {
				TotalAngel = 0;
			}

			//			for (int l = 0; l < Text_TotalAngel.Length; l++) {
			//				Text_TotalAngel[l].text = "Total Programmers " + MoneyManagerObj.GetStringForValueNumbers(TotalAngel);
			//			}

			int numTotalManager = ArrayTocheck.Length - 1;
			bool checker = false;
			double numCounterAvailBuy = 0;
			tempNumToQuickBuy = 0;
			for (int l = 0; l < ArrayTocheck.Length; l++) {//for (int l = 0; l < AngelUpgradeObject.Length; l++) {//
				int i = ArrayTocheck [l];
				if (TotalAngel >= upgradeCost[i]) {
					numCounterAvailBuy += upgradeCost [i];
					tempNumToQuickBuy++;
					checker = true;
					if (AngelUpgradeObjectPort [i].transform.Find ("ItemBtn").GetComponent<Image> ().color == notAvailToBuy && AngelUpgradeObjectPort [i].activeSelf == true) {
						MessageDispatcher.SendMessage(this, "OnTriggerMenuNotifyOn",12, 0);
					}
					AngelUpgradeObjectPort [i].transform.Find ("ItemBtn").GetComponent<Image> ().color = availToBuy;

				} else {
					AngelUpgradeObjectPort [i].transform.Find ("ItemBtn").GetComponent<Image> ().color = notAvailToBuy;

				}
			}
			if (ES2.Exists ("OnShowQuickBuyUpgrades_" + MoneyManagerObj.worldName)) {
				if (tempNumToQuickBuy >= 1) {
					double[] numSend = new double[]{ 1, numCounterAvailBuy };
					MessageDispatcher.SendMessage (this, "OnChangeStatusBuy_Angle", numSend, 0);
				} else {
					double[] numSend = new double[]{ 1, numCounterAvailBuy };
					MessageDispatcher.SendMessage (this, "OnChangeStatusBuy_Angle", numSend, 0);
				}
			}

			if (checker == false) {
				MessageDispatcher.SendMessage(this, "OnTriggerMenuNotifyOff",12, 0);
			}

			yield return OnStarChecking;
		}
	}

	public void OnAcceptSpendProgrammer(){
		int numbtn = NumToUpgrade;
		MessageDispatcher.SendMessage(this, "OnTapGA_Report","BuyZombieIdolsUpgradeHasIdols", 0);
		AngelUpgradeObjectPort[numbtn].SetActive (false);

		ES2.Save(1, "AngleUpgrades_"+nameString [numbtn] + "-upNum:"+numbtn + "_" + MoneyManagerObj.worldName);
		numUpgrade++;

		numStartCount++;
		MessageDispatcher.SendMessage(this, "OnRestUpgradesAngel",0, 0);	
		ES2.Save(numUpgrade, "TotalAngelUnlock_" + MoneyManagerObj.worldName);
		MessageDispatcher.SendMessage(this, "OnDicrementAngel",upgradeCost[numbtn], 0);	

		if (nameString [numbtn] == "All") {
			for (int i = 0; i < mainGen.genNames_Code.Length; i++) {
				MessageDispatcher.SendMessage (this, "OnAngelMultiplier"+mainGen.genNames_Code[i], numProfit [numbtn], 0);
			}
		}else if (nameString [numbtn] == "Investor") {
			MessageDispatcher.SendMessage (this, "OnUpgradePerBonus",numProfit [numbtn], 0);
//		}else if (nameString [numbtn].Substring(nameString [numbtn].Length - 6) == "_Level" ) {
//			MessageDispatcher.SendMessage (this, "OnLevelUpgrade_"+nameString [numbtn],numProfit [numbtn], 0);
		} else {
			bool checkforLevel = false;
			for (int i = 0; i < mainGen.genNames_Code.Length; i++) {
				if (nameString [numbtn] == mainGen.genNames_Code [i] + "_Level") {
					checkforLevel = true;
					break;
				}
			}
			if (checkforLevel) {
				MessageDispatcher.SendMessage (this, "OnLevelUpgrade_"+nameString [numbtn],numProfit [numbtn], 0);
			} else {
				MessageDispatcher.SendMessage (this, "OnAngelMultiplier" + nameString [numbtn], numProfit [numbtn], 0);
			}

		}	

		OnCancelSpendProgrammer ();
	}

	public void OnCancelSpendProgrammer(){
		for (int i = 0; i < angelSpenTpopUp.Length; i++) {
			angelSpenTpopUp [i].SetActive (false);
		}
	}

	void OnShowPopupProgrammerSpend(){
		for (int i = 0; i < angelSpenTpopUp.Length; i++) {
			angelSpenTpopUp [i].SetActive (true);
			string messageString = " Are you sure you want to spend <#fff200>"+MoneyManagerObj.GetStringForValueNumbers(upgradeCost[NumToUpgrade])+"</color> Zombie Idols for this upgrade?";
			angelSpenTpopUp [i].transform.Find ("BG/Text03").gameObject.GetComponent<TextMeshProUGUI> ().text = messageString;
		}
	}

	public void OnUpProfit(int numbtn){
		double TotalAngel = 0;
		if (ES2.Exists ("TotalAngel_" + MoneyManagerObj.worldName)) {
			TotalAngel = ES2.Load<double> ("TotalAngel_" + MoneyManagerObj.worldName);
		} else {
			TotalAngel = 0;
		}
		if (TotalAngel - upgradeCost [numbtn] >= 0) {
			MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
			NumToUpgrade = numbtn;
			Debug.Log (TotalAngel + " " + upgradeCost [numbtn]);
			OnShowPopupProgrammerSpend ();
		} else {
			MessageDispatcher.SendMessage(this, "OnTapGA_Report","BuyZombieIdolsUpgradeNoIdols", 0);
			MessageDispatcher.SendMessage (this, "PopButton", 2, 0f);
		
		}
//		
	}

	public void OnBuyNumberToUpgrade(){
		OnQuickUpgrade = true;
		MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
		for (int i = 0; i < tempNumToQuickBuy; i++) {
//			OnUpProfit (ArrayTocheck [i]);
			NumToUpgrade = ArrayTocheck [i];
			OnAcceptSpendProgrammer ();
		}
	}
}
