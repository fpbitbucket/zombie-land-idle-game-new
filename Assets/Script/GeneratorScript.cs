﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using com.ootii.Messages;
public class GeneratorScript : MonoBehaviour {
	public GameObject GeneratorObj;
	private GameObject RoomLight;

	public MainGeneratorScript mainGen;
	public int num_generator = 0;
	public UnlockSript getNumLevels;
	public MoneyManager getMoneyUnit;
	private GameObject fillBarAnimPort;
	private Transform fillbarPort;
	private GameObject lockObjPort;
	private GameObject fillLevelPort;
	private GameObject upBtnPort;
	private Button buyBtnPort;
//	private Image IconnBgPort;
	public string nameString; // name of generator
	public float basicTime; // time for progress bar
	private TextMeshProUGUI textMultiplierPort;
	private TextMeshProUGUI textTimePort;
	private TextMeshProUGUI startAmountTextPort;
	private TextMeshProUGUI itemAmountTextPort;
	private TextMeshProUGUI levelTextPort;
	private TextMeshProUGUI upTextAmountPort;
	private TextMeshProUGUI card_nameLandPort;
	private TextMeshProUGUI gen_titleNamePort;

	public double basicCost = 1; //Money Earning
	public double percentIncrease;
	public double upgradeAmount = 0; //Money Upgrade
	public int levelItem = 1; // Level of generator
	public bool isActiveGen = false; //active Generator

	private double xTimesAmount = 1; // multiplier

//	public Color colorAvail; // color of  the button for unlocked generator

//	public Color iconColor; // color of the icon bg button for buy
//	public Color iconColor_Buying; // color of the icon bg button for buying
	public Color readycolorBuy; // color of the bg button for upgrade generator
	public Color OnDisableCardColor = Color.gray;
	private bool timerStart = false; // bool for time to start for text timer

	private float timeleftOffline = 0f;

	private float origTime; // a original time for basic time use for calculator double speed upgrade


	private double mainMultiplier = 1;
	private double startTingPercentage = 1;
	float tempScaler = 0;


	private int Orig_Level;
	private float Orig_basicTime;
	private double Orig_basicCost;
	private double Orig_percentIncrease;
	private double Orig_upgradeAmount;
	private bool Orig_isActiveGen;

	private double unlockMultiplierOfProft = 1;
	private double upgradeMultipliers  = 1;

	private double adMultipliers = 1;
	private double angelMultiplier  = 1;
	private double permanentMultiplier = 1;
	private double explorationMultiplier = 0;
	private bool OnStartGame = false;
	private bool OnSuspend = true;

	public bool hasManagerGen = false;
	public bool hasAccountantGen = false;
	public bool OnStartCheck = false;
	public double earningsPerCycle;
	public float tempTime; // a temporary time for basic time for reset cycle
	int levelTemp_LeveUp;
	double upgrade_AmountBuy =0;
	protected AnimatorOverrideController animatorOverrideController;
	public double TotalMultiplierAdditive = 0;
	bool AdvisortUpgrade = false;
	void OnEnable(){
		nameString = mainGen.genNames_Code [num_generator];
//		buyBtnPort.gameObject.transform.Find("Icon_Anim").GetComponent<Image>().sprite = mainGen.genIcons[num_generator];

		GeneratorObj.SetActive (true);
		fillBarAnimPort = GeneratorObj.transform.GetChild (4).gameObject.transform.Find ("FILL-B").gameObject;
		fillbarPort = GeneratorObj.transform.GetChild (4).gameObject.transform.Find ("FILL-A").gameObject.transform;
		itemAmountTextPort = GeneratorObj.transform.GetChild (4).gameObject.transform.Find ("Text").GetComponent<TextMeshProUGUI>();

		lockObjPort = GeneratorObj.transform.GetChild (7).gameObject;
		upBtnPort = GeneratorObj.transform.GetChild (3).gameObject;

		upBtnPort.GetComponent<Button>().onClick.AddListener(OnUpgradeEarningCyle);

		lockObjPort.transform.Find("UnlockBtn").GetComponent<Button> ().onClick.AddListener(OnUnlockItem);

		textTimePort = GeneratorObj.transform.GetChild (2).gameObject.transform.Find ("Text").GetComponent<TextMeshProUGUI>();

		upTextAmountPort = upBtnPort.transform.Find ("Text").GetComponent<TextMeshProUGUI>();

		levelTextPort = GeneratorObj.transform.GetChild (5).gameObject.transform.Find ("Level_Text").GetComponent<TextMeshProUGUI>();
		fillLevelPort = GeneratorObj.transform.GetChild (5).gameObject.transform.Find ("FillMask/Fill").gameObject;

		startAmountTextPort = lockObjPort.transform.Find("Unlock_Text").GetComponent<TextMeshProUGUI>();

		textMultiplierPort  = GeneratorObj.transform.GetChild (6).gameObject.transform.Find ("multiplier_text").GetComponent<TextMeshProUGUI>();

		gen_titleNamePort = GeneratorObj.transform.GetChild (0).gameObject.transform.Find ("Text").GetComponent<TextMeshProUGUI>();
		card_nameLandPort = lockObjPort.transform.Find("Text").GetComponent<TextMeshProUGUI>().GetComponent<TextMeshProUGUI>();
		RoomLight = GeneratorObj.transform.GetChild (1).gameObject.transform.Find ("LightRoom").gameObject;
//		
//		if (buyBtn.gameObject.transform.Find ("Icon_Anim")) {
//			Debug.Log (num_generator);
//			buyBtn.gameObject.transform.Find ("Icon_Anim").GetComponent<Animator> ().runtimeAnimatorController = mainGen.genAnims [num_generator] as RuntimeAnimatorController;
//		}
//
//		if (buyBtnPort.gameObject.transform.Find ("Icon_Anim")) {
//			buyBtnPort.gameObject.transform.Find ("Icon_Anim").GetComponent<Animator> ().runtimeAnimatorController = mainGen.genAnims [num_generator] as RuntimeAnimatorController;
//		}

//		
//		lockObjPort.transform.Find("UnlockBtn").GetComponent<Button> ().enabled = true;
//		lockObjPort.transform.Find("UnlockBtn").GetComponent<Image>().color = readycolorBuy;
		OnStartCheck = true;
		gen_titleNamePort.text = mainGen.genNamesString [num_generator];
		card_nameLandPort.text = mainGen.genNamesString [num_generator];

		buyBtnPort = GeneratorObj.GetComponent<Button> ();
		buyBtnPort.GetComponent<Button>().onClick.AddListener(OnBuy);

//		IconnBgPort.color = iconColor;
		Orig_Level = levelItem;
		Orig_basicCost = basicCost;
		Orig_basicTime = basicTime;
		Orig_percentIncrease = percentIncrease;
		Orig_upgradeAmount = upgradeAmount;
		Orig_isActiveGen = isActiveGen;
		tempTime = basicTime;
		timeleftOffline = 0f;
		origTime = basicTime;
		fillbarPort.GetComponent<Image> ().fillAmount = 0;
		fillBarAnimPort.SetActive (false);

		MessageDispatcher.AddListener ("OnUpdateMultiplier", OnUpdateMultiplier, true); //update multipeler for upgrade
		MessageDispatcher.AddListener ("OnHireManager" + nameString, OnHireManager, true); // function for hire manager and upgrade profit
		MessageDispatcher.AddListener ("OnHireAccountant" + nameString, OnHireAccountant, true); // function for hire Accountant and upgrade profit
		MessageDispatcher.AddListener ("On_UpdateSpeed" + nameString, OnDoubleSpeedEarning, true); // function for double speed for unlocks
		MessageDispatcher.AddListener ("On_UnlockedMultiplier_" + nameString, OnunLockUpgradeProfitEarining, true); //update amount earning from Unlocked
		MessageDispatcher.AddListener ("OnResetAllGenerator", OnResetAllGenerator,true);
		MessageDispatcher.AddListener ("OnSetAdMultiplier", OnSetAdMultiplier,true);
		MessageDispatcher.AddListener ("OnSetExplorationMultiplier", OnSetExplorationMultiplier,true);
		MessageDispatcher.AddListener ("OnUpgradeMultiplier" + nameString, OnSetUpgradeMultiplier,true);
		MessageDispatcher.AddListener ("OnAngelMultiplier" + nameString, OnAngelMultiplier,true);
		MessageDispatcher.AddListener ("OnPermanentMultiplier", OnPermanentMultiplier,true);
		MessageDispatcher.AddListener ("OnLevelUpgrade_" + nameString + "_Level", OnLevelUpgrade,true);
		MessageDispatcher.AddListener ("OnRefreshGenerator", OnRefreshGenerator, true);
		MessageDispatcher.AddListener ("OnRefreshGeneratorMoney", OnRefreshGeneratorMoney, true);


		MessageDispatcher.AddListener ("OnUnlocked_Button_" + nameString, OnUnlockedButton, true);
		MessageDispatcher.AddListener ("OnUpgrade_Button_" + nameString, OnUpgradeButton, true);
		MessageDispatcher.AddListener ("OnBuy_Button_" + nameString, OnBuyButton, true);

		MessageDispatcher.AddListener ("OnAdvisorUpgrade" + nameString, OnAdvisorUpgrade, true);
		MessageDispatcher.AddListener ("OnAdvisorUnlock" + nameString, OnAdvisorUnlock, true);

		OnSetupGenerators ();
	}

//	void OnEnable(){
//		
//	}

	void OnDisable(){
		OnStartCheck = false;
		MessageDispatcher.RemoveListener ("OnUpdateMultiplier", OnUpdateMultiplier, true); //update multipeler for upgrade
		MessageDispatcher.RemoveListener ("OnHireManager" + nameString, OnHireManager, true); // function for hire manager and upgrade profit
		MessageDispatcher.RemoveListener ("OnHireAccountant" + nameString, OnHireAccountant, true); // function for hire Accountant and upgrade profit
		MessageDispatcher.RemoveListener ("On_UpdateSpeed" + nameString, OnDoubleSpeedEarning, true); // function for double speed for unlocks
		MessageDispatcher.RemoveListener ("On_UnlockedMultiplier_" + nameString, OnunLockUpgradeProfitEarining, true); //update amount earning from Unlocked
		MessageDispatcher.RemoveListener ("OnResetAllGenerator", OnResetAllGenerator,true);
		MessageDispatcher.RemoveListener ("OnSetAdMultiplier", OnSetAdMultiplier,true);
		MessageDispatcher.RemoveListener ("OnSetExplorationMultiplier", OnSetExplorationMultiplier,true);
		MessageDispatcher.RemoveListener ("OnUpgradeMultiplier" + nameString, OnSetUpgradeMultiplier,true);
		MessageDispatcher.RemoveListener ("OnAngelMultiplier" + nameString, OnAngelMultiplier,true);
		MessageDispatcher.RemoveListener ("OnPermanentMultiplier", OnPermanentMultiplier,true);
		MessageDispatcher.RemoveListener ("OnLevelUpgrade_" + nameString + "_Level", OnLevelUpgrade,true);
		MessageDispatcher.RemoveListener ("OnRefreshGenerator", OnRefreshGenerator, true);
		MessageDispatcher.RemoveListener ("OnRefreshGeneratorMoney", OnRefreshGeneratorMoney, true);


		MessageDispatcher.RemoveListener ("OnUnlocked_Button_" + nameString, OnUnlockedButton, true);
		MessageDispatcher.RemoveListener ("OnUpgrade_Button_" + nameString, OnUpgradeButton, true);
		MessageDispatcher.RemoveListener ("OnBuy_Button_" + nameString, OnBuyButton, true);

		MessageDispatcher.RemoveListener ("OnAdvisorUpgrade" + nameString, OnAdvisorUpgrade, true);
		MessageDispatcher.RemoveListener ("OnAdvisorUnlock" + nameString, OnAdvisorUnlock, true);
	}

	void OnSetExplorationMultiplier(IMessage rMessage){
		explorationMultiplier = (double)rMessage.Data;
		OnEarningFunction ("Update");
	}

	void OnRefreshGeneratorMoney(IMessage rMessage){
		OnEarningFunction ("Update");
	}

	void OnRefreshGenerator(IMessage rMessage){
		OnSetupGenerators ();
	}

	void OnLevelUpgrade(IMessage rMessage){
		double numUpMultiplier = (double)rMessage.Data;
		levelItem = levelItem + (int)numUpMultiplier;
		ES2.Save(levelItem, "btnName_" + nameString+"?tag=Level");
		levelTextPort.text = levelItem.ToString();
		OnEarningFunction ("Update");
	}

	void OnPermanentMultiplier(IMessage rMessage){
		double numUpMultiplier = (double)rMessage.Data;
		if (permanentMultiplier == 1) { permanentMultiplier = 0; }
		permanentMultiplier = permanentMultiplier + numUpMultiplier;
		ES2.Save(permanentMultiplier, "btnName_" + nameString+"?tag=PermanentMultiplier");
		OnEarningFunction ("Update");
	}

	void OnAngelMultiplier(IMessage rMessage){
		double numUpMultiplier = (double)rMessage.Data;
//		print ("send Multiplier" + numUpMultiplier);
		angelMultiplier = angelMultiplier * numUpMultiplier;
		ES2.Save(angelMultiplier, "btnName_" + nameString+"?tag=angelMultiplier");
		OnEarningFunction ("Update");
	}

	void OnSetUpgradeMultiplier(IMessage rMessage){
		int numUpMultiplier = (int)rMessage.Data;

		upgradeMultipliers = upgradeMultipliers * numUpMultiplier;
		ES2.Save(upgradeMultipliers, "btnName_" + nameString+"?tag=UpgradeprofitMultiplier");
		OnEarningFunction ("Update");
	}

	void OnSetAdMultiplier(IMessage rMessage){ 
		adMultipliers = (double)rMessage.Data;
//		Debug.Log ("setup ad multiplier " + adMultipliers + " " + nameString);
//		if (activeAdMultiplier) {
//			adMultipliers = 2;
//		} else {
//			adMultipliers = 0;
//		}
		OnEarningFunction ("update");

	}

	private void OnResetAllGenerator(IMessage rMessage){ 
		double numInvestor = (double)rMessage.Data; //percentage
		timerStart = false;
		hasManagerGen = false;
		hasAccountantGen = false;
		levelItem = Orig_Level;
		Orig_basicCost = Orig_basicCost + (Orig_basicCost*numInvestor);
		basicCost = Orig_basicCost;
		buyBtnPort.enabled = true;
		ES2.Save(basicCost, "btnName_" + nameString+"?tag=Amount");
		ES2.Save(hasManagerGen, "btnName_" + nameString+"?tag=HasManager");
		ES2.Save(hasAccountantGen, "btnName_" + nameString+"?tag=HasAccountant");
		basicTime = Orig_basicTime;
		tempTime = Orig_basicTime;
		ES2.Save(basicTime, "btnName_" + nameString+"?tag=progressTime");
		ES2.Save(levelItem, "btnName_" + nameString+"?tag=Level");
		percentIncrease = Orig_percentIncrease;
		upgradeAmount = Orig_upgradeAmount;
		isActiveGen = Orig_isActiveGen;

		unlockMultiplierOfProft = 1;
		upgradeMultipliers  = 1;
		angelMultiplier  = 1;
		if (num_generator == 0) {
			Orig_isActiveGen = true;
		} else {
			Orig_isActiveGen = false;
		}
		ES2.Delete("btnName_" + nameString);
		OnSetupGenerators ();
	}

	void OnSetupGenerators(){ //setup Generator data 
		
		System.TimeSpan t = System.TimeSpan.FromSeconds(basicTime);
		textTimePort.text = string.Format("{0:D2}:{1:D2}:{2:D2}", t.Hours, t.Minutes, t.Seconds);
		ChangeRoomLight (8);
		if (ES2.Exists ("btnName_" + nameString)) {
			OnLoadDatas ();
			timeleftOffline = ES2.Load<float> ("btnName_" + nameString + "?tag=time_GenLeft");
			if (isActiveGen == true) {
				if (hasManagerGen == true) {
					ChangeRoomLight (0);
					timerStart = false;
					buyBtnPort.enabled = false;
					float tempFloatGetLeft = TimerSingleton.instance.getTimeManager (nameString);
					basicTime = tempFloatGetLeft;
					float percentTime = (basicTime / tempTime);
					tempScaler =  (float)System.TimeSpan.FromSeconds (1 - (basicTime/tempTime)).TotalSeconds;
					timerStart = true;

				} else {

					lockObjPort.SetActive (false); 
					float tempFloatGetLeft = TimerSingleton.instance.getTimeLeft (nameString);
					if (tempFloatGetLeft == 0f) {
						if (timeleftOffline != 0){
							OnEarningFunction ("earning");
							timeleftOffline = 0;
							ES2.Save(timeleftOffline, "btnName_" + nameString+"?tag=time_GenLeft");
						}
						buyBtnPort.enabled = true;
						fillbarPort.GetComponent<Image> ().fillAmount = 0;
						basicTime = ES2.Load<float> ("btnName_" + nameString + "?tag=progressTime");
					} else {
						timerStart = false;
						buyBtnPort.enabled = false;
//						IconnBgPort.color = iconColor_Buying;
						basicTime = tempFloatGetLeft;
						float percentTime = (basicTime / tempTime);
						tempScaler =  (float)System.TimeSpan.FromSeconds (1 - (basicTime/tempTime)).TotalSeconds;

						timerStart = true;
						ChangeRoomLight (0);
					}
				}
			} else {
				basicTime = tempTime;
				ES2.Save(false, "btnName_" + nameString+"?tag=Status");
				isActiveGen = false;
				timerStart = false;
				lockObjPort.SetActive (true); 
				
				lockObjPort.transform.Find("UnlockBtn").GetComponent<Button> ().enabled = true;
				lockObjPort.transform.Find("UnlockBtn").GetComponent<Image>().color = readycolorBuy;
			}
		} else {
			timerStart = false;
			lockObjPort.SetActive (true); 
			
			lockObjPort.transform.Find("UnlockBtn").GetComponent<Button> ().enabled = true;
			lockObjPort.transform.Find("UnlockBtn").GetComponent<Image>().color = readycolorBuy;

			fillBarAnimPort.SetActive (false);
			fillbarPort.GetComponent<Image> ().fillAmount = 0;

			ES2.Save(isActiveGen, "btnName_" + nameString+ "?tag=Status");
			ES2.Save(hasManagerGen, "btnName_" + nameString+"?tag=HasManager");
			ES2.Save(hasAccountantGen, "btnName_" + nameString+"?tag=HasAccountant");
			ES2.Save(levelItem, "btnName_" + nameString+"?tag=Level");
			ES2.Save(basicCost, "btnName_" + nameString+"?tag=Amount");
			ES2.Save(upgradeAmount, "btnName_" + nameString+"?tag=AmountUpgrade");
			ES2.Save(percentIncrease, "btnName_" + nameString+"?tag=percentIncrease");
			ES2.Save(basicTime, "btnName_" + nameString+"?tag=progressTime");
			ES2.Save(timeleftOffline, "btnName_" + nameString+"?tag=time_GenLeft");
			ES2.Save(0, "btnName_" + nameString+"?tag=UpgradeLevel");
			ES2.Save(0, "btnName_" + nameString+"?tag=Unlocked");

			ES2.Save(upgradeMultipliers, "btnName_" + nameString+"?tag=UpgradeprofitMultiplier");
			ES2.Save(unlockMultiplierOfProft, "btnName_" + nameString+"?tag=UnlockedprofitMultiplier");
			ES2.Save(angelMultiplier, "btnName_" + nameString+"?tag=angelMultiplier");
			ES2.Save(permanentMultiplier, "btnName_" + nameString+"?tag=PermanentMultiplier");

			OnLoadDatas ();
			basicTime = tempTime;
		}

		levelTextPort.text = levelItem.ToString();
		textMultiplierPort.text = "x"+xTimesAmount.ToString ();
		StartCoroutine (OnCheckTotalMoney ());
		OnEarningFunction ("update");
		OnStartGame = true;
		tempTime = ES2.Load<float> ("btnName_" + nameString + "?tag=progressTime");
		OnCheckUnlock_Small_Fill ();
	}

	private void OnLoadDatas(){
		if (ES2.Exists ("btnName_" + nameString + "?tag=Status")) {
			isActiveGen = ES2.Load<bool> ("btnName_" + nameString + "?tag=Status");
		}
		if (isActiveGen == true) {
			lockObjPort.SetActive (false); 
		}

		if (ES2.Exists ("btnName_" + nameString + "?tag=Level")) {
			levelItem = ES2.Load<int> ("btnName_" + nameString + "?tag=Level");
		}

		if (ES2.Exists ("btnName_" + nameString + "?tag=Amount")) {
			basicCost = ES2.Load<double> ("btnName_" + nameString + "?tag=Amount");
		}

		if (ES2.Exists ("btnName_" + nameString + "?tag=AmountUpgrade")) {
			upgradeAmount = ES2.Load<double> ("btnName_" + nameString + "?tag=AmountUpgrade");
		}

		if (ES2.Exists ("btnName_" + nameString + "?tag=HasManager")) {
			hasManagerGen = ES2.Load<bool> ("btnName_" + nameString + "?tag=HasManager");
			if (hasManagerGen) {
				ChangeRoomLight (0);
			}
		}

		if (ES2.Exists ("btnName_" + nameString + "?tag=HasAccountant")) {
			hasAccountantGen = ES2.Load<bool> ("btnName_" + nameString + "?tag=HasAccountant");
		}

		if (ES2.Exists ("btnName_" + nameString + "?tag=UnlockedprofitMultiplier")) {
			unlockMultiplierOfProft = ES2.Load<double> ("btnName_" + nameString + "?tag=UnlockedprofitMultiplier");
		}

		if (ES2.Exists ("btnName_" + nameString + "?tag=angelMultiplier")) {
			angelMultiplier = ES2.Load<double> ("btnName_" + nameString + "?tag=angelMultiplier");
		}

		if (ES2.Exists ("btnName_" + nameString + "?tag=UpgradeprofitMultiplier")) {
			upgradeMultipliers = ES2.Load<double> ("btnName_" + nameString + "?tag=UpgradeprofitMultiplier");
		}

		if (ES2.Exists ("btnName_" + nameString + "?tag=PermanentMultiplier")) {
			permanentMultiplier = ES2.Load<double> ("btnName_" + nameString + "?tag=PermanentMultiplier");
		}
	}


	IEnumerator OnCheckTotalMoney(){ //check total money if available to upgrade or unlock if locked
		while (OnStartCheck) {
			yield return new WaitForSeconds (0f);
			if (ES2.Exists ("btnName_" + nameString)) {
				double temptotalMoney = getMoneyUnit.totalMoney;
				double amountUpgrade = 0;
				int maxLevel = 0;
				int maxDisplay = 0;
				if (xTimesAmount == 0) { // multipler Max
					int tempmaxLevel = highest_level (levelItem, upgradeAmount, percentIncrease, temptotalMoney);
					if (tempmaxLevel == 0) {
						maxLevel = levelItem;
						amountUpgrade = cost_total (levelItem, upgradeAmount, percentIncrease, maxLevel + 1);
						tempmaxLevel = 1;
					} else {
						amountUpgrade = cost_total (levelItem, upgradeAmount, percentIncrease, levelItem + tempmaxLevel);
						maxLevel = levelItem + tempmaxLevel;
					}
					maxDisplay = tempmaxLevel;
				} else if (xTimesAmount == 1) { //multilier x1
					amountUpgrade = cost_total (levelItem, upgradeAmount, percentIncrease, levelItem + (int)xTimesAmount);
					maxLevel = levelItem + (int)xTimesAmount;
					maxDisplay = (int)xTimesAmount;
				} else { // multiplier x10 and x100
					maxDisplay = (int)xTimesAmount;
					int tempLevel_Upgrade = 0;
					if (levelItem == 0) {
						tempLevel_Upgrade = 1;
					}else {
						tempLevel_Upgrade = levelItem;
					}
					amountUpgrade = cost_total (tempLevel_Upgrade, upgradeAmount, percentIncrease, tempLevel_Upgrade + (int)xTimesAmount);
					maxLevel = tempLevel_Upgrade + (int)xTimesAmount;
				}


				bool canbuy_gen = false;

				if (temptotalMoney >= amountUpgrade) {
					MessageDispatcher.SendMessage(this, "OnShowTutorial",1, 0);
					canbuy_gen = true;

					upBtnPort.GetComponent<Image> ().color = readycolorBuy;
					upBtnPort.GetComponent<Button> ().enabled = true;
				} else {

					upBtnPort.GetComponent<Image> ().color = Color.gray;
					upBtnPort.GetComponent<Button> ().enabled = false;
				}

				if (isActiveGen == false) {
					double tempAmountUpgrade = amountUpgrade;
					if (xTimesAmount == 0) {
						maxLevel = maxDisplay;
					} else {
						maxLevel = (int)xTimesAmount;
					}
					if (canbuy_gen) {
						
						MessageDispatcher.SendMessage(this, "OnShowTutorial",2, 0);
						lockObjPort.transform.Find("UnlockBtn").GetComponent<Button> ().enabled = true;
						lockObjPort.transform.Find("UnlockBtn").GetComponent<Image>().color = readycolorBuy;
					} else {
						lockObjPort.transform.Find("UnlockBtn").GetComponent<Button> ().enabled = false;
						lockObjPort.transform.Find("UnlockBtn").GetComponent<Image>().color = Color.gray;
					}
					double tempNum = amountUpgrade;

					startAmountTextPort.text = getMoneyUnit.GetStringForValue (amountUpgrade).ToString ();
				} else {
					
				}
				textMultiplierPort.text = "x"+maxDisplay.ToString ();

				double tempNum_1 = amountUpgrade;

				if (tempNum_1 == 0) {
					tempNum_1 = 1;
				}

				upTextAmountPort.text = getMoneyUnit.GetStringForValue (amountUpgrade).ToString ();

				upgrade_AmountBuy = amountUpgrade;
				levelTemp_LeveUp = maxLevel;
			}
			yield return OnStartCheck;
		}
	}


	//___________________________________________________________________________________
	double compound_amount(double principle,double rate_percent,int level_){ //
		double return_amount;
		return_amount = principle * System.Math.Pow ((1f + (double)rate_percent), (double)level_);
		return return_amount;
	}


	double cost_total(int currenLevel,double principle,double rate_percent,int level_togo){
		double total_cost = 0;
		int temp_level = currenLevel;
		for (int i = 0; i < (level_togo - currenLevel); i++) {
			total_cost += compound_amount (principle, rate_percent, temp_level);
			temp_level += 1;
		}

		return total_cost;
	}

	int highest_level (int current_level,double principle,double rate_percent,double current_cash){
		double total_cost = 0;
		int temp_level = current_level;

		while (total_cost < current_cash) {
			total_cost += compound_amount(principle,rate_percent,temp_level);
			temp_level++;
		}

		int level_return = temp_level - 1 - current_level;

		if (level_return <= 0) {
			level_return = 0;
		}

		return level_return;
	}


	//__________________________________________________________________________________________


	void OnunLockUpgradeProfitEarining(IMessage rMessage){ // upgrade profit earnings from unlock
		
		double multiplierUnlocked = (double)rMessage.Data;
		unlockMultiplierOfProft = unlockMultiplierOfProft * multiplierUnlocked;
		ES2.Save(unlockMultiplierOfProft, "btnName_" + nameString+"?tag=UnlockedprofitMultiplier");
		OnEarningFunction ("Update");

	}

	private void OnEarningFunction (string callName){
		double totalMultiplier = 0;
		double TotalAdditiveMultiplier = 1;
	
		//check additive boost profit (ad boost and Gold upgrades)
		double adMultipliers_Temp = adMultipliers;
		double permanentMultiplier_Temp = permanentMultiplier;
		if (adMultipliers == 1) { adMultipliers_Temp = 0; }
		if (permanentMultiplier == 1) {permanentMultiplier_Temp = 0; }

		double SpinnerMultiplier = 0;
		if (GameObject.Find ("SpinHandler")) {
			SpinnerMultiplier = GameObject.Find ("SpinHandler").GetComponent<SpinHandler> ().TotalMultiplier;
		}

		TotalAdditiveMultiplier = adMultipliers_Temp + permanentMultiplier_Temp + SpinnerMultiplier  + explorationMultiplier;

		if (TotalAdditiveMultiplier == 0) {TotalAdditiveMultiplier = 1;}


		totalMultiplier = unlockMultiplierOfProft * upgradeMultipliers * angelMultiplier; 

		double TotalGenratorEarnCycle = (basicCost * levelItem) * totalMultiplier * TotalAdditiveMultiplier;

		if (TotalAdditiveMultiplier == 1) {
			TotalMultiplierAdditive = 0;
		} else {
			TotalMultiplierAdditive = TotalAdditiveMultiplier;
		}
	

		if (hasAccountantGen) {
			itemAmountTextPort.text =  getMoneyUnit.GetStringForValue (TotalGenratorEarnCycle/tempTime).ToString () + " / sec";
		} else {
			itemAmountTextPort.text = getMoneyUnit.GetStringForValue (TotalGenratorEarnCycle).ToString ();
		}



		if (isActiveGen) {
			earningsPerCycle = TotalGenratorEarnCycle;
		} else {
			earningsPerCycle = 0;
		}

		if (callName == "earning") {
			MessageDispatcher.SendMessage(this, "OnIncrementMoney",TotalGenratorEarnCycle, 0);	
		}
	}


	void OnDoubleSpeedEarning(IMessage rMessage){ //double speed from unlocked
		if (ES2.Exists ("btnName_" + nameString + "?tag=UpgradeLevel")) {
			int checkMaxfillLevel = ES2.Load<int> ("btnName_" + nameString + "?tag=UpgradeLevel"); //level for upgrade
			checkMaxfillLevel++;
			ES2.Save(checkMaxfillLevel, "btnName_" + nameString+"?tag=UpgradeLevel");
			float finalTimeAnswer = 0;

			float getPow = Mathf.Pow (2, (float)checkMaxfillLevel);
			float getCal = origTime / getPow;
			finalTimeAnswer = getCal;
			//-----------------------------------------------------------------
			Debug.Log("nameString "+nameString+" speed: " + finalTimeAnswer);
			//formula for 2 - 10 generators 
			if (timerStart == false) {
				basicTime = finalTimeAnswer;
				System.TimeSpan t = System.TimeSpan.FromSeconds (basicTime);
				textTimePort.text = string.Format ("{0:D2}:{1:D2}:{2:D2}", t.Hours, t.Minutes, t.Seconds);
			} else {
				timerStart = false; //turn off 1st to get the time left and get the percentage so can set to the new time double speed
				float tempNumers = tempTime - basicTime; //get time left (fulltime - coutingdowntime)
				float tempnum2 = tempNumers / tempTime; //get percent for old time fill progress;
				float timededuc = finalTimeAnswer - (finalTimeAnswer * tempnum2);
				if (tempNumers <= 0) {
					tempNumers = 0;
					basicTime = finalTimeAnswer;
				} else {
					if (tempNumers >= finalTimeAnswer) {
						tempNumers = 0;
						basicTime = finalTimeAnswer;
					} else {
						basicTime = timededuc;
					}
				}
				if (tempNumers == 0) {
					fillbarPort.GetComponent<Image> ().fillAmount = 0;
					tempScaler = 0;
					timerStart = false;
					StartCoroutine(OnEndFill ());
				} else {
					timerStart = true;
					float getTimePercent = basicTime / tempTime;
					float tempFiller = (float)System.TimeSpan.FromSeconds (getTimePercent).TotalSeconds;
					tempScaler =  (float)System.TimeSpan.FromSeconds (1 - (basicTime/tempTime)).TotalSeconds;
					fillbarPort.GetComponent<Image> ().fillAmount = returnLerpValue(tempScaler, tempFiller);
				}
				Debug.Log (tempNumers + "bb aa" + timerStart);

			//get percent left for new time

			}
			//-----------------------------------------------------------------
			ES2.Save(finalTimeAnswer, "btnName_" + nameString+"?tag=progressTime");	
			tempTime = finalTimeAnswer;
		}
	}

	bool hasLevelUp = false;
	void OnCheckUnlock_Small_Fill(){
		if (ES2.Exists ("btnName_" + nameString + "?tag=Unlocked")) {

			int numUnlock = ES2.Load<int> ("btnName_" + nameString + "?tag=Unlocked");
			int tempPreviewsLevel = 0;

			int Level_Hold = 0;
			if (numUnlock < 1) {
				tempPreviewsLevel = getNumLevels.numLevels [numUnlock];;
				Level_Hold = levelItem;
			} else {
				if (numUnlock <= (getNumLevels.numLevels.Length - 1)) {
					tempPreviewsLevel = getNumLevels.numLevels [numUnlock] - getNumLevels.numLevels [numUnlock - 1];
					Level_Hold = levelItem - getNumLevels.numLevels [numUnlock - 1];
				} else {

				}
			}
			hasLevelUp = false;

			float tempFiller = ((float)1 / (float)tempPreviewsLevel) * (float)(Level_Hold);
			fillLevelPort.GetComponent<Image> ().fillAmount = returnLerpValue (0, tempFiller);

			if (fillLevelPort.GetComponent<Image> ().fillAmount == 0) {
				fillLevelPort.GetComponent<Image> ().fillAmount = 1;
				hasLevelUp = true;
				StartCoroutine (OnBackFill ());
			}		

			if (fillLevelPort.GetComponent<Image> ().fillAmount == 1 && getNumLevels.OnDoneUnlock == false) {
				hasLevelUp = true;
				StartCoroutine (OnBackFill ());
			}
			if (getNumLevels.OnDoneUnlock == true) {
				fillLevelPort.GetComponent<Image> ().fillAmount = 1;
			}
		}
	}




	IEnumerator OnBackFill(){
		yield return new WaitForSeconds (1f);
		if (hasLevelUp) {
			fillLevelPort.GetComponent<Image> ().fillAmount = 0;
		}
	}

	void OnUpgradeButton (IMessage rMessage){
		if (ES2.Exists ("btnName_" + nameString + "?tag=Level")) {
			if ((getMoneyUnit.totalMoney - upgrade_AmountBuy) >= 0) {
				MessageDispatcher.SendMessage (this, "PopButton", 6, 0f);
				levelItem = levelTemp_LeveUp;
				ES2.Save (levelItem, "btnName_" + nameString + "?tag=Level"); //save immediately level for checking for unlocks
				MessageDispatcher.SendMessage (this, "OnDicrementMoney", upgrade_AmountBuy, 0);	
				MessageDispatcher.SendMessage (this, "OnCloseTutorial", 1, 0);

				levelTextPort.text = levelItem.ToString ();
				OnEarningFunction ("Update");
				MessageDispatcher.SendMessage (this, "On" + nameString + "CheckLevel", 0, 0f);
				OnCheckUnlock_Small_Fill ();
				if (AdvisortUpgrade) {
					xTimesAmount = mainMultiplier;
					AdvisortUpgrade = false;
				}
			}

//			MessageDispatcher.SendMessage (this, "OnUpdateTickerData", 0, 0);
		}
	}

	void OnUnlockedButton (IMessage rMessage){
		if (ES2.Exists ("btnName_" + nameString + "?tag=Level")) {
			if ((getMoneyUnit.totalMoney - upgrade_AmountBuy) >= 0) {
				MessageDispatcher.SendMessage (this, "PopButton", 8, 0f);
				if (xTimesAmount == 1) {
					levelItem += levelTemp_LeveUp;
				} else {
					levelItem = levelTemp_LeveUp;
				}

				ES2.Save (levelItem, "btnName_" + nameString + "?tag=Level"); //save immediately level for checking for unlocks
				MessageDispatcher.SendMessage (this, "OnCloseTutorial", 2, 0);
				MessageDispatcher.SendMessage (this, "OnDicrementMoney", upgrade_AmountBuy, 0);	
				lockObjPort.SetActive (false); 
				isActiveGen = true;
				levelTextPort.text = levelItem.ToString ();

				ES2.Save (isActiveGen, "btnName_" + nameString + "?tag=Status");
				if (hasManagerGen == true) {
					ChangeRoomLight (0);
					timerStart = true;
					buyBtnPort.enabled = false;
//					IconnBgPort.color = iconColor_Buying;
				}

				OnEarningFunction ("Update");
				MessageDispatcher.SendMessage (this, "On" + nameString + "CheckLevel", 0, 0f);
				OnCheckUnlock_Small_Fill ();
				if (AdvisortUpgrade) {
					xTimesAmount = mainMultiplier;
					AdvisortUpgrade = false;
				}
				MessageDispatcher.SendMessage (this, "OnUpdateScrollData", 0, 0);
			}
		}
	}


	public void OnUpgradeEarningCyle(){ //upgrade/buy 
		MessageDispatcher.SendMessage(this, "OnTapGA_Report","OnGeneratorBtnUpgrade", 0);
		xTimesAmount = mainMultiplier;
		MessageDispatcher.SendMessage (this, "OnUpgrade_Button_" + nameString, 0, 0);
	}

	void OnAdvisorUpgrade (IMessage rMessage){
		xTimesAmount = (double)rMessage.Data;
//		Debug.Log ("Jump Level : " + xTimesAmount);
		AdvisortUpgrade = true;
		MessageDispatcher.SendMessage (this, "OnUpgrade_Button_" + nameString, 0, 0.3f);
	}

	void OnAdvisorUnlock (IMessage rMessage){
		xTimesAmount = (double)rMessage.Data;
		//		Debug.Log ("Jump Level : " + xTimesAmount);
		AdvisortUpgrade = true;
		MessageDispatcher.SendMessage(this, "OnUnlocked_Button_" + nameString, 0, 0.3f);
	}

	public void OnUnlockItem(){ //unlock the generator during the start
		xTimesAmount = mainMultiplier;
		MessageDispatcher.SendMessage(this, "OnTapGA_Report","OnGeneratorBtnUnlock", 0);
		MessageDispatcher.SendMessage(this, "OnUnlocked_Button_" + nameString,0, 0);	
	}
		
	private void OnUpdateMultiplier(IMessage rMessage){ //update multiplier for upgrade
		double numMulti = (double)rMessage.Data;
		if (numMulti == 0) {// if select as Max
			mainMultiplier = 0;
		} else {
			mainMultiplier = numMulti;
		}
		xTimesAmount = mainMultiplier;
	}

	private void OnHireAccountant(IMessage rMessage){ 
		hasAccountantGen = true;
		ES2.Save (true, "btnName_" + nameString + "?tag=HasAccountant");
		upgradeAmount = upgradeAmount - (upgradeAmount * 0.10f);
		ES2.Save (upgradeAmount, "btnName_" + nameString + "?tag=AmountUpgrade");

		OnEarningFunction ("update");
	}

	private void OnHireManager(IMessage rMessage){ //dispatch for hiremanager
		hasManagerGen = true;
		ES2.Save (true, "btnName_" + nameString + "?tag=HasManager");
		if (isActiveGen == true) {
			ChangeRoomLight (0);
			buyBtnPort.enabled = false;
//			IconnBgPort.color = iconColor_Buying;
			if (timerStart == false) {
				timerStart = true;
				if (0.125f >= tempTime) { //limit the progress bar cause when the number speed is too small the progress bar will run fast and not look good
					fillbarPort.GetComponent<Image> ().fillAmount = 1;
					fillBarAnimPort.SetActive (true);
				}
			}
		}
	}

	private IEnumerator OnEndFill(){ //progress bar cycle
		
		OnEarningFunction ("earning");
		yield return new WaitForSeconds (0.1f);

		if (0.125f >= tempTime) {//check time if will do the moving progress bar using or just animation fill
			
			if (hasManagerGen == true) { 
				fillbarPort.GetComponent<Image> ().fillAmount = 1;
				fillBarAnimPort.SetActive (true);
			} else {
				ChangeRoomLight (8);
				fillbarPort.GetComponent<Image> ().fillAmount = 0;
				fillBarAnimPort.SetActive (false);
			}
		} else {
			fillbarPort.GetComponent<Image> ().fillAmount = 0;
			fillBarAnimPort.SetActive (false);
		}
			
		if (hasManagerGen == true) { //check if there is manager to the cycle will continue
			timerStart = true;
			buyBtnPort.enabled = false;
//			IconnBgPort.color = iconColor_Buying;
		} else {
//			IconnBgPort.color = iconColor;
			ChangeRoomLight (8);
			buyBtnPort.enabled = true;
		}

		System.TimeSpan t = System.TimeSpan.FromSeconds(tempTime);
		textTimePort.text =  string.Format("{0:D2}:{1:D2}:{2:D2}", t.Hours, t.Minutes, t.Seconds);

	}

	void OnBuyButton (IMessage rMessage){
		if (timerStart == false && isActiveGen) {
			MessageDispatcher.SendMessage (this, "PopButton", 6, 0f);
			fillbarPort.GetComponent<Image> ().fillAmount = 0;
			fillBarAnimPort.SetActive (false);
			timerStart = true;
			buyBtnPort.enabled = false;
//			IconnBgPort.color = iconColor_Buying;
			ChangeRoomLight (0);
			MessageDispatcher.SendMessage (this, "OnCloseTutorial", 0, 0);
		}
	}

	public void OnBuy(){ // buy for earning when no manger yet
		MessageDispatcher.SendMessage(this, "OnTapGA_Report","OnGeneratorBtnBuy", 0);
		MessageDispatcher.SendMessage (this, "OnBuy_Button_" + nameString, 0, 0);
	}

	void Update()
	{
		
		if (timerStart && isActiveGen && OnSuspend) { //couting for progress bar
			float tempDeltaTime = Time.deltaTime;
			//calculation for fill bar so it will not base in dotween and better that separating progress bar
			basicTime -= tempDeltaTime; //couting down time
			float tempNumers = tempTime - basicTime; //get time left (fulltime - coutingdowntime)
			float tempnum2 = tempNumers / tempTime; //get percent for the fill progress

			timeleftOffline = basicTime; //store the time for the offline time to continue during suspend

			float tempFiller = (float)System.TimeSpan.FromSeconds (tempnum2).TotalSeconds;
			bool checkhasManager = hasManagerGen; //check if already hire manager
//			Debug.Log("name of gen " + nameString);
			bool canCycle = true;
			if (checkhasManager == false) { //if no manager
				if ( 0.125f >= tempTime) {
					fillbarPort.GetComponent<Image> ().fillAmount = 1;
					fillBarAnimPort.SetActive (true);
					canCycle = false;
				} else {
					fillbarPort.GetComponent<Image> ().fillAmount = returnLerpValue(tempScaler, tempFiller);//Mathf.Lerp (tempScaler, 1, tempFiller);
				}
			} else {
				if (0.125f >= tempTime) {
					fillbarPort.GetComponent<Image> ().fillAmount = 1;
					fillBarAnimPort.SetActive (true);
					canCycle = false;
				} else {
					fillbarPort.GetComponent<Image> ().fillAmount = returnLerpValue(tempScaler, tempFiller);//Mathf.Lerp (tempScaler, 1, tempFiller);
				}
			}
			if (basicTime < 0) { // end time progress bar
				basicTime = tempTime;
				timeleftOffline = 0;
				tempScaler = 0;
				ES2.Save(timeleftOffline, "btnName_" + nameString+"?tag=time_GenLeft");
				textTimePort.text =   timeTextFormat(basicTime);//string.Format("{0:D2}:{1:D2}:{2:D2}", t.Hours, t.Minutes, t.Seconds);
				timerStart = false;
				StartCoroutine(OnEndFill ());
			} else {
				textTimePort.text =  timeTextFormat(basicTime);//string.Format("{0:D2}:{1:D2}:{2:D2}", t.Hours, t.Minutes, t.Seconds);
			}
		}
	}

	float returnLerpValue(float tempScaler_Float , float tempFill_Float){
		float lerpReturn = Mathf.Lerp (tempScaler_Float, 1, tempFill_Float);
		return lerpReturn;
	}

	string timeTextFormat(float basicTimeFloat){ //convert Text format in time for fill
		string returnString;
		System.TimeSpan t = System.TimeSpan.FromSeconds(basicTime);
		returnString = string.Format("{0:D2}:{1:D2}:{2:D2}", t.Hours, t.Minutes, t.Seconds);
		return returnString;
	}

	void ChangeRoomLight(int newLayer)
	{
		if (newLayer == 0) {
			RoomLight.SetActive (false);
		} else {
			RoomLight.SetActive (true);
		}
	}

	private string HasDecimalValue(double value) { // string for use for Generators so it will different than Total money format cause total money format has no 3 decimal places 
		double newMoneyAmount;
		string moneySuffix = "";
		if (value > 999999999999999) {
			// quadrillion
			moneySuffix = " Quad";
			newMoneyAmount = (double)value / (double)1000000000000000;
			return ((string.Format ("{0:n3}", newMoneyAmount) + moneySuffix));

		} else if (value > 999999999999) {
			// trillion
			moneySuffix = " Tril";
			newMoneyAmount = (double)value / (double)1000000000000;
			return ((string.Format ("{0:n3}", newMoneyAmount) + moneySuffix));

		} else if (value > 999999999) {
			// billion
			moneySuffix = " Bil";
			newMoneyAmount = (double)value / (double)1000000000;
			return ((string.Format ("{0:n3}", newMoneyAmount) + moneySuffix));

		} else if (value > 999999) {
			// million
			moneySuffix = " Mil";
			newMoneyAmount = (double)value / (double)1000000;
			return ((string.Format ("{0:n3}", newMoneyAmount) + moneySuffix));

		} else {
			return (string.Format ("{0:n2}", value) + moneySuffix); // string.Format("{0:#.00}", Convert.ToDecimal(totalMoney.ToString ()) / 100);
		}
	}


	void OnApplicationQuit()
	{
		ES2.Save((float)System.TimeSpan.FromSeconds (timeleftOffline).TotalSeconds, "btnName_" + nameString+"?tag=time_GenLeft"); //save time when application quit
	}

	void OnApplicationPause( bool pauseStatus )
	{
		
		if (pauseStatus == true) {
			if (timerStart == true) {
				OnSuspend = false;
			}
			OnStartGame = false;
			ES2.Save((float)System.TimeSpan.FromSeconds (timeleftOffline).TotalSeconds, "btnName_" + nameString+"?tag=time_GenLeft");//save time when application suspend
//			print(timeleftOffline + " TIME!!");


		} else if (pauseStatus == false && OnStartGame == false) {
			OnSuspend = true;
			if (timerStart == true) {
				OnSetupGenerators ();//load game when start or back from suspend
			} else {
				if (ES2.Exists ("btnName_" + nameString)) {
					if (isActiveGen == true) {
						if (hasManagerGen == true) {
							OnSetupGenerators ();
						}
					}
				}
			}
		}
	}

}
