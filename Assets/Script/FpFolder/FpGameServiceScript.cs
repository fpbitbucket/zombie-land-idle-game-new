﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TMPro;
using com.ootii.Messages;

#if UNITY_ANDROID
//using GooglePlayGames;
//using GooglePlayGames.BasicApi;
#endif

using UnityEngine.SocialPlatforms.GameCenter;

public class FpGameServiceScript : MonoBehaviour {
	public Image useAccount;
	public TextMeshProUGUI StateText,playerName;
	public bool InConnected = false;
	public string leaderboardAndroid;
	public string leaderboardIos;
	public bool OnStartConnection = false;
	public string[] achievementIdsAndroid;
	public string[] achievementIdsIos;

	public long playerleaderscore = 100;

	private static FpGameServiceScript _instance;
	public static FpGameServiceScript Instance 
	{
		get {
			return _instance;
		}
	}

	void OnEnable(){
		MessageDispatcher.AddListener ("OnUnlockedFpGCAchiement", OnUnlockedFpGCAchiement, true);
		MessageDispatcher.AddListener ("OnGCLeaderBoard", OnGCLeaderBoard, true);
		MessageDispatcher.AddListener ("OnSetGameServices", OnSetGameServices, true);
		MessageDispatcher.AddListener ("OnLoginGameServices", OnLoginGameServices, true);

		if (ES2.Exists ("playerleaderscore")) {
			playerleaderscore = ES2.Load<long> ("playerleaderscore");
		} else {
			ES2.Save(playerleaderscore, "playerleaderscore");
		}

		StateText.text = "Connect";
		OnStartConnection = true;

	}

	void OnDisable(){


		MessageDispatcher.RemoveListener ("OnUnlockedFpGCAchiement", OnUnlockedFpGCAchiement, true);
		MessageDispatcher.RemoveListener ("OnGCLeaderBoard", OnGCLeaderBoard, true);;
		MessageDispatcher.RemoveListener ("OnSetGameServices", OnSetGameServices, true);
		MessageDispatcher.RemoveListener ("OnLoginGameServices", OnLoginGameServices, true);
	}

	void OnLoginGameServices(IMessage rMessage){
		StartCoroutine(checkInternetConnection());
	}

	IEnumerator checkInternetConnection(){
		while (OnStartConnection) {
			if (gameObject.GetComponent<NetWorkCheck>().networkState == true) {
//				MessageDispatcher.SendMessage (this, "OnSetGameServices", 0, 3);
				OnStartConnection = false;
			} else {
			}
			yield return new WaitForSeconds (3);
		}
	} 


	void OnSetGameServices(IMessage rMessage){

		#if UNITY_IPHONE
			Social.localUser.Authenticate(success => {
			if (success){
				OnStartConnection = false;
				OnPlayerConnected ();
			}else
				Debug.Log("Failed to authenticate");
			});
		#elif UNITY_ANDROID

//		PlayGamesClientConfiguration config = new 
//			PlayGamesClientConfiguration.Builder()
//			.Build();
//
//		// Enable debugging output (recommended)
//		PlayGamesPlatform.DebugLogEnabled = true;
//
//		// Initialize and activate the platform
//		PlayGamesPlatform.InitializeInstance(config);
//		PlayGamesPlatform.Activate();
//
//		if (!PlayGamesPlatform.Instance.localUser.authenticated) {
//			PlayGamesPlatform.Instance.Authenticate(SignInCallback, false);
//		} else {
//
//
//		}
		#endif
	}


	public void SignInCallback(bool success) {
		if (success) {
			Debug.Log("(Lollygagger) Signed in!");

			OnStartConnection = false;
			OnPlayerConnected ();

		} else {
			Debug.Log("(Lollygagger) Sign-in failed...");
		}
	}

	void OnGCLeaderBoard (IMessage rMessage){
		double numLearboard = (double)rMessage.Data;
		playerleaderscore = playerleaderscore + 100;
		#if UNITY_ANDROID
//			GooglePlayManager.Instance.SubmitScoreById(leaderboardAndroid,playerleaderscore*(long)1000000);
			print("submit score " + playerleaderscore);
		#endif

		#if UNITY_IOS 
			long context = 5;
//			GameCenterManager.ReportScore(playerleaderscore ,leaderboardIos, context);
		#endif

		ES2.Save(playerleaderscore, "playerleaderscore");
	}

	void OnUnlockedFpGCAchiement (IMessage rMessage){
		int numId = (int)rMessage.Data;

		#if UNITY_ANDROID
		if (achievementIdsAndroid[numId] !=null){
//			GooglePlayManager.Instance.UnlockAchievementById(achievementIdsAndroid[numId]);
		}
	
		#endif

		#if UNITY_IOS 
		if (achievementIdsIos[numId] !=null){
//			GameCenterManager.SubmitAchievement(1f, achievementIdsIos[numId], true);
		}
		#endif

	}

	public void OnUnlockedTestAchievements(int i){
		MessageDispatcher.SendMessage(this, "OnUnlockedFpGCAchiement",i, 0);	
	}


	public void OnTestSendLeaderBoard(){
		MessageDispatcher.SendMessage(this, "OnGCLeaderBoard",(double)100, 0);	
	}

	private void OnPlayerConnected() {
		
		playerName.text = Social.localUser.userName;

//		Texture2D tex = Social.localUser.image;
//		useAccount.sprite = Sprite.Create(tex as Texture2D, new Rect(0f, 0f, tex.width, tex.height), Vector2.zero);
		StateText.text = "Connected";
		InConnected = true;

		#if UNITY_ANDROID

		#endif
	}




	private void OnPlayerDisconnected() {
		playerName.text = "Name";
		StateText.text = "Disconnected";
	}

	public bool getGGCStatus(){
		return InConnected;
	}

	public void OnLogin(){
		
	}

	public void OnLoadAchievements(){

		#if UNITY_ANDROID

		#endif

		#if UNITY_IOS 

		#endif
	}

	public void OnLoadLeaderBoard(){
		#if UNITY_ANDROID

		#endif

		#if UNITY_IOS 

		#endif
	}
		
}
