﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.SimpleAndroidNotifications;
using com.ootii.Messages;
#if UNITY_IOS
using NotificationServices = UnityEngine.iOS.NotificationServices;
using NotificationType = UnityEngine.iOS.NotificationType;
#endif
public class FpNotificationSCript : MonoBehaviour {
	public string[] ShowPopupNotificationMessage;
	public float[] ScheduleNotificationTime;
	int numMessageUsed,numNotificationSet = 0;
	int[] NotificationId;
	bool tokenSent;
	private static FpNotificationSCript _instance;
	private AudioSource audioPlayer;

	public static FpNotificationSCript instance
	{
		get
		{
			if(_instance == null)
			{
				_instance = GameObject.FindObjectOfType<FpNotificationSCript>();

				//Tell unity not to destroy this object when loading a new scene!
//				DontDestroyOnLoad(_instance.gameObject);
			}

			return _instance;
		}
	}

	void Awake() 
	{
		StartCoroutine (OnSetNotification ());
		
	}

	IEnumerator OnSetNotification(){
		yield return new WaitForSeconds (3);
		tokenSent = true;

//		#if UNITY_IOS
//			NotificationServices.RegisterForNotifications(
//			NotificationType.Alert |
//			NotificationType.Badge |
//			NotificationType.Sound);
//		#endif 

	if(GameObject.Find("GameTimer")){
		float numOffline = GameObject.Find("GameTimer").GetComponent<TimerSingleton>().getTimeOfflineSeconds();
		if(numOffline >= ScheduleNotificationTime[12] || numOffline >= ScheduleNotificationTime[13] || numOffline >= ScheduleNotificationTime[14]){
			Debug.Log("Got Big Time Reward");
			if (ES2.Exists ("BigTimeReward") == false) {
				MessageDispatcher.SendMessage (this, "OnBigTimeReward", 0, 0);
			}
		}
	}


	}

	void Update()
	{

		#if UNITY_IOS
//		if (!tokenSent)
//		{
//		byte[] token = NotificationServices.deviceToken;
//		if (token != null)
//		{
//		// send token to a provider
//		Debug.Log ("send token to a provider");
//		string hexToken = "%" + System.BitConverter.ToString(token).Replace('-', '%');
//		new WWW("http:/example.com?token=" + hexToken);
//		tokenSent = true;
//		}
//		}
		#endif
	}

	void OnEnable(){
		
		MessageDispatcher.AddListener ("OnCancelScheduleNotification", OnCancelScheduleNotification, true);
		MessageDispatcher.AddListener ("OnStartSchedulaNotification", OnStartSchedulaNotification, true);
		MessageDispatcher.AddListener ("OnCancelSchedulaNotification", OnCancelSchedulaNotification, true);
		MessageDispatcher.AddListener ("OnInitNotification", OnInitNotification, true);
		MessageDispatcher.AddListener ("OnSetCustomNotification", OnSetCustomNotification, true);
		MessageDispatcher.AddListener ("OnSetupNotificationBusiness", OnSetupNotificationBusiness, true);
	}

	void OnDisable(){
		MessageDispatcher.RemoveListener ("OnCancelScheduleNotification", OnCancelScheduleNotification, true);
		MessageDispatcher.RemoveListener ("OnStartSchedulaNotification", OnStartSchedulaNotification, true);
		MessageDispatcher.RemoveListener ("OnCancelSchedulaNotification", OnCancelSchedulaNotification, true);
		MessageDispatcher.RemoveListener ("OnInitNotification", OnInitNotification, true);
		MessageDispatcher.RemoveListener ("OnSetCustomNotification", OnSetCustomNotification, true);
		MessageDispatcher.RemoveListener ("OnSetupNotificationBusiness", OnSetupNotificationBusiness, true);
	}

	void OnSetCustomNotification (IMessage rMessage){
		string textMessage = ((MyCustomMessage)rMessage).NotificationMessage;
		float notificationTime = ((MyCustomMessage)rMessage).NotificationTime;

		#if UNITY_ANDROID

		#endif

		#if UNITY_IOS

		#endif 
	
	}

	void OnSetupNotificationBusiness (IMessage rMessage){
		string[] dataBusiness =(string[])rMessage.Data;
		string messageTest = dataBusiness [0];
		float TimeSet = float.Parse(dataBusiness [1]);

		#if UNITY_IOS
		// schedule notification to be delivered in 24 hours
		if (tokenSent){
		UnityEngine.iOS.LocalNotification notif = new UnityEngine.iOS.LocalNotification();

		notif.fireDate = DateTime.Now.AddSeconds(TimeSet);

		notif.alertBody = messageTest;

		UnityEngine.iOS.NotificationServices.ScheduleLocalNotification(notif);
		}
		#endif


		#if UNITY_ANDROID
		var notificationParams = new NotificationParams {
			Id = UnityEngine.Random.Range (0, int.MaxValue),
			Delay = TimeSpan.FromSeconds (TimeSet),
			Title = Application.productName.ToString(),
			Message = messageTest,
			Ticker = "Ticker",
			Sound = true,
			Vibrate = true,
			Light = true,
			SmallIcon = NotificationIcon.Bell,
			SmallIconColor = new Color (0, 0.5f, 0),
			LargeIcon = "app_icon"
		};

		NotificationManager.SendCustom (notificationParams);

		#endif

	}


	void OnInitNotification (IMessage rMessage){
		NotificationId = new int[ShowPopupNotificationMessage.Length];
		numMessageUsed = ShowPopupNotificationMessage.Length;
		for (int i = 0; i < numMessageUsed; i++) {
			


		}
	}
	void OnCancelSchedulaNotification (IMessage rMessage){
//		UM_NotificationController.Instance.CancelAllLocalNotifications();
	}

	void OnStartSchedulaNotification (IMessage rMessage){
		numMessageUsed = ShowPopupNotificationMessage.Length;
		for (int i = 0; i < numMessageUsed; i++) {
			bool canNotify = false;
			string messageTest = ShowPopupNotificationMessage[i];
			float TimeSet = ScheduleNotificationTime [i];

			string TitleString = Application.productName.ToString ();
			if (i == 2) {
				if (Application.loadedLevelName == "MainScene") {
					if (GameObject.Find ("TimeManager")){
					if (GameObject.Find ("TimeManager").GetComponent<TimeManager> ().adMultiplierStatus) {
						TimeSet = GameObject.Find ("TimeManager").GetComponent<TimeManager> ().adTimerHolder;
						canNotify = true;
					}
					}
				}
			} else if (i == 4) {
				double TotalGold = ES2.Load<double> ("TotalGold");
				double[] timewarpsGold = GameObject.Find ("ShopManager").GetComponent<ShopManager> ().timeWarpGold;
				for (int j = 0; j < timewarpsGold.Length; j++) {
					
					if (TotalGold >= timewarpsGold [j]) {
//						Debug.Log (timewarpsGold [j] + " " + TotalGold + "Can notify Timewarp");
						canNotify = true;
						break;
					} else {
//						Debug.Log (timewarpsGold [j] + " " + TotalGold + "Cant notify Timewarp");
					}
				}
			} else if (i == 6) {
				if (Application.loadedLevelName == "MainScene") {
					if (GameObject.Find ("TimeManager").GetComponent<TimeManager> ().adMultiplierStatus) {
						TimeSet = GameObject.Find ("TimeManager").GetComponent<TimeManager> ().adTimerHolder + 14400;
						canNotify = true;
					}
				}
			} else if (i == 7) {
				if (Application.loadedLevelName == "MainScene") {
					if (GameObject.Find ("StarterPackManager")) {
						float timeEventHolder = GameObject.Find ("StarterPackManager").GetComponent<StartPackScript> ().timeEventHolder;
						if (timeEventHolder > 0) {
							TimeSet = timeEventHolder - 10800f;
							canNotify = true;
							if (TimeSet < 0) {
								Debug.Log ("less 0 3hrs");
								TimeSet = timeEventHolder;
								canNotify = false;
							}
						} else {
							canNotify = false;
						}
					}
				}
			} else if (i == 8) {
				if (GameObject.Find ("StarterPackManager")) {
					float timeEventHolder = GameObject.Find ("StarterPackManager").GetComponent<StartPackScript> ().timeEventHolder;
					if (timeEventHolder > 0) {
						TimeSet = timeEventHolder - 86400;
						canNotify = true;
						if (TimeSet < 0) {
							Debug.Log ("less 0 24hrs");
							TimeSet = timeEventHolder;
							canNotify = false;
						}
					} else {
						canNotify = false;
					}
				}
			} else if (i == 9) {
				if (GameObject.Find ("Exploration")) {
					if (GameObject.Find ("Exploration").GetComponent<ExplorationScriptNew> ().OnStartTime) {
						TimeSet = GameObject.Find ("Exploration").GetComponent<ExplorationScriptNew> ().timeShowup;
						TitleString = "Zombie Mission Finished!";
						Debug.Log (TitleString + " Set Notification " + " " + TimeSet);
						canNotify = true;
					}
				}
			} else if (i == 10) {
				if (GameObject.Find ("Exploration")) {
					if (GameObject.Find ("Exploration").GetComponent<ExplorationScriptNew> ().OnStartTime == false) {
						TitleString = "Zombie Mission Challenge!";
						DateTime date = DateTime.Now;
						DayOfWeek day = DateTime.Now.DayOfWeek;
						string dayToday = day.ToString ();
						int numGetnumToday = int.Parse (day.ToString ("d"));
						int numToRoll = 7 - numGetnumToday;
						for (int l = 0; l < numToRoll; l++) {
							if (date.AddDays (l + 1).DayOfWeek.ToString () == "Wednesday" || date.AddDays (l + 1).DayOfWeek.ToString () == "Sunday") {
								int numGetnumday = int.Parse (date.AddDays (l + 1).DayOfWeek.ToString ("d"));
								int numDifference = (numGetnumday - numGetnumToday);
								if (numDifference < 0) {
									numDifference = 7;
								}
								float getTime = ((numDifference - 1) * 24) * 60 * 60;
								DateTime tomorrow = date.AddDays (1).Date; // this is the "next" midnight
								float secondsUntilMidnight = (float)((tomorrow - date).TotalSeconds);
								float TotalTime = getTime + secondsUntilMidnight;
								TimeSet = TotalTime;
								canNotify = true;
								break;
							} 
						}
					}
				}
			} else if (i == 11) {
				if (GameObject.Find ("Exploration")) {
					if (GameObject.Find ("Exploration").GetComponent<ExplorationScriptNew> ().OnStartTime && GameObject.Find ("Exploration").GetComponent<ExplorationScriptNew> ().OnCanBoostExploration == false) {
						TimeSet = GameObject.Find ("Exploration").GetComponent<ExplorationScriptNew> ().timeBoostTemp;
						TitleString = "Zombie Mission Boost!";
//						Debug.Log (TitleString + " Set Notification " + " " + TimeSet);
						canNotify = true;
					}
				
				}
			}else{
				canNotify = true;
			}
			if (canNotify) {
				#if UNITY_IOS
					// schedule notification to be delivered in 24 hours
					if (tokenSent){
					UnityEngine.iOS.LocalNotification notif = new UnityEngine.iOS.LocalNotification();

					notif.fireDate = DateTime.Now.AddSeconds(TimeSet);

					notif.alertBody = messageTest;

					UnityEngine.iOS.NotificationServices.ScheduleLocalNotification(notif);
					}
				#endif


				#if UNITY_ANDROID
				var notificationParams = new NotificationParams {
					Id = UnityEngine.Random.Range (0, int.MaxValue),
					Delay = TimeSpan.FromSeconds (TimeSet),
					Title = Application.productName.ToString(),
					Message = messageTest,
					Ticker = "Ticker",
					Sound = true,
					Vibrate = true,
					Light = true,
					SmallIcon = NotificationIcon.Bell,
					SmallIconColor = new Color (0, 0.5f, 0),
					LargeIcon = "app_icon"
				};

				NotificationManager.SendCustom (notificationParams);

				#endif
			}
		}

	}

	void OnCancelScheduleNotification (IMessage rMessage){
		#if UNITY_IOS
		if (tokenSent){
		UnityEngine.iOS.NotificationServices.ClearLocalNotifications();
		UnityEngine.iOS.NotificationServices.CancelAllLocalNotifications();
		}
		#endif

		#if UNITY_ANDROID
		NotificationManager.CancelAll();
		#endif
	}

	public void OnNotificationShowPopup(){
		
	}

	public void OnScheduleNotification(){
		MessageDispatcher.SendMessage(this, "OnStartSchedulaNotification", 0, 0);
	}


	void OnApplicationQuit()
	{
		MessageDispatcher.SendMessage(this, "OnCancelScheduleNotification", 0, 0);
		MessageDispatcher.SendMessage(this, "OnStartSchedulaNotification", 0, 0);
	}

	void OnApplicationPause( bool pauseStatus )
	{
		if (pauseStatus == true) {
			MessageDispatcher.SendMessage(this, "OnCancelScheduleNotification", 0, 0);
			MessageDispatcher.SendMessage(this, "OnStartSchedulaNotification", 0, 0);
		} else if (pauseStatus == false) {
			MessageDispatcher.SendMessage(this, "OnCancelScheduleNotification", 0, 0);
		}
	}
}
