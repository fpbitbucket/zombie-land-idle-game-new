﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Messages;
public class AudioFiles : MonoBehaviour {
	public AudioClip[] audioFiles;
	private AudioSource audioPlayer;

	void Start(){
		audioPlayer = gameObject.GetComponent<AudioSource> () as AudioSource;
	}

	void OnEnable() 
	{
		MessageDispatcher.AddListener("OnSoundFx", OnSoundFx, true);
	}

	void OnDisable(){
		MessageDispatcher.RemoveListener("OnSoundFx", OnSoundFx, true);
	}

	void OnSoundFx(IMessage rMessage)
	{
		if (audioPlayer.isPlaying) {
			audioPlayer.Stop ();
		}
		Debug.Log (audioFiles [(int)rMessage.Data].name);
		audioPlayer.clip = audioFiles [(int)rMessage.Data];
		audioPlayer.Play ();
	}
}
