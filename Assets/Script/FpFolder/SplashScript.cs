﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

//#if UNITY_IOS
//using NotificationServices = UnityEngine.iOS.NotificationServices;
//using NotificationType = UnityEngine.iOS.NotificationType;
//#endif

public class SplashScript : MonoBehaviour {

	// MainScene
	public GameObject portrat;
	public float delaytime = 1f;
	public string nameScene = "MainScene";
	public GameObject loadingBar;
	void Start () {
//		Screen.orientation = ScreenOrientation.LandscapeLeft;

		portrat.SetActive (true);
		if (loadingBar != null) {
			loadingBar.GetComponent<Image> ().fillAmount = 0;
		}
//		#if UNITY_IOS
//		NotificationServices.RegisterForNotifications(
//		NotificationType.Alert |
//		NotificationType.Badge |
//		NotificationType.Sound);
//		#endif 

//		StartCoroutine (OnChangeScene ());
		StartCoroutine(AsynchronousLoad(nameScene));
	}

	void Update() {
//		if ((Input.deviceOrientation == DeviceOrientation.LandscapeLeft || Input.deviceOrientation == DeviceOrientation.LandscapeRight)) {
//			landScape.SetActive (true);
//			portrat.SetActive (false);
//		}else if ((Input.deviceOrientation == DeviceOrientation.Portrait || Input.deviceOrientation == DeviceOrientation.PortraitUpsideDown)) {
//			landScape.SetActive (false);
//			portrat.SetActive (true);
//		}
	}


	IEnumerator AsynchronousLoad (string scene)
	{
		yield return new WaitForSeconds (delaytime);

		AsyncOperation ao = Application.LoadLevelAsync (scene);//SceneManager.LoadSceneAsync(scene);
//		float progress = Mathf.Clamp01(ao.progress / 0.9f);
		ao.allowSceneActivation = false;
//		Debug.Log ("awake");

		while (! ao.isDone)
		{
			float progress = Mathf.Clamp01(ao.progress / 0.9f);
			if (loadingBar != null) {
				loadingBar.GetComponent<Image> ().fillAmount = progress;
			}
			if (ao.progress == 0.9f)
			{
				if (loadingBar != null) {
					loadingBar.GetComponent<Image> ().fillAmount = 1;
				}
				ao.allowSceneActivation = true;
				System.GC.Collect();
			}

			yield return null;
		}
	}

//	IEnumerator OnChangeScene () {
//		yield return new WaitForSeconds (0.3f);
//		SceneManager.LoadScene("MainScene", LoadSceneMode.Single);
//	}
}
