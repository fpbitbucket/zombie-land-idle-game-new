﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using com.ootii.Messages;
public class RateReviewScript : MonoBehaviour {
	public GeneratorScript[] generatorsObj;
	public GameObject[] rateReviewObj;
	public GameObject[] rateReviewObjReward;
	public bool OnStartTime = false,tapRateBtn = false,clearToShow = false,hasRateReview = false; 
	public float timeShowup;
	float timeShowupTemp;
	//	public string[] messageString;
	public int ActivePopups = 0;

	public string iOSRateMeLink;
	public string GplayRateMeLink;

	void OnEnable () {
		timeShowupTemp = timeShowup;
		for (int i = 0; i < rateReviewObjReward.Length; i++) {
			rateReviewObjReward [i].SetActive (false);
			rateReviewObjReward [i].transform.GetChild (1).Find ("Message").GetComponent<TextMeshProUGUI> ().text = "";
			rateReviewObjReward [i].transform.GetChild (1).Find ("YesBtn").GetComponent<Button> ().onClick.AddListener (() => {
				OnCloseReward ();
			});
		}


		for (int i = 0; i < rateReviewObj.Length; i++) {
			rateReviewObj [i].SetActive (false);
			rateReviewObj [i].transform.GetChild (1).Find ("Message").GetComponent<TextMeshProUGUI> ().text = "Help us by rating the the game."+"\n"+"<color=#00D5FFFF>(we have gift as thanks)";
			int numYes = 0;
			int numNo = 1;
			rateReviewObj [i].transform.GetChild (1).Find ("YesBtn").GetComponent<Button>().onClick.AddListener(() => { OnUpdateMessage(numYes); });
			rateReviewObj [i].transform.GetChild (1).Find ("NoBtn").GetComponent<Button>().onClick.AddListener(() => { OnUpdateMessage(numNo); });
		}

		if (ES2.Exists ("HasRateReviewSetTimeV2") == false) {
			if (ES2.Exists ("RateReviewSetTimeV2")) {
				timeShowup = ES2.Load<float> ("RateReviewSetTimeV2");
				OnCalculateTime ();
			} else {
				OnStartTime = true; 
			}
		} else {
			OnStartTime = false; 
		}
	}

	void OnDisable(){
		ES2.Save (System.DateTime.Now.ToBinary ().ToString (), "RateReviewTimeLeftOffline");
	}

	public void OnCheckIfHasRateReview(){
		clearToShow = true; 
		if (hasRateReview) {
			OnShowPopup ();
		}
	}

	void OnCloseReward(){

		for (int i = 0; i < rateReviewObjReward.Length; i++) {
			rateReviewObjReward [i].SetActive (false);
		}

	}

	void OpenReward(){
		int numReward = 12;
		if (ES2.Exists ("firstRateReviewSetTimeV2") == false) {
			ES2.Save (true, "firstRateReviewSetTimeV2"); 
			numReward = 12;
		} else {
			numReward = 24;
		}
		double OnRewardAmount = OnTimeWarpHours (numReward);
		MoneyManager TempMoney = GameObject.Find ("Managers/MoneyManager").GetComponent<MoneyManager> ();
		for (int i = 0; i < rateReviewObjReward.Length; i++) {
			rateReviewObjReward [i].SetActive (true);
			rateReviewObjReward [i].transform.GetChild (1).Find ("Message").GetComponent<TextMeshProUGUI> ().text = "Thank you for rating our GAME! You get <color=#00D5FFFF>$"+TempMoney.GEtNumberNoDecimal (OnRewardAmount)+"</color> from "+numReward+" hours Time-warp.";
		}
		MessageDispatcher.SendMessage (this, "OnIncrementMoney", OnRewardAmount, 0);
		MessageDispatcher.SendMessage (this, "OnGA_Report", "Rating_v3:GiveReward_v3", 0);
		tapRateBtn = false;
	}

	double OnTimeWarpHours (int numHours) // Hours
	{
		int numDays = numHours;
		double timewarpEarned = 0;
		for (int i = 0; i < generatorsObj.Length; i++) {

			if (generatorsObj [i].hasManagerGen && generatorsObj [i].isActiveGen) {
				double TotalGenratorEarnCycle = generatorsObj [i].earningsPerCycle;
				float progressTime = generatorsObj [i].tempTime;
				double GetEarnPerMinute = (TotalGenratorEarnCycle / progressTime) * 60;
				double GetEarnPerHour = GetEarnPerMinute * 60;
				timewarpEarned += GetEarnPerHour *  numDays; // Hours 
			}
		}
		return timewarpEarned;
	}


	bool checkifhasManagerRunning(){
		bool hasManager = false;
		bool hasTutorial = false;
		if (ES2.Exists ("TotalTutorial")) {
			bool[] tutorialStats = ES2.LoadArray<bool> ("TotalTutorial");
			if(tutorialStats[3] == true){
				hasTutorial = true;
			}
		} 
		
		for (int i = 0; i < generatorsObj.Length; i++) {
			if (generatorsObj [i].hasManagerGen && generatorsObj [i].isActiveGen && hasTutorial) {
				hasManager = true;
				break;
			}
		}
		
		return hasManager;
	}

	void OnShowPopup(){
		MessageDispatcher.SendMessage (this, "OnGA_Report", "Rating_v3:ShowingRatePopup_v3", 0);
		tapRateBtn = false;
		int numReward = 12;
		if (ES2.Exists ("firstRateReviewSetTimeV2") == false) {
			numReward = 12;
		} else {
			numReward = 24;
		}
		string MessageReview = "";
		for (int i = 0; i < rateReviewObj.Length; i++) {
			rateReviewObj [i].SetActive (true);
			rateReviewObj [i].transform.GetChild (1).Find ("RewardText").GetComponent<TextMeshProUGUI> ().text = "<color=#00D5FFFF>+"+numReward+" hours Time-warp.</color>";

		}
	}

	public void OnUpdateMessage(int numbtn){

		if (numbtn == 0) { //yes 2nd 
			MessageDispatcher.SendMessage (this, "OnGA_Report", "Rating_v3:YES_RateTheApp_v3", 0);
			ES2.Save (true, "HasRateReviewSetTimeV2"); 
			tapRateBtn = true;
			hasRateReview = false; 
			OnCloseRatePopup();
			#if UNITY_ANDROID
			Application.OpenURL (GplayRateMeLink);
			#endif
			#if UNITY_IOS
			Application.OpenURL (iOSRateMeLink);
			#endif
		}else if (numbtn == 1) {//Later 2nd 
			MessageDispatcher.SendMessage (this, "OnGA_Report", "Rating_v3:Later_RateTheApp_v3", 0);
			ES2.Save (true, "firstRateReviewSetTimeV2"); 
			OnSetTime (1);
			OnStartTime = true;
			OnCloseRatePopup();
		}
	}

	void OnSetTime(int numSet){
		timeShowup = 86400f;
		ES2.Save (timeShowup, "RateReviewSetTimeV2");
	}

	void OnCloseRatePopup(){
		ActivePopups = 0;
		for (int i = 0; i < rateReviewObj.Length; i++) {
			rateReviewObj [i].SetActive (false);
		}
	}

	void Update ()
	{
		if (OnStartTime && checkifhasManagerRunning()) {
			float tempDeltaTime = Time.deltaTime;
			timeShowup -= tempDeltaTime;
			if (timeShowup < 0 && GameObject.Find("SpinHandler").GetComponent<SpinHandler>().activePopup == false) {
				timeShowup = timeShowupTemp;
				OnStartTime = false;
				hasRateReview = true; 
				ES2.Save (timeShowup, "RateReviewSetTimeV2"); 
				if (clearToShow) {
					OnShowPopup ();
				}

			}
		}
	}
	DateTime currentDate;
	void OnCalculateTime ()
	{
		if (ES2.Exists ("RateReviewSetTimeV2")) {
			OnStartTime = false;
			currentDate = System.DateTime.Now;
			long temp = 0;
			if (ES2.Exists ("RateReviewTimeLeftOffline")) {
				temp = Convert.ToInt64 (ES2.Load<string> ("RateReviewTimeLeftOffline")); //load the time starts offline
			} 
			DateTime oldDate = DateTime.FromBinary (temp);
			TimeSpan difference = currentDate.Subtract (oldDate);
			int secondsInt = Convert.ToInt32 (difference.TotalSeconds); //seconds during offline
			float tempGenTimeLeft = ES2.Load<float> ("RateReviewSetTimeV2");
			float numReturnValue = 0;

			if (tempGenTimeLeft < difference.TotalSeconds) { // time offline is bigger than time remaining
				numReturnValue = 0;
				timeShowup = timeShowupTemp;
				OnStartTime = false;
				ES2.Save (0f, "RateReviewSetTimeV2"); 
				hasRateReview = true; 
				if (ES2.Exists ("HasRateReviewSetTimeV2") == false && clearToShow && GameObject.Find("SpinHandler").GetComponent<SpinHandler>().activePopup == false) {
					OnShowPopup ();
				}

			} else { // time remaining is bigger than time offline
				numReturnValue = tempGenTimeLeft - (float)difference.TotalSeconds;
				timeShowup = numReturnValue;
				OnStartTime = true;
				hasRateReview = false; 
			}
			ES2.Save (System.DateTime.Now.ToBinary ().ToString (), "RateReviewTimeLeftOffline");
		}
	}

	void OnApplicationQuit ()
	{
		ES2.Save (System.DateTime.Now.ToBinary ().ToString (), "RateReviewTimeLeftOffline");
		if (OnStartTime && checkifhasManagerRunning()) {

			ES2.Save (timeShowup, "RateReviewSetTimeV2"); 

		}

	}

	void OnApplicationPause (bool pauseStatus)
	{

		if (pauseStatus == true) {
			ES2.Save (System.DateTime.Now.ToBinary ().ToString (), "RateReviewTimeLeftOffline");
			if (OnStartTime && checkifhasManagerRunning()) {
				ES2.Save (timeShowup, "RateReviewSetTimeV2"); 
			}
		} else if (pauseStatus == false) {
			if (OnStartTime && checkifhasManagerRunning()) {
				OnCalculateTime ();
			}
			if (tapRateBtn) {
				OpenReward ();
			}
		}
	}


	string MyEscapeURL (string url)
	{
		return WWW.EscapeURL(url).Replace("+","%20");
	}

}
