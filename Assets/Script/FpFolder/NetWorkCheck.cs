﻿using UnityEngine;
using System.Collections;
using com.ootii.Messages;
using System.Net;
public class NetWorkCheck : MonoBehaviour {

	public bool networkState = false;

	private static NetWorkCheck _instance;
	public static NetWorkCheck Instance 
	{
		get {
			return _instance;
		}
	}

	IEnumerator checkInternetConnection(){
		while (true) {
			WWW www = new WWW("http://google.com");
			yield return www;
			if (www.error != null) {
//				print ("no net");
				networkState = false;
			} else {
//				print ("yes net");
				networkState = true;
			}
			yield return new WaitForSeconds (3);
		}
	} 

	void Start(){
		
		StartCoroutine(checkInternetConnection());
	}

	public bool getNetWorkStatus(){
		return networkState;
	}

//	private const bool allowCarrierDataNetwork = false;
//	private const string pingAddress = "8.8.8.8"; // Google Public DNS server
//	private const float waitingTime = 2.0f;
//
//	private Ping ping;
//	private float pingStartTime;
//
//	public void Start()
//	{
//		bool internetPossiblyAvailable;
//		switch (Application.internetReachability)
//		{
//		case NetworkReachability.ReachableViaLocalAreaNetwork:
//			internetPossiblyAvailable = true;
//			break;
//		case NetworkReachability.ReachableViaCarrierDataNetwork:
//			internetPossiblyAvailable = allowCarrierDataNetwork;
//			break;
//		default:
//			internetPossiblyAvailable = false;
//			break;
//		}
//		if (!internetPossiblyAvailable)
//		{
//			InternetIsNotAvailable();
//			return;
//		}
//		ping = new Ping(pingAddress);
//		pingStartTime = Time.time;
//	}
//
//	public void Update()
//	{
//		if (ping != null)
//		{
//			bool stopCheck = true;
//			if (ping.isDone)
//			{
//				if (ping.time >= 0)
//					InternetAvailable();
//				else
//					InternetIsNotAvailable();
//			}
//			else if (Time.time - pingStartTime < waitingTime)
//				stopCheck = false;
//			else
//				InternetIsNotAvailable();
//			if (stopCheck)
//				ping = null;
//		}
//	}
//
//	private void InternetIsNotAvailable()
//	{
//		MessageDispatcher.SendMessage(this, "OnNetWorkCheck", false , 0);
//		Debug.Log("No Internet :(");
//	}
//
//	private void InternetAvailable()
//	{
//		MessageDispatcher.SendMessage(this, "OnNetWorkCheck", true , 0);
//		Debug.Log("Internet is available! ;)");
//	}
}
