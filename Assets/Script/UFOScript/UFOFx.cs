﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class UFOFx : MonoBehaviour {

	// Use this for initialization
	void Start () {
		float posY = gameObject.transform.localPosition.y;
		gameObject.transform.DOLocalMoveY(posY+200,5).SetEase(Ease.Linear).SetLoops(999,LoopType.Yoyo);

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
