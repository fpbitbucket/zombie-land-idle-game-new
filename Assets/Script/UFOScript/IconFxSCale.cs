﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using com.ootii.Messages;

public class IconFxSCale : MonoBehaviour {
	public string nameIcon;
	
	void OnEnable() {
    MessageDispatcher.AddListener("OnAnimate_"+nameIcon, OnGotoCoins, true);
    MessageDispatcher.AddListener("OnAnimate_"+nameIcon+"_Big", OnGotoCoins, true);
	}

  void OnDisable() {
    MessageDispatcher.RemoveListener("OnAnimate_"+nameIcon, OnGotoCoins, true);
    MessageDispatcher.RemoveListener("OnAnimate_"+nameIcon+"_Big", OnGotoCoins, true);
	}

	void OnGotoCoins(IMessage rMessage)
  {
		gameObject.transform.DOScale(1.3f,0.2f).OnComplete(() => {
			gameObject.transform.DOScale(1,0.2f);
		});
	}
}
