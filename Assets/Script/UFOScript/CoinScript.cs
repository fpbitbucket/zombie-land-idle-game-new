﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using com.ootii.Messages;
using DG.Tweening;
public class CoinScript : MonoBehaviour
{
  public float Speed = 30;//,xPos = 0;
  public double CashAmout = 0;
  public Transform GotoObj;
  bool IsTapped = false;
  bool countdownSpawn = false;
  float timeShowup = 5;
  public string nameIcon;
  void Start()
  {
    gameObject.GetComponent<Button>().onClick.AddListener(() =>
    {
      countdownSpawn = false;
      IsTapped = true;
      MessageDispatcher.SendMessage(this, "OnGotoCoins", 0, 0);
    });
    // countdownSpawn = true;
    OnDropCoin();
  }

  void OnEnable()
  {
    MessageDispatcher.AddListener("OnGotoCoins", OnGotoCoinsAll, true);
  }

  void OnDisable()
  {
    MessageDispatcher.RemoveListener("OnGotoCoins", OnGotoCoinsAll, true);
  }

  void OnGotoCoinsAll(IMessage rMessage)
  {
    IsTapped = true;
    countdownSpawn = false;
    OnGotoCoins();
  }

  void OnGotoCoins()
  {
    gameObject.GetComponent<Canvas>().sortingOrder = 102;
    gameObject.transform.DOLocalMove(GotoObj.transform.localPosition, 0.5f).SetDelay(Random.Range(0.2f, 0.5f)).OnComplete(() =>
    {
      if (nameIcon == "Cash" || nameIcon == "Cash_Big")
      {
        MessageDispatcher.SendMessage(this, "OnIncrementMoney", CashAmout, 0);
      }
      else if (nameIcon == "RocketFuel" || nameIcon == "RocketFuel_Big")
      {
        MessageDispatcher.SendMessage(this, "OnIncrementGold", CashAmout, 0);
      }
      // gameObject.GetComponent<AudioSource>().Play();
      // gameObject.GetComponent<Image>().enabled = false;
      // MessageDispatcher.SendMessage(this, "CoinCashReward", 0, 0);
      MessageDispatcher.SendMessage(this, "OnAnimate_" + nameIcon, CashAmout, 0);
      Destroy(gameObject);
    });
  }
  
  void OnDropCoin()
  {
    gameObject.transform.DOLocalMoveX(Random.Range(-350, 238), 0.3f).SetEase(Ease.Linear);
    float yPosRandom = Random.Range(-460, 140);
    gameObject.transform.DOLocalMoveY(yPosRandom, 0.3f).SetEase(Ease.Linear).OnComplete(() =>
    {
      countdownSpawn = true;
    });
  }

  void Update()
  {
    if (countdownSpawn)
    {
      float tempDeltaTime = Time.deltaTime;
      timeShowup -= tempDeltaTime;
      if (timeShowup < 0)
      {
        OnGotoCoins();
        countdownSpawn = false;
      }
    }
  }
}
