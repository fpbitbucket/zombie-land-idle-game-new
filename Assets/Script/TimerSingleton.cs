﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;
using System.Collections;
using TMPro;
using com.ootii.Messages;
using DG.Tweening;
public class TimerSingleton : MonoBehaviour {

	private static TimerSingleton _instance;
	public static TimerSingleton instance
	{
		get
		{
			if(_instance == null)
			{
				_instance = GameObject.FindObjectOfType<TimerSingleton>();

				//Tell unity not to destroy this object when loading a new scene!
				DontDestroyOnLoad(_instance.gameObject);
			}

			return _instance;
		}
	}

	private TimeManager admultiplierStatus;
	private MainGeneratorScript mainGen;
	public double numOfflineErn; //10% of earnings every 5secs ingame 
	public string[] nameGenerators; //name of the generators  that will be load
	public double offlineSeconds = 5; //coins earn offline per seconds
	private double rewardGet = 0; //total reward to get
	DateTime currentDate; //current time when get back
	DateTime oldDate; //old date will save
	bool adplier = false,adMultiplierStatus = false;

//	public float openResortTime;
	public string ActiveScene = "";
	private float origTime;

	void Awake() 
	{
		
//		Application.targetFrameRate = 60;
//		Screen.SetResolution(600, 800, true);

		if(_instance == null)
		{
			//If I am the first instance, make me the Singleton
			_instance = this;
			DontDestroyOnLoad(this);
		}
		else
		{
			//If a Singleton already exists and you find
			//another reference in scene, destroy it!
			if(this != _instance)
			Destroy(this.gameObject);
		}

	}

	void OnEnable () {
//		MessageDispatcher.AddListener ("OnStartCountDownUnlockBusiness", OnStartCountDownUnlockBusiness, true);
		MessageDispatcher.AddListener ("OnStartChangeScene", OnStartChangeScene, true); 
		mainGen = GameObject.Find ("Generators").GetComponent<MainGeneratorScript> ();
		nameGenerators = mainGen.genNames_Code;
//		origTime = openResortTime;
		adplier = true;

//		if (ES2.Exists ("OnBuyNextWord")) {
//			if (ES2.Exists ("openNextWord")) {
//				openResortTime = ES2.Load<float> ("openNextWord");
//				if (openResortTime < 0) {
//					OpenResort = false;
//					MessageDispatcher.SendMessage(this, "OnOpenResortWorld", 0 , 0f);
//				} else {
//					OpenResort = true;
//				}
//			}
//		} else {
//			OpenResort = false;
//			ES2.Save (openResortTime, "openNextWord");
//		}



		MessageDispatcher.SendMessage(this, "OnLoginGameServices", 0 , 1f);
		if (Application.loadedLevelName == "MainScene") {
			CalculateReward ();
		}
	}
	void OnStartChangeScene (IMessage rMessage){
		ES2.Save(System.DateTime.Now.ToBinary().ToString(), "sysString_"+ActiveScene);
		ActiveScene = (string)rMessage.Data;
		SceneManager.LoadScene ("LoadScene");
	}

//	void OnStartCountDownUnlockBusiness (IMessage rMessage){
//		ES2.Save (1, "OnBuyNextWord");
//		OpenResort = true;
//	}

	public float getTimeOfflineSeconds(){
		float secondsInt = 0;
		if (ES2.Exists ("sysString")) {
			currentDate = System.DateTime.Now;
			long temp = Convert.ToInt64 (ES2.Load<string> ("sysString")); //load the time starts offline
			DateTime oldDate = DateTime.FromBinary (temp);
			TimeSpan difference = currentDate.Subtract (oldDate);
			secondsInt = (float)difference.TotalSeconds;

		}
		return secondsInt;
	}
		
	public float getTimeLeft(string genName) //get time left when generator has no manager yet
	{
		float numReturnValue = 0;
		if (ES2.Exists ("sysString_"+ActiveScene)) {
			currentDate = System.DateTime.Now;
			long temp = Convert.ToInt64(ES2.Load<string> ("sysString_"+ActiveScene)); //load the time starts offline
			DateTime oldDate = DateTime.FromBinary(temp);
			TimeSpan difference = currentDate.Subtract(oldDate);
			float genSecondsCycle = ES2.Load<float> ("btnName_" + genName + "?tag=progressTime");
			float tempGenTimeLeft = ES2.Load<float> ("btnName_" + genName+"?tag=time_GenLeft");
			float tempRemaining = genSecondsCycle - tempGenTimeLeft;

			if (tempGenTimeLeft < difference.TotalSeconds) { // time offline is bigger than time remaining
				numReturnValue = 0;
			} else { // time remaining is bigger than time offline
				numReturnValue = tempGenTimeLeft - (float)difference.TotalSeconds;
			}
		}
		return numReturnValue;
	}

	public float getTimeManager (string genName){ // get time left and cycle when generator has manager
		float numReturnValue = 0;
		if (ES2.Exists ("sysString_"+ActiveScene)) {
			currentDate = System.DateTime.Now;
			long temp = Convert.ToInt64 (ES2.Load<string> ("sysString_"+ActiveScene)); //load the time starts offline
			DateTime oldDate = DateTime.FromBinary (temp);
			TimeSpan difference = currentDate.Subtract (oldDate);
			int secondsInt = Convert.ToInt32 (difference.TotalSeconds); //seconds during offline
			float genSecondsCycle = ES2.Load<float> ("btnName_" + genName + "?tag=progressTime");
			float tempGenTimeLeft = ES2.Load<float> ("btnName_" + genName+"?tag=time_GenLeft");

			if (tempGenTimeLeft < difference.TotalSeconds) { // time offline is bigger than time remaining

				float getCycleNum = (float)difference.TotalSeconds / genSecondsCycle;
				float timeleftDouble = (float)difference.TotalSeconds % genSecondsCycle;
				double basicCost = ES2.Load<double> ("btnName_" + genName + "?tag=Amount");
				double unlockMultiplierOfProft = ES2.Load<double> ("btnName_" + genName + "?tag=UnlockedprofitMultiplier");
				double upgradeMultipliers = ES2.Load<double> ("btnName_" + genName + "?tag=UpgradeprofitMultiplier");
				double angelMultiplier = ES2.Load<double> ("btnName_" + genName + "?tag=angelMultiplier");
				int levelItem = ES2.Load<int> ("btnName_" + genName + "?tag=Level");
				double totalMultiplier = 0;
				double adMultipliers = 0;
				adMultiplierStatus = GameObject.Find ("TimeManager").GetComponent<TimeManager> ().adMultiplierStatus;
				if (adMultiplierStatus) {
					adMultipliers = 2;
				} else {
					adMultipliers = 0;
				}

				if (unlockMultiplierOfProft == 0 && upgradeMultipliers == 0 && adMultipliers == 0) {
					totalMultiplier = 1;
				} else {
					totalMultiplier = unlockMultiplierOfProft + upgradeMultipliers + adMultipliers;
				}

				double TotalGenratorEarnCycle = (basicCost * levelItem) * totalMultiplier * angelMultiplier;
				numReturnValue = tempGenTimeLeft;
			} else {
				numReturnValue = tempGenTimeLeft - (float)difference.TotalSeconds;
			}
		}
		return numReturnValue;
	}

	public void CalculateReward(){ //calculate Rewards
		if (ES2.Exists ("sysString")) {
			numOfflineErn = 0;
			for (int i = 0; i < nameGenerators.Length; i++) {
				if (ES2.Exists ("btnName_" + nameGenerators[i]) && ES2.Load<bool> ("btnName_" + nameGenerators[i] + "?tag=HasManager")) {// get 10% of every Generator earnings 

					if (ES2.Load<bool> ("btnName_" + nameGenerators[i] + "?tag=Status") == true) { // check if the multiplier is already buy or not

						double basicEarning = ES2.Load<double> ("btnName_" + nameGenerators[i] + "?tag=Amount"); //basic earnings of generator 
						double unlockMultiplierOfProft = ES2.Load<double> ("btnName_" + nameGenerators[i] + "?tag=UnlockedprofitMultiplier"); //unlocked Multiplier of generator 
						double angelMultiplier = ES2.Load<double> ("btnName_" + nameGenerators[i] + "?tag=angelMultiplier");	//angel Multiplier of generator 
						double upgradeMultipliers = ES2.Load<double> ("btnName_" + nameGenerators[i] + "?tag=UpgradeprofitMultiplier");	//upgrade Multiplier of generator 

						double adMultipliers = 0; //ads multiplier

						int levelItem = ES2.Load<int> ("btnName_" + nameGenerators[i]  + "?tag=Level"); //get level of the generator

						if (ES2.Exists ("AdMultiplierTime")) {
							if (ES2.Load<float> ("AdMultiplierTime") == 0) { // check if the add multipler is still running or not
								adMultipliers = 1;
							} else {
								adMultipliers = 2;
							}
						}
						//						Debug.Log ("Set AdMultiplierTime " + adMultipliers);

						double totalMultiplier = 0; // calculate the total multiplier
						if (unlockMultiplierOfProft == 0 && upgradeMultipliers == 0 && adMultipliers == 0) { //check if all multipler is 0 or else the answer for earnings will be always to 0
							totalMultiplier = 1;
						} else {
							totalMultiplier = unlockMultiplierOfProft + upgradeMultipliers + adMultipliers; // total of all multiplier
						}

						double TotalGenratorEarnCycle = (basicEarning * levelItem) * totalMultiplier * angelMultiplier; // get the total earnings for each generator

						numOfflineErn = numOfflineErn + (TotalGenratorEarnCycle * 0.10f); //get the earnings during offline and 10% of generator

					}
				}
			}


			currentDate = System.DateTime.Now;
			long temp = Convert.ToInt64(ES2.Load<string> ("sysString")); //load the time starts offline

			DateTime oldDate = DateTime.FromBinary(temp);
			TimeSpan difference = currentDate.Subtract(oldDate);

			int secondsInt = Convert.ToInt32(difference.TotalSeconds); //seconds during offline

//			Debug.Log (secondsInt + " offline time Seconds"); 

			if (secondsInt > 59) { // check if the offline time is higher than 1min to earn reward
				double rewardIn = secondsInt / offlineSeconds; //getting the reward total seconds time during offline divid to offline per seconds (5)
				rewardGet = rewardIn*numOfflineErn;

				double[] dataRay = new double[]{ rewardGet ,secondsInt};
//				Debug.Log (rewardGet + " reward to give"); 
				//				print ("reward " + rewardIn + " " + numOfflineErn);
				if (0.9f < rewardGet) {
					MessageDispatcher.SendMessage (this, "OnCheckReward", dataRay, 0.5f);
				} else {
					GameObject.Find ("RateReview").GetComponent<RateReviewScript> ().OnCheckIfHasRateReview ();
				}
			} else {
				GameObject.Find ("RateReview").GetComponent<RateReviewScript> ().OnCheckIfHasRateReview ();
			}

		} else {
			GameObject.Find ("RateReview").GetComponent<RateReviewScript> ().OnCheckIfHasRateReview ();
		}
	}

	

	void Update(){
//		if (OpenResort) {
//			float gamtetime = Time.deltaTime;
//			openResortTime -= gamtetime;
//			if (openResortTime < 0) {
//				OpenResort = false;
//				MessageDispatcher.SendMessage(this, "OnOpenResortWorld", 0 , 0.5f);
//				openResortTime = 0f;
//				ES2.Save (openResortTime, "openNextWord");
//			}
//		}
	}

	void OnApplicationQuit()
	{
		ES2.Save(System.DateTime.Now.ToBinary().ToString(), "sysString");
		ES2.Save(System.DateTime.Now.ToBinary().ToString(), "sysString_"+ActiveScene);
	}

	void OnApplicationPause( bool pauseStatus )
	{
		if (pauseStatus == true) {
			ES2.Save (System.DateTime.Now.ToBinary ().ToString (), "sysString");
			ES2.Save(System.DateTime.Now.ToBinary().ToString(), "sysString_"+ActiveScene);
			if (Application.loadedLevelName == "MainScene") {
				adplier = false;
			}

		} else if (pauseStatus == false) {

			currentDate = System.DateTime.Now;
			if (adplier == false && Application.loadedLevelName == "MainScene") {
				adplier = true;
				CalculateReward ();
			}
		}
	}
}
