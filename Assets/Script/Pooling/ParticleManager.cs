﻿using UnityEngine;
using System.Collections;
using com.ootii.Messages;

public class ParticleManager : MonoBehaviour {
	public GameObject prefabs;
	private IEnumerator coroutine;
	void Awake () {
		MessageDispatcher.AddListener ("OnParticle", OnParticle, true);
	}
	
	
	void OnParticle(IMessage rMessage){
		GameObjectUtil.Instantiate(prefabs, (Vector3)rMessage.Data);
	}
	

	void OnDisable(){
		MessageDispatcher.RemoveListener ("OnParticle", OnParticle, true);
		GameObjectUtil.clearAll ();

	}
}
