﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class TmproAnimBonus : MonoBehaviour {

	public float moveDis;
	public float timeMove;
	public bool isStopped = false;
	void Awake(){
		isStopped = false;
		gameObject.transform.DOLocalMoveY (gameObject.transform.localPosition.y + moveDis, timeMove).SetEase(Ease.Linear).OnComplete (OnCompleteAnim);
	}

	void OnCompleteAnim(){
		
		Destroy (gameObject);
		isStopped = true;
	}
}
