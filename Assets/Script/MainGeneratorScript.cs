﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class MainGeneratorScript : MonoBehaviour {
	public GeneratorScript gen;
	public GameObject totalMultiplier;
	public Sprite[] genIcons;
	public RuntimeAnimatorController[] genAnims;

	public string[] genNames_Code;
	public string[] genNamesString;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (gen.TotalMultiplierAdditive != 0) {
			totalMultiplier.SetActive (true);
		} else if (gen.TotalMultiplierAdditive == 0) {
			totalMultiplier.SetActive (false);
		}
		totalMultiplier.GetComponentInChildren<TextMeshProUGUI>().text = "MULTIPLIER"+" X"+gen.TotalMultiplierAdditive;
	}
}
