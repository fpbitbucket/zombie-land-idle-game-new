﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using com.ootii.Messages;

public class StartPackScript : MonoBehaviour
{
	public MoneyManager mainMoney;
	public GameObject[] PackPopup, starterPackBtns;
	public GameObject[] MoneyStrings;
	public GameObject[] TimerString, TimerStringBtn;
//	public GameObject startPackIdleBtn;
	DateTime currentDate;
	//current time when get back
	DateTime oldDate;
	//old date will save
	public float timeEvent, timeEventHolder = 0f;
	public bool startEventTime = false;//IdleState = false;
	public GameObject starterPackBtnShop;
	public float timeIdle = 600f;//,timeIdleTemp= 0;

	void OnEnable ()
	{
		
//		timeIdleTemp = timeIdle;
//		startPackIdleBtn.SetActive (false);

		for (int i = 0; i < PackPopup.Length; i++) {
			PackPopup [i].SetActive (false);
		}

		if (ES2.Exists ("DoneStarterPack" + mainMoney.worldName)) {
			starterPackBtnShop.SetActive (false);
			for (int i = 0; i < starterPackBtns.Length; i++) {
				starterPackBtns [i].SetActive (false);
			}
			if (GameObject.Find ("WeekendDealsHandler")) {
				GameObject.Find ("WeekendDealsHandler").GetComponent<WeekendDealsHandler> ().OnStartWeekendDeal ();
			}

			MessageDispatcher.SendMessage (this, "OnShowExplorationBtn", 0, 0.5f);
//			MessageDispatcher.SendMessage (this, "OnStartSpinBtn", 0, 0.5f);	
		} else {
			if (ES2.Exists ("ShowStarterPackFirstTime")) {
				startEventTime = true;
//				IdleState = true;
//				MessageDispatcher.SendMessage (this, "OnStartSpinBtn", 0, 0.5f);	
				MessageDispatcher.SendMessage (this, "OnShowExplorationBtn", 0, 0.5f);
				starterPackBtnShop.SetActive (true);
				for (int i = 0; i < starterPackBtns.Length; i++) {
					starterPackBtns [i].SetActive (true);
				}
				GameObject.Find("ShopManager").GetComponent<ShopManager>().ShopSpaceObj.SetActive (false);
			} else {
				GameObject.Find("ShopManager").GetComponent<ShopManager>().ShopSpaceObj.SetActive (true);
				starterPackBtnShop.SetActive (false);
				for (int i = 0; i < starterPackBtns.Length; i++) {
					starterPackBtns [i].SetActive (false);
				}
			}
		}



		StartCoroutine (OnCheckCurrencyShop ());
		timeEventHolder = timeEvent;
		CloseScreen ();
		MessageDispatcher.AddListener ("OnCloseStarterPack", OnCloseStarterPack, true); 
		MessageDispatcher.AddListener ("OnSuccessBuyStarterPack", OnSuccessBuyStarterPack, true); 

		if (ES2.Exists ("DoneStarterPackTime_1" + mainMoney.worldName)) {
			CalculateTime ();
		}
	}

	void OnDisable ()
	{
		MessageDispatcher.RemoveListener ("OnCloseStarterPack", OnCloseStarterPack, true);
		MessageDispatcher.RemoveListener ("OnSuccessBuyStarterPack", OnSuccessBuyStarterPack, true);
	}

	void OnSuccessBuyStarterPack (IMessage rMessage)
	{
		double moneyGet = 1000000000;
		MessageDispatcher.SendMessage (this, "OnIncrementMoney", moneyGet, 0);	
		timeEventHolder = 0f;
	}

	void OnCloseStarterPack (IMessage rMessage)
	{
		CloseScreen ();
	}

	public void OnShowPackPopup (string fromTap)
	{
		MessageDispatcher.SendMessage(this, "OnTapGA_Report","StartPackPopupOpen_"+fromTap, 0);
//		IdleState = false;
		for (int i = 0; i < PackPopup.Length; i++) {
			PackPopup [i].SetActive (true);
		}
	}

	public void OnClosePackPopup ()
	{
		MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
		CloseScreen ();
	}

	void CloseScreen ()
	{
//		IdleState = true;
//		timeIdleTemp = timeIdle;
		MessageDispatcher.SendMessage(this, "OnTapGA_Report","StartPackPopupClose", 0);
		for (int i = 0; i < PackPopup.Length; i++) {
			PackPopup [i].SetActive (false);
		}
	}

	private IEnumerator OnCheckCurrencyShop ()
	{ 
		bool OnCheckCurreny = true;
		int numIapCurrency = GameObject.Find ("GameTimer").GetComponent<IAP_Script> ().StringtempCurrency.Length;
		while (OnCheckCurreny) {
			numIapCurrency = GameObject.Find ("GameTimer").GetComponent<IAP_Script> ().StringtempCurrency.Length;
			yield return new WaitForSeconds (2f);
//			Debug.Log (numIapCurrency + " Currency Count"); 
			if (numIapCurrency == 8) {
				OnCheckCurreny = false;
				OnUpdateCurreny ();
			}
		}
	}


	void OnUpdateCurreny ()
	{
		for (int i = 0; i < MoneyStrings.Length; i++) {
			MoneyStrings [i].GetComponent<TextMeshProUGUI> ().text = GameObject.Find ("GameTimer").GetComponent<IAP_Script> ().StringtempCurrency [7];
		}
	}

	public void OnShowStarterPackFromUpgrade(){
		if (GameObject.Find ("Managers/TutorialManager").GetComponent<TutorialManager> ().tutorialStats [13] == true && ES2.Exists ("ShowStarterPackFirstTime") == false) {
//			MessageDispatcher.SendMessage (this, "OnStartSpinBtn", 0, 0.5f);
			MessageDispatcher.SendMessage (this, "OnShowExplorationBtn", 0, 0.5f);	
			ES2.Save(1, "ShowStarterPackFirstTime");
			startEventTime = true;
//			IdleState = true;

			starterPackBtnShop.SetActive (true);
			for (int i = 0; i < starterPackBtns.Length; i++) {
				starterPackBtns [i].SetActive (true);
			}
			GameObject.Find("ShopManager").GetComponent<ShopManager>().ShopSpaceObj.SetActive (false);
		}
	}

	public void OnShowStartPackBtn(){
//		IdleState = true;
//		timeIdleTemp = timeIdle;
		GameObject.Find("ShopManager").GetComponent<ShopManager>().ShopSpaceObj.SetActive (false);
		starterPackBtnShop.SetActive (true);
		for (int i = 0; i < starterPackBtns.Length; i++) {
			starterPackBtns [i].SetActive (true);
		}
//		startPackIdleBtn.SetActive (false);
	}

	void Update ()
	{
		if (startEventTime) { 
			float tempDeltaTime = Time.deltaTime;
			timeEventHolder -= tempDeltaTime;
			if (timeEventHolder < 0) {
				startEventTime = false;
//				IdleState = false;

				starterPackBtnShop.SetActive (false);
				for (int i = 0; i < starterPackBtns.Length; i++) {
					starterPackBtns [i].SetActive (false);
				}

				for (int i = 0; i < PackPopup.Length; i++) {
					PackPopup [i].SetActive (false);
				}

//				startPackIdleBtn.SetActive (false);
				ES2.Save (1, "DoneStarterPack" + mainMoney.worldName);
				ES2.Save (0f, "DoneStarterPackTime_1" + mainMoney.worldName);
				timeEventHolder = 0f;

				if (GameObject.Find ("WeekendDealsHandler")) {
					GameObject.Find ("WeekendDealsHandler").GetComponent<WeekendDealsHandler> ().OnStartWeekendDeal ();
				}
//				MessageDispatcher.SendMessage (this, "OnStartSpinBtn", 0, 0.5f);
				MessageDispatcher.SendMessage (this, "OnShowExplorationBtn", 0, 0.5f);	

			}
			System.TimeSpan t = System.TimeSpan.FromSeconds (timeEventHolder);
			for (int i = 0; i < MoneyStrings.Length; i++) {
				TimerString [i].GetComponent<TextMeshProUGUI> ().text = ((int)t.TotalHours).ToString ("d2") + ":" + t.Minutes.ToString ("d2") + ":" + t.Seconds.ToString ("d2");
				TimerStringBtn [i].GetComponent<TextMeshProUGUI> ().text = ((int)t.TotalHours).ToString ("d2") + ":" + t.Minutes.ToString ("d2") + ":" + t.Seconds.ToString ("d2");
			}

//			if (IdleState) {
//				timeIdleTemp -= tempDeltaTime;
//				if (timeIdleTemp < 0) {
//					IdleState = false;
//					timeIdleTemp = timeIdle;
//					for (int i = 0; i < starterPackBtns.Length; i++) {
//						starterPackBtns [i].SetActive (false);
//					}
//					startPackIdleBtn.SetActive (true);
//
//				}
//			}
		}
	}

	void CalculateTime ()
	{
		if (ES2.Exists ("DoneStarterPackTime_1" + mainMoney.worldName)) {
			string ActiveScene = GameObject.Find ("GameTimer").GetComponent<TimerSingleton> ().ActiveScene;
			currentDate = System.DateTime.Now;
			long temp = 0;
			if (ES2.Exists ("sysString_" + ActiveScene)) {
				temp = Convert.ToInt64 (ES2.Load<string> ("sysString_" + ActiveScene));
			}
//			long temp = Convert.ToInt64 (ES2.Load<string> ("sysString_" + ActiveScene)); //load the time starts offline
			DateTime oldDate = DateTime.FromBinary (temp);
			TimeSpan difference = currentDate.Subtract (oldDate);
			int secondsInt = Convert.ToInt32 (difference.TotalSeconds); //seconds during offline
			float tempGenTimeLeft = ES2.Load<float> ("DoneStarterPackTime_1" + mainMoney.worldName);
			float numReturnValue = 0;
//			Debug.Log(secondsInt + " " + tempGenTimeLeft + " Starter PAck Timer" );
			if (tempGenTimeLeft < difference.TotalSeconds) { // time offline is bigger than time remaining
				numReturnValue = 0;
			} else { // time remaining is bigger than time offline
				numReturnValue = tempGenTimeLeft - (float)difference.TotalSeconds;
			}
			timeEventHolder = numReturnValue;
		}
	}

	void OnApplicationQuit ()
	{

		if (startEventTime == true) {
			ES2.Save (timeEventHolder, "DoneStarterPackTime_1" + mainMoney.worldName); 
		}
	}

	void OnApplicationPause (bool pauseStatus)
	{
//		Debug.Log (pauseStatus + " " + startEventTime + " " + timeEventHolder + " DoneStarterPackTime_1");
		if (pauseStatus == true) {
			if (startEventTime == true) {
				ES2.Save (timeEventHolder, "DoneStarterPackTime_1" + mainMoney.worldName); 
			}

		} else if (pauseStatus == false) {
			if (startEventTime == true) {
				CalculateTime ();
			} else {
				if (ES2.Exists ("DoneStarterPack" + mainMoney.worldName) && ES2.Exists ("DoneStarterPackTime_1" + mainMoney.worldName)) {

					if (GameObject.Find ("WeekendDealsHandler")) {
						Debug.Log ("WeekendDealsHandler Start");
						GameObject.Find ("WeekendDealsHandler").GetComponent<WeekendDealsHandler> ().OnStartWeekendDeal ();
					}
//					MessageDispatcher.SendMessage (this, "OnStartSpinBtn", 0, 0.5f);	
					MessageDispatcher.SendMessage (this, "OnShowExplorationBtn", 0, 0.5f);
				}
			}
		}

	}

}
