﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;
using System.Collections;
using TMPro;
using com.ootii.Messages;
using DG.Tweening;
public class CrossPromoScript : MonoBehaviour {
	public GameObject CrosspromoObj;

	public float timeShowup;
	float timeShowupTemp;

	public bool OnStartTime = false,HasCrosspromo = false,HasTheApp = false; 
	void OnEnable () {
		OnStartTime = true;
		timeShowupTemp = timeShowup;
		CrosspromoObj.SetActive (false);
		#if UNITY_EDITOR
		HasTheApp = false;
		#else
		HasTheApp = IsAppInstalled("com.kuronekogames.cryptocapitalist");
		#endif
		if (HasTheApp == false) {
			if (ES2.Exists ("CrossPromoSetTime")) {
				timeShowup = ES2.Load<float> ("CrossPromoSetTime");
				OnCalculateTime ();
			} else {
				HasCrosspromo = true; 
				OnStartTime = false;
				ES2.Save (0f, "CrossPromoSetTime"); 
				CrosspromoObj.SetActive (true);
			}
		}
	}

	public void OnCloseCrossPromo () {
		HasCrosspromo = false; 
		timeShowup = timeShowupTemp;
		ES2.Save (timeShowup, "CrossPromoSetTime"); 
		OnStartTime = true;
		CrosspromoObj.SetActive (false);
	}

	public void OnOpenCrossPromo(){
		OnCloseCrossPromo ();
		#if UNITY_ANDROID
		Application.OpenURL ("https://play.google.com/store/apps/details?id=com.kuronekogames.cryptocapitalist");
		#endif
		#if UNITY_IOS
//		Application.OpenURL (iOSRateMeLink);
		#endif
	}

	DateTime currentDate;
	void OnCalculateTime ()
	{
		if (ES2.Exists ("CrossPromoSetTime")) {
			OnStartTime = false;
			currentDate = System.DateTime.Now;
			long temp = 0;
			if (ES2.Exists ("CrossPromoTimeLeftOffline")) {
				temp = Convert.ToInt64 (ES2.Load<string> ("CrossPromoTimeLeftOffline")); //load the time starts offline
			} 
			DateTime oldDate = DateTime.FromBinary (temp);
			TimeSpan difference = currentDate.Subtract (oldDate);
			//			int secondsInt = Convert.ToInt32 (difference.TotalSeconds); //seconds during offline
			float tempGenTimeLeft = ES2.Load<float> ("CrossPromoSetTime");
			float numReturnValue = 0;

			if (tempGenTimeLeft < difference.TotalSeconds) { // time offline is bigger than time remaining
				numReturnValue = 0;
				timeShowup = timeShowupTemp;
				OnStartTime = false;
				ES2.Save (0f, "CrossPromoSetTime"); 
				HasCrosspromo = true;
				CrosspromoObj.SetActive (true);
			} else { // time remaining is bigger than time offline
				numReturnValue = tempGenTimeLeft - (float)difference.TotalSeconds;
				ES2.Save (numReturnValue, "CrossPromoSetTime"); 
				timeShowup = numReturnValue;
				OnStartTime = true;
			}

			//			Debug.Log(numReturnValue + " OffLine Time - " + difference.TotalSeconds + " " + temp);
			ES2.Save (System.DateTime.Now.ToBinary ().ToString (), "CrossPromoTimeLeftOffline");
		}
	}

	public  bool IsAppInstalled(string bundleID){
		#if UNITY_ANDROID
		AndroidJavaClass up = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject ca = up.GetStatic<AndroidJavaObject>("currentActivity");
		AndroidJavaObject packageManager = ca.Call<AndroidJavaObject>("getPackageManager");
		Debug.Log(" ********LaunchOtherApp ");
		AndroidJavaObject launchIntent = null;
		//if the app is installed, no errors. Else, doesn't get past next line
		try{
			launchIntent = packageManager.Call<AndroidJavaObject>("getLaunchIntentForPackage",bundleID);
			//        
			//        ca.Call("startActivity",launchIntent);
		}catch(Exception ex){
			Debug.Log("exception"+ex.Message);
		}
		if(launchIntent == null)
			return false;
		return true;
		#else
		return false;
		#endif
	}

	void Update ()
	{
		if (OnStartTime && HasTheApp == false) {
			float tempDeltaTime = Time.deltaTime;
			timeShowup -= tempDeltaTime;
			if (timeShowup < 0) {
				timeShowup = 0;
				OnStartTime = false;
				ES2.Save (0f, "CrossPromoSetTime"); 
				HasCrosspromo = true;
			}
		}
	}

	void OnApplicationQuit ()
	{
		ES2.Save (System.DateTime.Now.ToBinary ().ToString (), "CrossPromoTimeLeftOffline");
		if (OnStartTime && HasTheApp == false) {
			ES2.Save (timeShowup, "CrossPromoSetTime"); 
		}

	}

	void OnApplicationPause (bool pauseStatus)
	{

		if (pauseStatus == true) {
			ES2.Save (System.DateTime.Now.ToBinary ().ToString (), "CrossPromoTimeLeftOffline");
			if (OnStartTime) {
				ES2.Save (timeShowup, "CrossPromoSetTime"); 
			}
		} else if (pauseStatus == false) {
			if (OnStartTime && HasCrosspromo == false && HasTheApp == false) {
				OnCalculateTime ();
			}
		}
	}

}
