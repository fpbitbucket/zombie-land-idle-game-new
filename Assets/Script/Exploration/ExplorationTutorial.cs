﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using com.ootii.Messages;
public class ExplorationTutorial : MonoBehaviour {
	public GameObject[] TutorialPanels;
	public string[] tutorialMessage;
	public GameObject[] TutorialObjPort;
	int numTutorialShowing = 0;

	public List<bool> tutorialActive;

	void OnEnable () {
		OnCloseTutorialsPanel ();
		OnCloseTutorials ();
		if (ES2.Exists ("ExplorationTutorial")) {
			tutorialActive = ES2.LoadList<bool> ("ExplorationTutorial");
		} else {
			ES2.Save(tutorialActive, "ExplorationTutorial");
		}

		if (ES2.Exists ("ActiveExploration")) {
			for (int i = 0; i < tutorialActive.Count; i++) {
				tutorialActive [i] = true;
			}
			ES2.Save (tutorialActive, "ExplorationTutorial");

		}
//		if (tutorialActive [3] == true) {
//			MessageDispatcher.SendMessage (this, "OnStartSpinBtn", 0, 0.5f);
//		}

		MessageDispatcher.AddListener ("ExplorationTutorial", OnShowTutorial, true); 
		MessageDispatcher.AddListener ("ExplorationTutorialClose", OnTutorialClose, true); 
	}

	void OnDisable () {
		MessageDispatcher.RemoveListener ("ExplorationTutorial", OnShowTutorial, true); 
		MessageDispatcher.RemoveListener ("ExplorationTutorialClose", OnTutorialClose, true); 
	}

	void OnShowTutorial(IMessage rMessage){
		int num = (int)rMessage.Data;
		if (tutorialActive [num] == false) {
			OnCloseTutorials ();
			OnShowTutorialsPanel ();

			TutorialObjPort[num].SetActive (true);
			TutorialObjPort [num].transform.GetChild (2).GetComponentInChildren<TextMeshProUGUI> ().text = tutorialMessage [num];

			if (num == 0 || num == 1) {
				TutorialObjPort [num].transform.GetChild (1).gameObject.SetActive (true);
			} else if (num == 3){
				MessageDispatcher.SendMessage (this, "ExplorationTutorialClose",3, 5f);
			}


		}
	

	}

	void OnTutorialClose(IMessage rMessage){
		int num = (int)rMessage.Data;
		if (tutorialActive [num] == false) {
			OnCloseTutorialsPanel ();
			OnCloseTutorials ();
			tutorialActive [num] = true;
			ES2.Save (tutorialActive, "ExplorationTutorial");
			if (num == 3) {
//				MessageDispatcher.SendMessage (this, "OnStartSpinBtn", 0, 0.5f);
			}
		}
	}

	void OnCloseTutorialsPanel(){
		for (int i = 0; i < TutorialPanels.Length; i++) {
			TutorialPanels [i].SetActive (false);
		}
	}

	void OnShowTutorialsPanel(){
		for (int i = 0; i < TutorialPanels.Length; i++) {
			TutorialPanels [i].SetActive (true);
		}
	}


	void OnCloseTutorials(){
		for (int i = 0; i < TutorialPanels.Length; i++) {
			TutorialPanels [i].SetActive (false);
		}

		for (int i = 0; i < TutorialObjPort.Length; i++) {
			TutorialObjPort [i].SetActive (false);
			TutorialObjPort [i].transform.GetChild (1).gameObject.SetActive (false);
		}

	}
}
