﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using com.ootii.Messages;
using TMPro;
public class ExplorationMultiplierTimeHandler : MonoBehaviour {
	public bool OnStartTime = false; 
	public float timeShowup;
	float timeShowupTemp;
	public double numExplorationMultiplier = 0;
	public GameObject[] btnMultiplier;
	void OnEnable () {
		timeShowupTemp = timeShowup;
		for (int i = 0;i < btnMultiplier.Length; i++) {
			btnMultiplier [i].transform.GetChild (1).gameObject.SetActive (false);
		}
		MessageDispatcher.AddListener ("OnStartMultiplierExploration", OnStartMultiplierExploration, true);
		if (ES2.Exists ("ExplorerMultiplierSetTime")) {
			timeShowup = ES2.Load<float> ("ExplorerMultiplierSetTime");

		} else {
			
		}

		if (ES2.Exists ("ExplorerMultiplier")) {
			numExplorationMultiplier = ES2.Load<double> ("ExplorerMultiplier");
		}
		OnCalculateTime ();
	}

	void OnDisable(){
		MessageDispatcher.RemoveListener ("OnStartMultiplierExploration", OnStartMultiplierExploration, true);

		ES2.Save (System.DateTime.Now.ToBinary ().ToString (), "ExplorerMultiplierTimeLeftOffline");
	}


	void OnStartMultiplierExploration(IMessage rMessage){
		double[] timeGiven = (double[])rMessage.Data;
		timeShowup = (float)timeGiven[0];
		ES2.Save (timeGiven[1], "ExplorerMultiplier"); 
		numExplorationMultiplier = timeGiven [1];
		OnStartTime = true; 
		OnAddMultiplier ();
	}

	void OnAddMultiplier(){
		MessageDispatcher.SendMessage(this, "OnSetExplorationMultiplier",numExplorationMultiplier, 0.3f);
		string multiplierString = "";
		if (numExplorationMultiplier != 0) {
			multiplierString = "x" + numExplorationMultiplier.ToString ();
			for (int i = 0; i < btnMultiplier.Length; i++) {
				btnMultiplier [i].SetActive (true);
				btnMultiplier [i].transform.GetChild (1).gameObject.SetActive (true);
				btnMultiplier [i].transform.GetChild (1).Find ("MultiplierText").GetComponent<TextMeshProUGUI> ().text = multiplierString;
			}
		} else if (numExplorationMultiplier == 0) {
			for (int i = 0; i < btnMultiplier.Length; i++) {
				btnMultiplier [i].SetActive (false);
			}
		}

	}

	void Update ()
	{
		if (OnStartTime) {
			float tempDeltaTime = Time.deltaTime;
			timeShowup -= tempDeltaTime;
			if (timeShowup < 0) {
				timeShowup = timeShowupTemp;
				OnStartTime = false;
				ES2.Save (0f, "ExplorerMultiplierSetTime"); 
				numExplorationMultiplier = 0;
				ES2.Save (numExplorationMultiplier, "ExplorerMultiplier"); 
				OnAddMultiplier ();
			}
			System.TimeSpan t2 = System.TimeSpan.FromSeconds (timeShowup);
			string boostTimeString = ((int)t2.TotalHours).ToString ("d2") + ":" + t2.Minutes.ToString ("d2") + ":" + t2.Seconds.ToString ("d2");

			for (int i = 0; i < btnMultiplier.Length; i++) {
				btnMultiplier [i].transform.GetChild (1).Find("Timer").GetComponent<TextMeshProUGUI> ().text = boostTimeString;
			}
		}
	}

	DateTime currentDate;
	void OnCalculateTime ()
	{
		if (ES2.Exists ("ExplorerMultiplierSetTime")) {
			currentDate = System.DateTime.Now;
			long temp = 0;
			if (ES2.Exists ("ExplorerMultiplierTimeLeftOffline")) {
				temp = Convert.ToInt64 (ES2.Load<string> ("ExplorerMultiplierTimeLeftOffline")); //load the time starts offline
			} 
			DateTime oldDate = DateTime.FromBinary (temp);
			TimeSpan difference = currentDate.Subtract (oldDate);
			int secondsInt = Convert.ToInt32 (difference.TotalSeconds); //seconds during offline
			float tempGenTimeLeft = ES2.Load<float> ("ExplorerMultiplierSetTime");
			float numReturnValue = 0;

			if (tempGenTimeLeft < difference.TotalSeconds) { // time offline is bigger than time remaining
				numReturnValue = 0;
				timeShowup = timeShowupTemp;
				OnStartTime = false;
				ES2.Save (0f, "ExplorerMultiplierSetTime"); 

				for (int i = 0; i < btnMultiplier.Length; i++) {
					btnMultiplier [i].transform.GetChild (1).gameObject.SetActive (false);
				}
			} else { // time remaining is bigger than time offline
				numReturnValue = tempGenTimeLeft - (float)difference.TotalSeconds;
				timeShowup = numReturnValue;
				OnStartTime = true;
				OnAddMultiplier ();
			}
			ES2.Save (System.DateTime.Now.ToBinary ().ToString (), "ExplorerMultiplierTimeLeftOffline");
		}
	}

	void OnApplicationQuit ()
	{
		ES2.Save (System.DateTime.Now.ToBinary ().ToString (), "ExplorerMultiplierTimeLeftOffline");
		if (OnStartTime) {
			ES2.Save (timeShowup, "ExplorerMultiplierSetTime"); 
		}
	}


	void OnApplicationPause (bool pauseStatus)
	{

		if (pauseStatus == true) {
			ES2.Save (System.DateTime.Now.ToBinary ().ToString (), "ExplorerMultiplierTimeLeftOffline");
			if (OnStartTime) {
				ES2.Save (timeShowup, "ExplorerMultiplierSetTime"); 
			}
		} else if (pauseStatus == false) {
			if (OnStartTime) {
				OnStartTime = false;
				OnCalculateTime ();
			}
		}
	}
}
