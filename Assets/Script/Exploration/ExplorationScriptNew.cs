﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using com.ootii.Messages;
public class ExplorationScriptNew : MonoBehaviour {

	public GeneratorScript[] generatorsObj;

	public GameObject[] explorationPanel;
	public GameObject[] explorationBoostWarning;
	public GameObject[] RewardPpopup;

	public bool OnStartTime = false; 

	public float timeShowupTemp,timeShowup;
	bool OnSetupUI = false;
	public string stringActiveExploration = "";
	TextMeshProUGUI[] activeTextExplocation;
	public bool OnCompleteExploration = false, OnCanBoostExploration = false;


//	public float[] timeMultiplierSet1;
//	public double[] moneySet_1;
//	public double[] chipSet_1;
//	public double[] MultiplierSet1;
//	public string[] RareNameReward_Set1;
//	
	public double RareChipsReward;
	public double RareMultiplierReward;
	public int RareTimeWarpReward;
	public float RareTimeMultiplierReward;
	public double[] CommonChipsReward;
	public double[] CommonMultiplierReward;
	public int[] CommonTimeWarpReward;
	public float[] CommonTimeMultiplierReward;

	public double chipsReward;
	public double MultiplierReward;
	public double TimeWarpReward;
	public float TimeMultiplierReward;

	public float RareTimeExplore = 0;
	public float[] CommontimeExplore;

	public float timeBoost = 3600;
	public float timeBoostTemp = 0;

	public bool OnActiveScreen = false;

	public GameObject[] btnMultiplier;

	public Sprite[] IconReward;
	public int TotalNumberBoost = 2;

	string dayToday = "";
	int numReward = 0;
	void OnEnable () {
		DateTime date = DateTime.Now;
		string dateToday = date.ToString ("d");
		DayOfWeek day = DateTime.Now.DayOfWeek;
		dayToday = day.ToString ();
		activeTextExplocation = new TextMeshProUGUI[explorationPanel.Length];
		OnSetupExploration (true);
		OnActiveScreen = false;
		OnhideButton ();
		if (ES2.Exists ("ActiveExploration")) {
			if(string.IsNullOrEmpty(ES2.Load<string> ("ActiveExploration")) == false){
				stringActiveExploration = ES2.Load<string> ("ActiveExploration");

				if (stringActiveExploration == "Rare") {
					activeExploration (0);
				}else if (stringActiveExploration == "Common0") {
					activeExploration (0);
				}else if (stringActiveExploration == "Common1") {
					activeExploration (1);
				}else if (stringActiveExploration == "Common2") {
					activeExploration (2);
				}
				if (ES2.Exists ("ExplorationSetTime")) {
					OnCalculateTime ();
				}
			};
		}

		if (ES2.Exists ("TotalNumberBoost")) {
			TotalNumberBoost = ES2.Load<int> ("TotalNumberBoost");
		} else {
			ES2.Save (TotalNumberBoost, "TotalNumberBoost");
		}

		for (int i = 0; i < explorationBoostWarning.Length; i++) {
			explorationBoostWarning [i].transform.Find ("NoBoost/OkBtn").GetComponent<Button>().onClick.AddListener(() => { OnCloseBoostWarning(); });
			explorationBoostWarning [i].SetActive (false);
		}

		for (int i = 0; i < RewardPpopup.Length; i++) {
			RewardPpopup [i].transform.Find ("bg").GetComponent<Button>().onClick.AddListener(() => { OnStartCollecRewards(""); });
			RewardPpopup [i].SetActive (false);
		}

		for (int i = 0; i < explorationPanel.Length; i++) {
			explorationPanel [i].SetActive (false);
			explorationPanel [i].transform.GetChild (2).transform.Find ("StartBtn").GetComponent<Button>().onClick.AddListener(() => { OnSelectExploration(); });

			GameObject ItemContainer = explorationPanel [i].transform.GetChild (4).transform.Find ("Expedition/Scroll/ItemContainer").gameObject;

			for (int l = 0; l < ItemContainer.transform.childCount; l++) {
				if (l == 0) {
					ItemContainer.transform.GetChild(l).Find("Body/Bottom/StartBtn").GetComponent<Button>().onClick.AddListener(() => { OnStartRareExploration(); });
				} else {
					int numCommon = l - 1;
					ItemContainer.transform.GetChild(l).Find("Body/Bottom/StartBtn").GetComponent<Button>().onClick.AddListener(() => { OnStartCommonExploration(numCommon); });
				}
			}

			explorationPanel [i].transform.GetChild(3).transform.Find ("Body/Bottom/StartBtn").GetComponent<Button>().onClick.AddListener(() => { OnCollectExploration(); });
			explorationPanel [i].transform.Find ("ExitBtn").GetComponent<Button>().onClick.AddListener(() => { OnCloseExplorationPopup(); });

		}
		MessageDispatcher.AddListener ("OnDoneWatchingBoost", OnDoneWatchingBoost, true);
		MessageDispatcher.AddListener ("OnShowExplorationBtn", OnShowExplorationBtn, true);
	
	}

	void OnDisable(){
		MessageDispatcher.RemoveListener ("OnDoneWatchingBoost", OnDoneWatchingBoost, true);
		MessageDispatcher.RemoveListener ("OnShowExplorationBtn", OnShowExplorationBtn, true);
		for (int i = 0; i < explorationPanel.Length; i++) {

			//			if (explorationPanel [i].transform.GetChild(3).gameObject) {
			//				explorationPanel [i].transform.GetChild(3).GetComponent<Button>().onClick.RemoveAllListeners();
			//			}
			//			if (explorationPanel [i].transform.GetChild(2).transform.Find ("Scroll/ItemContainer/RareItem/Body/Bottom/StartBtn").gameObject) {
			//				explorationPanel [i].transform.GetChild(2).transform.Find("Scroll/ItemContainer/RareItem/Body/Bottom/StartBtn").GetComponent<Button>().onClick.RemoveAllListeners();
			//			}
			//			if (explorationPanel [i].transform.GetChild(2).transform.Find("Scroll/ItemContainer/CommonItem/Body/Bottom/StartBtn").gameObject) {
			//				explorationPanel [i].transform.GetChild(2).transform.Find("Scroll/ItemContainer/CommonItem/Body/Bottom/StartBtn").GetComponent<Button>().onClick.RemoveAllListeners();
			//			}
		}

		ES2.Save (System.DateTime.Now.ToBinary ().ToString (), "ExplorationTimeLeftOffline");
	}

	void OnShowExplorationBtn(IMessage rMessage){
		
		// MessageDispatcher.SendMessage (this, "OnStartExplorationTutorialTime", 0, 0f);

		Debug.Log ("Day Today " + dayToday);
		bool canShowButton = false;
		if (dayToday != "Wednesday" && dayToday != "Sunday") {
			for (int i = 0; i < explorationPanel.Length; i++) {
				GameObject ItemContainer = explorationPanel [i].transform.GetChild (4).transform.Find ("Expedition/Scroll/ItemContainer").gameObject;
				for (int l = 0; l < ItemContainer.transform.childCount; l++) {
					if (l == 0) {
						ItemContainer.transform.GetChild (l).gameObject.SetActive (false);
					} 
				}
			}
		} else {
		
		}
		OnShowButtons ();
		MessageDispatcher.SendMessage (this, "ExplorationTutorial", 0, 0.25f);
	}

	void OnDoneWatchingBoost(IMessage rMessage){

		TotalNumberBoost--;
		if ((timeShowup - 1800) < 0) {
			timeShowup = 0;
		} else {
			timeShowup = timeShowup - 1800;
		}
		if (TotalNumberBoost <= 0) {
			timeBoostTemp = timeBoost;
			OnCanBoostExploration = false;
			TotalNumberBoost = 2;
			ES2.Save (TotalNumberBoost, "TotalNumberBoost");
		} else {
			ES2.Save (TotalNumberBoost, "TotalNumberBoost");
		}
		OnStartBoost ();
	}

	void OnStartBoost(){
		string boostTimeString = "Get your boost and finish faster!!" + "\n" +" You have "+ TotalNumberBoost + " boost available";
		for (int i = 0; i < explorationPanel.Length; i++) {
			explorationPanel [i].transform.GetChild (3).transform.Find ("Body/Bottom/BoostTime").GetComponentInChildren<TextMeshProUGUI> ().text = boostTimeString;
		}
	}

	void OnSetupExploration(bool ifStart){



		for (int i = 0; i < explorationPanel.Length; i++) {
			if (ifStart) {
				explorationPanel [i].transform.GetChild (2).gameObject.SetActive (true);

				explorationPanel [i].transform.GetChild (3).gameObject.SetActive (false);
				explorationPanel [i].transform.GetChild (4).gameObject.SetActive (false);
			}
			GameObject ItemContainer = explorationPanel [i].transform.GetChild (4).transform.Find ("Expedition/Scroll/ItemContainer").gameObject;
			System.TimeSpan t3 = System.TimeSpan.FromSeconds (RareTimeExplore);
			for (int l = 0; l < ItemContainer.transform.childCount; l++) {
				ItemContainer.transform.GetChild(l).gameObject.SetActive (true);
				if (l == 0) {
					if (dayToday != "Wednesday" && dayToday != "Sunday") {
						ItemContainer.transform.GetChild(l).gameObject.SetActive (false);
					}
				}
				ItemContainer.transform.GetChild(l).Find("Body/Bottom/StartBtn").GetComponentInChildren<TextMeshProUGUI>().text = "Start";
				GameObject BodyObj = ItemContainer.transform.GetChild(l).Find("Body").gameObject;
				for (int k = 1; k < BodyObj.transform.childCount; k++) {
					if (k == 1) {
						BodyObj.transform.GetChild (k).Find ("Icon").gameObject.SetActive (true);
						BodyObj.transform.GetChild (k).Find ("Text").GetComponent<TextMeshProUGUI> ().text = "";
						BodyObj.transform.GetChild (k).Find ("AmountReward").GetComponent<TextMeshProUGUI> ().text = "Up to 20 brains";
					} else {
						BodyObj.transform.GetChild (k).Find ("Icon").gameObject.SetActive (false);
						BodyObj.transform.GetChild (k).Find ("Text").GetComponent<TextMeshProUGUI> ().text = "?";
						BodyObj.transform.GetChild (k).Find ("AmountReward").GetComponent<TextMeshProUGUI> ().text = "";
					}
				}

				if (l == 0) {
					ItemContainer.transform.GetChild(l).Find("Header/Title").GetComponent<TextMeshProUGUI> ().text = "Rare Mission";
					ItemContainer.transform.GetChild(l).Find("Header/BgTime/Text01").GetComponent<TextMeshProUGUI> ().text = ((int)t3.TotalHours).ToString("d2")+":"+t3.Minutes.ToString("d2")+":"+t3.Seconds.ToString("d2");
				} else {
					int numCommon = l - 1;
					System.TimeSpan t2 = System.TimeSpan.FromSeconds (CommontimeExplore [numCommon]);
					ItemContainer.transform.GetChild (l).Find ("Header/Title").GetComponent<TextMeshProUGUI> ().text = "Common Mission";
					ItemContainer.transform.GetChild(l).Find("Header/BgTime/Text01").GetComponent<TextMeshProUGUI> ().text = ((int)t2.TotalHours).ToString("d2")+":"+t2.Minutes.ToString("d2")+":"+t2.Seconds.ToString("d2");
				}

			}

			System.TimeSpan t = System.TimeSpan.FromSeconds (timeShowup);
			explorationPanel [i].transform.GetChild (3).transform.Find ("Header/BgTime/Text01").GetComponent<TextMeshProUGUI> ().text = ((int)t.TotalHours).ToString("d2")+":"+t.Minutes.ToString("d2")+":"+t.Seconds.ToString("d2");
		}
	}

	void activeExploration(int numExploration){
		
		for (int i = 0; i < explorationPanel.Length; i++) {
			explorationPanel [i].transform.GetChild (2).gameObject.SetActive (false);
			explorationPanel [i].transform.GetChild (4).gameObject.SetActive (false);
			explorationPanel [i].transform.GetChild (3).gameObject.SetActive (true);
			explorationPanel [i].transform.GetChild(3).transform.Find ("Body/Bottom/StartBtn").GetComponentInChildren<TextMeshProUGUI>().text = "Boost";
			GameObject rareItems = explorationPanel [i].transform.GetChild (3).transform.Find ("Body").gameObject;

			string[] ItemsExploration = new string[0];
			GameObject ItemContainer = explorationPanel [i].transform.GetChild (4).transform.Find ("Expedition/Scroll/ItemContainer").gameObject;
			for (int l = 0; l < ItemContainer.transform.childCount; l++) {
				if (stringActiveExploration == "Rare") {
					explorationPanel [i].transform.GetChild (3).transform.Find ("Header").gameObject.GetComponent<Image> ().color = ItemContainer.transform.GetChild (0).transform.Find ("Header").gameObject.GetComponent<Image> ().color;
					explorationPanel [i].transform.GetChild (3).transform.Find ("Header/Title").gameObject.GetComponent<TextMeshProUGUI> ().text = ItemContainer.transform.GetChild (0).transform.Find ("Header/Title").gameObject.GetComponent<TextMeshProUGUI> ().text;
				} else {
					explorationPanel [i].transform.GetChild (3).transform.Find ("Header").gameObject.GetComponent<Image> ().color = ItemContainer.transform.GetChild (1).transform.Find ("Header").gameObject.GetComponent<Image> ().color;
					explorationPanel [i].transform.GetChild (3).transform.Find ("Header/Title").gameObject.GetComponent<TextMeshProUGUI> ().text = ItemContainer.transform.GetChild (1).transform.Find ("Header/Title").gameObject.GetComponent<TextMeshProUGUI> ().text;
				}
				break;
			}

			for (int l = 1; l < rareItems.transform.childCount; l++) {
				if (l == 1) {
					rareItems.transform.GetChild (l).Find ("Icon").gameObject.SetActive (true);
					rareItems.transform.GetChild (l).Find ("Text").GetComponent<TextMeshProUGUI> ().text = "";
					rareItems.transform.GetChild (l).Find ("AmountReward").GetComponent<TextMeshProUGUI> ().text = "Up to 20 brains";
				} else {
					rareItems.transform.GetChild (l).Find ("Icon").gameObject.gameObject.SetActive (false);
					rareItems.transform.GetChild (l).Find ("Text").GetComponent<TextMeshProUGUI> ().text = "?";
					rareItems.transform.GetChild (l).Find ("AmountReward").GetComponent<TextMeshProUGUI> ().text = "";
				}
			}
		}
		OnSetupUI = true;

		OnSetupBoostTime ();
	}

	void OnSetupBoostTime(){
		System.TimeSpan t = System.TimeSpan.FromSeconds (timeBoostTemp);
		string boostTimeString = ((int)t.TotalHours).ToString("d2")+":"+t.Minutes.ToString("d2")+":"+t.Seconds.ToString("d2");
		for (int i = 0; i < explorationPanel.Length; i++) {
			explorationPanel [i].transform.GetChild(3).transform.Find ("Body/Bottom/BoostTime").GetComponentInChildren<TextMeshProUGUI>().text = "Time to Boost" + "\n" + boostTimeString;
		}
	}

	void OnSelectExploration(){
		for (int i = 0; i < explorationPanel.Length; i++) {
			explorationPanel [i].transform.GetChild (2).gameObject.SetActive (false);
			explorationPanel [i].transform.GetChild (4).gameObject.SetActive (true);
		}
		MessageDispatcher.SendMessage (this, "ExplorationTutorialClose",1, 0f);
		MessageDispatcher.SendMessage (this, "ExplorationTutorial", 2, 0f);
	}


	string[] RewardString;
	void OnEndExploration(){
		MessageDispatcher.SendMessage (this, "OnStopAnimation", 0, 0);	
		Debug.Log ("EndExploration");
		OnShowButtons ();

		for (int i = 0; i < explorationPanel.Length; i++) {
			explorationPanel [i].transform.GetChild (3).transform.Find ("Body/Bottom/StartBtn").GetComponentInChildren<TextMeshProUGUI> ().text = "Collect";
			timeBoostTemp = 0;
			ES2.Save (0f, "ExplorationSetTimeBoost"); 
			System.TimeSpan t2 = System.TimeSpan.FromSeconds (timeBoostTemp);
			string boostTimeString = "";
			explorationPanel [i].transform.GetChild (3).transform.Find ("Body/Bottom/BoostTime").GetComponentInChildren<TextMeshProUGUI> ().text = boostTimeString;
			explorationPanel [i].transform.GetChild (3).transform.Find ("Header/BgTime/Text01").GetComponent<TextMeshProUGUI> ().text = ((int)t2.TotalHours).ToString("d2")+":"+t2.Minutes.ToString("d2")+":"+t2.Seconds.ToString("d2");//string.Format ("{0:D2}:{1:D2}:{2:D2}", t.Hours, t.Minutes, t.Seconds);

			GameObject rareItems = explorationPanel [i].transform.GetChild (3).transform.Find ("Body").gameObject;

			RewardString = new string[3]; 
			double[] Rewarddouble = new double[3]; 
//			public double chipsReward;
//			public double MultiplierReward;
//			public double TimeWarpReward;
//			public float TimeMultiplierReward;
			if (stringActiveExploration == "Rare") {

				Rewarddouble = new double[]{UnityEngine.Random.Range ((int)RareChipsReward, 20),
					UnityEngine.Random.Range ((int)RareMultiplierReward, (int)RareMultiplierReward + 2),
					UnityEngine.Random.Range ((int)RareTimeWarpReward, (int)RareTimeWarpReward + 3),
					RareTimeMultiplierReward};

				if (ES2.Exists ("RewardEploration")) {
					Rewarddouble = ES2.LoadArray<double>("RewardEploration");
				} else {
					ES2.Save(Rewarddouble, "RewardEploration");
					MessageDispatcher.SendMessage (this, "OnGA_Report", "ZOMBIEMISSION:End:" + stringActiveExploration+" Exploration", 0);
				}

				chipsReward = Rewarddouble[0];
				MultiplierReward = Rewarddouble[1];
				TimeWarpReward = Rewarddouble[2];
				TimeMultiplierReward = (float)Rewarddouble[3];


				string TimeWarpString = TimeWarpReward + " Day Time-Warp";
				if (TimeWarpReward > 1) {
					TimeWarpString = TimeWarpReward + " Days Time-Warp";
				}
				int hours = (int)((TimeMultiplierReward / 3600) % 24);
				string hrString = "hr";
				if (hours > 1) {
					hrString = "hrs";
				}
				RewardString = new string[] {
					chipsReward + " Golden Brains",
					TimeWarpString,
					"x" + MultiplierReward + " in " + hours + hrString,
				};


			} else {
				int commonGot = 0;
				double chipRandom = 0;
				if (stringActiveExploration == "Common0") {
					commonGot = 0;
					chipRandom = UnityEngine.Random.Range ((int)CommonChipsReward [commonGot],10);
				} else if (stringActiveExploration == "Common1") {
					commonGot = 1;
					chipRandom = UnityEngine.Random.Range ((int)CommonChipsReward [commonGot],10);
				} else if (stringActiveExploration == "Common2") {
					commonGot = 2;
					chipRandom = UnityEngine.Random.Range ((int)CommonChipsReward [commonGot], 11);
				}

				Rewarddouble = new double[]{chipRandom,
					(int)CommonMultiplierReward [commonGot],
					(int)CommonTimeWarpReward [commonGot],
					CommonTimeMultiplierReward [commonGot] };
				
				if (ES2.Exists ("RewardEploration")) {
					Rewarddouble = ES2.LoadArray<double>("RewardEploration");
				} else {
					ES2.Save(Rewarddouble, "RewardEploration");
					MessageDispatcher.SendMessage (this, "OnGA_Report", "ZOMBIEMISSION:End:" + stringActiveExploration+" Exploration", 0);

				}

				chipsReward = Rewarddouble[0];
				MultiplierReward = Rewarddouble[1];
				TimeWarpReward = Rewarddouble[2];
				TimeMultiplierReward = (float)Rewarddouble[3];

				string TimeWarpString = TimeWarpReward + " hrs Time-Warp";

				if (TimeWarpReward > 1) {
					TimeWarpString = TimeWarpReward + " hrs Time-Warp";
				}

				int hours = (int)((TimeMultiplierReward / 3600) % 24);
				string hrString = "hr";
				if (hours > 1) {
					hrString = "hrs";
				}

				RewardString = new string[] {
					chipsReward + " Golden Brains",
					TimeWarpString,
					"x" + MultiplierReward + " in " + hours + hrString
				};

			}
//			for (int l = 1; l < rareItems.transform.childCount; l++) {
//				rareItems.transform.GetChild (l).gameObject.SetActive (true);
//				rareItems.transform.GetChild (l).Find ("Icon").gameObject.SetActive (true);
//				rareItems.transform.GetChild (l).Find ("Text").GetComponent<TextMeshProUGUI> ().text = "";
//				rareItems.transform.GetChild (l).Find ("AmountReward").GetComponent<TextMeshProUGUI> ().text = RewardString[l-1];
//			}
		}


	}



	void Update ()
	{
		if (OnStartTime && OnSetupUI) {
			float tempDeltaTime = Time.deltaTime;
			timeShowup -= tempDeltaTime;
			System.TimeSpan t = System.TimeSpan.FromSeconds (timeShowup);

			if (timeShowup < 0) {
				timeShowup = 0;
				OnStartTime = false;
				ES2.Save (0f, "ExplorationSetTime"); 
				OnCompleteExploration = true;
				OnEndExploration ();
			}

			for (int i = 0; i < activeTextExplocation.Length; i++) {
				explorationPanel [i].transform.GetChild (3).transform.Find ("Header/BgTime/Text01").GetComponent<TextMeshProUGUI> ().text = ((int)t.TotalHours).ToString("d2")+":"+t.Minutes.ToString("d2")+":"+t.Seconds.ToString("d2");//string.Format ("{0:D2}:{1:D2}:{2:D2}", t.Hours, t.Minutes, t.Seconds);
			}


			if (OnCanBoostExploration == false && OnStartTime) {
				timeBoostTemp -= tempDeltaTime;
				System.TimeSpan t2 = System.TimeSpan.FromSeconds (timeBoostTemp);
				string boostTimeString = "Time to Boost" + "\n" + ((int)t2.TotalHours).ToString ("d2") + ":" + t2.Minutes.ToString ("d2") + ":" + t2.Seconds.ToString ("d2");
				string boostTimeString2 = "Boost will be available in " + "\n" + ((int)t2.TotalHours).ToString ("d2") + ":" + t2.Minutes.ToString ("d2") + ":" + t2.Seconds.ToString ("d2");

				if (timeBoostTemp < 0) {
					timeBoostTemp = 0;
					ES2.Save (0f, "ExplorationSetTimeBoost"); 
					OnCanBoostExploration = true;
					boostTimeString = "Get your boost and finish faster!!" + "\n" +" You have "+ TotalNumberBoost + " boost available";
					TotalNumberBoost = 2;
					ES2.Save (TotalNumberBoost, "TotalNumberBoost");

				} else {
					boostTimeString = "Time to Boost" + "\n" + ((int)t2.TotalHours).ToString ("d2") + ":" + t2.Minutes.ToString ("d2") + ":" + t2.Seconds.ToString ("d2");
				}
				if (OnActiveScreen) {
					for (int i = 0; i < explorationPanel.Length; i++) {
						explorationPanel [i].transform.GetChild (3).transform.Find ("Body/Bottom/BoostTime").GetComponentInChildren<TextMeshProUGUI> ().text = boostTimeString;
					}
					for (int i = 0; i < explorationBoostWarning.Length; i++) {
						explorationBoostWarning [i].transform.Find ("NoBoost/Title").GetComponent<TextMeshProUGUI> ().text = "Zombie's Mission";
						explorationBoostWarning [i].transform.Find ("NoBoost/OkBtn").GetComponentInChildren<TextMeshProUGUI> ().text = "OKAY";
						explorationBoostWarning [i].transform.Find ("NoBoost/Message").GetComponent<TextMeshProUGUI> ().text = boostTimeString2;
					}
				}
			}
		}


	}
	string TempRewardMission = "";
	void OnCollectExploration(){
		if (OnStartTime == false) {

			MessageDispatcher.SendMessage (this, "OnIncrementGold", chipsReward, 0);
//			OnTimeWarp ((int)TimeWarpReward);
			double[] multiplierData = new double[]{ TimeMultiplierReward,MultiplierReward };
			MessageDispatcher.SendMessage (this, "OnStartMultiplierExploration", multiplierData, 0);	
			

			ES2.Delete("RewardEploration");
			MessageDispatcher.SendMessage (this, "OnGA_Report", "ZOMBIEMISSION:Collect:" + stringActiveExploration+" Exploration", 0);
			OnCompleteExploration = false;
			OnSetupExploration (true);
			ES2.Save ("", "ActiveExploration"); 

			if (dayToday != "Sunday" && dayToday != "Wednesday") {
				for (int i = 0; i < explorationPanel.Length; i++) {
					GameObject ItemContainer = explorationPanel [i].transform.GetChild (4).transform.Find ("Expedition/Scroll/ItemContainer").gameObject;
					for (int l = 0; l < ItemContainer.transform.childCount; l++) {
						if (l == 0) {
							ItemContainer.transform.GetChild (l).gameObject.SetActive (false);
						} 
					}
				}
			} 
			OnStartCollecRewards (stringActiveExploration);
			TempRewardMission = stringActiveExploration;
			stringActiveExploration = "";
		
		} else {
			if (OnCanBoostExploration == true) {
				

				#if UNITY_EDITOR
				GameObject.Find ("GameTimer").GetComponent<AdController> ().adNameUsed = "BoostExploration";
				GameObject.Find ("GameTimer").GetComponent<AdController> ().OnSHowingADS ();
				#else
				GameObject.Find ("Managers/AdManager").GetComponent<AdManagerScript> ().OnWatchAd ("BoostExploration");
				#endif
			} else {
				System.TimeSpan t2 = System.TimeSpan.FromSeconds (timeBoostTemp);
				string boostTimeString = "Boost will be available in " + "\n" + ((int)t2.TotalHours).ToString ("d2") + ":" + t2.Minutes.ToString ("d2") + ":" + t2.Seconds.ToString ("d2");
				for (int i = 0; i < explorationBoostWarning.Length; i++) {
					explorationBoostWarning [i].SetActive (true);
					explorationBoostWarning [i].transform.Find ("NoBoost/Title").GetComponent<TextMeshProUGUI> ().text = "Zombie's Mission";
					explorationBoostWarning [i].transform.Find ("NoBoost/OkBtn").GetComponentInChildren<TextMeshProUGUI> ().text = "OKAY";
					explorationBoostWarning [i].transform.Find ("NoBoost/Message").GetComponent<TextMeshProUGUI> ().text = boostTimeString;
				}
			}

		}

	}

	void OnStartCollecRewards(string TypeMission){
		if (gameObject.GetComponent<AudioSource> ().isPlaying) {
			gameObject.GetComponent<AudioSource> ().Stop ();
		}
		Debug.Log ("Tap Reward");

		if (numReward >= 3) {
			numReward = 0;
			OnShowButtons ();
			for (int i = 0; i < RewardPpopup.Length; i++) {
				RewardPpopup [i].SetActive (false);
			}
			if (TempRewardMission == "Rare") {
				OnTimeWarp ((int)TimeWarpReward);
			} else {
				OnTimeWarpHours ((int)TimeWarpReward);
			}

		} else {
			gameObject.GetComponent<AudioSource> ().Play ();
			for (int i = 0; i < RewardPpopup.Length; i++) {
				RewardPpopup [i].SetActive (true);
				RewardPpopup [i].transform.Find ("bg/Icon/Icon/Text").GetComponent<TextMeshProUGUI> ().text = "";
				RewardPpopup [i].transform.Find ("bg/Icon/Icon").GetComponent<Image> ().sprite = IconReward [numReward];
				if (numReward == 2) {
					RewardPpopup [i].transform.Find ("bg/Icon/Icon/Text").GetComponent<TextMeshProUGUI> ().text = "MULTIPLIER";
				}
				Debug.Log (RewardString [numReward]);
				RewardPpopup [i].transform.Find ("bg/Footer/Text").GetComponent<TextMeshProUGUI> ().text = RewardString [numReward];
			}
			TempRewardMission = "";
			numReward++;
		}


	}

	void OnTimeWarp (int numDaysExploration) // Days
	{
		int numDays = numDaysExploration;
		double timewarpEarned = 0;
		for (int i = 0; i < generatorsObj.Length; i++) {

			if (generatorsObj [i].hasManagerGen && generatorsObj [i].isActiveGen) {
				double TotalGenratorEarnCycle = generatorsObj [i].earningsPerCycle;
				float progressTime = generatorsObj [i].tempTime;
				double GetEarnPerMinute = (TotalGenratorEarnCycle / progressTime) * 60;
				double GetEarnPerHour = GetEarnPerMinute * 60;
				timewarpEarned += GetEarnPerHour * (24* numDays); // Hours 
			}
		}
		MessageDispatcher.SendMessage (this, "OnIncrementMoney", timewarpEarned, 0);
		MoneyManager TempMoney = GameObject.Find ("Managers/MoneyManager").GetComponent<MoneyManager> ();
		string boostTimeString = "You Just got (" + TempMoney.GEtNumberNoDecimal (timewarpEarned) + ") From " +numDays+" Days Time-Warp";
		OnSetPopupTimeWarp (boostTimeString);
	}

	void OnTimeWarpHours (int numDaysExploration) // Hours
	{
		int numDays = numDaysExploration;
		double timewarpEarned = 0;
		for (int i = 0; i < generatorsObj.Length; i++) {

			if (generatorsObj [i].hasManagerGen && generatorsObj [i].isActiveGen) {
				double TotalGenratorEarnCycle = generatorsObj [i].earningsPerCycle;
				float progressTime = generatorsObj [i].tempTime;
				double GetEarnPerMinute = (TotalGenratorEarnCycle / progressTime) * 60;
				double GetEarnPerHour = GetEarnPerMinute * 60;
				timewarpEarned += GetEarnPerHour *  numDays; // Hours 
			}
		}
		MessageDispatcher.SendMessage (this, "OnIncrementMoney", timewarpEarned, 0);
		MoneyManager TempMoney = GameObject.Find ("Managers/MoneyManager").GetComponent<MoneyManager> ();
		string boostTimeString = "You Just got (" + TempMoney.GEtNumberNoDecimal (timewarpEarned) + ") From " +numDays+" Hours Time-Warp";
		OnSetPopupTimeWarp (boostTimeString);
	}


	void OnSetPopupTimeWarp(string messageString){
		for (int i = 0; i < explorationBoostWarning.Length; i++) {
			explorationBoostWarning [i].SetActive (true);
			explorationBoostWarning [i].transform.Find ("NoBoost/Title").GetComponent<TextMeshProUGUI> ().text = "Zombie's Mission";
			explorationBoostWarning [i].transform.Find ("NoBoost/OkBtn").GetComponentInChildren<TextMeshProUGUI> ().text = "GREAT!";
			explorationBoostWarning [i].transform.Find ("NoBoost/Message").GetComponent<TextMeshProUGUI> ().text = messageString;
		}
	}

	void OnCloseBoostWarning(){
		MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
		for (int i = 0; i < explorationBoostWarning.Length; i++) {
			explorationBoostWarning [i].SetActive (false);
		}
	}

	void OnStartCommonExploration(int numCommon){
		if (OnStartTime == false) {
			if (OnCompleteExploration == false) {
				MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
				stringActiveExploration = "Common"+numCommon;
				ES2.Save (stringActiveExploration, "ActiveExploration"); 
				timeShowup = CommontimeExplore [numCommon];
				timeShowupTemp = timeShowup;
//				timeBoostTemp = timeBoost;
				activeExploration (numCommon);
				OnStartTime = true;
				OnCanBoostExploration = true;
				OnStartBoost ();
				MessageDispatcher.SendMessage (this, "OnStartAnimWalk", 0, 0);
				MessageDispatcher.SendMessage (this, "OnGA_Report", "ZOMBIEMISSION:Start:" + stringActiveExploration+" Exploration", 0);

				MessageDispatcher.SendMessage (this, "ExplorationTutorialClose",2, 0f);
				MessageDispatcher.SendMessage (this, "ExplorationTutorial", 3, 0f);
				if (gameObject.GetComponent<ExplorationMultiplierTimeHandler> ().OnStartTime) {
					OnShowButtons ();
				} else {
//					OnhideButton ();
				}
			}

		} else {

		}
	}

	void OnStartRareExploration(){
		if (OnStartTime == false) {
			if (OnCompleteExploration == false) {
				MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
				stringActiveExploration = "Rare";
				ES2.Save (stringActiveExploration, "ActiveExploration"); 
				timeShowup = RareTimeExplore;
				timeShowupTemp = timeShowup;
//				timeBoostTemp = timeBoost;
				activeExploration (0);
				OnStartTime = true;
				OnCanBoostExploration = true;
				OnStartBoost ();
				MessageDispatcher.SendMessage (this, "OnStartAnimWalk", 0, 0);
				MessageDispatcher.SendMessage (this, "OnGA_Report", "ZOMBIEMISSION:Start:" + stringActiveExploration+" Exploration", 0);

				MessageDispatcher.SendMessage (this, "ExplorationTutorialClose",2, 0f);
				MessageDispatcher.SendMessage (this, "ExplorationTutorial", 3, 0f);
				if (gameObject.GetComponent<ExplorationMultiplierTimeHandler> ().OnStartTime) {
					OnShowButtons ();
				} else {
//					OnhideButton ();
				}

			}
		} else {
		
		}
	}

	void OnhideButton(){
		
		for (int i = 0; i < btnMultiplier.Length; i++) {
			btnMultiplier [i].SetActive (false);
		}
	}


	void OnShowButtons(){
		for (int i = 0; i < btnMultiplier.Length; i++) {
			btnMultiplier [i].SetActive (true);
		}
	}

	void OnCloseExplorationPopup(){

		MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
//		MainCamera.orthographic = true;
		OnActiveScreen = false;
		if (OnStartTime) {
			MessageDispatcher.SendMessage (this, "OnStopAnimation", 0, 0);	
		}

		for (int i = 0; i < explorationPanel.Length; i++) {
			explorationPanel [i].SetActive (false);
		}

		if (ES2.Exists ("ExplorationTutorial")) {
			List<bool> tutorialActiveTemp = ES2.LoadList<bool> ("ExplorationTutorial");
			if (tutorialActiveTemp [3] == true) {
				MessageDispatcher.SendMessage (this, "OnStartSpinBtn", 0, 0.5f);
			}
		}

	}

	public void OnShowExplorationPopup(){
		MessageDispatcher.SendMessage (this, "PopButton", 1, 0f);
//		MainCamera.orthographic = false;
		OnActiveScreen = true;
		MessageDispatcher.SendMessage (this, "ExplorationTutorialClose", 0, 0f);
		MessageDispatcher.SendMessage (this, "ExplorationTutorial", 1, 0f);
		for (int i = 0; i < explorationPanel.Length; i++) {
			explorationPanel [i].SetActive (true);
		}
		if (OnStartTime) {
			MessageDispatcher.SendMessage (this, "OnStartAnimWalk", 0, 0);
		}
	}

	DateTime currentDate;
	void OnCalculateTime ()
	{
		if (ES2.Exists ("ExplorationSetTime")) {
			currentDate = System.DateTime.Now;
			long temp = 0;
			if (ES2.Exists ("ExplorationTimeLeftOffline")) {
				temp = Convert.ToInt64 (ES2.Load<string> ("ExplorationTimeLeftOffline")); //load the time starts offline
			} 
			DateTime oldDate = DateTime.FromBinary (temp);
			TimeSpan difference = currentDate.Subtract (oldDate);
			int secondsInt = Convert.ToInt32 (difference.TotalSeconds); //seconds during offline
			float tempGenTimeLeft = ES2.Load<float> ("ExplorationSetTime");
			float numReturnValue = 0;

			if (tempGenTimeLeft < difference.TotalSeconds) { // time offline is bigger than time remaining
				numReturnValue = 0;
				timeShowup = timeShowupTemp;
				OnStartTime = false;
				ES2.Save (0f, "ExplorationSetTime"); 
				OnEndExploration ();
			} else { // time remaining is bigger than time offline
				OnCompleteExploration = false;
				numReturnValue = tempGenTimeLeft - (float)difference.TotalSeconds;
				timeShowup = numReturnValue;
				OnStartTime = true;
			}

			if (OnCanBoostExploration == false && OnStartTime) {
				string boostTimeString = "Boost Now!";
				float tempGenTimeLeftBoost = ES2.Load<float> ("ExplorationSetTimeBoost");
				if (tempGenTimeLeftBoost < difference.TotalSeconds) { // time offline is bigger than time remaining
					timeBoostTemp = 0;
					ES2.Save (0f, "ExplorationSetTimeBoost"); 
					OnCanBoostExploration = true;
					boostTimeString = "Get your boost and finish faster!!" + "\n" +" You have "+ TotalNumberBoost + " boost available";
					TotalNumberBoost = 2;
					ES2.Save (TotalNumberBoost, "TotalNumberBoost");
					OnStartBoost ();
				} else {
					timeBoostTemp = tempGenTimeLeftBoost - (float)difference.TotalSeconds;
					System.TimeSpan t2 = System.TimeSpan.FromSeconds (timeBoostTemp);
					boostTimeString = "Time to Boost" + "\n" + ((int)t2.TotalHours).ToString ("d2") + ":" + t2.Minutes.ToString ("d2") + ":" + t2.Seconds.ToString ("d2");
				}
			
				for (int i = 0; i < explorationPanel.Length; i++) {
					explorationPanel [i].transform.GetChild (3).transform.Find ("Body/Bottom/BoostTime").GetComponentInChildren<TextMeshProUGUI> ().text = boostTimeString;
				}
			} else {
				
			}

			ES2.Save (System.DateTime.Now.ToBinary ().ToString (), "ExplorationTimeLeftOffline");
		} else {
		
		}
	}

	void OnApplicationQuit ()
	{
		ES2.Save (System.DateTime.Now.ToBinary ().ToString (), "ExplorationTimeLeftOffline");
		if (OnStartTime) {
			ES2.Save (timeShowup, "ExplorationSetTime"); 
			ES2.Save (timeBoostTemp, "ExplorationSetTimeBoost"); 
		}

	}

	void OnApplicationPause (bool pauseStatus)
	{

		if (pauseStatus == true) {
			ES2.Save (System.DateTime.Now.ToBinary ().ToString (), "ExplorationTimeLeftOffline");
			if (OnStartTime) {
				ES2.Save (timeShowup, "ExplorationSetTime"); 
				ES2.Save (timeBoostTemp, "ExplorationSetTimeBoost"); 
			}
		} else if (pauseStatus == false) {
			if (OnStartTime) {
				OnStartTime = false;
				OnCalculateTime ();
			}
		}
	}
}
