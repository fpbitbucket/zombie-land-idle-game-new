﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using com.ootii.Messages;

public class GroundScriptParallax : MonoBehaviour
{
	public Animator[] char_;
	public GameObject[] groundObjPort;
	public GameObject[] CloudsPort;
	public GameObject[] builingPortrait;
	public float RoadSpeed = 0, CloudSpeed = 0,buildingSpeed;
	private int numRail2 = 2,numBuildingPort = 0,numcloud = 0;
	private int lastRail = 0;

	public bool OnStartAnimation = false;
	bool portraitcheck = false;

	float[] groundPosXPort;
	float[] buildingPosXPort;
	float[] CloudsPosXPort;

	void Start ()
	{
		
		for (int i = 0; i < char_.Length; i++) {
			if (char_ [i].gameObject.activeInHierarchy) {
				char_ [i].Play ("Stop");
			}
		}
		numRail2 = groundObjPort.Length - 1;
		numBuildingPort = builingPortrait.Length - 1;
		numcloud  = CloudsPort.Length - 1;
		portraitcheck = true;

		groundPosXPort = new float[groundObjPort.Length];
		buildingPosXPort = new float[builingPortrait.Length];
		CloudsPosXPort = new float[CloudsPort.Length];

		for (int i = 0; i < groundPosXPort.Length; i++) {
			groundPosXPort [i] = groundObjPort [i].transform.localPosition.x;
		}

		for (int i = 0; i < builingPortrait.Length; i++) {
			buildingPosXPort [i] = builingPortrait [i].transform.localPosition.x;
		}

		for (int i = 0; i < CloudsPort.Length; i++) {
			CloudsPosXPort [i] = CloudsPort [i].transform.localPosition.x;
		}

	}

	void OnEnable ()
	{
//		for (int i = 0; i < char_.Length; i++) {
//			if(char_[i].gameObject.activeSelf){
//				char_ [i].Play ("Stop");
//			}
//		}
		OnStartAnimation = false;
		MessageDispatcher.AddListener ("OnStartAnimWalk", OnStartAnimWalk, true);
		MessageDispatcher.AddListener ("OnStopAnimation", OnStopAnimation, true);
	}

	void OnDisable ()
	{
		MessageDispatcher.RemoveListener ("OnStartAnimWalk", OnStartAnimWalk, true);
		MessageDispatcher.RemoveListener ("OnStopAnimation", OnStopAnimation, true);
	}

	void OnStartAnimWalk (IMessage rMessage)
	{ 
		OnStartAnimation = true;
		for (int i = 0; i < char_.Length; i++) {
			if (char_ [i].gameObject.activeInHierarchy) {
				char_ [i].Play ("Walk");
			}
		}
		OnCheckCharacter ();
	}

	void OnStopAnimation (IMessage rMessage)
	{ 
		
		OnStartAnimation = false;
		for (int i = 0; i < char_.Length; i++) {
			if (char_ [i].gameObject.activeInHierarchy) {
				char_ [i].Play ("Stop");
			}
		}
		OnCheckCharacter ();
	}

	void OnCheckCharacter ()
	{
		if (OnStartAnimation) {
			for (int i = 0; i < char_.Length; i++) {
				if (char_ [i].gameObject.activeInHierarchy) {
					char_ [i].Play ("Walk");
				}
			}

			numRail2 = groundObjPort.Length - 1;
			numBuildingPort = builingPortrait.Length - 1;
			numcloud  = CloudsPort.Length - 1;

			for (int i = 0; i < groundObjPort.Length; i++) {
				groundObjPort [i].transform.localPosition = new Vector3 (groundPosXPort [i], groundObjPort [i].transform.localPosition.y, groundObjPort [i].transform.localPosition.z);
			}

			for (int i = 0; i < builingPortrait.Length; i++) {
				builingPortrait [i].transform.localPosition = new Vector3 (buildingPosXPort [i], builingPortrait [i].transform.localPosition.y, builingPortrait [i].transform.localPosition.z);
			}


			for (int i = 0; i < CloudsPort.Length; i++) {
				CloudsPort [i].transform.localPosition = new Vector3 (CloudsPosXPort [i], CloudsPort [i].transform.localPosition.y, CloudsPort [i].transform.localPosition.z);
			}
		}
	}

	void Update ()
	{

		if (OnStartAnimation) {
			float screenWidth = Screen.width;

			if (portraitcheck) {
//				Debug.Log ((screenWidth + groundObjPort [0].transform.localPosition.x));
				for (int i = 0; i < groundObjPort.Length; i++) {
					if ((screenWidth + groundObjPort [i].transform.localPosition.x) <= -760) {
						float tempXpos = groundObjPort [numRail2].transform.localPosition.x + (groundObjPort [i].GetComponent<RectTransform> ().rect.width);
						groundObjPort [i].transform.localPosition = new Vector3 (tempXpos, groundObjPort [i].transform.localPosition.y, groundObjPort [i].transform.localPosition.z);
						numRail2 = i;
					}
				}

				for (int i = 0; i < groundObjPort.Length; i++) {
					groundObjPort [i].transform.localPosition = new Vector3 ((Time.deltaTime * RoadSpeed + groundObjPort [i].transform.localPosition.x), groundObjPort [i].transform.localPosition.y, groundObjPort [i].transform.localPosition.z);
				}

//				Debug.Log ((screenWidth + CloudsPort [0].transform.localPosition.x));

				for (int i = 0; i < CloudsPort.Length; i++) {
					if ((screenWidth + CloudsPort [i].transform.localPosition.x) <= -1100) {
						float tempXpos = CloudsPort [numcloud].transform.localPosition.x + (CloudsPort [i].GetComponent<RectTransform> ().rect.width - 50f);
						CloudsPort [i].transform.localPosition = new Vector3 (tempXpos, CloudsPort [i].transform.localPosition.y, CloudsPort [i].transform.localPosition.z); 
						numcloud = i;
					}
				}

				for (int i = 0; i < CloudsPort.Length; i++) {
					CloudsPort [i].transform.localPosition = new Vector3 ((Time.deltaTime * CloudSpeed + CloudsPort [i].transform.localPosition.x), CloudsPort [i].transform.localPosition.y, CloudsPort [i].transform.localPosition.z);
				}
//				Debug.Log ((screenWidth + builingPortrait [0].transform.localPosition.x));
				for (int i = 0; i < builingPortrait.Length; i++) {
					if ((screenWidth + builingPortrait [i].transform.localPosition.x) <= -40) {
						float tempXpos = builingPortrait [numBuildingPort].transform.localPosition.x + (builingPortrait [i].GetComponent<RectTransform> ().rect.width - 50f);
						builingPortrait [i].transform.localPosition = new Vector3 (tempXpos, builingPortrait [i].transform.localPosition.y, builingPortrait [i].transform.localPosition.z); 
						numBuildingPort = i;
					}
				}
				for (int i = 0; i < builingPortrait.Length; i++) {
					builingPortrait [i].transform.localPosition = new Vector3 ((Time.deltaTime * buildingSpeed + builingPortrait [i].transform.localPosition.x), builingPortrait [i].transform.localPosition.y, builingPortrait [i].transform.localPosition.z);
				}

			}
		}
	}
}